﻿CREATE TYPE [dbo].[PickListValueType] AS TABLE (

    Id INT ,
	PickListId INT,
	IsDefault BIT,
	DisplayOrder INT,
	Name VARCHAR(255),
	Value VARCHAR(255),
	Note VARCHAR(500));