﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[AddOrEditPickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[AddOrEditImplantFields]
					 @Id = null,
					 @ImplantType = null,
					 @FieldName = 'Description1',
					 @DisplayName = 'Description1',
					 @DataTypeId = 2,
					 @PickListId = null,
					 @DefaultValue = null,
					 @MaxLength = null,
					 @MinLength = null,
					 @IsRequired = 1,
					 @Regex = null,
					 @RerversePropertyId = null,
					 @DisplayOrder = null,
					 @IsPartOfFilter = 0,
					 @FilterDisplayOrder = null,
					 @IsMultiSelectOnCreate = null,
					 @IsMultiSelectOnFilter = null,
					 @IsSystemDefined = null,
					 @MaxImageAllowed = null,
					 @MaxTotalSize	= null,
					 @MaxSingleSize	= null,
					 @CurrentDate = @Currentdate1,
					 @UserId = 1,
					 @IsEdit = false,
					 @IsApplicableToAllImplant = null
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[AddOrEditImplantFields]
(
	@Id INT,
	@ImplantType SMALLINT,
	@FieldName VARCHAR(255),
	@DisplayName NVARCHAR(255),
	@DataTypeId TINYINT,
	@PickListId INT,
	@DefaultValue VARCHAR(255),
	@MaxLength SMALLINT,
	@MinLength SMALLINT,
	@IsRequired BIT,
	@Regex VARCHAR(255),
	@RerversePropertyId INT,
	@DisplayOrder INT,
	@IsPartOfFilter	BIT,
	@FilterDisplayOrder INT,
	@IsMultiSelectOnCreate BIT,
	@IsMultiSelectOnFilter BIT,
	@IsSystemDefined	BIT,
	@MaxImageAllowed	INT,
	@MaxTotalSize	BIGINT,
	@MaxSingleSize	BIGINT,
	@CurrentDate DATETIME,
	@UserId INT,
	@IsEdit BIT,
	@IsApplicableToAllImplant BIT
)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	BEGIN TRAN
		IF (@IsEdit = 0 AND @Id IS NULL)
		BEGIN
			INSERT INTO ImplantFields
			(
				ImplantType,
				FieldName,
				DisplayName,
				DataTypeId,
				PickListId,
				DefaultValue,
				MaxLength,
				MinLength,
				IsRequired,
				Regex,
				RerversePropertyId,
				DisplayOrder,
				IsPartOfFilter,
				FilterDisplayOrder,
				IsMultiSelectOnCreate,
				IsMultiSelectOnFilter,
				IsDeleted,
				IsSystemDefined,
				CreatedOn,
				CreatedBy,
				MaxImageAllowed,
				MaxTotalSize,
				MaxSingleSize,
				IsApplicableToAllImplant
			)
			VALUES
			(
				@ImplantType,
				@FieldName,
				@DisplayName,
				@DataTypeId,
				@PickListId,
				@DefaultValue,
				@MaxLength,
				@MinLength,
				@IsRequired,
				@Regex,
				@RerversePropertyId,
				@DisplayOrder,
				@IsPartOfFilter,
				@FilterDisplayOrder,
				@IsMultiSelectOnCreate,
				@IsMultiSelectOnFilter,
				0,
				@IsSystemDefined,
				@CurrentDate,
				@UserId,
				@MaxImageAllowed,
				@MaxTotalSize,
				@MaxSingleSize,
				ISNULL(@IsApplicableToAllImplant,0)
			)

			--DECLARE @DataTypeName NVARCHAR(MAX), @IsRequiredValue VARCHAR(100) = 'NULL';
			--DECLARE @Query NVARCHAR(MAX);
			--IF(@IsRequired = 1)
			--BEGIN
			--	SET @IsRequiredValue = 'NOT NULL';
			--END
			--SELECT  @DataTypeName = Name FROM DataTypes WHERE Id = @DataTypeId
			--SET @Query = 'ALTER TABLE Implant ADD ' + @FieldName + ' '+ @DataTypeName + ' ' + @IsRequiredValue;
			--EXEC(@Query) 
		END
		ELSE
		BEGIN
			UPDATE 
				ImplantFields
			SET 
				ImplantType = @ImplantType,
				--FieldName = @FieldName,
				DisplayName = @DisplayName,
				--DataTypeId = @DataTypeId,
				--PickListId = @PickListId,
				DefaultValue = @DefaultValue,
				MaxLength = @MaxLength,
				MinLength = @MinLength,
				IsRequired = @IsRequired,
				Regex = @Regex ,
				RerversePropertyId = @RerversePropertyId,
				DisplayOrder = @DisplayOrder,
				IsPartOfFilter = @IsPartOfFilter,
				FilterDisplayOrder = @FilterDisplayOrder,
				IsMultiSelectOnCreate = @IsMultiSelectOnCreate,
				IsMultiSelectOnFilter = @IsMultiSelectOnFilter,
				IsSystemDefined = @IsSystemDefined,
				MaxImageAllowed = @MaxImageAllowed,
				MaxTotalSize = @MaxTotalSize,
				MaxSingleSize = @MaxSingleSize,
				ModifiedOn = @CurrentDate,
				ModifiedBy = @UserId,
				IsApplicableToAllImplant = ISNULL(@IsApplicableToAllImplant,0)
			WHERE
				Id = @Id
		END
	COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK
		DECLARE
		@stringmsg VARCHAR(MAX),
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
			set  @ErrorNumber =  ERROR_NUMBER();
			set  @ErrorSeverity= ERROR_SEVERITY();
			set  @ErrorState =  ERROR_STATE();
			set  @ErrorMessage =   ERROR_MESSAGE();
			set  @ErrorLine =  ERROR_LINE();
			 set @stringmsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;

			 THROW 51000,@stringmsg , 1; 
	END CATCH;
END	
