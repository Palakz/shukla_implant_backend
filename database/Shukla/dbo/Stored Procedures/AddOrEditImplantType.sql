﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[AddOrEditPickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[AddOrEditImplantType]
					 @Id = null,
					 @Name = 'Nose',
					 @CurrentDate = @Currentdate1,
					 @UserId = 1,
					 @IsEDIT = 1
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[AddOrEditImplantType]
(
	@Id INT,
	@Name VARCHAR(255),
	@CurrentDate DATETIME,
	@UserId INT,
	@IsEdit BIT
)
AS
BEGIN
	SET NOCOUNT ON;

	IF (@IsEdit = 0)
	BEGIN
		INSERT INTO ImplantType
		(
			[Name],
			CreatedOn,
			CreatedBy,
			IsDeleted
		)
		VALUES
		(
			@Name,
			@CurrentDate,
			@UserId,
			0
		)
	END
	ELSE
	BEGIN
		UPDATE 
			ImplantType
		SET 
			[Name] = @Name,
			ModifiedOn = @CurrentDate,
			ModifiedBy = @UserId
		WHERE
			Id = @Id
	END
END	
