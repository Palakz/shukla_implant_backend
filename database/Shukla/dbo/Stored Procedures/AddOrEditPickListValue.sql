﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[AddOrEditPickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   DECLARE @PickListValueType  as  [dbo].[PickListValueType]
				   insert into  @PickListValueType (Id,PickListId,IsDefault,DisplayOrder,Name,Value,Note)
				   values(35,24,0,1,'R11u','R11u','NOTEu'),
				   	     (36,24,1,2,'R12u','R12u','NOTEu'),
						 (NULL,24,0,1,'R13','R11','NOTE'),
				   	     (NULL,24,1,2,'R14','R12','NOTE');
				   --,	(5,10,1,2,'India updated','IND updated','India country updated'),
				   --      (6,10,0,1,'Nepal updated','Nep updated','Nepal country updated');
				   EXEC [dbo].[AddOrEditPickListValue]
					@PickListValueType = @PickListValueType,
					@CurrentDate = @Currentdate1,
					@UserId = 12,
					@IsEDIT = 1
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[AddOrEditPickListValue]
(
	@PickListValueType [dbo].[PickListValueType] READONLY,
	@CurrentDate DATETIME,
	@UserId INT,
	@IsEdit BIT
)
AS
BEGIN
	SET NOCOUNT ON;

	select * into #tempInsert from @PickListValueType where id is null
	select * into #tempUpdate from @PickListValueType where id is not null

	--select * from #tempInsert; 
	--select * from #tempUpdate;


	IF EXISTS(select * from #tempInsert)
	BEGIN
	print 'insert';
		INSERT INTO PickListValue
		(
			PickListId,
			IsDefault,
			DisplayOrder,
			[Name],
			[Value],
			Note,
			IsDeleted,
			CreatedOn,
			CreatedBy
		)
		SELECT
			PickListId,
			IsDefault,
			DisplayOrder,
			[Name],
			[Value],
			Note,
			0,
			@CurrentDate,
			@UserId
		FROM 
			#tempInsert;
	END
	IF EXISTS(select * from #tempUpdate)
	BEGIN
		print 'updated';
		UPDATE p
		SET 
			p.IsDefault = pt.IsDefault,
			p.DisplayOrder = pt.DisplayOrder,
			p.[Name] = pt.[Name],
			p.[Value] = pt.[Value],
			p.Note = pt.Note,
			p.ModifedOn = @CurrentDate,
			p.ModifiedBy = @UserId
		FROM 
			dbo.PickListValue p
		INNER JOIN 
			#tempUpdate pt ON pt.pickListId = p.PickListId AND pt.Id = p.Id
	END
	DROP TABLE #tempInsert, #tempUpdate;
END	
