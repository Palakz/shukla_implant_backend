﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllPickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetAllPageViewProperty]
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetAllPageViewProperty]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		Id
		,FieldId
		,DisplayName
		,ContainerId
		,ImplantTypeId
		,IsActive
		,IsReadOnly
		,IsVisible
		,CssClass
		,Script
	FROM
		PageViewProperty
	WHERE 
		IsDeleted = 0 
	ORDER BY 1 DESC
END	


