﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[AddOrEditPickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[AddOrEditPickList]
					 @Id = 9,
					 @Name = 'Name1',
					 @Description = 'Desc1',
					 @IsValueNameSame = 1,
					 @IsValueAutoId = 1,
					 @CurrentDate = @Currentdate1,
					 @UserId = 1,
					 @DisplayOrder = 1,
					 @OrderBy = 'Name DESC',
					 @DataType = 'string'
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[AddOrEditPickList]
(
	@Id INT,
	@Name VARCHAR(255),
	@Description VARCHAR(255),
	@IsValueNameSame BIT,
	@IsValueAutoId BIT,
	@CurrentDate DATETIME,
	@UserId INT,
	@DisplayOrder INT,
	@OrderBy VARCHAR(255),
	@DataType VARCHAR(255)
)
AS
BEGIN
	SET NOCOUNT ON;


	IF (@Id IS NULL)
	BEGIN
		INSERT INTO PickList
		(
			[Name],
			[Description],
			IsValueNameSame,
			IsValueAutoId,
			CreatedOn,
			CreatedBy,
			DisplayOrder,
			OrderBy,
			IsDeleted,
			DataType
		)
		VALUES
		(
			@Name,
			@Description,
			@IsValueNameSame,
			@IsValueAutoId,
			@CurrentDate,
			@UserId,
			@DisplayOrder,
			@OrderBy,
			0,
			@DataType
		)
	END
	ELSE
	BEGIN
		UPDATE 
			PickList
		SET 
			[Name] = @Name,
			[Description] = @Description,
			IsValueNameSame = @IsValueNameSame,
 			IsValueAutoId = @IsValueAutoId,
			ModifiedOn = @CurrentDate,
			ModifiedBy = @UserId,
			DisplayOrder = @DisplayOrder,
			OrderBy = @OrderBy,
			DataType = @DataType
		WHERE
			Id = @Id
	END
END	
