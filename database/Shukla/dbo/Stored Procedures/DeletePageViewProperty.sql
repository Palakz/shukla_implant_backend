﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[DeletePickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[DeletePageViewProperty]
					 @Id = 9,
					 @CurrentDate = @Currentdate1,
					 @UserId = 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[DeletePageViewProperty]
(
	@Id INT,
	@CurrentDate DATETIME,
	@UserId INT
)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE 
		PageViewProperty
	SET 
		ModifiedOn = @CurrentDate,
		ModifiedBy = @UserId,
		IsDeleted = 1
	WHERE
		Id = @Id;
END	