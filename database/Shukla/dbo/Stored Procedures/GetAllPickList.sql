﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllPickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetAllPickList]
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[GetAllPickList]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		Id,
		[Name],
		[Description],
		IsValueNameSame,
		IsValueAutoId,
		DisplayOrder,
		OrderBy,
		DataType
	FROM
		PickList
	WHERE 
		IsDeleted = 0 
	ORDER BY 1 DESC
END	


