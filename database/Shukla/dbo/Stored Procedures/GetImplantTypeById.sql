﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetPickListById]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetImplantTypeById]
					 @Id = 3
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetImplantTypeById]
(
	@Id INT
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		Id,
		[Name]
	FROM
		ImplantType
	WHERE
		Id = @Id
		AND IsDeleted = 0;
END	