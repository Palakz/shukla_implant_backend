﻿USE [db_a46e77_surgicalimplant]
GO
/****** Object:  StoredProcedure [dbo].[InsertPageViewContainer]    Script Date: 7/23/2022 1:12:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InsertPageViewContainer]
@Id int,
@Name Nvarchar(50),
@PageViewId int,
@CreatedOn Datetime,
@CreatedBy int,
@ModifiedBy int = null,
@ModifiedOn Datetime = null

AS
BEGIN
	If(@Id IS NULL)
		BEGIN
			INSERT Into PageViewContainer 
				(Name,PageViewId,IsDeleted,CreatedOn,CreatedBy)
			VALUES
				(@Name,@PageViewId,0,@CreatedOn,@CreatedBy)
		END
	ELSE
		BEGIN
			UPDATE PageViewContainer SET 
			Name=@Name,PageViewId=@PageViewId,IsDeleted=0,
			--CreatedOn=@CreatedOn,CreatedBy=@CreatedBy,
			ModifiedOn=@ModifiedOn,ModifiedBy=@ModifiedBy
			WHERE Id=@Id
		END

END
