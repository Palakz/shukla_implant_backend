﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserDetailsByEmail]
	@EmailAddress varchar(250)
AS
BEGIN
	SELECT Id,FirstName,LastName,UserName,MobileNumber,EmailAddress,IsEmailVerified,EmailVerificationToken FROM Users WHERE EmailAddress= @EmailAddress
END
