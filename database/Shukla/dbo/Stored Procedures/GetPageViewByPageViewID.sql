﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllPickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetPageViewByPageViewID]  2
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetPageViewByPageViewID]
(
	@PageViewID INT 
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT Id,Name,URL FROM	PageView WHERE Id = @PageViewId	AND IsDeleted = 0 ORDER BY 1 DESC

	Select Id,Name,PageViewId from PageViewContainer Where PageViewId=@PageViewID and IsDeleted=0
	
	Select id,FieldId,DisplayName,ContainerId,ImplantTypeId,IsActive,IsVisible,IsReadOnly,CssClass,Script
	FROM PageViewProperty where ContainerId in (
	Select Id from PageViewContainer Where PageViewId=@PageViewID and IsDeleted=0) and IsDeleted=0
END	

