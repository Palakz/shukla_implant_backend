﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllPickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetAllDataTypes]
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetAllDataTypes]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		Id,
		[Name]
	FROM
		DataTypes
END	


