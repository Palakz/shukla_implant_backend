﻿USE [db_a46e77_surgicalimplant]
GO
/****** Object:  StoredProcedure [dbo].[InsertPageView]    Script Date: 7/23/2022 1:02:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InsertPageView]
@Id int,
@Name Nvarchar(50),
@PageViewType int,
@ImplantType int,
@Url nvarchar(50),
@IsActive bit,
@CreatedOn Datetime,
@CreatedBy int

AS
BEGIN
	If(@Id IS NULL)
		BEGIN
			INSERT Into PageView 
				(Name,PageViewType,ImplantType,URL,IsDeleted,IsActive,CreatedOn,CreatedBy)
			VALUES
				(@Name,@PageViewType,@ImplantType,@Url,0,@IsActive,@CreatedOn,@CreatedBy)
		END
	ELSE
		BEGIN
			UPDATE PageView SET 
			Name=@Name,PageViewType=@PageViewType,ImplantType=@ImplantType,IsActive=@IsActive,URL=@Url,
			--CreatedOn=@CreatedOn,CreatedBy=@CreatedBy,
			ModifiedOn=@CreatedOn,ModifiedBy=@CreatedBy
			WHERE Id=@Id
		END
END
