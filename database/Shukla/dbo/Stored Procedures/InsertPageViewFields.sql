﻿USE [db_a46e77_surgicalimplant]
GO
/****** Object:  StoredProcedure [dbo].[InsertPageViewFields]    Script Date: 7/23/2022 1:21:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InsertPageViewFields]
	@Id int,
	@PageViewContainerId int,
	@FieldId int,
	@OnChangeJavaScriptFunc nvarchar(max),
	@CreatedOn DateTime,
	@CreatedBy int
AS
BEGIN
	IF(@Id is null)
		BEGIN
			INSERT INTO PageViewFields (PageViewContainerId,FieldId,OnChangeJavaScriptFunc,CreatedOn,CreatedBy)
			VALUES(@PageViewContainerId,@FieldId,@OnChangeJavaScriptFunc,@CreatedOn,@CreatedBy)
		END
	ELSE
		BEGIN
			UPDATE PageViewFields SET PageViewContainerId=@PageViewContainerId,FieldId=@FieldId,OnChangeJavaScriptFunc=@OnChangeJavaScriptFunc,
			ModifiedOn=@CreatedOn,ModifiedBy=@CreatedBy
			WHERE Id=@Id
		END

END
