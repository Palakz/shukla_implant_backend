﻿CREATE TABLE [dbo].[PageViewFields] (
    [Id]                     INT            NOT NULL,
    [PageViewContainerId]    INT            NOT NULL,
    [FieldId]                INT            NOT NULL,
    [OnChangeJavaScriptFunc] NVARCHAR (MAX) NOT NULL,
    [CreatedOn]              DATETIME       NULL,
    [CreatedBy]              INT            NULL,
    [ModifiedOn]             DATETIME       NULL,
    [ModifiedBy]             INT            NULL,
    CONSTRAINT [PK_PageViewFields] PRIMARY KEY CLUSTERED ([Id] ASC)
);

