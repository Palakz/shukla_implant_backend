﻿CREATE TABLE [dbo].[ImplantFields] (
    [Id]                    INT            IDENTITY (1, 1) NOT NULL,
    [ImplantType]           SMALLINT       NOT NULL,
    [FieldName]             VARCHAR (50)   NOT NULL,
    [DisplayName]           NVARCHAR (100) NOT NULL,
    [DataTypeId]            TINYINT        NOT NULL,
    [PickListId]            INT            NULL,
    [DefaultValue]          VARCHAR (MAX)  NULL,
    [MaxLength]             SMALLINT       NULL,
    [MinLength]             SMALLINT       NULL,
    [IsRequired]            BIT            NULL,
    [Regex]                 VARCHAR (250)  NULL,
    [RerversePropertyId]    INT            NULL,
    [DisplayOrder]          INT            NULL,
    [IsPartOfFilter]        BIT            CONSTRAINT [DF_ImplantFields_IsPartOfFilter] DEFAULT ((0)) NOT NULL,
    [FilterDisplayOrder]    INT            NULL,
    [IsMultiSelectOnCreate] BIT            NULL,
    [IsMultiSelectOnFilter] BIT            NULL,
    [IsDeleted]             BIT            NOT NULL,
    [IsSystemDefined]       BIT            NULL,
    [CreatedOn]             DATETIME       NULL,
    [CreatedBy]             INT            NULL,
    [ModifiedOn]            DATETIME       NULL,
    [ModifiedBy]            INT            NULL,
    CONSTRAINT [PK_ImplantFields] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE TRIGGER [dbo].[AddFieldsOnInsert]
ON [dbo].[ImplantFields]
AFTER INSERT 
AS 
BEGIN
	
	DECLARE @FieldName NVARCHAR(50);
	DECLARE @DataTypeId TINYINT;
	DECLARE @Query NVARCHAR(MAX);
	DECLARE @DataTypeName NVARCHAR(MAX);
	
	SELECT @FieldName = FieldName  FROM inserted
	SELECT @DataTypeId = DataTypeId  FROM inserted

	SELECT  @DataTypeName = Name FROM DataTypes WHERE Id=@DataTypeId

	SET @Query = 'ALTER TABLE Implant 
				  ADD ' + @FieldName + ' '+ @DataTypeName

	 print(@Query)
	 exec(@Query)
END
