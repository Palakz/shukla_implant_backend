﻿CREATE TABLE [dbo].[Organisations] (
    [Id]              SMALLINT      IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (250) NOT NULL,
    [IsDeleted]       BIT           NOT NULL,
    [CreatedOn]       DATETIME      NULL,
    [CreatedBy]       INT           NULL,
    [ModifiedOn]      DATETIME      NULL,
    [ModifiedBy]      INT           NULL,
    [IsSystemDefined] BIT           NULL,
    CONSTRAINT [PK_Organisations] PRIMARY KEY CLUSTERED ([Id] ASC)
);

