﻿CREATE TABLE [dbo].[PickListValue] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [PickListId]   INT           NOT NULL,
    [IsDefault]    BIT           NULL,
    [DisplayOrder] INT           NULL,
    [Name]         VARCHAR (MAX) NOT NULL,
    [Value]        VARCHAR (MAX) NOT NULL,
    [Note]         VARCHAR (MAX) NOT NULL,
    [IsDeleted]    BIT           NOT NULL,
    [CreatedOn]    DATETIME      NOT NULL,
    [ModifedOn]    DATETIME      NULL,
    [CreatedBy]    INT           NOT NULL,
    [ModifiedBy]   INT           NULL
);

