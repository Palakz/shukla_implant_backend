﻿CREATE TABLE [dbo].[PageView] (
    [Id]                    INT           NOT NULL,
    [ImplantDetailPageView] NVARCHAR (50) NOT NULL,
    [Name]                  NVARCHAR (50) NOT NULL,
    [URL]                   NVARCHAR (50) NOT NULL,
    [IsDefault]             BIT           NOT NULL,
    [CreatedOn]             DATETIME      NULL,
    [CreatedBy]             INT           NULL,
    [ModifiedOn]            DATETIME      NULL,
    [ModifiedBy]            INT           NULL,
    CONSTRAINT [PK_PageView] PRIMARY KEY CLUSTERED ([Id] ASC)
);

