﻿CREATE TABLE [dbo].[DataTypes] (
    [Id]   TINYINT      IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_DataTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

