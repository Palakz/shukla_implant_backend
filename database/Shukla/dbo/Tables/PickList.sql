﻿CREATE TABLE [dbo].[PickList] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (50)  NULL,
    [Description]     VARCHAR (MAX) NULL,
    [IsValueNameSame] BIT           NULL,
    [IsValueAutoId]   BIT           NULL,
    [IsDeleted]       BIT           NOT NULL,
    [CreatedOn]       DATETIME      NOT NULL,
    [ModifiedOn]      DATETIME      NULL,
    [CreatedBy]       INT           NOT NULL,
    [ModifiedBy]      INT           NULL,
    [DisplayOrder]    INT           NULL,
    [OrderBy]         VARCHAR (255) NULL
);

ALTER TABLE  [dbo].[PickList]
ADD DataType varchar(255);

