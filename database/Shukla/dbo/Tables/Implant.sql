﻿CREATE TABLE [dbo].[Implant] (
    [Id]          INT            NOT NULL,
    [Name]        NVARCHAR (50)  NULL,
    [CreatedBy]   INT            NULL,
    [CreatedDate] DATETIME       NULL,
    [ModifyBy]    INT            NULL,
    [ModifyDate]  DATETIME       NULL,
    [Implant]     NVARCHAR (MAX) NULL,
    [sholder]     INT            NULL,
    CONSTRAINT [PK_Implant] PRIMARY KEY CLUSTERED ([Id] ASC)
);

