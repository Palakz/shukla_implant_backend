﻿CREATE TABLE [dbo].[PageViewContainer] (
    [Id]         INT      NOT NULL,
    [PageViewId] INT      NOT NULL,
    [CreatedOn]  DATETIME NULL,
    [CreatedBy]  INT      NULL,
    [ModifiedOn] DATETIME NULL,
    [ModifiedBy] INT      NULL,
    CONSTRAINT [PK_PageViewContainer] PRIMARY KEY CLUSTERED ([Id] ASC)
);

