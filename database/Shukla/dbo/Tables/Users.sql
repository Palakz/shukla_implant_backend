﻿CREATE TABLE [dbo].[Users] (
    [Id]                     INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]              VARCHAR (100)  NOT NULL,
    [LastName]               VARCHAR (100)  NOT NULL,
    [UserName]               VARCHAR (50)   NOT NULL,
    [Password]               NVARCHAR (MAX) NOT NULL,
    [EmailAddress]           VARCHAR (250)  NOT NULL,
    [MobileNumber]           VARCHAR (12)   NULL,
    [OccupationId]           INT            NOT NULL,
    [OrganisationId]         INT            NOT NULL,
    [IsActive]               BIT            CONSTRAINT [DF_Users_IsActive] DEFAULT ((0)) NOT NULL,
    [IsDeleted]              BIT            CONSTRAINT [DF_Users_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsEmailVerified]        BIT            CONSTRAINT [DF_Users_IsEmailVerified] DEFAULT ((0)) NOT NULL,
    [EmailVerificationToken] NVARCHAR (MAX) NULL,
    [CreatedOn]              DATETIME       NULL,
    [CreatedBy]              INT            NULL,
    [ModifiedOn]             DATETIME       NULL,
    [ModifiedBy]             INT            NULL,
    [IsAdmin]                BIT            CONSTRAINT [DF_Users_IsAdmin] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([Id] ASC)
);

