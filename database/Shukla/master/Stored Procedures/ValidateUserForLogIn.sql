﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [master].[ValidateUserForLogIn]
Comments	: 13-04-2022 | PIYUSH PRAJAPATI | This procedure is used to validate user while login into the application.

Test Execution : EXEC [master].[ValidateUserForLogIn]
					@UserName = 'Jalpa.Dangi',
					@Password = 'nqj/iPMqYVM=',
					--@IpAddress = NULL,
					@StatusTypeActive = 1
					--@UserTypeAdmin = 2
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[ValidateUserForLogIn]
(
	@UserName VARCHAR(100),
	@Password VARCHAR(MAX),
	--@IpAddress VARCHAR(45),
	@StatusTypeActive TINYINT
	--@UserTypeAdmin TINYINT
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT Id AS UserId, UserName AS UserName FROM [dbo].[Users] WHERE UserName = @UserName AND [PASSWORD] =@Password AND IsEmailVerified=1 AND IsActive=1
End;
