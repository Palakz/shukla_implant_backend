﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [master].[GetAllOrganisations] 
AS
BEGIN
	Select Id,Name from Organisations where IsDeleted=0 Order By Id Desc
END
