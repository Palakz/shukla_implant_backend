﻿/*-----------------------------------------------------
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>		DECLARE @Currentdate1 datetime =getdate();
									EXEC [master].[UpdateOccupations] 
									@Name ='piyush',
									@ModifiedBy =15,
									@ModifiedDate= @Currentdate1,
									@Id =1
---------------------------------------------------*/
CREATE PROCEDURE [master].[UpdateOccupations] 
	@Name varchar(50),
	@ModifiedBy int,
	@ModifiedDate DATETIME,
	@Id INT
AS
BEGIN
	UPDATE Occupations SET Name = @Name , ModifiedBy= @ModifiedBy, ModifiedOn=GETDATE() WHERE Id=@Id
END
