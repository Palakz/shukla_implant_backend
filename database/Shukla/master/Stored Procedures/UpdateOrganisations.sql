﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [master].[UpdateOrganisations] 
	@Name varchar(250),
	@ModifiedBy int,
	@ModifiedDate DATETIME,
	@Id INT
AS
BEGIN
	UPDATE Organisations SET Name = @Name , ModifiedBy= @ModifiedBy, ModifiedOn=GETDATE() WHERE Id=@Id
END
