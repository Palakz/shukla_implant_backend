﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [master].[GetAllOccupations] 
AS
BEGIN
	Select Id,Name from Occupations WHERE IsDeleted=0 order by Id Desc
END
