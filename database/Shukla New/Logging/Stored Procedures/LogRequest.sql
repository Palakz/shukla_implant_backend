﻿Create PROCEDURE [Logging].[LogRequest]
(
	@SubscriberId INT,
	@UserId INT,
	@RequestId UNIQUEIDENTIFIER,
	@Data VARCHAR(MAX),
	@FileName VARCHAR(MAX),
	@Method VARCHAR(6),
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(200),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [logging].[LogRequest]','
										@SubscriberId =',@SubscriberId,',
										@UserId =',@UserId,',
										@RequestId =''',@RequestId,''',
										@Data =''',@Data,''',
										@IpAddress =''',@IpAddress,''',
										@Method =''',@Method,''',
										@FileName =''',@FileName,''
									  ),
			@ProcedureName = '[logging].[LogRequest]',
			@ExecutionTime = GETDATE()	
    BEGIN TRY
		INSERT INTO logging.Requests
		(
			SubscriberId,
			RequestId,
			UserId,
			[Data],
			[FileName],
			Method,
			IpAddress,
			Stamp
		)	
		VALUES
		(
			@SubscriberId,
			@RequestId,
			@UserId,
			@Data,
			@FileName,
			@Method,
			@IpAddress,
			GETDATE()		
		)

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH
	;
