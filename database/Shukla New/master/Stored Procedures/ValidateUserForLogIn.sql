﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [master].[ValidateUserForLogIn]
Comments	: 13-04-2022 | PIYUSH PRAJAPATI | This procedure is used to validate user while login into the application.

Test Execution : EXEC [master].[ValidateUserForLogIn]
					@UserName = 'admin',
					@Password = '6G94qKPK8LYNjnTllCqm2G3BUM08AzOK7yW30tfjrMc=',
					--@IpAddress = NULL,
					@StatusTypeActive = 1
					--@UserTypeAdmin = 2
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [master].[ValidateUserForLogIn]
(
	@UserName VARCHAR(100),
	@Password VARCHAR(MAX),
	@StatusTypeActive TINYINT
)
AS
BEGIN
	SET NOCOUNT ON;
	Declare @IsEmail int, @UserId INT, @OccupationId INT;
	select @IsEmail= CHARINDEX('@',@UserName)

	SELECT @UserId = Id, @OccupationId = OccupationId FROM Users WHERE Username = @UserName OR EmailAddress = @UserName

	if( @IsEmail> 0 )
		BEGIN
			SELECT Id AS UserId, UserName AS UserName, FirstName, LastName, EmailAddress, MobileNumber,IsActive,IsEmailVerified, IsAdmin,
			OccupationId,OrganisationId ,
			ProfileImage,ApprovedStatus, HoldStatus
			FROM [dbo].[Users] WHERE EmailAddress = @UserName AND [PASSWORD] =@Password AND IsDeleted=0

		END
	ELSE
		BEGIN
			SELECT Id AS UserId, UserName AS UserName, FirstName, LastName, EmailAddress, MobileNumber,IsActive,IsEmailVerified, IsAdmin,
			OccupationId,OrganisationId, HoldStatus
			ProfileImage,ApprovedStatus FROM [dbo].[Users] WHERE UserName = @UserName AND [PASSWORD] =@Password AND IsDeleted=0
		END

		IF EXISTS(SELECT * FROM UserRights WHERE UserId = @UserId)
		BEGIN
			SELECT 
				mgr.Id AS ModuleGroupRightsId
				,mgr.[Name]
				,mgr.Acronym
				,CASE WHEN ocr.Id IS NULL THEN 0 ELSE 1 END AS SelectedRight
			FROM 
				ModuleGroupRights mgr
				LEFT JOIN UserRights ocr ON ocr.ModuleGroupRightsId = mgr.Id  AND ocr.userid = @UserId
		END
		ELSE
			SELECT 
				mgr.Id AS ModuleGroupRightsId
				,mgr.[Name]
				,mgr.Acronym
				,CASE WHEN ocr.Id IS NULL THEN 0 ELSE 1 END AS SelectedRight
			FROM 
				ModuleGroupRights mgr
				LEFT JOIN OccupationRights ocr ON ocr.ModuleGroupRightsId = mgr.Id  AND ocr.OccupationId = @OccupationId
		END


