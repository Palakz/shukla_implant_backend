﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [master].[UpdateProfileById]
Comments	: 13-04-2022 | Kartik Bariya | This procedure is used to validate user while login into the application.

Test Execution :	DECLARE @Currentdate1 datetime =getdate();
					EXEC [master].[UpdateProfileById] 
					@UserId =95,
					@CurrentDate =@Currentdate1,
					@FirstName	='FNupdated',
					@LastName	= 'lNupdated',
					@MobileNumber	= '1234567890',
					@OccupationId	=1,
					@OrganisationId	=1
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [master].[UpdateProfileById]
(
	@UserId INT,
	@CurrentDate DATETIME,
	@FirstName	VARCHAR(100),
	@LastName	VARCHAR(100),
	@MobileNumber	VARCHAR(MAX),
	@OccupationId	INT,
	@OrganisationId	INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
		UPDATE [dbo].[Users]
		SET
			FirstName = @FirstName,
			LastName = @LastName,
			MobileNumber = @MobileNumber,
			OccupationId = @OccupationId,
			OrganisationId = @OrganisationId,
			ModifiedOn = @CurrentDate,
			ModifiedBy = @UserId
		WHERE
			 Id = @UserId;
End
