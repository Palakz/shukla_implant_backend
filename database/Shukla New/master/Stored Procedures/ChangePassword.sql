﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[CheckIfEmployeeIdExists]
Comments	: 13-04-2022 | Kartik Bariya | This procedure is used to validate user while login into the application.

Test Execution :	DECLARE @Currentdate1 datetime =getdate();
					EXEC [master].[ChangePassword] 
					@UserId = 81,
					@CurrentPassword ="6G94qKPK8LYNjnTllCqm2G3BUM08AzOK7yW30tfjrMc=",
					@NewPassword ="6G94qKPK8LYNjnTllCqm2G3BUM08AzOK7yW30tfjrMc=",
					--@StatusActive = 1,
					@CurrentDate = @Currentdate1
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [master].[ChangePassword]
(
	@UserId INT,
	@CurrentPassword NVARCHAR(100),
	@NewPassword NVARCHAR(100),
	--@StatusActive SMALLINT,
	@CurrentDate DATETIME
)
AS
BEGIN
	SET NOCOUNT ON;
	Declare @Reponse  INT = 1 ;

	IF NOT EXISTS(SELECT 1 from [dbo].[Users] Where [Password] = @CurrentPassword AND Id = @UserId)
    BEGIN
		SET @Reponse = -1;
        SELECT @Reponse; 
	END
	ELSE
	BEGIN
		UPDATE [dbo].[Users]
		SET
			[Password] = @NewPassword,
			ModifiedOn = @CurrentDate,
			ModifiedBy = @UserId
		WHERE
			[Password] = @CurrentPassword AND Id = @UserId;
		SELECT @Reponse;
	END
End;
