﻿USE [db_a46e77_surgicalimplant]
GO
/****** Object:  StoredProcedure [master].[UpdateProfileById]    Script Date: 9/28/2022 11:37:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------
Procedure	: [master].[UpdateProfileById]
Comments	: 13-04-2022 | Kartik Bariya | This procedure is used to validate user while login into the application.

Test Execution :	DECLARE @Currentdate1 datetime =getdate();
					EXEC [master].[UpdateProfileWithImageById] 
					@UserId =95,
					@CurrentDate =@Currentdate1,
					@FirstName	='FNupdated',
					@LastName	= 'lNupdated',
					@MobileNumber	= '1234567890',
					@OccupationId	=1,
					@OrganisationId	=1,
					@ProfileImage ='path',
					@IsImageUpdate = 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[UpdateProfileWithImageById]
(
	@UserId INT,
	@CurrentDate DATETIME,
	@FirstName	VARCHAR(100),
	@LastName	VARCHAR(100),
	@MobileNumber	VARCHAR(MAX),
	@OccupationId	INT,
	@OrganisationId	INT,
	@ProfileImage VARCHAR(MAX),
	@IsImageUpdate BIT
)
AS
BEGIN
	SET NOCOUNT ON;
	
		UPDATE [dbo].[Users]
		SET
			FirstName = @FirstName,
			LastName = @LastName,
			MobileNumber = @MobileNumber,
			OccupationId = @OccupationId,
			OrganisationId = @OrganisationId,
			ModifiedOn = @CurrentDate,
			ModifiedBy = @UserId,
			ProfileImage = CASE WHEN @IsImageUpdate = 1 THEN @ProfileImage ELSE ProfileImage END
		WHERE
			 Id = @UserId;
End
