﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [master].[DeleteOrganization]
Comments	: 23-06-2022 | NIRALI CHITRODA | This procedure is used to deleted the organisations.

Test Execution : EXEC [master].[DeleteOrganisations]
						@ModifiedBy = 1 ,
						@ModifiedDate = @CurrentDate
						@Id = 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[DeleteOrganisations]
(
	@ModifiedBy INT,
	@ModifiedDate DATETIME,
	@Id INT
)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE Organisations SET IsDeleted = 1 , ModifiedBy = @ModifiedBy , ModifiedOn = GETDATE() WHERE Id = @Id
End;


--select * from Organisations