﻿USE [db_a46e77_surgicalimplant]
GO
/****** Object:  StoredProcedure [master].[UpdateProfileById]    Script Date: 9/28/2022 11:37:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------
Procedure	: [master].[GetUserDetailsByUserId.sql]
Comments	: 13-04-2022 | Kartik Bariya | This procedure is used to validate user while login into the application.

Test Execution :	DECLARE @Currentdate1 datetime =getdate();
					EXEC [master].[GetUserDetailsByUserId.sql] 
					@UserId =95,
					@CurrentDate =@Currentdate1,
					@FirstName	='FNupdated',
					@LastName	= 'lNupdated',
					@MobileNumber	= '1234567890',
					@OccupationId	=1,
					@OrganisationId	=1,
					@ProfileImage ='path',
					@IsImageUpdate = 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetUserDetailsByUserId.sql]
(
	@UserId INT,
)
AS
BEGIN
	SET NOCOUNT ON;
	
		
			
End
