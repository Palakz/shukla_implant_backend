﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [master].[DeleteOccupation]
Comments	: 23-06-2022 | NIRALI CHITRODA | This procedure is used to deleted the occupation.

Test Execution : EXEC [master].[DeleteOccupation]
						@ModifiedBy = 1 ,
						@ModifiedDate = @CurrentDate
						@Id = 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[DeleteOccupation]
(
	@ModifiedBy INT,
	@ModifiedDate DATETIME,
	@Id INT
)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE Occupations SET IsDeleted = 1 , ModifiedBy = @ModifiedBy , ModifiedOn = GETDATE() WHERE Id = @Id
End;