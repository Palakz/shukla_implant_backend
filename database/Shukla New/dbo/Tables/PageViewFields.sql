﻿CREATE TABLE [dbo].[PageViewFields] (
    [Id]                     INT            IDENTITY (1, 1) NOT NULL,
    [PageViewContainerId]    INT            NOT NULL,
    [FieldId]                INT            NOT NULL,
    [OnChangeJavaScriptFunc] NVARCHAR (MAX) NOT NULL,
    [IsDeleted]              BIT            CONSTRAINT [DF_PageViewFields_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]              DATETIME       NULL,
    [CreatedBy]              INT            NULL,
    [ModifiedOn]             DATETIME       NULL,
    [ModifiedBy]             INT            NULL,
    CONSTRAINT [PK_PageViewFields] PRIMARY KEY CLUSTERED ([Id] ASC)
);

