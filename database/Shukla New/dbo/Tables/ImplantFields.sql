﻿CREATE TABLE [dbo].[ImplantFields] (
    [Id]                       INT            IDENTITY (1, 1) NOT NULL,
    [ImplantType]              SMALLINT       NULL,
    [FieldName]                VARCHAR (50)   NOT NULL,
    [DisplayName]              NVARCHAR (100) NOT NULL,
    [DataTypeId]               TINYINT        NOT NULL,
    [PickListId]               INT            NULL,
    [DefaultValue]             VARCHAR (MAX)  NULL,
    [MaxLength]                SMALLINT       NULL,
    [MinLength]                SMALLINT       NULL,
    [IsRequired]               BIT            NULL,
    [Regex]                    VARCHAR (250)  NULL,
    [RerversePropertyId]       INT            NULL,
    [DisplayOrder]             INT            NULL,
    [IsPartOfFilter]           BIT            CONSTRAINT [DF_ImplantFields_IsPartOfFilter] DEFAULT ((0)) NOT NULL,
    [FilterDisplayOrder]       INT            NULL,
    [IsMultiSelectOnCreate]    BIT            NULL,
    [IsMultiSelectOnFilter]    BIT            NULL,
    [IsDeleted]                BIT            NOT NULL,
    [IsSystemDefined]          BIT            NULL,
    [CreatedOn]                DATETIME       NULL,
    [CreatedBy]                INT            NULL,
    [ModifiedOn]               DATETIME       NULL,
    [ModifiedBy]               INT            NULL,
    [MaxImageAllowed]          INT            NULL,
    [MaxTotalSize]             BIGINT         NULL,
    [MaxSingleSize]            BIGINT         NULL,
    [IsApplicableToAllImplant] BIT            CONSTRAINT [DF__ImplantFi__IsApp__46B27FE2] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_ImplantFields] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE TRIGGER [dbo].[AddFieldsOnInsert]ON [dbo].[ImplantFields]AFTER INSERT AS BEGIN	DECLARE @FieldName NVARCHAR(50), @IsRequiredValue VARCHAR(100) = 'NULL';	DECLARE @DataTypeId TINYINT, @IsRequired BIT;	DECLARE @Query NVARCHAR(MAX);	DECLARE @DataTypeName NVARCHAR(MAX);		SELECT @FieldName = FieldName  FROM inserted	SELECT @DataTypeId = DataTypeId  FROM inserted	SELECT @IsRequired = IsRequired  FROM inserted	--IF(@IsRequired = 1)
	--BEGIN
	--	SET @IsRequiredValue = 'NOT NULL';
	--END	--SELECT  @DataTypeName = DataTypeValue FROM DataTypes WHERE Id=@DataTypeId	Set @DataTypeName = 'varchar(max)';	SET @Query = 'ALTER TABLE Implant 				  ADD ' + @FieldName + ' '+ @DataTypeName + ' ' + @IsRequiredValue;	EXEC(@Query)END

