﻿CREATE TABLE [dbo].[PageViewType] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (250) NOT NULL,
    [IsDelete]    BIT            NOT NULL,
    [CreatedBy]   INT            NOT NULL,
    [CreatedOn]   DATE           NOT NULL,
    [ModifiedBy]  INT            NULL,
    [ModifiedOn]  DATE           NULL,
    CONSTRAINT [PK_PageViewType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

