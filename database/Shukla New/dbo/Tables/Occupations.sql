﻿CREATE TABLE [dbo].[Occupations] (
    [Id]              SMALLINT     IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (50) NOT NULL,
    [IsDeleted]       BIT          NOT NULL,
    [CreatedOn]       DATETIME     NULL,
    [CreatedBy]       INT          NULL,
    [ModifiedOn]      DATETIME     NULL,
    [ModifiedBy]      INT          NULL,
    [IsSystemDefined] BIT          NULL,
    CONSTRAINT [PK_Occupations] PRIMARY KEY CLUSTERED ([Id] ASC)
);

