﻿CREATE TABLE [dbo].[RecentlyViewed] (
    [Id]            INT      IDENTITY (1, 1) NOT NULL,
    [ImplantId]     INT      NULL,
    [ImplantTypeId] INT      NULL,
    [CreatedBy]     INT      NOT NULL,
    [CreatedDate]   DATETIME NOT NULL,
    CONSTRAINT [PK__Recently__3214EC07AA716C60] PRIMARY KEY CLUSTERED ([Id] ASC)
);

