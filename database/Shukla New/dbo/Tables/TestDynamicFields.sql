﻿CREATE TABLE [dbo].[TestDynamicFields] (
    [Id]    INT           IDENTITY (1, 1) NOT NULL,
    [Name]  VARCHAR (255) NULL,
    [Value] VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

