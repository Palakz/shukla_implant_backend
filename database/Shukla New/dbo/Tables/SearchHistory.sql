﻿CREATE TABLE [dbo].[SearchHistory] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [SearchName]    VARCHAR (MAX) NOT NULL,
    [SearchPayload] VARCHAR (MAX) NULL,
    [SearchBy]      INT           NOT NULL,
    [SearchDate]    DATETIME      NOT NULL
);

