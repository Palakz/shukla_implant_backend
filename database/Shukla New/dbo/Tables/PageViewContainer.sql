﻿CREATE TABLE [dbo].[PageViewContainer] (
    [Id]                    INT           IDENTITY (1, 1) NOT NULL,
    [Name]                  NVARCHAR (50) NOT NULL,
    [PageViewId]            INT           NOT NULL,
    [IsSystemGenerated]     BIT           CONSTRAINT [DF_PageViewContainer_IsSystemGenerated] DEFAULT ((0)) NOT NULL,
    [IsDeleted]             BIT           NOT NULL,
    [CreatedOn]             DATETIME      NULL,
    [CreatedBy]             INT           NULL,
    [ModifiedOn]            DATETIME      NULL,
    [ModifiedBy]            INT           NULL,
    [ContainerIcon]         VARCHAR (MAX) NULL,
    [ContainerIconPosition] VARCHAR (MAX) NULL,
    [DisplayColumnNumber]   SMALLINT      NULL,
    CONSTRAINT [PK_PageViewContainer] PRIMARY KEY CLUSTERED ([Id] ASC)
);

