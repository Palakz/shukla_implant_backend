﻿CREATE TABLE [dbo].[Favorite] (
    [Id]        INT      IDENTITY (1, 1) NOT NULL,
    [UserId]    INT      NOT NULL,
    [ImplantId] INT      NOT NULL,
    [CreatedOn] DATETIME NOT NULL
);

