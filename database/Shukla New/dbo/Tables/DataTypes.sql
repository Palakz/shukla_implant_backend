﻿CREATE TABLE [dbo].[DataTypes] (
    [Id]            TINYINT       IDENTITY (1, 1) NOT NULL,
    [Name]          VARCHAR (50)  NOT NULL,
    [DataTypeValue] VARCHAR (100) NULL,
	[IsDeleted] BIT NOT NULL,
    CONSTRAINT [PK_DataTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

