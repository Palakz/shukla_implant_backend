﻿CREATE TABLE [dbo].[ImplantType] (
    [Id]         TINYINT      IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (50) NOT NULL,
    [IsDeleted]  BIT          NOT NULL,
    [CreatedOn]  DATETIME     NULL,
    [CreatedBy]  INT          NULL,
    [ModifiedOn] DATETIME     NULL,
    [ModifiedBy] INT          NULL,
    CONSTRAINT [PK_ImplantType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

