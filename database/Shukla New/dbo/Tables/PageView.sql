﻿CREATE TABLE [dbo].[PageView] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (50) NOT NULL,
    [PageViewType] INT           NOT NULL,
    [ImplantType]  INT           NOT NULL,
    [URL]          NVARCHAR (50) NOT NULL,
    [IsDefault]    BIT           CONSTRAINT [DF_PageView_IsDefault] DEFAULT ((0)) NOT NULL,
    [IsActive]     BIT           CONSTRAINT [DF_PageView_IsActive] DEFAULT ((0)) NOT NULL,
    [IsDeleted]    BIT           NOT NULL,
    [CreatedOn]    DATETIME      NULL,
    [CreatedBy]    INT           NULL,
    [ModifiedOn]   DATETIME      NULL,
    [ModifiedBy]   INT           NULL,
    CONSTRAINT [PK_PageView] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AddSystemGeneratedContainer]
ON [dbo].[PageView]
AFTER INSERT 
AS 
BEGIN
	DECLARE @PageViewId Int		SELECT @PageViewId = Id  FROM inserted	INSERT INTO PageViewContainer (Name,PageViewId,IsSystemGenerated,IsDeleted,CreatedBy,CreatedOn)	Values('Header',@PageViewId,1,0,1,Getdate())
END
