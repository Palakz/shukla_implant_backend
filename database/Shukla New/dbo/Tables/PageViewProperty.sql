﻿CREATE TABLE [dbo].[PageViewProperty] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [FieldId]          INT            NOT NULL,
    [DisplayName]      NVARCHAR (50)  NULL,
    [ContainerId]      INT            NOT NULL,
    [IsActive]         BIT            CONSTRAINT [DF_PageViewProperty_IsActive] DEFAULT ((0)) NOT NULL,
    [IsReadOnly]       BIT            CONSTRAINT [DF_PageViewProperty_IsReadOnly] DEFAULT ((0)) NOT NULL,
    [IsVisible]        BIT            CONSTRAINT [DF_PageViewProperty_IsVisible] DEFAULT ((0)) NOT NULL,
    [CssClass]         NVARCHAR (50)  NULL,
    [Script]           NVARCHAR (500) NULL,
    [IsDeleted]        BIT            CONSTRAINT [DF_PageViewProperty_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]        INT            NOT NULL,
    [CreatedOn]        DATETIME       NOT NULL,
    [ModifiedBy]       INT            NULL,
    [ModifiedOn]       DATETIME       NULL,
    [FieldControlType] VARCHAR (100)  NULL,
    CONSTRAINT [PK_PageViewProperty] PRIMARY KEY CLUSTERED ([Id] ASC)
);

