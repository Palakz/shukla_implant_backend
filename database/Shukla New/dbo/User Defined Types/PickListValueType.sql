﻿CREATE TYPE [dbo].[PickListValueType] AS TABLE (
    [Id]           INT           NULL,
    [PickListId]   INT           NULL,
    [IsDefault]    BIT           NULL,
    [DisplayOrder] INT           NULL,
    [Name]         VARCHAR (255) NULL,
    [Value]        VARCHAR (MAX) NULL,
    [Note]         VARCHAR (500) NULL,
	[Description]    VARCHAR (MAX) NULL
	);

