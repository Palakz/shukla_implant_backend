﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetModuleRightsByOccupationId]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetModuleRightsByOccupationId]
					 @OccupationId = 1
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetModuleRightsByOccupationId]
(
	@OccupationId INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		Id
		,[Name]
		,Acronym
	FROM 
		ModuleGroup

	SELECT 
		mgr.Id AS ModuleGroupRightsId
		,mgr.ModuleGroupId
		,mgr.[Name]
		,mgr.Acronym
		,CASE WHEN ocr.Id IS NULL THEN 0 ELSE 1 END AS SelectedRight
	FROM 
		ModuleGroupRights mgr
		LEFT JOIN OccupationRights ocr ON ocr.ModuleGroupRightsId = mgr.Id  AND ocr.OccupationId = @OccupationId

END	

--select * from OccupationRights

--select * from ModuleGroupRights