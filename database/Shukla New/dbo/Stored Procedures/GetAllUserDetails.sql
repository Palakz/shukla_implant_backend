﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllUserDetails]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetAllUserDetails]
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetAllUserDetails]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		Id AS UserId, 
		UserName, 
		FirstName, 
		LastName,
		EmailAddress, 
		IsAdmin,
		OccupationId,
		OrganisationId ,
		ApprovedStatus,
		HoldStatus
	FROM 
		[dbo].[Users] 
	WHERE 
		IsDeleted=0 AND IsEmailVerified = 1 order by 1 desc
END	


