﻿
/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllImplantByImplantTyppeId]
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	:	DECLARE @Currentdate1 datetime =getdate(); 
					EXEC [dbo].[GetAllImplantByImplantTyppeId] 1,100
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetAllImplantByImplantTyppeId]
@ImplantType varchar(max),
@UserId  INT
AS
BEGIN
	SELECT 
		(CASE WHEN fav.id IS NOT NULL AND fav.UserId = @UserId THEN 1 ELSE 0 END ) AS IsFavourite,
		imp.*  
	FROM 
		Implant imp LEFT JOIN Favorite fav ON fav.ImplantId = imp.Id
	WHERE 
		ImplantTypeId = @ImplantType 
	ORDER BY imp.id DESC
END
