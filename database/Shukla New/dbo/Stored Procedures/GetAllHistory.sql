﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- exec [dbo].[GetAllHistory] 28
-- =============================================
alter PROCEDURE [dbo].[GetAllHistory]
(
	@UserId INT ,
	@ImplantTypeId INT
)
AS
BEGIN
	
	Select
		Id,
		SearchName,
		SearchPayload,
		SearchBy,
		SearchDate 
	From 
		SearchHistory 
	WHERE 
		SearchBy=@UserId AND ImplantTypeId = @ImplantTypeId
		ORDER BY 1 DESC
END
