﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetModuleRightsByUserId]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetModuleRightsByUserId]
					 @UserId = 1
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[GetModuleRightsByUserId]
(
	@UserId INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		Id
		,[Name]
		,Acronym
	FROM 
		ModuleGroup

	SELECT 
		mgr.Id AS ModuleGroupRightsId
		,mgr.ModuleGroupId
		,mgr.[Name]
		,mgr.Acronym
		,CASE WHEN ocr.Id IS NULL THEN 0 ELSE 1 END AS SelectedRight
	FROM 
		ModuleGroupRights mgr
		LEFT JOIN UserRights ocr ON ocr.ModuleGroupRightsId = mgr.Id  AND ocr.UserId = @UserId

END	

--select * from OccupationRights

--select * from ModuleGroupRights