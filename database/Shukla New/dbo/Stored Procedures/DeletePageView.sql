﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeletePageView]
@Id int,
@ModifiedBy int,
@ModifiedOn Date
AS
BEGIN
	
	UPDATE PageView SET IsDeleted = 1,ModifiedBy=@ModifiedBy,ModifiedOn=@ModifiedOn where Id=@Id
END
