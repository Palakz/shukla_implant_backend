﻿-- =============================================
/*-----------------------------------------------------
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>		DECLARE @Currentdate1 datetime =getdate();
									EXEC [dbo].[InsertPickList]
									@Name ='piyush',
									@Description = 'desc',
									@IsValueNameSame =1,
									@IsValueAutoId =1,
									@CreatedOn = @Currentdate1,
									@DisplayOrder = 1,
									@CreatedBy = 12
---------------------------------------------------*/
CREATE PROCEDURE [dbo].[InsertPickList]
	@Name varchar(50),
	@Description varchar(max),
	@IsValueNameSame bit,
	@IsValueAutoId bit,
	@CreatedOn DATETIME,
	@DisplayOrder int,
	@CreatedBy INT
AS
BEGIN
	INSERT INTO PickList(Name,Description,IsValueNameSame,IsValueAutoId,CreatedOn,DisplayOrder,CreatedBy,IsDeleted)
	VALUES(@Name,@Description,@IsValueNameSame,@IsValueAutoId,@CreatedOn,@DisplayOrder,@CreatedBy,0)
END
