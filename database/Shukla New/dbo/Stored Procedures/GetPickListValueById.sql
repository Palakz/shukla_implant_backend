﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetPickListById]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:	EXEC [dbo].[GetPickListValueById]
					 @PickListId = 3
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetPickListValueById]
(
	@PickListId INT
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		Id,
		PickListId,
		IsDefault,
		DisplayOrder,
		[Name],
		[Value],
		Note,
		[Description],
		[Image]
	FROM
		PickListValue
	WHERE
		PickListId = @PickListId
		AND IsDeleted = 0;
END	