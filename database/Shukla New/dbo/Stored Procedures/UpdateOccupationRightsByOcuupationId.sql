﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[UpdateOccupationRightsByOccupationId]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   DECLARE @RightIdList as [dbo].[IdType]
				   insert into  @RightIdList (Id)
				   values(6),(5),(12),(13);
				   EXEC [dbo].[UpdateOccupationRightsByOccupationId]
					 @RightIdList = @RightIdList,
					 @CurrentDate = @Currentdate1,
					 @UserId = 1,
					 @OccupationId = 2
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[UpdateOccupationRightsByOccupationId]
(
	@RightIdList [dbo].[IdType] READONLY,
	@CurrentDate DATETIME,
	@UserId INT
	,@OccupationId INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM dbo.OccupationRights
	WHERE OccupationId = @OccupationId
	
	INSERT INTO dbo.OccupationRights
	(
		OccupationId
		,ModuleGroupRightsId
		,Status
		,ModifiedBy
		,ModifiedDate
	)
	SELECT 
		@OccupationId
		,Id
		,1
		,@UserId
		,@CurrentDate
	FROM
		@RightIdList 

	--UPDATE ocr
	--SET 
	--	ocr.OccupationId = @OccupationId
	--	,ocr.ModuleGroupRightsId = rl.Id
	--	,ocr.[Status] = 1
	--	,ocr.ModifiedBy = @UserId
	--	,ocr.ModifiedDate = @CurrentDate	
	--FROM 
	--	dbo.OccupationRights ocr
	--INNER JOIN 
	--	@RightIdList rl ON rl.Id = ocr.Id
END	

select * from OccupationRights

--select * from Occupations


--select * from ModuleGroupRights


--holdon/off api

--occupation list,userlist wity id api,


--module rights api

--right by module,



--{Usernodule:[{
--RightName:'xyz',
--IsSelected:true
--}]