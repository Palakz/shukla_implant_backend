﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[UpdateIsAdminApproveByUserId]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[UpdateIsAdminApproveByUserId]
				    @UserId = 128,
					@ApprovedStatus = 2
					SELECT ApprovedStatus FROM USErS Where Id = 128
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[UpdateIsAdminApproveByUserId]
(
	@UserId INT,
	@ApprovedStatus INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE 
		Users
	SET
		ApprovedStatus = @ApprovedStatus
	WHERE 
		Id = @UserId
END



