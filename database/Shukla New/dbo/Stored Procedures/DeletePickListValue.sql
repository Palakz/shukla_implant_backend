﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[DeletePickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   DECLARE @IdType as [dbo].[IdType]
				   insert into  @IdType (Id)
				   values(6),(5);
				   EXEC [dbo].[DeletePickListValue]
					 @IdType = @IdType,
					 @CurrentDate = @Currentdate1,
					 @UserId = 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[DeletePickListValue]
(
	@IdType [dbo].[IdType] READONLY,
	@CurrentDate DATETIME,
	@UserId INT
)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE p
	SET 
		p.IsDeleted = 1,
		p.ModifedOn = @CurrentDate,
		p.ModifiedBy = @UserId
	FROM 
		dbo.PickListValue p
	INNER JOIN 
		@IdType pt ON pt.Id = p.Id
END	

	select * from PickListValue where picklistid =10
