﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertPageView]
@Id int,
@Name Nvarchar(50),
@PageViewType int,
@ImplantType int,
@Url nvarchar(50),
@IsActive bit,
@CreatedOn Datetime,
@CreatedBy int

AS
BEGIN
	If(@Id IS NULL)
		BEGIN
			INSERT Into PageView 
				(Name,PageViewType,ImplantType,URL,IsDeleted,IsActive,CreatedOn,CreatedBy)
			VALUES
				(@Name,@PageViewType,@ImplantType,@Url,0,@IsActive,@CreatedOn,@CreatedBy)
		END
	ELSE
		BEGIN
			UPDATE PageView SET 
			Name=@Name,PageViewType=@PageViewType,ImplantType=@ImplantType,IsActive=@IsActive,URL=@Url,
			--CreatedOn=@CreatedOn,CreatedBy=@CreatedBy,
			ModifiedOn=@CreatedOn,ModifiedBy=@CreatedBy
			WHERE Id=@Id
		END
END
