﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[AddOrEditPickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[AddPickListValueImage]
					@PicklistValueId = 29,
					@Image ='image.png',
					@CurrentDate = @Currentdate1,
					@UserId = 12
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[AddPickListValueImage]
(
	@PicklistValueId INT,
	@ImagePath VARCHAR(MAX),
	@CurrentDate DATETIME,
	@UserId INT
)
AS
BEGIN
	SET NOCOUNT ON;

		UPDATE PickListValue 
		SET 
			[Image] = @ImagePath,
			ModifedOn = @CurrentDate,
			ModifiedBy = @UserId
		WHERE
			Id = @PicklistValueId
END	
