USE [SurgicalImplant]
GO

/****** Object:  StoredProcedure [dbo].[ChangeImplantTypeStatus]    Script Date: 1/27/2023 11:01:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ChangeImplantTypeStatus]
(
	@Id INT,
	@ModifiedBy INT,
	@ModifiedOn DATETIME

)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE ImplantType
	SET [Status] = CASE Status 
				   WHEN 1 THEN 0 
				   ELSE 1 
				   END,
		ModifiedBy = @ModifiedBy,
		ModifiedOn = @ModifiedOn

	WHERE Id = @Id

END
GO


