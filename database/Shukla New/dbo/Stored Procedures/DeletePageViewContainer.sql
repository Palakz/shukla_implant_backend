﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeletePageViewContainer]
	@Id int,
	@ModifiedBy int,
	@ModifiedOn Date
AS
BEGIN

			--UPDATE PageViewContainer SET IsDeleted=1, ModifiedBy=@ModifiedBy,ModifiedOn=@ModifiedOn
			--WHERE Id=@Id

			DELETE from PageViewContainer WHERE Id=@Id
END
