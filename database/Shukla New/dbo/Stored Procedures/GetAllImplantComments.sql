﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllImplantComments]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetAllImplantComments]
					 @ImplantId = 1,
					 @UserId = 80
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetAllImplantComments]
(
	@ImplantId INT,
	@UserId INT
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		IC.Id,
		IC.Comment, 
		IC.CreatedOn, 
		IC.CreatedBy AS 'CreatedById',
		U.FirstName + ' '+ U.LastName AS 'CreatedBy' ,
		CASE
			WHEN CLDL.Liked = 1 THEN 1
			WHEN CLDL.DisLiked = 1 THEN 0
			ELSE NULL END
		AS 'Liked'
	FROM
		ImplantComment IC
	JOIN Users U ON U.Id = IC.CreatedBy
	LEFT JOIN CommentLikeDisLike CLDL ON IC.Id = CLDL.CommentId AND CLDL.UserId = @UserId
	WHERE IC.ImplantId  = @ImplantId
END	
