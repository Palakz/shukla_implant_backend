﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[AddOrRemoveSearchHistory]
@Id int,
@ImplantId varchar(max),
@SearchText varchar(100),
@CreatedOn Datetime,
@UserId int

AS
BEGIN
	If(@Id IS NULL)
		BEGIN
			INSERT Into SearchHistory 
				(SearchText,UserId,ImplantId,CreatedOn)
			VALUES
				(@SearchText,@UserId,@ImplantId,@CreatedOn)
		END
	ELSE
		BEGIN
			DELETE FROM SearchHistory WHERE Id =@Id
		END
END
