﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetPickListById]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetPickListById]
					 @Id = 8
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetPickListById]
(
	@Id INT
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		Id,
		[Name],
		[Description],
		IsValueNameSame,
		IsValueAutoId,
		DisplayOrder,
		OrderBy,
		DataType
	FROM
		PickList
	WHERE
		Id = @Id
		AND IsDeleted = 0;
END	