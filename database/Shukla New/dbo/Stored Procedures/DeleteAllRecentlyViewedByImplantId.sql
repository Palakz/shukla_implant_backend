﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[DeleteAllRecentlyViewedByImplantId]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[DeleteAllRecentlyViewedByImplantId] 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[DeleteAllRecentlyViewedByImplantId]
(
	@ImplantId INT ,
	@UserId INT
)
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM 
		RecentlyViewed 
	WHERE 
		ImplantId = @ImplantId AND CreatedBy = @UserId
END	


