﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[RejectImplant] 
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	:	DECLARE @Currentdate1 datetime =getdate(); 
					EXEC [dbo].[MarkReadImplantNotification] 
					@Id = 14,
					@UserId = 28,
					@CurrentDate = @Currentdate1
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[MarkReadImplantNotification] 
(
	@Id INT,
	@UserId INT,
	@IsReadAll BIT
)
AS
BEGIN
	SET NOCOUNT ON;	IF(@IsReadAll = 1)	BEGIN
		UPDATE 
			dbo.ImplantNotification
		SET 
			IsRead = 1
		WHERE
			ImplantUserId = @UserId
	END
	ELSE
	BEGIN
			UPDATE 
			dbo.ImplantNotification
		SET 
			IsRead = 1
		WHERE
			Id = @Id AND ImplantUserId = @UserId
	END
END