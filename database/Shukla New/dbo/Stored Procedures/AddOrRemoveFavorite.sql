﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[AddOrRemoveFavorite] 
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	:	DECLARE @Currentdate1 datetime =getdate(); 
					exec [dbo].[AddOrRemoveFavorite]
					@AddAsFavourite = 1,
					@ImplantId = 27,
					@CreatedOn = @Currentdate1,
					@UserId =28
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[AddOrRemoveFavorite]
@AddAsFavourite BIT,
@ImplantId int,
@CreatedOn Datetime,
@UserId int

AS
BEGIN
	If(@AddAsFavourite = 1)
		BEGIN
			INSERT Into Favorite 
				(UserId,ImplantId,CreatedOn)
			VALUES
				(@UserId,@ImplantId,@CreatedOn)
		END
	ELSE
		BEGIN
			DELETE FROM Favorite WHERE ImplantId =@ImplantId
		END
END

