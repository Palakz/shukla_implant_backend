﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllRecentlyViewed]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetAllRecentlyViewed] 102,1
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetAllRecentlyViewed]
(
	@UserId INT ,
	@ImplantTypeId INT
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		 distinct imp.id,(CASE WHEN fav.id IS NULL THEN 0 ELSE 1 END ) AS IsFavourite,
		 rv.Id AS RecentlyViewedId, rv.CreatedDate AS RecentlyViewedDate, imp.*
	FROM
		RecentlyViewed rv
	INNER JOIN 
		Implant imp ON rv.ImplantId = imp.Id 
	LEFT JOIN
		Favorite fav ON fav.ImplantId = imp.Id
	WHERE
		rv.CreatedBy = @UserId AND IsDeleted = 0 AND imp.ImplantTypeId = @ImplantTypeId
	ORDER BY rv.CreatedDate DESC
END	


