﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[DeleteAllHistoryByUserId]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[DeleteAllHistoryByUserId]
					 @UserId = 40
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[DeleteAllHistoryByUserId]
(
	@UserId INT
)
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM SearchHistory WHERE SearchBy = @UserId
END	
