﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[ApproveImplant] 
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	:	DECLARE @Currentdate1 datetime =getdate(); 
					EXEC [dbo].[ApproveImplant] 
					@Id = 14,
					@UserId = 28,
					@CurrentDate = @Currentdate1
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[ApproveImplant] 
(
	@Id INT,
	@UserId INT,
	@CurrentDate varchar(max)
)
AS
BEGIN
	SET NOCOUNT ON;	UPDATE 
		Implant
	SET 
		IsApproved = 1,
		ModifyBy = @UserId,
		ModifyDate = @CurrentDate
	WHERE
		Id = @Id

	DECLARE @ImplantName VARCHAR(MAX), @ImplantUserId VARCHAR(MAX), @Email VARCHAR(MAX);
	SELECT @ImplantName = Name, @ImplantUserId = CreatedBy FROM Implant WHERE Id = @Id;
	SELECT @Email = EmailAddress FROM Users WHERE Id = @ImplantUserId;

	IF Exists(Select 1 From Implant Where Id = @Id)
	BEGIN
		INSERT INTO dbo.ImplantNotification 
		(	
			ImplantId,
			ImplantName,
			ImplantUserId,
			IsRead,
			IsApprovedOrRejected
		)	
		VALUES
		(
			@Id,
			@ImplantName,
			@ImplantUserId,
			0,
			1
		)
	END

	SELECT 
		@Id AS ImplantId, 
		@ImplantName AS ImplantName,
		@Email AS EmailAddress,
		'Approved' AS IsApprovedOrRejected
END