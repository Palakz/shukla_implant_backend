﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[UpdateUserRightsByUserId]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   DECLARE @RightIdList as [dbo].[IdType]
				   insert into  @RightIdList (Id)
				   values(6),(5),(12),(13);
				   EXEC [dbo].[UpdateUserRightsByUserId]
					 @RightIdList = @RightIdList,
					 @CurrentDate = @Currentdate1,
					 @UserId = 1,
					 @UserIdForRights = 2
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[UpdateUserRightsByUserId]
(
	@RightIdList [dbo].[IdType] READONLY,
	@CurrentDate DATETIME,
	@UserId INT,
	@UserIdForRights INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM dbo.UserRights WHERE UserId = @UserIdForRights
	
	INSERT INTO dbo.UserRights
	(
		UserId
		,ModuleGroupRightsId
		,Status
		,ModifiedBy
		,ModifiedDate
	)
	SELECT 
		@UserIdForRights
		,Id
		,1
		,@UserId
		,@CurrentDate
	FROM
		@RightIdList 
END	

