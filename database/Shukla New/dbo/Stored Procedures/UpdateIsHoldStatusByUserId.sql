﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[UpdateIsHoldStatusByUserId]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[UpdateIsHoldStatusByUserId]
				    @UserId = 128,
					@HoldStatus = 0
					SELECT ApprovedStatus,holdstatus FROM USErS Where Id = 128
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[UpdateIsHoldStatusByUserId]
(
	@UserId INT,
	@HoldStatus INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE 
		Users
	SET
		HoldStatus = @HoldStatus
	WHERE 
		Id = @UserId
END



