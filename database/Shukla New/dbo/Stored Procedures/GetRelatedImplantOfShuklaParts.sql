﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetRelatedImplantOfShuklaParts]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime = getdate(); 
				   EXEC [dbo].[GetRelatedImplantOfShuklaParts]  
				   @ImplantId = 1,
				   @ImplantTypeID = 1,
				   @SearchToolValue = 'SXT022'
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetRelatedImplantOfShuklaParts]
(
	@ImplantId VARCHAR ,
	@ImplantTypeID VARCHAR,
	@SearchToolValue VARCHAR(MAX)
)
AS
BEGIN
	SET NOCOUNT ON;
	--DECLARE @selectedToolValue VARCHAR(MAX) = 'SXT022'
		DECLARE @ExecQuery VARCHAR(MAX);
	DECLARE @searchToolQuery VARCHAR(MAX)
	;WITH ToolsFields AS(
	SELECT  F.FieldName + ' like '+CHAR(39)+'%'+@SearchToolValue+'%'+CHAR(39) AS Name FROM PageView AS P
	JOIN PageViewContainer AS PVC ON P.Id = PVC.PageViewId
	JOIN PageViewProperty AS PVP ON PVC.Id = PVP.ContainerId
	JOIN ImplantFields AS F ON F.Id = PVP.FieldID
	WHERE P.IsActive = 1 AND P.ImplantType = @implantTypeId AND P.PageViewType =1
	--AND F.DataTypeId = (SELECT TOP 1 Id FROM DataTypes WHERE name = 'PickList')
	AND F.PickListID = (SELECT TOP 1 Id FROM PickList WHERE name ='Shukla Medical Tools')
	)
	
	SELECT @searchToolQuery = COALESCE(@searchToolQuery + ' OR ','') + Name
	FROM ToolsFields
	--SELECT @searchToolQuery
	
	--SELECT * FROM PickListValue WHERE Picklistid =12
	

	SET @ExecQuery = 'SELECT Id, Name,Manufacturer FROM Implant WHERE IsDeleted = 0 AND Id != '+ @ImplantId +' AND (' + @searchToolQuery + ')'
	--select @ExecQuery;

	exec (@ExecQuery);
END	
