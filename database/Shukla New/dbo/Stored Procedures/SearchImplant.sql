﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[SearchImplant] 
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	:	DECLARE @Currentdate1 datetime =getdate(); 
					exec [dbo].[SearchImplant] 
					@UserId = 28,
					@Name = 'name',
					@JsonSearchPayload = N'{"Manufacturer":"Zydus","Steam":"non"}',
					@AppendJsonString = '',
					@CurrentDate = @Currentdate1, 
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[SearchImplant] 
(
	@UserId INT,
	@Name VARCHAR(MAX),
	@JsonSearchPayload VARCHAR(MAX),
	@AppendJsonString VARCHAR(MAX),
	@ImplantTypeId INT,
	@CurrentDate DATETIME,
	@TotalRecords INT OUT
)
AS
BEGIN
	SET NOCOUNT ON;	EXEC(@AppendJsonString)
	
	IF(@Name IS NOT NULL)
	BEGIN 
		INSERT INTO SearchHistory 
		(
			 SearchName,
			 SearchPayload,
			 SearchBy,
			 SearchDate,
			 ImplantTypeId
		)
		VALUES
		(
			@Name,
			@JsonSearchPayload,
			@UserId,
			@CurrentDate,
			@ImplantTypeId
		)
	END
END