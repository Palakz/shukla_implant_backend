﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllPickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetAllImplantFieldsById]
				   @Id INT
--------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[GetImplantFieldsByImplantType]
(
	@ImplantType INT,
	@IncludeAllImplantTypeFields bit
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		Id
		,ImplantType
		,FieldName
		,DisplayName
		,DataTypeId
		,PickListId
		,DefaultValue
		,MaxLength
		,MinLength
		,IsRequired
		,Regex
		,RerversePropertyId
		,DisplayOrder
		,IsPartOfFilter
		,FilterDisplayOrder
		,IsMultiSelectOnCreate
		,IsMultiSelectOnFilter
		,IsSystemDefined
		,MaxImageAllowed
		,MaxTotalSize
		,MaxSingleSize
	FROM
		ImplantFields
	WHERE
		ImplantType = @ImplantType and IsApplicableToAllImplant=@IncludeAllImplantTypeFields
END	


