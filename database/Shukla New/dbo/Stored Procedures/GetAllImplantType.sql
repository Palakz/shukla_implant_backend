﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllPickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetAllImplantType]
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetAllImplantType]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		Id,
		[Name]
	FROM
		ImplantType
	WHERE 
		IsDeleted = 0 
	ORDER BY 1 DESC
END	


