﻿USE [db_a46e77_surgicalimplant]
GO
/****** Object:  UserDefinedFunction [dbo].[func_GetImplantPropertCountByFilter]    Script Date: 9/8/2022 5:53:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetImplantPropertCountByFilter]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetImplantPropertCountByFilter] 
				   @Property = 'Name',
				   @Value = 'Name1' ,
				   @ImplantTypeId = '20'
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[sp_GetImplantPropertCountByFilter]
(	
	@ImplantTypeId INT,
	@isGetCountForPickList BIT = 0,
	@implantNameSearchText VARCHAR(MAX) = NULL
)
AS
BEGIN
	DECLARE @Sql NVARCHAR(MAX);

	IF @isGetCountForPickList = 0 
	BEGIN
		DECLARE @fieldQueryList TABLE
		(Id INT,FieldId INT,DisplayName VARCHAR(MAX) NULL,ContainerId INT,IsActive BIT,IsVisible BIT,IsReadOnly BIT,CssClass VARCHAR(MAX) NULL,
		Script VARCHAR(MAX) NULL,FieldControlType VARCHAR(MAX) NULL,
		Query VARCHAR(MAX) NULL
		)
		INSERT INTO @fieldQueryList
		SELECT PVP.Id,PVP.FieldId,PVP.DisplayName,PVP.ContainerId,PVP.IsActive,PVP.IsVisible,PVP.IsReadOnly,PVP.CssClass,PVP.Script,PVP.FieldControlType,
		'SELECT @retvalOUT = Count('+I.FieldName +') FROM Implant WHERE '+I.FieldName+'=''1'' AND IsDeleted=0' AS Query
		FROM PageViewProperty PVP
		JOIN PageViewContainer PVC ON PVC.Id = PVP.ContainerId
		JOIN PageView PV ON PV.Id = PVC.PageViewId
		JOIN ImplantFields I ON PVP.FieldId = I.Id
		JOIN DataTypes DT ON DT.Id = I.DataTypeId
		WHERE DT.[Name] != 'PickList'
		AND PV.ImplantType = @ImplantTypeId AND PV.PageViewType = 3 AND PV.IsDeleted = 0 AND PV.IsActive=1
		AND PVP.IsDeleted = 0 AND PVP.IsActive = 1 AND PVC.IsDeleted = 0

		IF @implantNameSearchText IS NOT NULL AND LEN(@implantNameSearchText) > 0 
		BEGIN
			UPDATE @fieldQueryList SET Query = Query + ' AND Name like'+CHAR(39)+'%'+@implantNameSearchText+'%'+CHAR(39)
		END

		DECLARE Cur CURSOR LOCAL FAST_FORWARD FOR 
		SELECT  Query
		FROM @fieldQueryList  --<-- table where sql is stored   

		
		OPEN Cur
		
		  FETCH NEXT FROM Cur INTO @Sql 
		
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			 PRINT @sql;
			 DECLARE @retval int   
			 DECLARE @ParmDefinition nvarchar(500);
			 
			 SET @ParmDefinition = N'@retvalOUT int OUTPUT';
			 EXEC sp_executesql @SQL, @ParmDefinition, @retvalOUT=@retval OUTPUT;
			 UPDATE @fieldQueryList SET DisplayName = DisplayName + ' ('+ CAST(@retval AS VARCHAR(MAX))+')' WHERE Query = @Sql
		     FETCH NEXT FROM Cur INTO @Sql 
		END
		
		CLOSE Cur
		DEALLOCATE Cur;
		
		SELECT PVP.Id,PVP.FieldId,PVP.DisplayName,PVP.ContainerId,PVP.IsActive,PVP.IsVisible,PVP.IsReadOnly,PVP.CssClass,PVP.Script,PVP.FieldControlType		
		FROM PageViewProperty PVP
		JOIN PageViewContainer PVC ON PVC.Id = PVP.ContainerId
		JOIN PageView PV ON PV.Id = PVC.PageViewId
		JOIN ImplantFields I ON PVP.FieldId = I.Id
		JOIN DataTypes DT ON DT.Id = I.DataTypeId
		WHERE DT.[Name] = 'PickList'
		AND PV.ImplantType = @ImplantTypeId AND PV.PageViewType = 3 AND PV.IsDeleted = 0 AND PV.IsActive=1
		AND PVP.IsDeleted = 0 AND PVP.IsActive = 1 AND PVC.IsDeleted = 0
		UNION ALL
		SELECT Id,FieldId,DisplayName,ContainerId,IsActive,IsVisible,IsReadOnly,CssClass,Script,FieldControlType FROM @fieldQueryList
		
	END
	ELSE
	BEGIN
		
		DECLARE @fieldQueryPickList TABLE
		(PicklistId INT, [Name] VARCHAR(MAX),[Value] VARCHAR(MAX),[Description] VARCHAR(MAX),[Image] VARCHAR(MAX),
		Query VARCHAR(MAX)
		)
		INSERT INTO @fieldQueryPickList
		SELECT PLV.picklistId,PLV.Name,PLV.Value,PLV.Description,PLV.Image,
		'SELECT @retvalPickList = Count('+I.FieldName +') FROM Implant WHERE '+I.FieldName+'='''+PLV.[Value]+''' AND IsDeleted=0' AS Query
		FROM PageViewProperty PVP
		JOIN PageViewContainer PVC ON PVC.Id = PVP.ContainerId
		JOIN PageView PV ON PV.Id = PVC.PageViewId
		JOIN ImplantFields I ON PVP.FieldId = I.Id
		JOIN DataTypes DT ON DT.Id = I.DataTypeId
		JOIN PickList PL ON PL.ID = I.PickListId
		JOIN PickListValue PLV ON PL.Id = PLV.PickListId
		WHERE DT.[Name] = 'PickList'
		AND PV.ImplantType = 1 AND PV.PageViewType = 3 AND PV.IsDeleted = 0 AND PV.IsActive=1
		AND PVP.IsDeleted = 0 AND PVP.IsActive = 1 AND PVC.IsDeleted = 0

		IF @implantNameSearchText IS NOT NULL AND LEN(@implantNameSearchText) > 0 
		BEGIN
			UPDATE @fieldQueryPickList SET Query = Query + ' AND Name like'+CHAR(39)+'%'+@implantNameSearchText+'%'+CHAR(39)
		END

		DECLARE Cur CURSOR LOCAL FAST_FORWARD FOR 
		SELECT  Query
		FROM @fieldQueryPickList  --<-- table where sql is stored   

		
		OPEN Cur
		
		  FETCH NEXT FROM Cur INTO @Sql 
		
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			 PRINT @sql;
			 DECLARE @retvalPickList int   
			 DECLARE @ParmDefinitionPickList nvarchar(500);
			 
			 SET @ParmDefinitionPickList = N'@retvalPickList int OUTPUT';
			 EXEC sp_executesql @SQL, @ParmDefinitionPickList, @retvalPickList=@retval OUTPUT;
			 UPDATE @fieldQueryPickList SET [Name] = [Name] + ' ('+ CAST(@retval AS VARCHAR(MAX))+')' WHERE Query = @Sql
		     FETCH NEXT FROM Cur INTO @Sql 
		END
		
		CLOSE Cur
		DEALLOCATE Cur;
		
		SELECT PicklistId,[Name],[Value],[Description],[Image] FROM @fieldQueryPickList
	END
END	


