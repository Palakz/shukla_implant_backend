﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllImplant] 
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	:	DECLARE @Currentdate1 datetime =getdate(); 
					EXEC [dbo].[GetAllImplant] 
					
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetAllImplant] 
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		imp.Id,
		imp.Name,
		imp.CreatedBy,
		imp.Manufacturer
	FROM 
		Implant imp 
END