﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetImplantPropertCountByFilter]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetImplantPropertCountByFilter] 
				   @Property = 'Name',
				   @Value = 'Name1' ,
				   @ImplantTypeId = '20'
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetImplantPropertCountByFilter]
(
	@Property VARCHAR(max),
	@Value VARCHAR(max),
	@ImplantTypeId VARCHAR(MAX)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SQL nvarchar(1000)
	SET @Value = ''''+@Value+'''';
	SET @ImplantTypeId = ''''+@ImplantTypeId+'''';

	SET @SQL = 'SELECT Count( '+@Property +') AS Count FROM Implant WHERE ' + @Property +'='+ @Value + ' AND ' + 'ImplantTypeId = ' + @ImplantTypeId 
	
	EXEC (@SQL)
END	


