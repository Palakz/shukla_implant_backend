﻿
CREATE PROCEDURE [dbo].[ForgottPassword]
	@Password NVARCHAR(MAX),
	@EmailAddress NVARCHAR(250)
AS
BEGIN
	UPDATE Users SET Password = @Password WHERE EmailAddress = @EmailAddress
END
