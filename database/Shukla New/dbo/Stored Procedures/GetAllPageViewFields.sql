﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllPageViewFields]

AS
BEGIN

			Select Id,PageViewContainerId,FieldId,OnChangeJavaScriptFunc,IsDeleted,CreatedOn,CreatedBy,ModifiedOn,ModifiedBy from PageViewFields WHERE IsDeleted=0  Order by Id Desc
END
