﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[DeleteRecentlyViewedById]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[DeleteRecentlyViewedById] 42
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[DeleteRecentlyViewedById]
(
	@Id INT 
)
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM 
		RecentlyViewed 
	WHERE 
		Id = @Id
END	


