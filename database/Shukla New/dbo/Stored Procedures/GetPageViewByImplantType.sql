﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetPageViewByImplantType]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime = getdate(); 
				   EXEC [dbo].[GetPageViewByImplantType]  1,3,1,'mono',42,@Currentdate1
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetPageViewByImplantType]
(
	@ImplantTypeID INT,
	@PageViewTypeID INT,
	@ImplantId INT = NULL,
	@implantSearchText VARCHAR(MAX) = NULL,
	@UserId INT ,
	@CurrentDate DATETIME 
)
AS
BEGIN
	SET NOCOUNT ON;

	CREATE TABLE #ImplantIds (id INT) 
	CREATE TABLE #ContainerIds (id INT) 
	CREATE TABLE #PickListIds (id INT) 

	-- 1. PageViewData
	SELECT Id,Name,URL FROM	PageView WHERE ImplantType = @ImplantTypeID AND PageViewType = @PageViewTypeID AND IsDeleted = 0 AND IsActive=1 ORDER BY 1 DESC
	
	DECLARE @PageViewID INT;
	SELECT @PageViewID=ID FROM	PageView WHERE ImplantType = @ImplantTypeID AND PageViewType = @PageViewTypeID AND IsDeleted = 0 AND IsActive=1 

	-- 2. PageViewContainerData
	Select Id,Name,PageViewId,IsSystemGenerated,DisplayColumnNumber from PageViewContainer Where PageViewId=@PageViewID and IsDeleted=0
	
	INSERT INTO #ContainerIds Select Id from PageViewContainer Where PageViewId=@PageViewID and IsDeleted=0

	--3. PageviewPropertyData
	IF @PageViewTypeID != 3
	BEGIN
		Select id,FieldId,DisplayName,ContainerId,IsActive,IsVisible,IsReadOnly,CssClass,Script,FieldControlType
		FROM PageViewProperty where 
		ContainerId in (Select id from #ContainerIds) and IsDeleted=0
	END
	ELSE
	BEGIN
		EXEC sp_GetImplantPropertCountByFilter @ImplantTypeID,0, @implantSearchText
	END

	INSERT INTO #ImplantIds SELECT FieldId	FROM PageViewProperty where 
	ContainerId in (Select id from #ContainerIds) and IsDeleted=0

	--4. Implant Fields Data
	Select IFs.Id, IFs.FieldName,IFs.DisplayName,IFs.DataTypeId,DT.Name as 'DataTypeName',IFs.PickListId,IFs.DefaultValue,IFs.MaxLength,IFs.MinLength,IFs.IsRequired,IFs.Regex,IFs.RerversePropertyId,IFs.DisplayOrder,IFs.IsPartOfFilter,IFs.FilterDisplayOrder,IFs.IsMultiSelectOnCreate,IFs.IsMultiSelectOnFilter,
	IFs.IsSystemDefined from ImplantFields IFs
	left join DataTypes DT ON IFs.DataTypeId = DT.Id
	Where IFs.Id in (Select id from #ImplantIds)
	

	INSERT INTO #PickListIds Select PickListId from ImplantFields Where Id in (Select id from #ImplantIds)
	
	--5. Pick List value

	IF @PageViewTypeID != 3
	BEGIN
		SELECT picklistId,Name,Value,Description,Image FROM PickListValue WHERE pickListId IN (Select id from #PickListIds) and IsDeleted = 0
	END
	ELSE
	BEGIN	
		EXEC sp_GetImplantPropertCountByFilter @ImplantTypeID, 1, @implantSearchText
	END
	--6. Implant Detail
	IF(@ImplantId IS NOT NULL AND @ImplantId >0)
	BEGIN
		SELECT 
			(CASE WHEN fav.id IS NOT NULL AND fav.UserId = @UserId THEN 1 ELSE 0 END ) AS IsFavourite,
			imp.*  
		FROM 
			Implant imp LEFT JOIN Favorite fav ON fav.ImplantId = imp.Id
		WHERE 
			imp.id = @ImplantId
		--SELECT * FROM Implant WHERE id = @ImplantId
	END
	
	
	-- Recent view when user click on the implant from ui
	IF(@PageViewTypeID = 2 AND  @ImplantId IS NOT NULL AND @ImplantId >0)
	BEGIN

		DELETE RecentlyViewed WHERE ImplantId = @ImplantId AND CreatedBy = @UserId AND ImplantTypeId = @ImplantTypeID
		INSERT INTO RecentlyViewed 
		(
			ImplantId,
			ImplantTypeId,
			CreatedBy,
			CreatedDate
		)
		VALUES
		(
			@ImplantId,
			@ImplantTypeID,
			@UserId,
			@CurrentDate
		)
	END
END	
