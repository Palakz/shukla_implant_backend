﻿CREATE PROCEDURE [dbo].[GetAllPageViewProperty]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		Id
		,FieldId
		,DisplayName
		,ContainerId
		,IsActive
		,IsReadOnly
		,IsVisible
		,CssClass
		,Script
		,FieldControlType
	FROM
		PageViewProperty
	WHERE 
		IsDeleted = 0 
	ORDER BY 1 DESC
END	
