﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertPageViewContainer]
@Id int,
@Name Nvarchar(50),
@PageViewId int,
@CreatedOn Datetime,
@CreatedBy int,
@ModifiedBy int = null,
@ModifiedOn Datetime = null,
@ContainerIconPosition varchar(max),
@ContainerIcon varchar(max),
@DisplayColumnNumber SMALLINT

AS
BEGIN
	If(@Id IS NULL)
		BEGIN
			INSERT Into PageViewContainer 
				(Name,PageViewId,IsDeleted,CreatedOn,CreatedBy,ContainerIconPosition,ContainerIcon, DisplayColumnNumber)
			VALUES
				(@Name,@PageViewId,0,@CreatedOn,@CreatedBy,@ContainerIconPosition,@ContainerIcon, @DisplayColumnNumber)
		END
	ELSE
		BEGIN
			UPDATE PageViewContainer SET 
			Name=@Name,PageViewId=@PageViewId,IsDeleted=0,
			--CreatedOn=@CreatedOn,CreatedBy=@CreatedBy,
			ModifiedOn=@ModifiedOn,ModifiedBy=@ModifiedBy,
			ContainerIconPosition = @ContainerIconPosition,
			ContainerIcon = @ContainerIcon,
			DisplayColumnNumber = @DisplayColumnNumber
			WHERE Id=@Id
		END

END
