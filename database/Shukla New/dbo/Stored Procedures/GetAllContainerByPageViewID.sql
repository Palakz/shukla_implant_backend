﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllPickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetAllContainerByPageViewID] 12
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetAllContainerByPageViewID]
(
	@PageViewID INT 
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		Id
		,Name
		,PageViewId
		,ContainerIcon
		,ContainerIconPosition
	FROM
		PageViewContainer
	WHERE 
		PageViewId = @PageViewId
		AND IsDeleted = 0 
	ORDER BY 1 DESC
END	


