﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetUsersDetailsForDropDown]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetUsersDetailsForDropDown]
					 @OccupationId = 1
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetUsersDetailsForDropDown]
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		Id
		,UserName
		,ISADMIN
	FROM 
		Users
	WHERE
		IsActive = 1 
END	

--select * from OccupationRights

--select * from ModuleGroupRights