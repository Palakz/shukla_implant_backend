﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[DeletePageViewFields]
	@Id int,
	@ModifiedBy int,
	@ModifiedOn Date
AS
BEGIN

			UPDATE PageViewFields SET IsDeleted=1, ModifiedBy=@ModifiedBy,ModifiedOn=@ModifiedOn
			WHERE Id=@Id
END
