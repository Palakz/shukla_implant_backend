﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetImplantById] 
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	:	DECLARE @Currentdate1 datetime =getdate(); 
					EXEC [dbo].[GetImplantById] 
					@Id = 28
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetImplantById] 
(
	@Id INT
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		(CASE WHEN fav.id IS NULL THEN 0 ELSE 1 END ) AS IsFavourite,
		imp.*  
	FROM 
		Implant imp LEFT JOIN Favorite fav ON fav.ImplantId = imp.Id
	WHERE
		imp.Id = @Id AND IsApproved =1 
END