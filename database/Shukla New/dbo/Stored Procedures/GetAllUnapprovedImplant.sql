﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllUnapprovedImplant]
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	:	DECLARE @Currentdate1 datetime =getdate(); 
					EXEC [dbo].[GetAllUnapprovedImplant] 
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetAllUnapprovedImplant] 
AS
BEGIN
	SET NOCOUNT ON;	SELECT 
		* 
	FROM 
		Implant
	WHERE 
		IsApproved = 0 AND (IsRejected is NULL OR IsRejected  = 0)
END