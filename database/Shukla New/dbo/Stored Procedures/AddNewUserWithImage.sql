﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
alter PROCEDURE [dbo].[AddNewUserWithImage]  
	@FirstName varchar(100),
	@LastName varchar(100),
	@UserName varchar(50),
	@Password nvarchar(max),
	@EmailAddress varchar(250),
	@MobileNumber varchar(12),
	@OccupationId INT,
	@OrganisationId INT,
	@ProfileImage VARCHAR(MAX)
AS
BEGIN
	
INSERT INTO Users
(
FirstName,
LastName,
UserName,
Password,
EmailAddress,
MobileNumber,
OccupationId,
OrganisationId,
IsActive,
IsDeleted,
IsEmailVerified,
EmailVerificationToken,
CreatedOn,
CreatedBy,
ModifiedOn,
ModifiedBy,
IsAdmin,
ProfileImage
)
VALUES (
@FirstName,
@LastName,
@UserName,
@Password,
@EmailAddress,
@MobileNumber,
@OccupationId,
@OrganisationId,
0,
0,
0,
null,
GetDate(),
1,
null,
null,
0,
@ProfileImage
)

END
