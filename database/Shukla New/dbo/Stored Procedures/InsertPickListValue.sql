﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertPickListValue]
	@PickListId INT,
	@IsDefault	BIT,
	@DisplayOrder INT,
	@Name VARCHAR(MAX),
	@Value VARCHAR(MAX),
	@Note VARCHAR(MAX),
	@CreatedOn DateTime,
	@CreatedBy INT

AS
BEGIN
	INSERT INTO PickListValue(PickListId,IsDefault,DisplayOrder,Name,Value,Note,CreatedOn,CreatedBy,IsDeleted)
	VALUES(@PickListId,@IsDefault,@DisplayOrder,@Name,@Value,@Note,@CreatedOn,@CreatedBy,0)
END
