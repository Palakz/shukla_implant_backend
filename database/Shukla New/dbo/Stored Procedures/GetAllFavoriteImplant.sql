﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[GetAllFavoriteImplant]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[GetAllFavoriteImplant] 102,1
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[GetAllFavoriteImplant]
(
	@UserId INT ,
	@ImplantTypeId INT
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		f.Id AS FavouriteId,
		f.ImplantId,
		f.CreatedOn,
		imp.*
	FROM
		Favorite f
	INNER JOIN Implant imp ON imp.id =f.ImplantId
	WHERE 
		f.UserId = @UserId AND imp.ImplantTypeId = @ImplantTypeId
	ORDER BY 1 DESC
END	


