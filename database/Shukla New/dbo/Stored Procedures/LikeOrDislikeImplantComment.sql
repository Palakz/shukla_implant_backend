﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[AddOrEditPickList]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[LikeOrDislikeImplantComment]
					 @CommentId = 3,
					 @UserId = 80,
					 @IsLiked  = 0
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[LikeOrDislikeImplantComment]
(
	@CommentId INT,
	@UserId INT,
	@IsLiked BIT
)
AS
BEGIN
	SET NOCOUNT ON;

	IF Exists(SELECT 1 FROM CommentLikeDisLike WHERE CommentId = @CommentId AND UserId = @UserId)
	BEGIN
		UPDATE 
			CommentLikeDisLike
		SET 
			Liked = CASE WHEN @IsLiked = 1 THEN 1 ELSE NULL END,
			DisLiked = CASE WHEN @IsLiked = 0 THEN 1 ELSE NULL END
		WHERE
			CommentId = @CommentId AND UserId = @UserId
	END
	ELSE
	BEGIN
		INSERT INTO CommentLikeDisLike
			(
				CommentId,
				UserId,
				Liked,
				DisLiked
			)
			VALUES
			(
				@CommentId,
				@UserId,
				CASE WHEN @IsLiked = 1 THEN 1 ELSE NULL END,
				CASE WHEN @IsLiked = 0 THEN 1 ELSE NULL END
			)
		END
END	




select * from ImplantComment

select * from CommentLikeDisLike