﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllPageViewByImplantType]
@ImplantType int

AS
BEGIN
	if(@ImplantType = 0)
		BEGIN
			SELECT Id,Name,PageViewType,ImplantType,URL,IsDefault,IsActive FROM PageView WHERE  IsDeleted=0  Order by Id Desc
		END
	ELSE
		BEGIN
			SELECT Id,Name,PageViewType,ImplantType,URL,IsDefault,IsActive FROM PageView WHERE ImplantType = @ImplantType 
			and IsDeleted=0  Order by Id Desc
		END
	
END
