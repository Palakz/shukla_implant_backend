﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[AddOrEditImplantComment]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[AddOrEditImplantComment]
					@Id = NULL,
					 @ImplantId = 6,
					 @Comment = 'comment',
					 @UserId = 80,
					 @CurrentDate = @Currentdate1
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[AddOrEditImplantComment]
(
	@Id INT,
	@ImplantId INT,
	@Comment VARCHAR(MAX),
	@UserId INT,
	@CurrentDate DATETIME
)
AS
BEGIN
	SET NOCOUNT ON;

	IF (@Id IS NULL)
		BEGIN
		INSERT INTO ImplantComment
		(
			ImplantId,
			Comment,
			CreatedBy,
			CreatedOn
		)
		VALUES
		(
			@ImplantId,
			@Comment,
			@UserId,
			@CurrentDate
		)
	END
	ELSE
	BEGIN
		UPDATE 
			ImplantComment
		SET 
			ImplantId = @ImplantId,
			Comment= @Comment
		WHERE
			Id = @Id
	END
END	
