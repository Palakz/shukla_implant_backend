﻿CREATE PROCEDURE [dbo].[InsertPageViewProperty]
@Id int,
@FieldId int,
@DisplayName Nvarchar(50),
@ContainerId int,
@IsActive bit,
@IsReadOnly bit,
@IsVisible bit,
@CssClass nvarchar(50),
@Script nvarchar(500),
@FieldControlType nvarchar(500),
@CreatedOn Datetime,
@CreatedBy int

AS
BEGIN
	If(@Id is null)
		BEGIN
			INSERT Into PageViewProperty 
				(FieldId,DisplayName,ContainerId,IsActive,IsReadOnly,IsVisible,CssClass,Script,CreatedOn,CreatedBy,FieldControlType)
			VALUES
				(@FieldId,@DisplayName,@ContainerId,@IsActive,@IsReadOnly,@IsVisible,@CssClass,@Script,@CreatedOn,@CreatedBy, @FieldControlType)
		END
	ELSE
		BEGIN
			UPDATE PageViewProperty SET 
			FieldId=@FieldId, DisplayName=@DisplayName, ContainerId=@ContainerId,IsActive=@IsActive,IsReadOnly=@IsReadOnly,IsVisible=@IsVisible,CssClass=@CssClass,
			Script=@Script,
			FieldControlType = @FieldControlType,
			ModifiedOn=@CreatedOn,ModifiedBy=@CreatedBy
			WHERE Id=@Id
		END
END
