﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[DeleteImageByEntity]
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	:  DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[DeleteImageByEntity]
					@EntityId = '1',
					@EntityName = 'implant',
					@ImagePath = 'Images\Images_4cd28526-d686-4690-8412-df27d71f9b29_sswhite India.jpg',
					@EntityColumnName = 'images'

					DECLARE @Currentdate1 datetime =getdate(); 
				   EXEC [dbo].[DeleteImageByEntity]
					@EntityId = '1',
					@EntityName = 'PickListValue',
					@ImagePath = ' PickListValueImage/PickListValueImage_a0de0380-49a4-43ba-94b4-3030992a6bc6_logo-removebg-preview - Copy.png',
					@EntityColumnName = 'image'
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [dbo].[DeleteImageByEntity]
(
	@EntityId VARCHAR(255),
	@EntityName VARCHAR(255),
	@ImagePath VARCHAR(255),
	@EntityColumnName VARCHAR(255)
)
AS
BEGIN
	SET NOCOUNT ON;

--	declare @ImagePath varchar(max) = 'Images\Images_4cd28526-d686-4690-8412-df27d71f9b29_sswhite India.jpg'
--,@EntityColumnName varchar(max)= 'images';
	DECLARE @SQL nvarchar(max);
--	declare @Index varchar(10), @EntityId int = 1;

--	SET @SQL ='Select CHARINDEX('+CHAR(39)++@ImagePath+','+CHAR(39)+','+@EntityColumnName+') from implant where id ='+@EntityId+ ';'
--	print @SQL;
--	EXEc(@SQL)
--	print @Index;

	SET @SQL = 'IF((Select CHARINDEX('+CHAR(39)+@ImagePath+','+CHAR(39)+',' +@EntityColumnName+') from '+@EntityName+' where id = '+@EntityId+') > 0)
					BEGIN
						UPDATE '+@EntityName+ ' SET '+@EntityColumnName+' = REPLACE('+@EntityColumnName+',' +CHAR(39)+@ImagePath+','+CHAR(39)+','+CHAR(39)++CHAR(39)+') WHERE id = '+@EntityId+
				' END
				ELSE IF((Select CHARINDEX('+CHAR(39)+','+@ImagePath+CHAR(39)+',' +@EntityColumnName+') from '+@EntityName+' where id = '+@EntityId+') > 0)
					BEGIN
						UPDATE '+@EntityName+ ' SET '+@EntityColumnName+' = REPLACE('+@EntityColumnName+',' +CHAR(39)+','+@ImagePath+CHAR(39)+','+CHAR(39)++CHAR(39)+') WHERE id = '+@EntityId+
				' END
				ELSE IF((Select CHARINDEX('+CHAR(39)+@ImagePath+CHAR(39)+',' +@EntityColumnName+') from '+@EntityName+' where id = '+@EntityId+') > 0)
					BEGIN
						UPDATE '+@EntityName+ ' SET '+@EntityColumnName+' = REPLACE('+@EntityColumnName+',' +CHAR(39)+@ImagePath+CHAR(39)+','+CHAR(39)++CHAR(39)+') WHERE id = '+@EntityId+
				' END';
					print @SQL
	EXEc(@SQL)


	--IF(LOWER(@EntityName) = 'implant')
	--BEGIN
	--	IF((Select CHARINDEX(@ImagePath+',', Images) from implant where id = 1) > 0)
	--	BEGIN
	--	print 'a'
	--		UPDATE Implant SET Images = REPLACE(Images, @ImagePath+',','') WHERE id = 1
	--	END
	--	ELSE IF((Select CHARINDEX(','+@ImagePath, Images) from implant where id = 1) > 0)
	--	BEGIN
	--	print 'b'
	--		UPDATE Implant SET Images = REPLACE(Images, ','+@ImagePath,'') where id = 1
	--	END
	--	ELSE IF((Select CHARINDEX(@ImagePath, Images) from implant where id = 1) > 0)
	--	BEGIN
	--	print 'c'
	--		UPDATE Implant SET Images = REPLACE(Images, @ImagePath,'') where id = 1
	--	END
	--END
END	
