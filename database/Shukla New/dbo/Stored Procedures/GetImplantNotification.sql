﻿/*------------------------------------------------------------------------------------------------------------
Name			: [dbo].[RejectImplant] 
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	:	DECLARE @Currentdate1 datetime =getdate(); 
					EXEC [dbo].[GetImplantNotification] 
					@UserId = 80
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [dbo].[GetImplantNotification] 
(
	@UserId INT
)
AS
BEGIN
	SET NOCOUNT ON;	SELECT
		Id,
		ImplantId,
		ImplantName,
		IsApprovedOrRejected
	FROM
		 dbo.ImplantNotification 
	WHERE 
		ImplantUserId = @userId AND [IsRead] = 0
	ORDER BY 1 DESC
END