﻿CREATE TABLE [document].[PurchaseDocumentHeaders] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [PurchaseDocumentId] INT             NOT NULL,
    [HeaderId]           INT             NOT NULL,
    [CalculationType]    TINYINT         NOT NULL,
    [Percentage]         DECIMAL (18, 2) NOT NULL,
    [Value]              DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK__Purchase__3214EC07F525C2C4] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__PurchaseD__Heade__4EFDAD20] FOREIGN KEY ([HeaderId]) REFERENCES [master].[Headers] ([Id]),
    CONSTRAINT [FK__PurchaseD__Purch__4E0988E7] FOREIGN KEY ([PurchaseDocumentId]) REFERENCES [document].[PurchaseDocuments] ([Id])
);

