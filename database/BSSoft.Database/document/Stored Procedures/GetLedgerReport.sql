﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetLedgerReport
Comments		: 12-05-2021 | Amit Khanna | This procedure is used to get data for ledger report.

Test Execution	: EXEC [document].[GetLedgerReport]
						@SubscriberId =  1,
						@FinancialYear = 202122,
						@CompanyId = 4,
						@ClientId = 7,
						@FromDate = '2021-04-01',
						@ToDate  = '2021-05-31',
						@TransactionTypeCredit = 1,
						@TransactionTypeDebit =  2,
						@IpAddress =  1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[GetLedgerReport]
(
	@SubscriberId INT,
	@FinancialYear INT,
	@CompanyId INT,
	@ClientId INT,
	@FromDate DATE,
	@ToDate DATE,
	@TransactionTypeCredit TINYINT,
	@TransactionTypeDebit TINYINT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[GetLedgerReport]','
										@SubscriberId = ',@SubscriberId,',
										@FinancialYear = ',@FinancialYear,',
										@CompanyId = ',@CompanyId,',
										@ClientId = ',@ClientId,',
										@FromDate = ',@FromDate,',
										@ToDate = ',@ToDate,',
										@TransactionTypeCredit = ',@TransactionTypeCredit,',
										@TransactionTypeDebit = ',@TransactionTypeDebit,',
										@ToDate = ',@ToDate,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[document].[GetLedgerReport]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY
		DECLARE @OpeningBalance DECIMAL(18,2), 
				@TransactionType TINYINT,
				@PurchaseDocumentTotal DECIMAL(18,2),
				@SalesDocumentTotal DECIMAL(18,2),
				@PaymentTotal DECIMAL(18,2),
				@ReceiptTotal DECIMAL(18,2),
				@JVDebitedTotal DECIMAL(18,2),
				@JVCreditedTotal DECIMAL(18,2);

		CREATE TABLE #TempLedgerDetails
		(
			Id INT IDENTITY(1,1),
			[Date] DATE NOT NULL,
			Particulars VARCHAR(MAX),
			[Type] VARCHAR(50),
			ReferenceNumber VARCHAR(MAX),
			Debit DECIMAL(18,2),
			Credit DECIMAL(18,2),
			TransactionType TINYINT
		);

		SELECT
			@OpeningBalance = ISNULL(CASE WHEN ctb.[Type] = @TransactionTypeCredit THEN ctb.Balance ELSE -ctb.Balance END,0)
		FROM
			[master].ClientOpeningBalance AS ctb
		WHERE
			SubscriberId = @SubscriberId
			AND ClientId = @ClientId
			AND FinancialYear = @FinancialYear;

		SELECT
			@PurchaseDocumentTotal =  SUM(ISNULL(pd.TotalAmount,0))
		FROM
			document.PurchaseDocuments AS pd
		WHERE
			pd.SubscriberId = @SubscriberId
			AND pd.ClientId = @ClientId
			AND pd.CompanyId = @CompanyId
			AND pd.[Date] < @FromDate;

		SELECT
			@SalesDocumentTotal =  SUM(ISNULL(pd.TotalAmount,0))
		FROM
			document.SalesDocuments AS pd
		WHERE
			pd.SubscriberId = @SubscriberId
			AND pd.ClientId = @ClientId
			AND pd.CompanyId = @CompanyId
			AND pd.[Date] < @FromDate;

		SELECT
			@PaymentTotal = SUM(ISNULL(pt.PaidAmount,0))
		FROM
			document.Payments pt
		WHERE
			pt.SubscriberId = @SubscriberId
			AND pt.ClientId = @ClientId
			AND pt.CompanyId = @CompanyId
			AND pt.[Date] < @FromDate;

		
		SELECT
			@ReceiptTotal = SUM(ISNULL(pt.ReceivedAmount,0))
		FROM
			document.Receipts pt
		WHERE
			pt.SubscriberId = @SubscriberId
			AND pt.ClientId = @ClientId
			AND pt.CompanyId = @CompanyId
			AND pt.[Date] < @FromDate;
		
		SELECT
			@JVCreditedTotal = SUM(CASE WHEN pt.CreditedClientId = @ClientId THEN ISNULL(pt.TotalAmount,0) ELSE 0 END),
			@JVDebitedTotal = SUM(CASE WHEN pt.DebitedClientId = @ClientId THEN ISNULL(pt.TotalAmount,0) ELSE 0 END)
		FROM
			document.JournalVouchers pt
		WHERE
			pt.SubscriberId = @SubscriberId
			AND 
			(
				pt.CreditedClientId = @ClientId
				OR pt.DebitedClientId = @ClientId
			)
			AND pt.CompanyId = @CompanyId
			AND pt.[Date] < @FromDate;

		SET @OpeningBalance =	ISNULL(@OpeningBalance,0)
								+ ISNULL(@PurchaseDocumentTotal,0)
								- ISNULL(@PaymentTotal,0) 
								- ISNULL(@SalesDocumentTotal,0)
								+ ISNULL(@ReceiptTotal,0) 
								+ ISNULL(@JVCreditedTotal,0) 
								- ISNULL(@JVDebitedTotal,0);

		INSERT INTO #TempLedgerDetails
		(
			[Date],
			Particulars,
			[Type],
			ReferenceNumber,
			Debit,
			Credit,
			TransactionType
		)
		VALUES
		(
			@FromDate,
			NULL,
			'Opening Balance',
			NULL,
			CASE WHEN @OpeningBalance > 0 THEN NULL ELSE @OpeningBalance END,
			CASE WHEN @OpeningBalance > 0 THEN @OpeningBalance ELSE NULL END,
			CASE WHEN @OpeningBalance > 0 THEN @TransactionTypeCredit ELSE @TransactionTypeDebit END
		)

		INSERT INTO #TempLedgerDetails
		(
			[Date],
			Particulars,
			[Type],
			ReferenceNumber,
			Credit,
			TransactionType
		)
		SELECT
			pd.[Date],
			'PURCHASE',
			'Purchase',
			pd.VoucherNumber,
			ISNULL(pd.TotalAmount,0),
			@TransactionTypeCredit
		FROM
			document.PurchaseDocuments AS pd
		WHERE
			pd.SubscriberId = @SubscriberId
			AND pd.CompanyId = @CompanyId
			AND pd.ClientId = @ClientId
			AND pd.[Date] BETWEEN @FromDate AND @ToDate;

		INSERT INTO #TempLedgerDetails
		(
			[Date],
			Particulars,
			[Type],
			ReferenceNumber,
			Debit,
			TransactionType
		)
		SELECT
			pd.[Date],
			'SALES',
			'Sales',
			pd.VoucherNumber,
			ISNULL(pd.TotalAmount,0),
			@TransactionTypeDebit
		FROM
			document.SalesDocuments AS pd
		WHERE
			pd.SubscriberId = @SubscriberId
			AND pd.CompanyId = @CompanyId
			AND pd.ClientId = @ClientId
			AND pd.[Date] BETWEEN @FromDate AND @ToDate;

		INSERT INTO #TempLedgerDetails
		(
			[Date],
			Particulars,
			[Type],
			ReferenceNumber,
			Debit,
			TransactionType
		)
		SELECT
			pd.[Date],
			CASE WHEN pd.BankId IS NULL THEN 'CASH ACCOUNT' ELSE ct.[CompanyName] END,
			'Quick Payment',
			pd.PaymentNumber,
			pd.TotalAmount,
			@TransactionTypeDebit
		FROM
			document.Payments AS pd
			LEFT JOIN [master].Clients AS ct ON pd.BankId = ct.Id
		WHERE
			pd.SubscriberId = @SubscriberId
			AND pd.CompanyId = @CompanyId
			AND pd.ClientId = @ClientId
			AND pd.[Date] BETWEEN @FromDate AND @ToDate;

		INSERT INTO #TempLedgerDetails
		(
			[Date],
			Particulars,
			[Type],
			ReferenceNumber,
			Debit,
			TransactionType
		)
		SELECT
			pd.[Date],
			CASE WHEN pd.BankId IS NULL THEN 'CASH ACCOUNT' ELSE ct.[CompanyName] END,
			'Quick Receipt',
			pd.ReceiptNumber,
			pd.TotalAmount,
			@TransactionTypeCredit
		FROM
			document.Receipts AS pd
			LEFT JOIN [master].Clients AS ct ON pd.BankId = ct.Id
		WHERE
			pd.SubscriberId = @SubscriberId
			AND pd.CompanyId = @CompanyId
			AND pd.ClientId = @ClientId
			AND pd.[Date] BETWEEN @FromDate AND @ToDate;

		INSERT INTO #TempLedgerDetails
		(
			[Date],
			Particulars,
			[Type],
			ReferenceNumber,
			Credit,
			TransactionType
		)
		SELECT
			pd.[Date],
			ct.[CompanyName],
			'Journal Voucher',
			pd.VoucherNumber,
			pd.TotalAmount,
			@TransactionTypeCredit 
		FROM
			document.JournalVouchers AS pd
			INNER JOIN [master].Clients AS ct ON pd.DebitedClientId = ct.Id
		WHERE
			pd.SubscriberId = @SubscriberId
			AND pd.CompanyId = @CompanyId
			AND pd.CreditedClientId = @ClientId
			AND pd.[Date] BETWEEN @FromDate AND @ToDate;

		INSERT INTO #TempLedgerDetails
		(
			[Date],
			Particulars,
			[Type],
			ReferenceNumber,
			Debit,
			TransactionType
		)
		SELECT
			pd.[Date],
			ct.CompanyName,
			'Journal Voucher',
			pd.VoucherNumber,
			pd.TotalAmount,
			@TransactionTypeDebit
		FROM
			document.JournalVouchers AS pd
			INNER JOIN [master].Clients AS ct ON pd.CreditedClientId = ct.Id
		WHERE
			pd.SubscriberId = @SubscriberId
			AND pd.CompanyId = @CompanyId
			AND pd.DebitedClientId = @ClientId
			AND pd.[Date] BETWEEN @FromDate AND @ToDate;

		SELECT
			ldt.[Date],
			ldt.Particulars,
			ldt.[Type],
			ldt.ReferenceNumber,
			ABS(ldt.Debit) AS Debit,
			ABS(ldt.Credit) AS Credit,
			ldt.TransactionType
		FROM
			#TempLedgerDetails AS ldt
		ORDER BY 
			ldt.[Date] ASC,
			ldt.Id ASC;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
