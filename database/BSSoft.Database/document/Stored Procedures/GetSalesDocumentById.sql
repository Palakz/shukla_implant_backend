﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetSalesDocumentById
Comments		: 20-04-2021 | Amit Khanna | This procedure is used to get sales document by Id.

Test Execution	: EXEC [document].GetSalesDocumentById
					@SubscriberId =  1,
					@Id =  10,
					@IpAddress =  NULL;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[GetSalesDocumentById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].GetSalesDocumentById','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[document].GetSalesDocumentById',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			pd.Id,
			pd.CompanyId,
			pd.ClientId,
			ct.[CompanyName] AS ClientName,
			pd.VoucherNumber,
			pd.[Date],
			pd.VoucherType,
			pd.[Month],
			pd.FinancialYear,
			pd.ReceivedBy,
			pd.AdvanceAmount,
			pd.AdvanceReceivedBy,
			pd.TotalAmount,
			pd.Remarks
		FROM
			[document].SalesDocuments AS pd
			INNER JOIN [master].Clients AS ct ON pd.ClientId = ct.Id
		WHERE
			pd.Id = @Id;

		SELECT
			pdi.ItemId,
			it.[Name],
			pdi.UnitId,
			pdi.[Description],
			pdi.Quantity,
			pdi.[Weight],
			pdi.FreeQuantity,
			pdi.Rate,
			pdi.Amount,
			pdi.LotNumber
		FROM
			[document].SalesDocumentItems AS pdi
			INNER JOIN [master].Items AS it ON pdi.Itemid = it.Id
		WHERE
			pdi.SalesDocumentId = @Id;

		SELECT
			phdr.HeaderId,
			hdr.[Name] AS [Name],
			phdr.CalculationType,
			phdr.[Percentage],
			phdr.[Value]
		FROM
			[document].SalesDocumentHeaders phdr
			INNER JOIN [master].Headers AS hdr ON phdr.HeaderId = hdr.Id
		WHERE
			SalesDocumentId = @Id;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
