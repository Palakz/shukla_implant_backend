﻿/*------------------------------------------------------------------------------------------------------------
Name			: DeletePayment
Comments		: 28-04-2021 | Amit Khanna | This procedure is used to delete payment by Id.

Test Execution	: EXEC [document].[DeletePayment]
						@SubscriberId =  1,
						@Id =  5,
						@IpAddress =  NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[DeletePayment]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[DeletePayment]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[document].[DeletePayment]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY
		BEGIN TRAN
		
		DELETE FROM
			document.PaymentHeaders
		WHERE
			PaymentId = @Id;

		DELETE FROM 
			document.Payments
		WHERE
			Id = @Id;

		COMMIT TRAN;
		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
