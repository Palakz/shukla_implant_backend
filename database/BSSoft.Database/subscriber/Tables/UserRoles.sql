﻿CREATE TABLE [subscriber].[UserRoles] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [UserId]      INT           NOT NULL,
    [RoleId]      INT           NOT NULL,
    [CreatedBy]   INT           NOT NULL,
    [CreatedDate] SMALLDATETIME NOT NULL,
    [IpAddress]   VARCHAR (45)  NULL,
    [UpdatedBy]   INT           NULL,
    [UpdatedDate] SMALLDATETIME NULL,
    CONSTRAINT [PK__UserRole__3214EC07B59C34A4] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__UserRoles__Creat__70C8B53F] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__UserRoles__RoleI__6FD49106] FOREIGN KEY ([RoleId]) REFERENCES [subscriber].[Roles] ([Id]),
    CONSTRAINT [FK__UserRoles__Updat__71BCD978] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__UserRoles__UserI__6EE06CCD] FOREIGN KEY ([UserId]) REFERENCES [subscriber].[Users] ([Id])
);

