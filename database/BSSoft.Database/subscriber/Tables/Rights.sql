﻿CREATE TABLE [subscriber].[Rights] (
    [Id]            INT           NOT NULL,
    [RightsGroupId] INT           NOT NULL,
    [DisplayOrder]  SMALLINT      NOT NULL,
    [Name]          VARCHAR (100) NOT NULL,
    [Acronym]       VARCHAR (50)  NOT NULL,
    [Status]        TINYINT       NOT NULL,
    [EndDate]       SMALLDATETIME NULL,
    [CreatedDate]   SMALLDATETIME NOT NULL,
    [UpdatedDate]   SMALLDATETIME NULL,
    CONSTRAINT [PK__Rights__3214EC0773559C4B] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__Rights__RightsGr__74994623] FOREIGN KEY ([RightsGroupId]) REFERENCES [subscriber].[RightsGroup] ([Id])
);

