﻿CREATE TABLE [subscriber].[RoleRights] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [RoleId]      INT           NOT NULL,
    [RightId]     INT           NOT NULL,
    [CreatedDate] SMALLDATETIME NOT NULL,
    CONSTRAINT [PK__RoleRigh__3214EC0756E070D1] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__RoleRight__Right__7869D707] FOREIGN KEY ([RightId]) REFERENCES [subscriber].[Rights] ([Id]),
    CONSTRAINT [FK__RoleRight__RoleI__7775B2CE] FOREIGN KEY ([RoleId]) REFERENCES [subscriber].[Roles] ([Id])
);

