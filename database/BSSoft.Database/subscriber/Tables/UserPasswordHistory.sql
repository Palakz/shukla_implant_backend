﻿CREATE TABLE [subscriber].[UserPasswordHistory] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [UserId]      INT           NOT NULL,
    [Password]    VARCHAR (MAX) NOT NULL,
    [CreatedDate] SMALLDATETIME NOT NULL,
    CONSTRAINT [PK__UserPass__3214EC078647ECE2] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__UserPassw__UserI__0D7A0286] FOREIGN KEY ([UserId]) REFERENCES [subscriber].[Users] ([Id])
);

