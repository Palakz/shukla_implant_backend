﻿/*------------------------------------------------------------------------------------------------------------
Name			: DeleteRole
Comments		: 31-03-2021 | Amit Khanna | This procedure is used to delete role by Id. Status of role is set to 3 and enddate is set.

Test Execution	: EXEC subscriber.DeleteRole
					@SubscriberId =  1,
					@Id =  5,
					@IpAddress =  1,
					@StatusTypeDeleted = 3;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[DeleteRole]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45),
	@UpdatedBy INT,
	@UpdatedDate SMALLDATETIME,
	@StatusTypeDeleted TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[DeleteRole]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ''',@IpAddress,'','
										@UpdatedBy = ',@StatusTypeDeleted,',
										@UpdatedDate = ',@StatusTypeDeleted,',
										@StatusTypeDelete = ',@StatusTypeDeleted
									  ),
			@ProcedureName = '[subscriber].[DeleteRole]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		UPDATE
			subscriber.Roles
		SET
			[status]  = @StatusTypeDeleted,
			EndDate = GETDATE(),
			UpdatedBy = @UpdatedBy,
			UpdatedDate = @UpdatedDate
		WHERE
			Id = @Id
			AND SubscriberId = @SubscriberId;

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
