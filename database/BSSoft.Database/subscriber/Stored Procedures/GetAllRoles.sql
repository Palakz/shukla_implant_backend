﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetAllRoles
Comments		: 31-03-2021 | Amit Khanna | This procedure is used to get All roles.

Test Execution	: EXEC subscriber.GetAllRoles
					@SubscriberId = 1,
					@SearchKeyword = NULL,
					@Status = NULL,
					@SortExpression = NULL,
					@Start =  0,
					@Length = 100,
					@IpAddress = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[GetAllRoles]
(
	@SubscriberId INT,
	@SearchKeyword VARCHAR(100),
	@Status TINYINT,
	@SortExpression VARCHAR(50),
	@Start INT,
	@Length INT,
	@IpAddress VARCHAR(45),
	@TotalRecords INT OUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[GetAllRoles]','
										@SubscriberId =',@SubscriberId,',
										@SearchKeyword = ',''',@SearchKeyword,''','
										@Status = ',@Status,',
										@SortExpression = ',''',@SortExpression,''','
										@Start = ',@Start,',
										@Length = ',@Length,',
										@IpAddress = ',''',@IpAddress,''','
										@TotalRecords = ',@TotalRecords
									  ),
			@ProcedureName = '[subscriber].[GetAllRoles]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
		CREATE TABLE #TempTable(Id INT); 
		
		INSERT INTO #TempTable(Id)
		SELECT  
			us.Id
		FROM 
			[subscriber].[Roles] AS us
		WHERE 
			us.SubscriberId = @SubscriberId
			AND us.[Status] = ISNULL(@Status,us.[Status])
			AND us.EndDate IS NULL
			AND 
			(
					us.[Name] LIKE '%' + ISNULL(@SearchKeyword,us.[Name]) +'%'
			);

		SELECT @TotalRecords =COUNT(Id)  FROM #TempTable 

		SELECT
			us.Id,
			us.[Name],
			us.[Description],
			us.[Status]
		FROM 
			#TempTable AS tmp
			INNER JOIN subscriber.Roles us ON tmp.Id = us.Id
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN us.[Name] END ASC,
			CASE WHEN @SortExpression = 'name asc' THEN us.[Name] END ASC,
			CASE WHEN @SortExpression = 'name desc' THEN us.[Name] END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
