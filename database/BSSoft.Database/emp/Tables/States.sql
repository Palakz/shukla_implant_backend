﻿CREATE TABLE [app].[States] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [CountryId]   INT           NOT NULL,
    [Name]        VARCHAR (50)  NOT NULL,
    [Status]      TINYINT       CONSTRAINT [DF__States__Status__1E6F845E] DEFAULT ((1)) NOT NULL,
    [EndDate]     SMALLDATETIME NULL,
    [CreatedBy]   INT           NULL,
    [CreatedDate] SMALLDATETIME CONSTRAINT [DF__States__CreatedD__1F63A897] DEFAULT (getdate()) NOT NULL,
    [UpdatedBy]   INT           NULL,
    [UpdatedDate] SMALLDATETIME NULL,
    CONSTRAINT [PK__States__3214EC0705FD40DC] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__States__CountryI__1D7B6025] FOREIGN KEY ([CountryId]) REFERENCES [app].[Countries] ([Id])
);

