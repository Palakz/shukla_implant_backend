﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateOpeningBalance
Comments		: 13-05-2021 | Amit Khanna | This procedure is used to insert user or update client opening balance by clientId.

Test Execution	: EXEC master.InsertOrUpdateOpeningBalance
						@SubscriberId = 1,
						@FinancialYear = 202122,
						@ClientId = 1,
						@Balance = 10000,
						@TransactionType = 1,
						@IpAddress = '',
						@CreatedBy = 1,
						@CreatedDate = '2021-02-28',
						@StatusTypeActive = 1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateOpeningBalance]
(
	@SubscriberId INT,
	@FinancialYear INT,
	@ClientId INT,
	@Balance DECIMAL(18,2),
	@TransactionType TINYINT,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateOpeningBalance]','
										@SubscriberId =',@SubscriberId,',
										@FinancialYear =',@FinancialYear,',
										@ClientId =',@ClientId,',
										@Balance = ',@Balance,'
										@TransactionType = ',@TransactionType,'
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,'
										@CreatedDate = ',@CreatedDate
									  ),
			@ProcedureName = '[master].[InsertOrUpdateOpeningBalance]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1;
    BEGIN TRY
		IF EXISTS(SELECT Id FROM [master].ClientOpeningBalance WHERE FinancialYear = @FinancialYear AND ClientId = @ClientId)
		BEGIN
			UPDATE
				[master].ClientOpeningBalance
			SET
				Balance = @Balance,
				[Type] = @TransactionType,
				IpAddress = @IpAddress,
				UpdatedBy = @CreatedBy,
				UpdatedDate = @CreatedDate
			WHERE
				ClientId = @ClientId
				AND SubscriberId = @SubscriberId
				AND FinancialYear = @FinancialYear;
		END
		ELSE
		BEGIN
			INSERT INTO [master].ClientOpeningBalance
			(
				SubscriberId,
				ClientId,
				Balance,
				[Type],
				FinancialYear,
				IpAddress,
				CreatedBy,
				CreatedDate
			)
			VALUES
			(
				@SubscriberId,
				@ClientId,
				@Balance,
				@TransactionType,
				@FinancialYear,
				@IpAddress,
				@CreatedBy,
				@CreatedDate
			);
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	RETURN @ReturnValue;
	
