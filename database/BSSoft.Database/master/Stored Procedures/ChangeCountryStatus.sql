﻿/*------------------------------------------------------------------------------------------------------------
Name			: ChangeCountryStatus
Comments		: 26-03-2021 | Vikas Patel | This procedure is used to change countries status.

Test Execution	: EXEC master.ChangeCountryStatus
					@SubscriberId = 1,
					@Id =  1,
					@UpdatedBy = 1,
					@UpdatedDate = '2021-03-03',
					@IpAddress = NULL;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[ChangeCountryStatus]
(
	@SubscriberId INT,
	@Id INT,
	@UpdatedBy INT,
	@UpdatedDate SMALLDATETIME,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[ChangeCountryStatus]','
										@SubscriberId =',@SubscriberId,',
										@Id = ',@Id,',
										@UpdateBy = ',@UpdatedBy,',
										@UpdateDate = ',@UpdatedDate,',
										@IpAddress = ','',@IpAddress
									  ),
			@ProcedureName = '[master].[ChangeCountryStatus]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY
		UPDATE
			[app].Countries
		SET
			[Status] = CASE WHEN [Status] = 1 THEN 0 ELSE 1 END,
			UpdatedBy = @UpdatedBy,
			UpdatedDate = @UpdatedDate
		WHERE
			Id = @Id;
			
		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

