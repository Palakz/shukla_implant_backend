﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateItem
Comments		: 29-03-2021 | Amit Khanna | This procedure is used to insert item or update item by Id.

Test Execution	: EXEC master.InsertOrUpdateItem
						@SubscriberId = 1,
						@Id = NULL,
						@Name = 'demo',
						@Code = '1',
						@HsnOrSac = 'SJSds',
						@ItemGroupId = 1,
						@ItemSubGroupId = 1,
						@ItemCategoryId = 1,
						@ItemSubCategoryId = 1,
						@IpAddress = '',
						@CreatedBy = 1,
						@CreatedDate = '2021-02-28',
						@StatusTypeActive = 1,
						@ModuleTypeItem =1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateItem]
(
	@SubscriberId INT,
	@Id INT,
	@Name VARCHAR(100),
	@Code VARCHAR(MAX),
	@HsnOrSac VARCHAR(8),
	@ItemGroupId INT,
	@ItemSubGroupId INT,
	@ItemCategoryId INT,
	@ItemSubCategoryId INT,
	@Remarks VARCHAR(50),
	@Prefix VARCHAR(MAX),
	@Suffix VARCHAR(MAX),
	@ItemUnits [master].ItemUnitType READONLY,
	@ItemCrates [master].ItemCrateType READONLY,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT,
	@ModuleTypeItem TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateItem]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@Name =''',@Name,''',
										@Code =''',@Code,''',
										@HsnOrSac =''',@HsnOrSac,''',
										@ItemGroupId =',@ItemGroupId,',
										@ItemSubGroupId =',@ItemSubGroupId,',
										@ItemCategoryId =',@ItemCategoryId,',
										@ItemSubCategoryId =',@ItemSubCategoryId,',
										@Remarks = ','',@Remarks,'','
										@Suffix = ','',@Suffix,'','
										@Prefix = ','',@Prefix,'','
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive,',
										@ModuleTypeItem =',@ModuleTypeItem
									  ),

			@ProcedureName = '[master].[InsertOrUpdateItem]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1,@LastNumber INT,@ItemId INT;
	EXEC @LastNumber = [subscriber].GetLastNumber
									@SubscriberId = @SubscriberId,
									@EntityId = NULL,
									@FinancialYear = NULL,
									@DocumentType = NULL,
									@ModuleType = @ModuleTypeItem,
									@Status = @StatusTypeActive,
									@IpAddress = @IpAddress;

	SET @Code =  CONCAT(@Prefix,@Code,@Suffix);
    BEGIN TRY
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Items WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				INSERT INTO [master].Items
				(
					SubscriberId,
					[Name],
					Code,
					HsnOrSac,
					ItemGroupId,
					ItemSubGroupId,
					ItemCategoryId,
					ItemSubCategoryId,
					Remarks,
					ActualLastNumber,
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@Name,
					@Code,
					@HsnOrSac,
					@ItemGroupId,
					@ItemSubGroupId,
					@ItemCategoryId,
					@ItemSubCategoryId,
					@Remarks,
					@LastNumber,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);

				SET @ItemId = SCOPE_IDENTITY();

				INSERT INTO [master].ItemUnits
				(
					ItemId,
					UnitId,
					Labour
				)
				SELECT
					@ItemId,
					iu.UnitId,
					iu.Labour
				FROM
					@ItemUnits AS iu;

				INSERT INTO [master].ItemCrates
				(
					ItemId,
					CrateId,
					CrateDeposit
				)
				SELECT
					@ItemId,
					ic.CrateId,
					ic.Deposit
				FROM
					@ItemCrates AS ic;

				UPDATE
					subscriber.NumberConfigurations
				SET
					LastNumber = @LastNumber,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					SubscriberId = @SubscriberId
					AND ModuleType = @ModuleTypeItem;

				COMMIT TRAN;
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Items WHERE SubscriberId = @SubscriberId  AND [Name] = @Name AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				UPDATE
					[master].Items
				SET
					[Name] = @Name,
					HsnOrSac = @HsnOrSac,
					ItemGroupId = @ItemGroupId,
					ItemSubGroupId = @ItemSubGroupId,
					ItemCategoryId = @ItemCategoryId,
					ItemSubCategoryId = @ItemSubCategoryId,
					Remarks  = @Remarks,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id
					AND SubscriberId = @SubscriberId;

				DELETE FROM 
					[master].ItemUnits 
				WHERE
					ItemId = @Id;

				DELETE FROM
					[master].ItemCrates
				WHERE
					ItemId = @Id;

				INSERT INTO [master].ItemUnits
				(
					ItemId,
					UnitId,
					Labour
				)
				SELECT
					@Id,
					iu.UnitId,
					iu.Labour
				FROM
					@ItemUnits AS iu;

				INSERT INTO [master].ItemCrates
				(
					ItemId,
					CrateId,
					CrateDeposit
				)
				SELECT
					@Id,
					ic.CrateId,
					ic.Deposit
				FROM
					@ItemCrates AS ic;

				COMMIT TRAN;
			END
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN;
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	RETURN @ReturnValue;
