﻿/*------------------------------------------------------------------------------------------------------------
Name			: ChangeCompanyStatus
Comments		: 29-03-2021 | Amit Khanna | This procedure is used to change company status.

Test Execution	: EXEC master.ChangeCompanyStatus
					@SubscriberId = 1,
					@Id =  1,
					@UpdatedBy = 1,
					@UpdatedDate = '2021-03-03',
					@IpAddress = NULL;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[ChangeCompanyStatus]
(
	@SubscriberId INT,
	@Id INT,
	@UpdatedBy INT,
	@UpdatedDate SMALLDATETIME,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[ChangeCompanyStatus]','
										@SubscriberId =',@SubscriberId,',
										@Id = ',@Id,',
										@UpdateBy = ',@UpdatedBy,',
										@UpdateDate = ',@UpdatedDate,',
										@IpAddress = ','',@IpAddress
									  ),
			@ProcedureName = '[master].[ChangeCompanyStatus]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY
		UPDATE
			[master].Companies
		SET
			[Status] = CASE WHEN [Status] = 1 THEN 0 ELSE 1 END,
			UpdatedBy = @UpdatedBy,
			UpdatedDate = @UpdatedDate,
			IpAddress = @IpAddress
		WHERE
			SubscriberId = @SubscriberId
			AND Id = @Id;
			
		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

