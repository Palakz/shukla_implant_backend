﻿CREATE TABLE [master].[ItemUnits] (
    [Id]     INT             IDENTITY (1, 1) NOT NULL,
    [ItemId] INT             NOT NULL,
    [UnitId] INT             NOT NULL,
    [Labour] DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK__ItemUnit__3214EC076CFEB897] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ItemUnits__ItemI__0FC23DAB] FOREIGN KEY ([ItemId]) REFERENCES [master].[Items] ([Id]),
    CONSTRAINT [FK__ItemUnits__UnitI__10B661E4] FOREIGN KEY ([UnitId]) REFERENCES [master].[UQCS] ([Id])
);

