﻿CREATE TABLE [master].[CompanyBankDetails] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [CompanyId]     INT           NOT NULL,
    [Name]          VARCHAR (100) NOT NULL,
    [AccountNumber] VARCHAR (18)  NOT NULL,
    [Branch]        VARCHAR (50)  NOT NULL,
    [IfscCode]      VARCHAR (11)  NOT NULL,
    [IsDefault]     BIT           NOT NULL,
    CONSTRAINT [PK__CompanyB__3214EC0703D52B31] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__CompanyBa__Compa__67DE6983] FOREIGN KEY ([CompanyId]) REFERENCES [master].[Companies] ([Id])
);

