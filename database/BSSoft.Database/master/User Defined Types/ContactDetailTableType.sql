﻿CREATE TYPE [master].[ContactDetailTableType] AS TABLE (
    [Name]         VARCHAR (100) NULL,
    [MobileNumber] VARCHAR (20)  NULL,
    [Phone]        VARCHAR (20)  NULL,
    [EmailAddress] VARCHAR (100) NULL);

