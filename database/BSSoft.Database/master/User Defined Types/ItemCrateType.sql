﻿CREATE TYPE [master].[ItemCrateType] AS TABLE (
    [CrateId] INT             NOT NULL,
    [Deposit] DECIMAL (18, 2) NOT NULL);

