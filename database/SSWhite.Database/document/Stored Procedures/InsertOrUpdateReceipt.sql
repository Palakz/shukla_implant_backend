﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateReceipt
Comments		: 09-04-2021 | Amit Khanna | This procedure is used to insert or update receipt.

Test Execution	: EXEC document.InsertOrUpdateReceipt
						@SubscriberId = 1,
						@FinancialYear = 2,
						@Id = 1
						@ClientId = 1,
						@DayBookId = 1,
						@ReceiptNumber = 1,
						@Date = '2022-01-01',
						@ReceiptType = 1,
						@Month = 4,
						@ReceivingPerson = NULL,
						@TotalAmount = 410.05,
						@Remarks = NULL,
						@Prefix = NULL,
						@Suffix = NULL,
						@IpAddress = NULL,
						@CreatedBy = 1,
						@CreatedDate  = '2021-01-01',
						@StatusTypeActive = 1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[InsertOrUpdateReceipt]
(
	@SubscriberId INT,
	@FinancialYear INT,
	@Id INT,
	@CompanyId INT,
	@ClientId INT,
	@DayBookId INT,
	@ReceiptNumber VARCHAR(MAX),
	@Date DATE,
	@ReceiptType TINYINT,
	@Month TINYINT,
	@ReceivedAmount DECIMAL(18,2),
	@TotalAmount DECIMAL(18,2),
	@BankId INT,
	@ChequeNumber VARCHAR(6),
	@Branch VARCHAR(50),
	@IsReturnedCheque BIT,
	@ChequeReturnedDate DATE,
	@Remarks VARCHAR(100),
	@Prefix VARCHAR(MAX),
	@Suffix VARCHAR(MAX),
	@ReceiptHeaders [document].[DocumentHeaderTableType] READONLY,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC document.InsertOrUpdateReceipt','
										@SubscriberId =',@SubscriberId,',
										@FinancialYear =',@FinancialYear,',
										@Id =',@Id,',
										@CompanyId =',@CompanyId,',
										@ClientId =',@ClientId,',
										@DayBookId =',@DayBookId,',
										@ReceiptNumber =''',@ReceiptNumber,''',
										@Date =',@Date,',
										@ReceiptType =',@ReceiptType,',
										@Month =',@Month,',
										@ReceivedAmount =',@ReceivedAmount,',
										@TotalAmount =',@TotalAmount,',
										@BankId =',@BankId,',
										@ChequeNumber =''',@ChequeNumber,''',
										@Branch =''',@Branch,''',
										@IsReturnedCheque =',@IsReturnedCheque,',
										@ChequeReturnedDate =',@ChequeReturnedDate,',
										@Remarks =''',@Remarks,''',
										@Prefix = ','',@Prefix,'','
										@Suffix = ','',@Suffix,'','
										@IpAddress = ,','',@IpAddress,'','
										@CreatedBy = ,',@CreatedBy,',
										@CreatedDate = ,',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = 'document.InsertOrUpdateReceipt',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReceiptId INT, @LastNumber INT,@ReturnValue INT = 1;
    
	BEGIN TRY
		EXEC @LastNumber = [subscriber].GetLastNumber
									@SubscriberId = @SubscriberId,
									@EntityId = @CompanyId,
									@FinancialYear = @FinancialYear,
									@DocumentType = @ReceiptType,
									@ModuleType = NULL,
									@Status = @StatusTypeActive,
									@IpAddress = @IpAddress;
		
		SET @ReceiptNumber = CONCAT(@Prefix,@LastNumber,@Suffix);
		
		SELECT
			dh.HeaderId,
			dh.[Percentage],
			dh.CalculationType,
			dh.[Value]
		INTO
			#DocumentHeaders
		FROM
			@ReceiptHeaders AS dh
		
		
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [document].Receipts WHERE SubscriberId = @SubscriberId AND [ReceiptNumber] = @ReceiptNumber AND [Type] = @ReceiptType AND CompanyId = @CompanyId AND FinancialYear = @FinancialYear AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				INSERT INTO [document].Receipts
				(
					SubscriberId,
					CompanyId,
					ClientId,
					DayBookId,
					ReceiptNumber,
					[Date],
					[Type],
					ReceivedAmount,
					ChequeNumber,
					BankId,
					Branch,
					IsReturnedCheque,
					ChequeReturnedDate,
					[Month],
					FinancialYear,
					TotalAmount,
					Remarks,
					ActualReceiptNumber,
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@CompanyId,
					@ClientId,
					@DayBookId,
					@ReceiptNumber,
					@Date,
					@ReceiptType,
					@ReceivedAmount,
					@ChequeNumber,
					@BankId,
					@Branch,
					@IsReturnedCheque,
					@ChequeReturnedDate,
					@Month,
					@FinancialYear,
					@TotalAmount,
					@Remarks,
					@LastNumber,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);

				SET @ReceiptId = SCOPE_IDENTITY();

				INSERT INTO document.ReceiptHeaders
				(
					ReceiptId,
					HeaderId,
					CalculationType,
					[Percentage],
					[Value]
				)
				SELECT
					@ReceiptId,
					thd.HeaderId,
					thd.CalculationType,
					thd.[Percentage],
					thd.[Value]
				FROM
					#DocumentHeaders AS thd;

				UPDATE
					subscriber.NumberConfigurations
				SET
					LastNumber = @LastNumber,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					SubscriberId = @SubscriberId
					AND EntityId = @CompanyId
					AND DocumentType = @ReceiptType
					AND FinancialYear = @FinancialYear;

				COMMIT TRAN;
			END
		END
		ELSE
		BEGIN
			BEGIN TRAN
			UPDATE
				[document].Receipts
			SET
				ClientId = @ClientId,
				[Date] = @Date,
				DayBookId = @DayBookId,
				[Type] = @ReceiptType,
				ReceivedAmount = @ReceivedAmount,
				ChequeNumber = @ChequeNumber,
				BankId = @BankId,
				Branch = @Branch,
				IsReturnedCheque = @IsReturnedCheque,
				ChequeReturnedDate= @ChequeReturnedDate,
				[Month] = @Month,
				TotalAmount = @TotalAmount,
				Remarks = @Remarks,
				IpAddress = @IpAddress,
				UpdatedBy = @CreatedBy,
				UpdatedDate = @CreatedDate
			WHERE
				Id = @Id;

			DELETE FROM
					[document].ReceiptHeaders
				WHERE
					ReceiptId = @Id;

			INSERT INTO document.ReceiptHeaders
			(
				ReceiptId,
				HeaderId,
				CalculationType,
				[Percentage],
				[Value]
			)
			SELECT
				@Id,
				thd.HeaderId,
				thd.CalculationType,
				thd.[Percentage],
				thd.[Value]
			FROM
				#DocumentHeaders AS thd;
			
			COMMIT TRAN;
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN;
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	DROP TABLE #DocumentHeaders;
	RETURN @ReturnValue;
