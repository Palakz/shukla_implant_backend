﻿USE [db_a5dc4b_sswhite]
GO
/****** Object:  StoredProcedure [emp].[GetMpcVOteDetailsByEpoId]    Script Date: 6/3/2022 5:51:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------
Procedure	: EXEC [document].[GetDocumentsDropdownAndUserDetails]
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
USE [db_a5dc4b_sswhite]
 TEst Execution : [document].[GetDocumentsDropdownAndUserDetails] 42
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [document].[GetDocumentsDropdownAndUserDetails]
(
	@UserId INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		Id,
		[Name] 
	FROM 
		document.EcnChange

	SELECT
		DISTINCT DepartmentName
	FROM 
		emp.Department

	SELECT
		UserId AS Id,
		PersonName
	FROM 
		emp.VMSEmployeeDetails
	WHERE 
		ApprovalForDocument = 1

	SELECT
		PersonName,
		ReportingUnder
	FROM 
		emp.VMSEmployeeDetails
	WHERE 
		UserId = @UserId
END 
