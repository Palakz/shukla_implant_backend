﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[AddOrEditEpo]
Comments	: 26-04-2022 | amit| This procedure is used to Add New Employee.

Test Execution :	DECLARE  @ApprovalNotification AS document.ApprovalNotification,
							 @AssociatedDocument AS document.AssociatedDocument,
							 @DocumentNotification AS document.DocumentNotification,
							 @Attachment AS document.Attachment ;
					INSERT INTO  @ApprovalNotification (Name, UserId, PersonId)
					VALUES('Madan Singh',41,'emp1'),('Anushka Patni',43,'Emp3');

					INSERT INTO  @DocumentNotification (Name, UserId, PersonId)
					VALUES('Madan Singh',41,'emp1'),('Anushka Patni',43,'Emp3');

					INSERT INTO  @Attachment (LocalFileName, Title, AttachmentPath)
					VALUES('LocalFileName1','Title1','aws/Path'),('LocalFileName2','Title2','aws/Path');

					INSERT INTO  @AssociatedDocument (AssociatedDocumentId, Rev, Title)
					VALUES('DOC-ECD1','1','DocumentTitle1'),('DOC-ECD2','1','DocumentTitle2');
					DECLARE @Currentdate1 datetime =getdate();

					EXEC [emp].[AddOrEditDocument]
					@Id = null,
					@UserId = 42 ,
					@DocumentTitle = 'DocumentTitle1',
					@DocumentType = 1,
					@Rev = '1',
					@EcnNumber = NULL ,
					@EcnTitle = 'EcnTitle1' ,
					@EcnDate = '2022-06-03 23:11:40.250',
					@CustomerApprovalRequired = 1,
					@CustomerNotificationRequired = 0,
					@PartDescriptionName = 'PartDescriptionName' ,
					@PartNumber = 'PartNumber',
					@DrawingNumber = 'DrawingNumber',
					@CustomerPartNumber = 'CustomerPartNumber' ,
					@CurrentRev = 'CurrentRev',
					@NewRev = 'NewRev',
					@DescriptionOfChange = 'DescriptionOfChange',
					@ReasonForChange = 'ReasonForChange',
					@InstructionForEcnImplementation = 'InstructionForEcnImplementation',
					@EcnChange = '1',
					@Purpose = 'Purpose',
					@Scope = 'Scope',
					@Responsibility = 'Responsibility',
					@Defination = 'Defination',
					@Form = 'Form',
					@Notes = 'Notes',
					@WorkStation = 'WorkStation',
					@AttachmentApproval = 1,
					@CurrentDate = @Currentdate1,
					@StatusTypeActive = 1,
					@ApprovalNotification = @ApprovalNotification,
					@AssociatedDocument = @AssociatedDocument, 
					@DocumentNotification = @DocumentNotification,
					@Attachment = @Attachment 
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [emp].[AddOrEditDocument]
(
	@Id int ,
	@UserId int ,
	@DocumentTitle VARCHAR(255),
	@DocumentType INT,
	@Rev VARCHAR(MAX),
	@EcnNumber INT,
	@EcnTitle VARCHAR(255),
	@EcnDate DATETIME,
	@CustomerApprovalRequired BIT,
	@CustomerNotificationRequired BIT,
	@PartDescriptionName VARCHAR(MAX),
	@PartNumber VARCHAR(MAX),
	@DrawingNumber VARCHAR(MAX),
	@CustomerPartNumber VARCHAR(MAX),
	@CurrentRev VARCHAR(MAX),
	@NewRev VARCHAR(MAX),
	@DescriptionOfChange VARCHAR(MAX),
	@ReasonForChange VARCHAR(MAX),
	@InstructionForEcnImplementation VARCHAR(MAX),
	@EcnChange VARCHAR(100),
	@Purpose VARCHAR(MAX),
	@Scope VARCHAR(MAX),
	@Responsibility VARCHAR(MAX),
	@Defination VARCHAR(MAX),
	@Form VARCHAR(MAX),
	@Notes VARCHAR(MAX),
	@WorkStation VARCHAR(MAX),
	@AttachmentApproval BIT,
	@CurrentDate DATETIME,
	@StatusTypeActive int,
	@ApprovalNotification document.ApprovalNotification READONLY,
	@AssociatedDocument document.AssociatedDocument READONLY,
	@DocumentNotification document.DocumentNotification READONLY,
	@Attachment document.Attachment READONLY
)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		Begin tran

		DECLARE @InsertedId int, @DocumentTypeName VARCHAR(100),  @DocumentId VARCHAR(100), 
				@ApprovalEmailIds VARCHAR(1000), @NotificationEmailIds VARCHAR(100), @PersonName VARCHAR(1000); 
		SELECT @DocumentTypeName = Name from document.documentType WHere Id  = @DocumentType
		IF(@Id IS NULL)
		BEGIN
			INSERT INTO document.Documents
			(
				DocumentTitle,
				DocumentType,
				Rev,
				EcnNumber,
				EcnTitle,
				EcnDate,
				CustomerApprovalRequired,
				CustomerNotificationRequired,
				PartDescriptionName,
				PartNumber,
				DrawingNumber,
				CustomerPartNumber,
				CurrentRev,
				NewRev,
				DescriptionOfChange,
				ReasonForChange,
				InstructionForEcnImplementation,
				EcnChange,
				Purpose,
				Scope,
				Responsibility,
				Defination,
				Form,
				Notes,
				WorkStation,
				AttachmentApproval,
				Status,
				CreatedBy,
				CreatedDate
			)
			VALUES
			(
				@DocumentTitle,
				@DocumentType,
				@Rev,
				@EcnNumber,
				@EcnTitle,
				@EcnDate,
				@CustomerApprovalRequired,
				@CustomerNotificationRequired,
				@PartDescriptionName,
				@PartNumber,
				@DrawingNumber,
				@CustomerPartNumber,
				@CurrentRev,
				@NewRev,
				@DescriptionOfChange,
				@ReasonForChange,
				@InstructionForEcnImplementation,
				@EcnChange,
				@Purpose,
				@Scope,
				@Responsibility,
				@Defination,
				@Form,
				@Notes,
				@WorkStation,
				@AttachmentApproval,
				@StatusTypeActive,
				@UserId,
				@CurrentDate
			)
		SET @InsertedId = SCOPE_IDENTITY();

		UPDATE 
			document.Documents 
		SET 
			DocumentId = CONCAT(@DocumentTypeName,'-',@InsertedId) 
		WHERE 
			Id = @InsertedId
		END

		   --Insert into log table--
		INSERT INTO document.DocumentsLog(
			DocumentCreateId,
			DocumentType,
			DocumentId,
			DocumentTitle,
			Rev,
			EcnNumber,
			EcnTitle,
			EcnDate,
			CustomerApprovalRequired,
			CustomerNotificationRequired,
			PartDescriptionName,
			PartNumber,
			DrawingNumber,
			CustomerPartNumber,
			CurrentRev,
			NewRev,
			DescriptionOfChange,
			ReasonForChange,
			InstructionForEcnImplementation,
			EcnChange,
			Purpose,
			Scope,
			Responsibility,
			Defination,
			Form,
			Notes,
			WorkStation,
			AttachmentApproval,
			Status,
			CreatedBy,
			CreatedDate,
			ModifiedBy,
			ModifiedDate
			)
		SELECT @InsertedId,
			DocumentType,
			DocumentId,
			DocumentTitle,
			Rev,
			EcnNumber,
			EcnTitle,
			EcnDate,
			CustomerApprovalRequired,
			CustomerNotificationRequired,
			PartDescriptionName,
			PartNumber,
			DrawingNumber,
			CustomerPartNumber,
			CurrentRev,
			NewRev,
			DescriptionOfChange,
			ReasonForChange,
			InstructionForEcnImplementation,
			EcnChange,
			Purpose,
			Scope,
			Responsibility,
			Defination,
			Form,
			Notes,
			WorkStation,
			AttachmentApproval,
			Status,
			CreatedBy,
			CreatedDate,
			ModifiedBy,
			ModifiedDate
		FROM
			document.Documents
		WHERE  
			id = @InsertedId;

		INSERT INTO document.Attachment (
			DocumentId,
			LocalFileName,
			Title,
			AttachmentPath
		)
		(SELECT
			@InsertedId,
			LocalFileName,
			Title,
			AttachmentPath
		FROM
			@Attachment)

		INSERT INTO document.AssociatedDocument (
			DocumentId,
			AssociatedDocumentId,
			Rev,
			Title
		)
		(SELECT
			@InsertedId,
			AssociatedDocumentId,
			Rev,
			Title
		FROM
			@AssociatedDocument)

		INSERT INTO document.DocumentNotification (
			DocumentId,
			Name,
			UserId,
			PersonId,
			Email
		)
		(SELECT
			@InsertedId,
			Name,
			d.UserId,
			d.PersonId,
			EmailAddress
		FROM
			@DocumentNotification d INNER JOIN emp.UserMaster u ON d.UserId = u.Id)

		INSERT INTO document.ApprovalNotification (
			DocumentId,
			Name,
			UserId,
			PersonId,
			Email
		)
		(SELECT
			@InsertedId,
			Name,
			d.UserId,
			d.PersonId,
			EmailAddress
		FROM
			@ApprovalNotification d INNER JOIN emp.UserMaster u ON d.UserId = u.Id)

---------------------------------------------------UPDATE -------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		COMMIT
		SELECT  @DocumentId = DocumentId from document.documents where id =@InsertedId
		SELECT @NotificationEmailIds = STRING_AGG(Email,',') FROM document.DocumentNotification  WHERE DocumentId =@InsertedId
		SELECT @ApprovalEmailIds = STRING_AGG(Email,',') FROM document.ApprovalNotification	 WHERE DocumentId =@InsertedId
		SELECT @PersonName = PersonName FROM emp.vmsEmployeeDetails	 WHERE UserId = @UserId

		SELECT 
			@DocumentId AS DocumentId,
			@DocumentTitle AS DocumentTitle,
			@NotificationEmailIds AS NotificationEmailIds,
			@ApprovalEmailIds AS ApprovalEmailIds,
			@PersonName AS CreatedBy
    END TRY
	BEGIN CATCH
		Rollback
		DECLARE
			@stringmsg VARCHAR(MAX),
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
			set  @ErrorNumber =  ERROR_NUMBER();
			set  @ErrorSeverity= ERROR_SEVERITY();
			set  @ErrorState =  ERROR_STATE();
			set  @ErrorMessage =   ERROR_MESSAGE();
			set  @ErrorLine =  ERROR_LINE();
			 set @stringmsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;

			 THROW 51000,@stringmsg , 1; 
	END CATCH;
End
