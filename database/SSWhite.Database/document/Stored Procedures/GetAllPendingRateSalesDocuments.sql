﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetAllPendingRateSalesDocuments
Comments		: 11-05-2021 | Amit Khanna | This procedure is used to get All Sales Documents pending for sales rate add.

Test Execution	: EXEC [document].[GetAllPendingRateSalesDocuments]
						@SubscriberId =1,
						@CompanyId = NULL,
						@ClientId =  NULL,
						@VoucherType = NULL,
						@Month = NULL,
						@FinancialYear = NULL,
						@FromDate = NULL,
						@ToDate = NULL,
						@Status = NULL,
						@Start = 0,
						@Length = 10,
						@SearchKeyword = NULL,
						@SortExpression = NULL,
						@IpAddress = NULL,
						@TotalRecords = 0 
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[GetAllPendingRateSalesDocuments]
(
	@SubscriberId INT,
	@CompanyId INT,
	@ClientId INT,
	@VoucherType TINYINT,
	@Month SMALLINT,
	@FinancialYear INT,
	@FromDate SMALLDATETIME,
	@ToDate SMALLDATETIME,
	@Status TINYINT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@IpAddress VARCHAR(45),
	@TotalRecords INT OUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[GetAllSalesDocuments]','
										@SubscriberId =',@SubscriberId,',
										@CompanyId = ',@CompanyId,',
										@ClientId = ',@ClientId,',
										@VoucherType = ',@VoucherType,',
										@Month = ',@Month,',
										@FinancialYear = ',@FinancialYear,',
										@FromDate = ',@FromDate,',
										@ToDate = ',@ToDate,',
										@Status = ',@Status,',
										@Start = ',@Start,',
										@Length = ',@Length,',
										@SearchKeyword = ','',@SearchKeyword,'',',
										@SortExpression = ','',@SortExpression,'',',
										@IpAddress = ','',@IpAddress,'',',
										@TotalRecords = ',@TotalRecords
									  ),
			@ProcedureName = '[document].[GetAllSalesDocuments]',
			@ExecutionTime = GETDATE()	
			
	CREATE TABLE #TempTable(Id INT); 
    BEGIN TRY
		
		SELECT
			DISTINCT
			SalesDocumentId
		INTO
			#TempSalesDocumentIds
		FROM
			document.SalesDocumentItems AS sdi
		WHERE
			Amount = 0;

		INSERT INTO #TempTable(Id)
		SELECT  
			us.Id
		FROM 
			[document].SalesDocuments AS us
			INNER JOIN [master].Clients AS ct ON us.ClientId = ct.Id
			INNER JOIN #TempSalesDocumentIds AS sdi ON us.Id = sdi.SalesDocumentId
		WHERE 
			us.SubscriberId = @SubscriberId
			AND us.[Status] = ISNULL(@Status,us.[Status])
			AND us.CompanyId = ISNULL(@CompanyId,us.CompanyId)
			AND us.ClientId = ISNULL(@ClientId,us.ClientId)
			AND us.VoucherType = ISNULL(@VoucherType,us.VoucherType)
			AND us.[Date] BETWEEN ISNULL(@FromDate,us.[Date]) AND ISNULL(@ToDate,us.[Date])
			AND us.[Month]  = ISNULL(@Month,us.[Month])
			AND us.FinancialYear = ISNULL(@FinancialYear,us.FinancialYear)
			AND us.EndDate IS NULL
			AND 
			(
					us.VoucherNumber LIKE '%' + ISNULL(@SearchKeyword,us.[VoucherNumber]) + '%'
					OR ct.[CompanyName] LIKE '%' + ISNULL(@SearchKeyword,ct.[CompanyName]) + '%'
			);

		SELECT @TotalRecords = COUNT(Id)  FROM #TempTable; 

		SELECT
			us.Id,
			us.VoucherNumber,
			us.[Date],
			ct.[CompanyName] AS ClientName,
			us.VoucherType,
			us.ReceivedBy,
			us.TotalAmount,
			us.[Status]
		FROM 
			#TempTable AS tmp
			INNER JOIN document.SalesDocuments AS us ON tmp.Id = us.Id
			INNER JOIN [master].Clients AS ct ON us.ClientId = ct.Id
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN us.[ActualDocumentNumber] END DESC,
			CASE WHEN @SortExpression = 'date asc' THEN us.[Date] END ASC,
			CASE WHEN @SortExpression = 'date desc' THEN us.[Date] END DESC,
			CASE WHEN @SortExpression = 'totalAmount asc' THEN us.[TotalAmount] END ASC,
			CASE WHEN @SortExpression = 'totalAmount desc' THEN us.[TotalAmount] END DESC,
			CASE WHEN @SortExpression = 'clientName asc' THEN ct.[Name] END ASC,
			CASE WHEN @SortExpression = 'clientName desc' THEN ct.[Name] END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempTable;
