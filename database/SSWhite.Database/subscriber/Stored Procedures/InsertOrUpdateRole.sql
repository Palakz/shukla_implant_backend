﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateRole
Comments		: 31-03-2021 | Amit Khanna | This procedure is used to insert role or update role by Id.

Test Execution	: EXEC subscriber.InsertOrUpdateRole
						@SubscriberId = 1,
						@Id = NULL,
						@Name = 'AK Industries',
						@Decription = 'Demo',
						@IpAddress = '',
						@CreatedBy = 1,
						@CreatedDate = '2021-02-31',
						@StatusTypeActive = 1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[InsertOrUpdateRole]
(
	@SubscriberId INT,
	@Id INT,
	@Name VARCHAR(80),
	@Description VARCHAR(200),
	@RightsIds [common].IntType READONLY,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[InsertOrUpdateRole]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@Name =''',@Name,''',
										@Description =''',@Description,''',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),
			@ProcedureName = '[subscriber].[InsertOrUpdateRole]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1,@RoleId INT;

	SELECT
		rtId.Item
	INTO
		#TempRightsIds
	FROM
		@RightsIds AS rtId


    BEGIN TRY
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM subscriber.Roles WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN

				INSERT INTO subscriber.Roles
				(
					SubscriberId,
					[Name],
					[Description],
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@Name,
					@Description,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);

				SET @RoleId = SCOPE_IDENTITY();

				INSERT INTO subscriber.RoleRights
				(
					RoleId,
					RightId,
					CreatedDate
				)
				SELECT
					@RoleId,
					rtId.Item,
					@CreatedDate
				FROM
					#TempRightsIds AS rtId;
				
				COMMIT TRAN;
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM subscriber.Roles WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				UPDATE
					subscriber.Roles
				SET
					[Name] = @Name,
					[Description] = @Description,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id
					AND SubscriberId = @SubscriberId;

				DELETE FROM
					subscriber.RoleRights
				WHERE
					RoleId = @Id;

				INSERT INTO subscriber.RoleRights
				(
					RoleId,
					RightId,
					CreatedDate
				)
				SELECT
					@Id,
					rtId.Item,
					@CreatedDate
				FROM
					#TempRightsIds AS rtId;

				COMMIT TRAN;
			END
		END

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
		RETURN @ReturnValue;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN;
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
		RETURN @ReturnValue;
	END CATCH;

	DROP TABLE #TempRightsIds;
