﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetUserById
Comments		: 30-03-2021 | Amit Khanna | This procedure is used to get user by Id.

Test Execution	: EXEC subscriber.GetUserById
					@SubscriberId =  1,
					@Id =  1,
					@IpAddress =  1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[GetUserById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[GetUserById]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ''',@IpAddress,''
									  ),
			@ProcedureName = '[subscriber].[GetUserById]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			us.Id,
			us.FirstName,
			us.LastName,
			us.UserName,
			us.MobileNumber AS MobileNo,
			us.EmailAddress,
			us.[Address]
		FROM
			subscriber.users AS us
		WHERE
			us.Id = @Id;

		SELECT
			ur.RoleId
		FROM
			subscriber.UserRoles AS ur
		WHERE
			ur.UserId = @Id;

		SELECT
			ue.EntityId
		FROM
			subscriber.UserEntities AS ue
		WHERE
			ue.UserId = @Id;
			

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
