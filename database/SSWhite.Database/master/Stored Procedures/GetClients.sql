﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetClients
Comments		: 05-04-2021 | Amit Khanna | This procedure is used to get All clients for common call.

Test Execution	: EXEC master.GetClients
					@SubscriberId = 1,
					@Status = NULL,
					@IpAddress = NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetClients]
(
	@SubscriberId INT,
	@Status TINYINT,
	@AccountTypes [common].[IntType] READONLY,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetClients]','
										@SubscriberId =',@SubscriberId,',
										@Status = ',@Status,'
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetClients]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
		
		SELECT
			ct.Id,
			ct.[CompanyName] AS [Name],
			ct.[Status],
			ct.Gstin AS Gstin,
			SUBSTRING(ct.Gstin,1,2) AS StateCode
		FROM 
			[master].Clients ct
			INNER JOIN [master].LedgerGroups AS lg ON ct.LedgerGroupId = lg.Id
		WHERE
			ct.SubscriberId = @SubscriberId
			AND ct.[Status] = ISNULL(@Status,ct.[Status])
			AND lg.[AccountType] IN (SELECT Item FROM @AccountTypes)
		ORDER BY
			ct.[Name] ASC;
		
		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
