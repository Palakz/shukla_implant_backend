﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateClientItemDiscount
Comments		: 23-06-2021 | Tanvi Pathak | This procedure is used to insert client or update client by Id.

Test Execution	: DECLARE @ReturnValue INT;
					EXEC @ReturnValue =  [master].[InsertOrUpdateClientItemDiscount]
						 @SubscriberId =1,
						 @Id =NULL,
						 @ClientId = 1,
						 @IpAddress =NULL,
						 @CreatedBy = 1,
						 @CreatedDate = '2021-06-23',
						 @StatusTypeActive =1
					SELECT @ReturnValue
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateClientItemDiscount]
(
	@SubscriberId INT,
	@Id INT,
	@ClientId INT,
	@ClientItemDiscounts [master].ClientItemDiscountType READONLY,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateClientItemDiscount]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@ClientId =',@ClientId,',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = '[master].[InsertOrUpdateClientItemDiscount]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1,@ItemDiscountId INT;
	SELECT
		bdt.ItemtypeId,
		bdt.Discount,
		bdt.AdditionalDiscount
	INTO
		#TempDiscountDetails
	FROM
		@ClientItemDiscounts AS bdt;

    BEGIN TRY
	
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].ClientItemDiscounts WHERE SubscriberId = @SubscriberId AND ClientId = @ClientId AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				INSERT INTO [master].ClientItemDiscounts
				(
					SubscriberId,
					ClientId,
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@ClientId,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);
				
				SET @ItemDiscountId = SCOPE_IDENTITY();

				INSERT INTO [master].ClientItemDiscountDetails
				(
					ClientItemDiscountId,
					[ItemTypeId],
					[Discount],
					AdditionalDiscount
				)
				SELECT
					@ItemDiscountId,
					bdt.ItemTypeId,
					bdt.Discount,
					bdt.AdditionalDiscount
				FROM
					#TempDiscountDetails AS bdt;

				COMMIT TRAN
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].ClientItemDiscounts WHERE SubscriberId = @SubscriberId AND ClientId = @ClientId AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				UPDATE
					[master].ClientItemDiscounts
				SET
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id;

				DELETE FROM 
					[master].ClientItemDiscountDetails
				WHERE
					ClientItemDiscountId = @Id;

				INSERT INTO [master].ClientItemDiscountDetails
				(
					ClientItemDiscountId,
					[ItemTypeId],
					Discount,
					Additionaldiscount
				)
				SELECT
					@Id,
					bdt.ItemTypeId,
					bdt.Discount,
					bdt.AdditionalDiscount
				FROM
					#TempDiscountDetails AS bdt;

				COMMIT TRAN
			END
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempDiscountDetails;
	RETURN @ReturnValue;
