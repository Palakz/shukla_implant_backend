﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [master].[GetDepartmentByDepartmentSupervisorName]
Comments	: 25-04-2022 | Kartik Bariya | This procedure is used to get GetReportingUnder details.

Test Execution : [master].[GetDepartmentByDepartmentSupervisorName] 'Ahmed Pathan'
					
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetDepartmentByDepartmentSupervisorName]
(
	@DepartmentSupervisorName varchar(255)
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
		Id,
		departmentname AS DepartmentName 
	FROM 
		emp.Department 
	WHERE 
		departmentsupervisorname = @DepartmentSupervisorName 
		AND departmentname !='New Development-Flex'

End;
