﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateClient
Comments		: 31-03-2021 | Amit Khanna | This procedure is used to insert client or update client by Id.

Test Execution	: DECLARE @ReturnValue INT;
					EXEC @ReturnValue =  [master].[InsertOrUpdateClient]
						 @SubscriberId =1,
						 @Id =NULL,
						 @CompanyName ='AK Industries',
						 @Code ='3',
						 @Gstin ='',
						 @Pan ='',
						 @Address ='AddressLine1  Sarkhej',
						 @CityId = 331,
						 @StateId = 11,
						 @EmailAddress = 'akforever0099@gmail.com',
						 @MobileNumber = '9687654774',
						 @CreditLimit = NULL,
						 @OpeningBalance = ,
						 @TransactionType = 1,
						 @LedgerGroupId = 1,
						 @ZoneId = 2,
						 @IpAddress =NULL,
						 @CreatedBy = 1,
						 @CreatedDate = '2021-03-31',
						 @StatusTypeActive =1,
						 @ModuleTypeClient =2
					SELECT @ReturnValue
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateClient]
(
	@SubscriberId INT,
	@Id INT,
	@FinancialYear INT,
	@CompanyName VARCHAR(100),
	@Code VARCHAR(MAX),
	@Gstin VARCHAR(15),
	@Pan VARCHAR(10),
	@Address VARCHAR(500),
	@CityId INT,
	@StateId INT,
	@EmailAddress VARCHAR(100),
	@MobileNumber VARCHAR(20),
	@CreditLimit DECIMAL(18,2),
	@OpeningBalance DECIMAL(18,2),
	@TransactionType TINYINT,
	@LedgerGroupId INT,
	@ZoneId INT,
	@ContactDetails [master].ContactDetailTableType READONLY,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT,
	@ModuleTypeClient TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateClient]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@CompanyName =''',@CompanyName,''',
										@Code =''',@Code,''',
										@Gstin =''',@Gstin,''',
										@Pan =''',@Pan,''',
										@Address =''',@Address,''',
										@CityId = ',@CityId,',
										@StateId = ',@StateId,',
										@EmailAddress = ','',@EmailAddress,'','
										@MobileNumber = ','',@MobileNumber,'','
										@CreditLimit = ',@CreditLimit,',
										@OpeningBalance = ',@OpeningBalance,',
										@TransactionType = ',@TransactionType,',
										@LedgerGroupId = ',@LedgerGroupId,',
										@ZoneId = ',@ZoneId,',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive,',
										@ModuleTypeClient =',@ModuleTypeClient
									  ),

			@ProcedureName = '[master].[InsertOrUpdateClient]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1,@ClientId INT,@ClientCode INT;

	EXEC @ClientCode = [subscriber].GetLastNumber
									@SubscriberId = @SubscriberId,
									@EntityId = NULL,
									@FinancialYear = NULL,
									@DocumentType = NULL,
									@ModuleType = @ModuleTypeClient,
									@Status = @StatusTypeActive,
									@IpAddress = @IpAddress;

	SET @Code = @ClientCode;

	SELECT
		bdt.[Name],
		bdt.MobileNumber,
		bdt.Phone,
		bdt.EmailAddress
	INTO
		#TempContactDetails
	FROM
		@ContactDetails AS bdt;

    BEGIN TRY
	
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Clients WHERE SubscriberId = @SubscriberId AND [CompanyName] = @CompanyName AND IIF(@Gstin IS NULL,'',Gstin) = ISNULL(@Gstin,'') AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				INSERT INTO [master].Clients
				(
					SubscriberId,
					CompanyName,
					Code,
					Gstin,
					Pan,
					[Address],
					CityId,
					StateId,
					CountryId,
					EmailAddress,
					MobileNumber,
					CreditLimit,
					OpeningBalance,
					BalanceType,
					LedgerGroupId,
					ZoneId,
					ActualCodeNumber,
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@CompanyName,
					@Code,
					@Gstin,
					@Pan,
					@Address,
					@CityId,
					@StateId,
					1,
					@EmailAddress,
					@MobileNumber,
					@CreditLimit,
					@OpeningBalance,
					@TransactionType,
					@LedgerGroupId,
					@ZoneId,
					@Code,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);
				
				SET @ClientId = SCOPE_IDENTITY();

				INSERT INTO [master].ClientContactDetails
				(
					ClientId,
					[Name],
					MobileNumber,
					Phone,
					EmailAddress
				)
				SELECT
					@ClientId,
					bdt.[Name],
					bdt.MobileNumber,
					bdt.Phone,
					bdt.EmailAddress
				FROM
					#TempContactDetails AS bdt;

				INSERT INTO [master].ClientOpeningBalance
				(
					SubscriberId,
					ClientId,
					Balance,
					[Type],
					FinancialYear,
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					SCOPE_IDENTITY(),
					@OpeningBalance,
					@TransactionType,
					@FinancialYear,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);

				UPDATE
					subscriber.NumberConfigurations
				SET
					LastNumber = LastNumber + 1,
					ActualLastNumber = ActualLastNumber + 1,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					SubscriberId = @SubscriberId
					AND ModuleType = @ModuleTypeClient;
				COMMIT TRAN
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Clients WHERE SubscriberId = @SubscriberId AND [CompanyName] = @CompanyName AND IIF(@Gstin IS NULL,'',Gstin) = ISNULL(@Gstin,'') AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				UPDATE
					[master].Clients
				SET
					CompanyName = @CompanyName,
					Gstin = @Gstin,
					Pan = @Pan,
					[Address] = @Address,
					CityId = @CityId,
					StateId = @StateId,
					CountryId = 1,
					EmailAddress = @EmailAddress,
					MobileNumber = @MobileNumber,
					CreditLimit = @CreditLimit,
					LedgerGroupId = @LedgerGroupId,
					ZoneId = @ZoneId,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id;

				DELETE FROM 
					[master].ClientContactDetails
				WHERE
					ClientId = @Id;

				INSERT INTO [master].ClientContactDetails
				(
					ClientId,
					[Name],
					MobileNumber,
					Phone,
					EmailAddress
				)
				SELECT
					@Id,
					bdt.[Name],
					bdt.MobileNumber,
					bdt.Phone,
					bdt.EmailAddress
				FROM
					#TempContactDetails AS bdt;

				COMMIT TRAN
			END
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	DROP TABLE #TempContactDetails;
	RETURN @ReturnValue;

