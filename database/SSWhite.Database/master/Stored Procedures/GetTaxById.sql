﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetTaxById
Comments		: 22-06-2021 | Tanvi Pathak | This procedure is used to get tax by Id.

Test Execution	: EXEC master.GetTaxById
					@SubscriberId =  1,
					@Id =  1,
					@IpAddress =  1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetTaxById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetTaxById]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetTaxById]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			cmp.Id,
			cmp.[Name],
			cmp.Igst,
			cmp.Cgst,
			cmp.Sgst,
			cmp.Cess
		FROM
			[master].Taxes AS cmp
		WHERE
			cmp.Id = @Id;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
