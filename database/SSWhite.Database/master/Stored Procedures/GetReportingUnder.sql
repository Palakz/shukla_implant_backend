﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [master].[GetReportingUnder]
Comments	: 25-04-2022 | Kartik Bariya | This procedure is used to get GetReportingUnder details.

Test Execution : EXEC [master].[GetReportingUnder] 
					
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetReportingUnder]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		DISTINCT(departmentsupervisorname) AS DepartmentSupervisorName
	FROM 
		emp.Department 
	WHERE 
		mid=1
End;

