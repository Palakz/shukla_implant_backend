﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [master].[ValidateUserForLogIn]
Comments	: 13-04-2022 | Kartik Bariya | This procedure is used to validate user while login into the application.

Test Execution : EXEC [master].[ValidateUserForLogIn]
					@UserName = 'Jalpa.Dangi',
					@Password = 'nqj/iPMqYVM=',
					--@IpAddress = NULL,
					@StatusTypeActive = 1
					--@UserTypeAdmin = 2
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[ValidateUserForLogIn]
(
	@UserName VARCHAR(100),
	@Password VARCHAR(MAX),
	--@IpAddress VARCHAR(45),
	@StatusTypeActive TINYINT
	--@UserTypeAdmin TINYINT
)
AS
BEGIN
	SET NOCOUNT ON;
	--SELECT Id as UserId, UserName as UserName FROM [master].[Users] WHERE UserName = @UserName AND [PASSWORD] =@Password
	SELECT Id as UserId, LoginId as UserName FROM [emp].[UserMaster] WHERE LoginId = @UserName AND [PASSWORD] =@Password
End;
