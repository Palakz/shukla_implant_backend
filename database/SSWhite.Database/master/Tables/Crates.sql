﻿CREATE TABLE [master].[Crates] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT           NOT NULL,
    [Name]         VARCHAR (100) NOT NULL,
    [Status]       TINYINT       NOT NULL,
    [EndDate]      SMALLDATETIME NULL,
    [IpAddress]    VARCHAR (45)  NULL,
    [CreatedBy]    INT           NOT NULL,
    [CreatedDate]  SMALLDATETIME NOT NULL,
    [UpdatedBy]    INT           NULL,
    [UpdatedDate]  SMALLDATETIME NULL,
    CONSTRAINT [PK__Crates__3214EC07F492CAEB] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__Crates__CreatedB__6F7F8B4B] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__Crates__Subscrib__6E8B6712] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id]),
    CONSTRAINT [FK__Crates__UpdatedB__7073AF84] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id])
);

