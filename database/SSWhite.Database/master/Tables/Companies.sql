﻿CREATE TABLE [master].[Companies] (
    [Id]                 INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId]       INT           NOT NULL,
    [Name]               VARCHAR (100) NOT NULL,
    [Gstin]              VARCHAR (15)  NULL,
    [Pan]                VARCHAR (15)  NULL,
    [Address]            VARCHAR (500) NULL,
    [CountryId]          INT           NOT NULL,
    [StateId]            INT           NOT NULL,
    [CityId]             INT           NOT NULL,
    [Pincode]            VARCHAR (6)   NULL,
    [EmailAddress]       VARCHAR (100) NULL,
    [ContactPerson]      VARCHAR (100) NOT NULL,
    [ContactNumber]      VARCHAR (20)  NULL,
    [Website]            VARCHAR (80)  NULL,
    [OtherLicense]       VARCHAR (200) NULL,
    [TagLine]            VARCHAR (100) NULL,
    [TermsAndConditions] VARCHAR (MAX) NULL,
    [LogoPath]           VARCHAR (MAX) NULL,
    [Status]             TINYINT       NOT NULL,
    [EndDate]            SMALLDATETIME NULL,
    [IpAddress]          VARCHAR (45)  NULL,
    [CreatedBy]          INT           NOT NULL,
    [CreatedDate]        SMALLDATETIME NOT NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedDate]        SMALLDATETIME NULL,
    CONSTRAINT [PK__Companie__3214EC07B1192BC6] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__Companies__CityI__589C25F3] FOREIGN KEY ([CityId]) REFERENCES [app].[Cities] ([Id]),
    CONSTRAINT [FK__Companies__Count__56B3DD81] FOREIGN KEY ([CountryId]) REFERENCES [app].[Countries] ([Id]),
    CONSTRAINT [FK__Companies__Creat__59904A2C] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__Companies__State__57A801BA] FOREIGN KEY ([StateId]) REFERENCES [app].[States] ([Id]),
    CONSTRAINT [FK__Companies__Subsc__55BFB948] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id]),
    CONSTRAINT [FK__Companies__Updat__5A846E65] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id])
);

