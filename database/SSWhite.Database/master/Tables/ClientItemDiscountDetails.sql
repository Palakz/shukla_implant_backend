﻿CREATE TABLE [master].[ClientItemDiscountDetails] (
    [Id]                   INT             IDENTITY (1, 1) NOT NULL,
    [ClientItemDiscountId] INT             NOT NULL,
    [ItemTypeId]           INT             NOT NULL,
    [Discount]             DECIMAL (18, 2) NOT NULL,
    [AdditionalDiscount]   DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK__ClientIt__3214EC07B164DD62] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ClientIte__Clien__55DFB4D9] FOREIGN KEY ([ClientItemDiscountId]) REFERENCES [master].[ClientItemDiscounts] ([Id]),
    CONSTRAINT [FK__ClientIte__ItemT__56D3D912] FOREIGN KEY ([ItemTypeId]) REFERENCES [master].[ItemTypes] ([Id])
);

