﻿CREATE TABLE [master].[UQCS] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT           NOT NULL,
    [Name]         VARCHAR (100) NOT NULL,
    [Code]         VARCHAR (20)  NOT NULL,
    [Status]       TINYINT       NOT NULL,
    [EndDate]      SMALLDATETIME NULL,
    [IpAddress]    VARCHAR (45)  NULL,
    [CreatedBy]    INT           NOT NULL,
    [CreatedDate]  SMALLDATETIME NOT NULL,
    [UpdatedBy]    INT           NULL,
    [UpdatedDate]  SMALLDATETIME NULL,
    CONSTRAINT [PK__UQCS__3214EC0799B8785B] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__UQCS__CreatedBy__3587F3E0] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__UQCS__Subscriber__3493CFA7] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id]),
    CONSTRAINT [FK__UQCS__UpdatedBy__367C1819] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id])
);

