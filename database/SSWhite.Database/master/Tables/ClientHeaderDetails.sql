﻿CREATE TABLE [master].[ClientHeaderDetails] (
    [Id]       INT             IDENTITY (1, 1) NOT NULL,
    [ClientId] INT             NOT NULL,
    [HeaderId] INT             NOT NULL,
    [ModuleId] INT             NOT NULL,
    [Value]    DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK__ClientHe__3214EC07673C87BE] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ClientHea__Clien__47FBA9D6] FOREIGN KEY ([ClientId]) REFERENCES [master].[Clients] ([Id]),
    CONSTRAINT [FK__ClientHea__Heade__48EFCE0F] FOREIGN KEY ([HeaderId]) REFERENCES [master].[Headers] ([Id]),
    CONSTRAINT [FK__ClientHea__Modul__49E3F248] FOREIGN KEY ([ModuleId]) REFERENCES [subscriber].[Modules] ([Id])
);

