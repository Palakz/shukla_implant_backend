﻿CREATE TABLE [master].[ItemCrates] (
    [Id]           INT             IDENTITY (1, 1) NOT NULL,
    [ItemId]       INT             NOT NULL,
    [CrateId]      INT             NOT NULL,
    [CrateDeposit] DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK__ItemCrat__3214EC077C52D6C8] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ItemCrate__Crate__0CE5D100] FOREIGN KEY ([CrateId]) REFERENCES [master].[Crates] ([Id])
);

