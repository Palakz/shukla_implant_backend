﻿CREATE TABLE [master].[ItemSubCategories] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId]   INT           NOT NULL,
    [ItemCategoryId] INT           NOT NULL,
    [Name]           VARCHAR (50)  NOT NULL,
    [Rate]           VARCHAR (MAX) NULL,
    [Status]         TINYINT       NOT NULL,
    [EndDate]        SMALLDATETIME NULL,
    [IpAddress]      VARCHAR (45)  NULL,
    [CreatedBy]      INT           NOT NULL,
    [CreatedDate]    SMALLDATETIME NOT NULL,
    [UpdatedBy]      INT           NULL,
    [UpdatedDate]    SMALLDATETIME NULL,
    CONSTRAINT [PK__ItemSubC__3214EC077979C52A] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ItemSubCa__Creat__5CA1C101] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__ItemSubCa__ItemC__5BAD9CC8] FOREIGN KEY ([ItemCategoryId]) REFERENCES [master].[ItemCategories] ([Id]),
    CONSTRAINT [FK__ItemSubCa__Subsc__5AB9788F] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id]),
    CONSTRAINT [FK__ItemSubCa__Updat__5D95E53A] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id])
);

