﻿CREATE TABLE [master].[ClientItemRates] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT           NOT NULL,
    [ClientId]     INT           NOT NULL,
    [Status]       TINYINT       NOT NULL,
    [EndDate]      SMALLDATETIME NULL,
    [IpAddress]    VARCHAR (45)  NULL,
    [CreatedBy]    INT           NOT NULL,
    [CreatedDate]  SMALLDATETIME NOT NULL,
    [UpdatedBy]    INT           NULL,
    [UpdatedDate]  SMALLDATETIME NULL,
    CONSTRAINT [PK__ClientIt__3214EC07DFE6E3E6] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ClientIte__Clien__53385258] FOREIGN KEY ([ClientId]) REFERENCES [master].[Clients] ([Id]),
    CONSTRAINT [FK__ClientIte__Creat__542C7691] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__ClientIte__Subsc__52442E1F] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id]),
    CONSTRAINT [FK__ClientIte__Updat__55209ACA] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id])
);

