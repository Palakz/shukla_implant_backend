﻿CREATE TABLE [master].[Bases] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT           NOT NULL,
    [Name]         VARCHAR (100) NOT NULL,
    [Status]       TINYINT       NOT NULL,
    [EndDate]      SMALLDATETIME NULL,
    [IpAddress]    VARCHAR (45)  NULL,
    [CreatedBy]    INT           NOT NULL,
    [CreatedDate]  SMALLDATETIME NOT NULL,
    [UpdatedBy]    INT           NULL,
    [UpdatedDate]  SMALLDATETIME NULL,
    CONSTRAINT [PK__Bases__3214EC076D700203] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__Bases__CreatedBy__0169315C] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__Bases__Subscribe__00750D23] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id]),
    CONSTRAINT [FK__Bases__UpdatedBy__025D5595] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id])
);

