﻿CREATE TYPE [master].[ClientItemDiscountType] AS TABLE (
    [ItemTypeId]         INT             NOT NULL,
    [Discount]           DECIMAL (18, 2) NOT NULL,
    [AdditionalDiscount] DECIMAL (18, 2) NOT NULL);

