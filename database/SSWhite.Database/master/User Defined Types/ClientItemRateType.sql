﻿CREATE TYPE [master].[ClientItemRateType] AS TABLE (
    [ItemId] INT             NOT NULL,
    [Rate]   DECIMAL (18, 2) NOT NULL);

