﻿CREATE TABLE [emp].[PunchType] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50) NOT NULL,
    [IsDeleted]   BIT           CONSTRAINT [DF_PunchType_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]   BIGINT        NOT NULL,
    [CreatedDate] DATETIME      NOT NULL,
    [UpdateBy]    BIT           NULL,
    [UpdateDate]  DATETIME      NULL,
    CONSTRAINT [PK_PunchType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

