﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: EXEC  [emp].[GetEpoDetailsByEpoId]
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
USE [db_a5dc4b_sswhite]
 TEst Execution :	[emp].[ApproveOrDenyEpo] 
					@UserId =60,
					@EpoId =157,
					@IsApprove =1,
					@ApprovalTypeApproved =1,
					@ApprovalTypeDenied =8,
					@CurrentDate='2022-05-23 16:41:21.687'
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE  [emp].[ApproveOrDenyEpo]
(
	@UserId int,
	@EpoId int,
	@IsApprove bit,
	@ApprovalTypeApproved int,
	@ApprovalTypeDenied int,
	@CurrentDate datetime
)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	IF EXISTS (SELECT id FROM emp.EpoMpcVote WHERE EpoCreationId = @EpoId )
	BEGIN
	DECLARE @MpcNumber varchar(10), @IsHigherAuthorityMember varchar(10);
	Select @MpcNumber = MpcNumber from emp.EmpLimit where userid= @UserId and MPCMember = 'Y'
	Select @IsHigherAuthorityMember = IsHigherAuthorityMember  from emp.EmpLimit where userid= @userid and IsHigherAuthorityMember = 'Y'
		IF (@IsApprove = 1)
		BEGIN
			IF(@MpcNumber = 'MPC1')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set Mpc1Vote = 'Yes',
					Mpc1VoteDate = @CurrentDate,
					Mpc1Approval = @ApprovalTypeApproved,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
			END
			ELSE IF(@MpcNumber = 'MPC2')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set Mpc2Vote = 'Yes',
					Mpc2VoteDate = @CurrentDate,
					Mpc2Approval = @ApprovalTypeApproved,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
			END
			ELSE IF(@MpcNumber = 'MPC3')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set Mpc3Vote = 'Yes',
					Mpc3VoteDate = @CurrentDate,
					Mpc3Approval = @ApprovalTypeApproved,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
			END
			ELSE IF(@MpcNumber = 'MPC4')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set Mpc4Vote = 'Yes',
					Mpc4VoteDate = @CurrentDate,
					Mpc4Approval = @ApprovalTypeApproved,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
			END
			ELSE IF(@MpcNumber = 'MPC5')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set Mpc5Vote = 'Yes',
					Mpc5VoteDate = @CurrentDate,
					Mpc5Approval = @ApprovalTypeApproved,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
			END
			ELSE IF(@MpcNumber = 'MPC6')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set Mpc6Vote = 'Yes',
					Mpc6VoteDate = @CurrentDate,
					Mpc6Approval = @ApprovalTypeApproved,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
			END
			ELSE IF(@IsHigherAuthorityMember = 'Y')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set HigherAuthorityVote = 'Yes',
					HigherAuthorityVoteDate = @CurrentDate,
					HigherAuthorityApproval =  @ApprovalTypeApproved,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
				UPDATE 
					emp.EpoCreation 
				SET  MPCHigherApproval = @ApprovalTypeApproved
					,MPCHigherApproval_date = @CurrentDate
					WHERE id =@EpoId
			END
		END
		ELSE
		BEGIN
			IF(@MpcNumber = 'MPC1')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set Mpc1Vote = 'No',
					Mpc1VoteDate = @CurrentDate,
					Mpc1Approval = @ApprovalTypeDenied,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
			END
			ELSE IF(@MpcNumber = 'MPC2')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set Mpc2Vote = 'No',
					Mpc2VoteDate = @CurrentDate,
					Mpc2Approval = @ApprovalTypeDenied,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
			END
			ELSE IF(@MpcNumber = 'MPC3')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set Mpc3Vote = 'No',
					Mpc3VoteDate = @CurrentDate,
					Mpc3Approval = @ApprovalTypeDenied,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
			END
			ELSE IF(@MpcNumber = 'MPC4')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set Mpc4Vote = 'No',
					Mpc4VoteDate = @CurrentDate,
					Mpc4Approval = @ApprovalTypeDenied,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
			END
			ELSE IF(@MpcNumber = 'MPC5')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set Mpc5Vote = 'No',
					Mpc5VoteDate = @CurrentDate,
					Mpc5Approval = @ApprovalTypeDenied,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
			END
			ELSE IF(@MpcNumber = 'MPC6')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set Mpc6Vote = 'No',
					Mpc6VoteDate = @CurrentDate,
					Mpc6Approval = @ApprovalTypeDenied,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
			END
			ELSE IF(@IsHigherAuthorityMember = 'Y')
			BEGIN
				 UPDATE 
					[emp].[EpoMpcVote] 
				Set HigherAuthorityVote = 'NO',
					HigherAuthorityVoteDate = @CurrentDate,
					HigherAuthorityApproval = @ApprovalTypeDenied,
					Modifiedby = @userid,
					ModifiedDate = @CurrentDate
				WHERE 
					EpoCreationId = @EpoId
				UPDATE 
					emp.EpoCreation 
				SET  MPCHigherApproval = @ApprovalTypeDenied
					,MPCHigherApproval_date = @CurrentDate
					WHERE id =@EpoId
			END
		END

		IF EXISTS (SELECT id FROM emp.EpoMpcVote WHERE EpoCreationId = @EpoId AND Mpc1Vote = 'Yes' AND Mpc2Vote = 'Yes'AND Mpc3Vote = 'Yes'AND Mpc4Vote = 'Yes'AND Mpc5Vote = 'Yes'AND Mpc6Vote = 'Yes' )
		BEGIN
			IF Exists(SELECT id FROM emp.EpoCreation where id=@EpoId and (MPCHigherApproval IS NULL OR  MPCHigherApproval = 1))
			BEGIN
				UPDATE emp.EpoCreation 
				SET SelfApproval = @ApprovalTypeApproved ,ManagerApproval = @ApprovalTypeApproved
					,ManagerApprovalDate = @CurrentDate,MPCApproval =@ApprovalTypeApproved
					,MPCApprovalDate = @CurrentDate WHERE id =@EpoId
			END
			ELSE
			BEGIN
				UPDATE emp.EpoCreation SET  ManagerApproval = @ApprovalTypeApproved
					,ManagerApprovalDate = @CurrentDate,MPCApproval=@ApprovalTypeApproved
					,MPCApprovalDate = @CurrentDate WHERE id =@EpoId
			END
		END
	END 

	---Approval for manager and upper manager-------
	IF EXISTS (SELECT id FROM emp.EpoApprovalList WHERE EpoNo = @EpoId and userid =@UserId)
	BEGIN
		IF (@IsApprove = 1)
		BEGIN
		    UPDATE 
				[emp].[EpoCreation] 
			Set SelfApproval = @ApprovalTypeApproved,
				ManagerApproval = @ApprovalTypeApproved,
				ManagerApprovalDate = @CurrentDate
			WHERE 
				id = @EpoId
			UPDATE 
				[emp].[EpoApprovalList] 
			Set ApprovalType = @ApprovalTypeApproved,
				Modifiedby = @userid,
				modifiedDate = @CurrentDate
			WHERE 
				EpoNo = @EpoId and userid =@UserId
		END
		ELSE
		BEGIN
			UPDATE 
				[emp].[EpoCreation] 
			Set SelfApproval = @ApprovalTypeDenied,
				ManagerApproval = @ApprovalTypeDenied
			WHERE 
				id = @EpoId
			UPDATE 
				[emp].[EpoApprovalList] 
			Set ApprovalType = @ApprovalTypeDenied,
				Modifiedby = @userid,
				modifiedDate = @CurrentDate
			WHERE 
				EpoNo = @EpoId and userid =@UserId
		END
	END
	--else
	--begin
	--end
	END TRY
	BEGIN CATCH
		DECLARE
		@stringmsg VARCHAR(MAX),
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
			set  @ErrorNumber =  ERROR_NUMBER();
			set  @ErrorSeverity= ERROR_SEVERITY();
			set  @ErrorState =  ERROR_STATE();
			set  @ErrorMessage =   ERROR_MESSAGE();
			set  @ErrorLine =  ERROR_LINE();
			 set @stringmsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;

			 THROW 51000,@stringmsg , 1; 
	END CATCH;
END 

