﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[AddOrEditEpo]
Comments	: 26-04-2022 | amit| This procedure is used to Add New Employee.

Test Execution :	DECLARE @EpoAttachmentMstList  as  emp.EpoAttachmentMstType
					insert into  @EpoAttachmentMstList (AttachedFileName, Approval)
					values('file.name',0),
					('qwfile.name',0)
					DECLARE @EpoLineItemsList  as  [emp].[EpoLineItemsType]
					insert into @EpoLineItemsList
					([lineNo] ,[ProductDescription],[ProductGroup],[Qty] ,[UnitPrice],[Uom],[Department],[AccountNo] ,[Gujarat] ,[Tax],[Discount] ,[UnitPriceQty],[QtyUnitPriceTax]
					,[TotalCostDiscount] ,[Gst],[Cgst] ,[Sgst] ,[Igst] ,[ExtendedCost] ,[Purchase] ,[DueDate],[CgstValue],[SgstValue],[TaxValue] ,[IgstValue],[CurrencyType])  
					values(1,'Product desc',1,1,12,1,1,1,1,12,10,100,101,10,2,1,1,0,101,'purchase',getdate(),12,12,24,0,1),
					(2,'Product desc',1,1,12,1,1,1,1,12,10,100,101,10,2,1,1,0,101,'purchase',getdate(),12,12,24,0,1)
					EXEC [emp].[AddOrEditEpo]
					@Id = null,
					@UserId = 42,
					@EmployeeVendor= 'Vendor',
					@EmployeeId ='asd123',
					@Vendor = 'PATEL TRADING COMPANY',
					@VendorId = 'PAT12',
					@Street1 = 'street1',
					@Street2 = 'street2',
					@Country = 1,
					@State = 'gujarat',
					@City = 'rajkot',
					@Zip = '363002',
					@Phone = '9876789099',
					@Fax = '123',
					@GstNo = '24AGWPP5605Q1ZI',
					@Email = 'asfd@gmail.com',
					@PurposeOfPurchase = 'Trial'	,
					@EpoType = 1,
					@ReceiptMethod = 1,
					@EpoPurchaseStatus = 'I am the user of this product to be purchased.',
					@PaymentType = 1,
					@PurchaseNotes = 'notes',
					@InternalNotes = 'notes',
					@SubTotal = 100.00,
					@GrandTotal = 20022000.20,
					@DateOfPaymentCheckRequestOrCashAdvance = '2022-05-04 05:41:21.687',
					@DaysOfPaymentPayWhenBilled ='18',
					@CurrencyType = 1,
					@DollarPrice = 77.89,
					@TotalSgst= 12,
					@TotalCgst= 12,
					@TotalIgst= 12,
					@IsWitinGujarat = 1,
					@StatusTypeActive = 1,
					@CreatedDate = '2022-06-04 05:41:21.687' ,
					@IsAttachmentPending = 0,
					--@ModifiedBy  = null,
					--@ModifiedDate  = null,
					@currentprice = 20022000.20,
					@ApprovalTypeApproved =1,
					@ApprovalTypePending  =2,
					@ApprovalTypeSelfApproved =3,
					@ApprovalTypeManagerApproval =4,
					@ApprovalTypeHigherManagerApproval =5,
					@ApprovalTypeMPCApproval =6,
					@ApprovalTypeHigherAuthorityApprovalAndMPCApproval =7,
					@EpoLineItemsList = @EpoLineItemsList,
					@EpoAttachmentMstList = @EpoAttachmentMstList
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [emp].[AddOrEditEpo]
(
	@Id int ,
	@UserId int ,
	@EmployeeVendor nvarchar(50),
	@EmployeeId nvarchar(50),
	@Vendor nvarchar(max),
	@VendorId nvarchar(50),
	@Street1 nvarchar(max),
	@Street2 nvarchar(max),
	@Country int,
	@State nvarchar(500),
	@City nvarchar(500),
	@Zip nvarchar(100),
	@Phone nvarchar(50),
	@Fax nvarchar(50),
	@GstNo nvarchar(100),
	@Email nvarchar(50),
	@PurposeOfPurchase nvarchar(max),
	@EpoType int,
	@ReceiptMethod int,
	-- I am the user of this product to be purchased.
	@EpoPurchaseStatus  nvarchar(500),
	@PaymentType int,
	@PurchaseNotes nvarchar(max),
	@InternalNotes nvarchar(max),
	@SubTotal decimal(18, 2),
	@GrandTotal decimal(18, 2),
	@DateOfPaymentCheckRequestOrCashAdvance datetime,
	@DaysOfPaymentPayWhenBilled nvarchar(100),
	@CurrencyType int,
	@StatusTypeActive int,
	@CreatedDate  DateTime ,
	@IsAttachmentPending  bit,
	@currentprice decimal(18,6),
	@DollarPrice decimal(18,6),
	@TotalSgst decimal(18,6),
	@TotalCgst decimal(18,6),
	@TotalIgst decimal(18,6),
	@IsWitinGujarat bit,
	--@ModifiedBy  int,
	--@ModifiedDate  DateTime,
	@ApprovalTypeApproved int,
	@ApprovalTypePending  int,
	@ApprovalTypeSelfApproved int,
	@ApprovalTypeManagerApproval int,
	@ApprovalTypeHigherManagerApproval int,
	@ApprovalTypeMPCApproval int,
	@ApprovalTypeHigherAuthorityApprovalAndMPCApproval int,
	@EpoLineItemsList [emp].[EpoLineItemsType] READONLY,
	@EpoAttachmentMstList [emp].[EpoAttachmentMstType] READONLY
)
AS
BEGIN
	SET NOCOUNT ON;

	
	BEGIN TRY
		Begin tran

		DECLARE @LastEpoId int, @InsertedId int; 
		select @LastEpoId = ISNULL(max(Id) + 1,1) from [emp].[EpoCreation] 
		set @LastEpoId = @LastEpoId;

		   DECLARE @EpoApprovalMessage nvarchar(max),@personId varchar(100), @personEmailId varchar(100), @MpcEmailId varchar(100) ,@ManagerEmailId varchar(100) 
		   ,@managerPersonId varchar(100), @PersonName varchar(100), @MPClimit decimal(18,6) = 100000 ,@UpperManagerEmailId varchar(100),
		   	@upperManagerPersonId varchar(100), @personLimit decimal(18,6), @managerLimit decimal(18,6),@upperManagerLimit decimal(18,6), @managernew varchar(100)
			,@ApprovalManager varchar(100) ,@ApprovalEmailIds varchar(max), @uppermanager varchar(100),@ManagerUserId int , @UpperManagerUserId int ;  
		  
		  select @personId  = personId,@personEmailId = EmailAddress  from emp.UserMaster where id=@UserId
		   select @personLimit = EmpLimitNew  from emp.EmpLimit where employeeid = @personId
		   select @managernew = ReportingUnder, @PersonName = PersonName,@personEmailId = u.EmailAddress   from emp.VMSEmployeeDetails 
					v inner join emp.usermaster u on u.personid=v.personid  where v.PersonID = @personId
		   select @uppermanager = ReportingUnder,@ManagerEmailId = u.EmailAddress,@ManagerUserId =u.id  from emp.VMSEmployeeDetails v inner join emp.usermaster u on u.personid=v.personid
					where PersonName = @managernew
		   select @UpperManagerEmailId = u.EmailAddress,@UpperManagerUserId = u.id from emp.VMSEmployeeDetails v inner join emp.usermaster u on u.personid=v.personid 
			where PersonName = @uppermanager
		   select @managerLimit = EmpLimitNew  from emp.EmpLimit  e  inner join emp.VMSEmployeeDetails vms on vms.PersonId = e.EmployeeId 
		   where personName = @managernew
		   select @upperManagerLimit = EmpLimitNew  from emp.EmpLimit e inner join emp.VMSEmployeeDetails vms on vms.PersonId = e.EmployeeId
		   where personName = @uppermanager
		
		IF (@CurrencyType = 2)
		BEGIN
		       --set @currentprice = (@currentprice / @DollarPrice) ;
		       --set @GrandTotal = (@GrandTotal / @DollarPrice) ;
		       set @personlimit	 = (@personlimit / @DollarPrice) ;
		       set @managerlimit = (@managerlimit / @DollarPrice) ;
		       set @MPClimit	 = (@MPClimit	 / @DollarPrice) ;
		END
		IF (@CurrencyType = 1)
		BEGIN
		       set @DollarPrice = 0;
		END

		IF(@Id IS NULL)
		BEGIN
		
		INSERT INTO [emp].[EpoCreation]
           ( EId
			,PId
			,GId
			,LId
			,EPOId
			,EmployeeVendor
			,EmployeeId
			,Vendor
			,VendorId
			,Street1
			,Street2
			,Country
			,State
			,City
			,Zip
			,Phone
			,Fax
			,GstNo
			,Email
			,PurposeOfPurchase
			,EpoType
			,ReceiptMethod
			,EpoPurchaseStatus
			,PaymentType
			,PurchaseNotes
			,InternalNotes
			,SubTotal
			,GrandTotal
			,EmployeeName
			,DateOfPaymentCheckRequestOrCashAdvance
			,DaysOfPaymentPayWhenBilled
			,CurrencyType
			,IsAttachmentPending
			,[Status]
			,CreatedBy
			,CreatedDate
			,DollarPrice
			,TotalSgst
			,TotalCgst
			,TotalIgst
			,IsWitinGujarat
           )
       VALUES
           (
			@LastEpoId
			,1--,@PId
			,1--,@GId
			,1--,@LId
			,CONCAT(@LastEpoId,'DJ') --,@EPOId
			,@EmployeeVendor
			,@EmployeeId
			,@Vendor
			,@VendorId
			,@Street1
			,@Street2
			,@Country
			,@State
			,@City
			,@Zip
			,@Phone
			,@Fax
			,@GstNo
			,@Email
			,@PurposeOfPurchase
			,@EpoType
			,@ReceiptMethod
			,@EpoPurchaseStatus
			,@PaymentType
			,@PurchaseNotes
			,@InternalNotes
			,@SubTotal
			,@GrandTotal
			,@PersonName
			,@DateOfPaymentCheckRequestOrCashAdvance
			,@DaysOfPaymentPayWhenBilled
			,@CurrencyType
			,@IsAttachmentPending
			,@StatusTypeActive
			,@UserId
			,@CreatedDate
			,@DollarPrice
			,@TotalSgst
			,@TotalCgst
			,@TotalIgst
			,@IsWitinGujarat
		   )
		   
		 
		   SET @InsertedId = SCOPE_IDENTITY();
		   IF (@currentprice <= @personlimit)
		   BEGIN
		      UPDATE [emp].[EpoCreation] SET SelfApproval = @ApprovalTypeApproved, ApprovedBy = @PersonName, SelfDate = @CreatedDate Where id = @InsertedId
			  SET @EpoApprovalMessage = 'EPO has been self approved.' 
		   END
		   ELSE IF((@currentprice > @personlimit) AND (@currentprice <= @managerlimit))
		   BEGIN
		      UPDATE [emp].[EpoCreation] SET SelfApproval = @ApprovalTypePending, ManagerApproval = @ApprovalTypeManagerApproval, SelfDate = @CreatedDate Where id = @InsertedId;
			  insert into  emp.EpoApprovalList(UserId,epono,approvalType,createdby,createddate) values(@ManagerUserId,@InsertedId, @ApprovalTypeManagerApproval,@userid,@Createddate)
			 SET @EpoApprovalMessage = 'EPO was submitted for approval' 
			  SET @ApprovalEmailIds = @ManagerEmailId; 
		   END
		   ELSE IF(@currentprice > @managerlimit AND @currentprice < @MPClimit)
		   BEGIN
		      UPDATE [emp].[EpoCreation] SET SelfApproval = @ApprovalTypePending, ManagerApproval = @ApprovalTypeHigherManagerApproval, SelfDate = @CreatedDate Where id = @InsertedId
			  insert into  emp.EpoApprovalList(UserId,epono,approvalType,createdby,createddate) values(@UpperManagerUserId,@InsertedId, @ApprovalTypeManagerApproval,@userid,@Createddate)
			  SET @EpoApprovalMessage = 'EPO was submitted for approval' 
			  SET @ApprovalEmailIds = concat(@ManagerEmailId,',',@UpperManagerEmailId); 
		   END
		   ELSE IF(@currentprice > @managerlimit AND @currentprice <= @MPClimit)
		   BEGIN
		      UPDATE [emp].[EpoCreation] SET SelfApproval = @ApprovalTypePending, ManagerApproval = @ApprovalTypeMPCApproval,MPCApproval=@ApprovalTypePending, SelfDate = @CreatedDate Where id = @InsertedId;
			  SET @EpoApprovalMessage = 'EPO was submitted for approval';
			  INSERT INTO [emp].[EpoMpcVote] (EpoCreationId,Pid, Epo,Mpc1Vote,Mpc2Vote,Mpc3Vote,Mpc4Vote,Mpc5Vote,Mpc6Vote,CountVote,NeedHigherAuthority,Status,CreatedDate,CreatedBy)
			  values (@InsertedId,1,@InsertedId,'No', 'No', 'No', 'No', 'No', 'No', 0,'No',@StatusTypeActive,@CreatedDate,@UserId);
			SELECT @ApprovalEmailIds = STRING_AGG(u.EmailAddress, ',') FROM emp.EmpLimit e inner join emp.UserMaster u on u.PersonId =e.EmployeeId  where MPCMember = 'Y'
		   END
			ELSE IF(@currentprice > @MPClimit)
			BEGIN
		      UPDATE [emp].[EpoCreation] SET SelfApproval = @ApprovalTypePending, ManagerApproval = @ApprovalTypeHigherAuthorityApprovalAndMPCApproval,MPCApproval=@ApprovalTypePending,MPCHigherApproval=@ApprovalTypePending,
			  SelfDate = @CreatedDate Where id = @InsertedId;
			  SET @EpoApprovalMessage = 'EPO was submitted for approval' 
			  INSERT INTO [emp].[EpoMpcVote] (EpoCreationId,Pid, Epo,Mpc1Vote,Mpc2Vote,Mpc3Vote,Mpc4Vote,Mpc5Vote,Mpc6Vote,CountVote,NeedHigherAuthority,HigherAuthorityVote,Status,CreatedDate, CreatedBy)
			  values (@InsertedId,1,@InsertedId,'No', 'No', 'No', 'No', 'No', 'No', 0,'Yes', 'No',@StatusTypeActive,@CreatedDate,@UserId);
			SELECT @ApprovalEmailIds = STRING_AGG(u.EmailAddress, ',') FROM emp.EmpLimit e inner join emp.UserMaster u on u.PersonId =e.EmployeeId  
				where  (e.MPCMember = 'Y' or e.IsHigherAuthorityMember = 'Y')
		   END

		   --Insert into log table--
		INSERT INTO emp.EpoCreationLog(EpoCreationTableId
			,EId
			,PId
			,GId
			,LId
			,EPOId
			,EmployeeVendor
			,Vendor
			,VendorId
			,Street1
			,Street2
			,Country
			,State
			,City
			,Zip
			,Phone
			,Fax
			,GstNo
			,Email
			,PurposeOfPurchase
			,EpoType
			,ReceiptMethod
			,EpoPurchaseStatus
			,PaymentType
			,PurchaseNotes
			,InternalNotes
			,SubTotal
			,GrandTotal
			,EmployeeName
			,DateOfPaymentCheckRequestOrCashAdvance
			,DaysOfPaymentPayWhenBilled
			,SelfApproval
			,ManagerApproval
			,MPCApproval
			,MPCHigherApproval
			,SelfDate
			,ManagerApprovalDate
			,MPCApprovalDate
			,MPCHigherApproval_date
			,QtyStatus
			,revised_date
			,ApprovedBy
			,CurrencyType
			,IsAttachmentPending
			,Status
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,EmployeeId
			,DollarPrice
			,TotalSgst
			,TotalCgst
			,TotalIgst
			,IsWitinGujarat)
		SELECT @InsertedId
			,EId
			,PId
			,GId
			,LId
			,EPOId
			,EmployeeVendor
			,Vendor
			,VendorId
			,Street1
			,Street2
			,Country
			,State
			,City
			,Zip
			,Phone
			,Fax
			,GstNo
			,Email
			,PurposeOfPurchase
			,EpoType
			,ReceiptMethod
			,EpoPurchaseStatus
			,PaymentType
			,PurchaseNotes
			,InternalNotes
			,SubTotal
			,GrandTotal
			,EmployeeName
			,DateOfPaymentCheckRequestOrCashAdvance
			,DaysOfPaymentPayWhenBilled
			,SelfApproval
			,ManagerApproval
			,MPCApproval
			,MPCHigherApproval
			,SelfDate
			,ManagerApprovalDate
			,MPCApprovalDate
			,MPCHigherApproval_date
			,QtyStatus
			,revised_date
			,ApprovedBy
			,CurrencyType
			,IsAttachmentPending
			,Status
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,EmployeeId
			,DollarPrice
			,TotalSgst
			,TotalCgst
			,TotalIgst
			,IsWitinGujarat
		FROM emp.EpoCreation
		WHERE  id=@InsertedId;

		INSERT INTO [emp].[EpoLineItems] (
			EpoNo
			,[lineNo]
			,PID
			,ProductDescription
			,ProductGroup
			,Qty
			,UnitPrice
			,Uom
			,Department
			,AccountNo
			,IsWitinGujarat
			,Tax
			,Discount
			,UnitPriceQty
			,QtyUnitPriceTax
			,TotalCostDiscount
			,Gst
			,Cgst
			,Sgst
			,Igst
			,ExtendedCost
			,Purchase
			,DueDate
			,CgstValue
			,SgstValue
			,TaxValue
			,IgstValue
			,CurrencyType
			,Status
			,CreatedBy
			,CreatedDate
		)
		(SELECT
			@InsertedId
			,eli.[lineNo]
			,@UserId 
			,eli.ProductDescription
			,eli.ProductGroup
			,eli.Qty
			,eli.UnitPrice
			,eli.Uom
			,eli.Department
			,eli.AccountNo
			,eli.gujarat
			,eli.Tax
			,eli.Discount
			,eli.UnitPriceQty
			,eli.QtyUnitPriceTax
			,eli.TotalCostDiscount
			,eli.Gst
			,eli.Cgst
			,eli.Sgst
			,eli.Igst
			,eli.ExtendedCost
			,eli.Purchase
			,eli.DueDate
			,eli.CgstValue
			,eli.SgstValue
			,eli.TaxValue
			,eli.IgstValue
			,eli.CurrencyType
			,@StatusTypeActive
			,@UserId
			,@CreatedDate
		FROM
			@EpoLineItemsList AS eli)
		
		---Log item table
		INSERT INTO emp.EpoLineItemsLog(
		EpoNo
		,[lineNo]
		,PID
		,ProductDescription
		,ProductGroup
		,Qty
		,UnitPrice
		,Uom
		,Department
		,AccountNo
		,IsWitinGujarat
		,Tax
		,Discount
		,UnitPriceQty
		,QtyUnitPriceTax
		,TotalCostDiscount
		,Gst
		,Cgst
		,Sgst
		,Igst
		,ExtendedCost
		,Purchase
		,DueDate
		,CgstValue
		,SgstValue
		,TaxValue
		,IgstValue
		,CurrencyType
		,Status
		,CreatedBy
		,CreatedDate
		,ModifiedBy
		,ModifiedDate)
		SELECT 
			EpoNo
			,[lineNo]
			,PID
			,ProductDescription
			,ProductGroup
			,Qty
			,UnitPrice
			,Uom
			,Department
			,AccountNo
			,IsWitinGujarat
			,Tax
			,Discount
			,UnitPriceQty
			,QtyUnitPriceTax
			,TotalCostDiscount
			,Gst
			,Cgst
			,Sgst
			,Igst
			,ExtendedCost
			,Purchase
			,DueDate
			,CgstValue
			,SgstValue
			,TaxValue
			,IgstValue
			,CurrencyType
			,Status
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate FROM emp.EpoLineItems
		WHERE  EpoNo=@InsertedId;

		INSERT INTO emp.EpoAttachmentMst(
			MID
			,EmployeeId
			,UserId
			,AttachedFileName
			,AttachmentDate
			--,Title
			,approval
			,AttachmentType
			,EPONo
			,Status
			,CreatedBy
			,CreatedDate
		)
		(SELECT 
			1
			,@personId
			,@UserId
			,AttachedFileName
			,@CreatedDate
			--,Title
			,approval
			,'ECN'
			,@InsertedId
			,@StatusTypeActive
			,@UserId
			,@CreatedDate
		FROM 
			@EpoAttachmentMstList)

		END
---------------------------------------------------UPDATE -------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		ELSE
		BEGIN
			UPDATE [emp].[EpoCreation]
           SET
			EmployeeVendor = @EmployeeVendor
			,EmployeeId = @EmployeeId
			,Vendor	  = @Vendor
			,VendorId = @VendorId
			,Street1 = @Street1
			,Street2 = @Street2
			,Country = @Country
			,State	 = @State
			,City	 = @City
			,Zip   = @Zip
			,Phone = @Phone
			,Fax   = @Fax
			,GstNo = @GstNo
			,Email = @Email
			,PurposeOfPurchase = @PurposeOfPurchase
			,EpoType		   = @EpoType
			,ReceiptMethod	   = @ReceiptMethod
			,EpoPurchaseStatus = @EpoPurchaseStatus
			,PaymentType	   = @PaymentType
			,PurchaseNotes	   = @PurchaseNotes
			,InternalNotes	   = @InternalNotes
			,SubTotal		   = @SubTotal
			,GrandTotal		   = @GrandTotal
			,EmployeeName	   = @PersonName
			,DateOfPaymentCheckRequestOrCashAdvance = @DateOfPaymentCheckRequestOrCashAdvance
			,DaysOfPaymentPayWhenBilled = @DaysOfPaymentPayWhenBilled
			,CurrencyType  = @CurrencyType
			,DollarPrice  = @DollarPrice
			,TotalSgst = @TotalSgst
			,TotalCgst	= @TotalCgst
			,TotalIgst	= @TotalIgst
			,IsWitinGujarat = @IsWitinGujarat
			,IsAttachmentPending  = IsAttachmentPending
			,ModifiedBy	   = @UserId
			,ModifiedDate  = @CreatedDate
			,ManagerApproval = NULL 
			,SelfApproval = NULL
			,SelfDate = NULL
			,ApprovedBy = NULL
		 Where Id = @Id
		   
			  DELETE FROM  [emp].[EpoMpcVote]  where EpoCreationId = @Id
			  DELETE FROM  [emp].[EpoApprovalList]  where EpoNo = @Id
		  
		   IF (@currentprice <= @personlimit)
		   BEGIN
		      UPDATE [emp].[EpoCreation] SET SelfApproval = @ApprovalTypeApproved, ApprovedBy = @PersonName, SelfDate = @CreatedDate  Where Id = @Id
			  SET @EpoApprovalMessage = 'EPO has been self approved.' 
		   END
		   ELSE IF((@currentprice > @personlimit) AND (@currentprice <= @managerlimit))
		   BEGIN
		      UPDATE [emp].[EpoCreation] SET SelfApproval = @ApprovalTypePending, ManagerApproval = @ApprovalTypeManagerApproval, SelfDate = @CreatedDate Where id = @Id;
			  insert into  emp.EpoApprovalList(UserId,epono,approvalType,createdby,createddate)
			  values(@ManagerUserId,@Id, @ApprovalTypeManagerApproval,@userid,@Createddate)
			  SET @EpoApprovalMessage = 'EPO was submitted for approval' 
			   SET @ApprovalEmailIds = @ManagerEmailId; 
		   END
		   ELSE IF(@currentprice > @managerlimit AND @currentprice < @MPClimit)
		   BEGIN
		      UPDATE [emp].[EpoCreation] SET SelfApproval = @ApprovalTypePending, ManagerApproval = @ApprovalTypeHigherManagerApproval, SelfDate = @CreatedDate Where id = @Id
			  insert into  emp.EpoApprovalList(UserId,epono,approvalType,createdby,createddate) 
			  values(@UpperManagerUserId,@Id, @ApprovalTypeManagerApproval,@userid,@Createddate)
			  SET @EpoApprovalMessage = 'EPO was submitted for approval' 
			  SET @ApprovalEmailIds = concat(@ManagerEmailId,',',@UpperManagerEmailId); 
		   END
		   ELSE IF(@currentprice > @managerlimit AND @currentprice <= @MPClimit)
		   BEGIN
		      UPDATE [emp].[EpoCreation] SET SelfApproval = @ApprovalTypePending, ManagerApproval = @ApprovalTypeMPCApproval,MPCApproval=@ApprovalTypePending,MPCHigherApproval=@ApprovalTypePending, SelfDate = @CreatedDate Where id = @Id;
			  SET @EpoApprovalMessage = 'EPO was submitted for approval';
			  INSERT INTO [emp].[EpoMpcVote] (EpoCreationId,Pid, Epo,Mpc1Vote,Mpc2Vote,Mpc3Vote,Mpc4Vote,Mpc5Vote,Mpc6Vote,CountVote,NeedHigherAuthority,Status,ModifiedDate,ModifiedBy)
			  values (@Id,1,@Id,'No', 'No', 'No', 'No', 'No', 'No', 0,'No',@StatusTypeActive,@CreatedDate,@UserId) 
			  SELECT @ApprovalEmailIds = STRING_AGG(u.EmailAddress, ',') FROM emp.EmpLimit e inner join emp.UserMaster u on u.PersonId =e.EmployeeId  where MPCMember = 'Y'
		   END
			ELSE IF(@currentprice > @MPClimit)
			BEGIN
		      UPDATE [emp].[EpoCreation] SET SelfApproval = @ApprovalTypePending, ManagerApproval = @ApprovalTypeHigherAuthorityApprovalAndMPCApproval,MPCApproval=@ApprovalTypePending,MPCHigherApproval=@ApprovalTypePending, SelfDate = @CreatedDate Where id = @Id;
			  SET @EpoApprovalMessage = 'EPO was submitted for approval' 
			  INSERT INTO [emp].[EpoMpcVote] (EpoCreationId,Pid, Epo,Mpc1Vote,Mpc2Vote,Mpc3Vote,Mpc4Vote,Mpc5Vote,Mpc6Vote,CountVote,NeedHigherAuthority,HigherAuthorityVote,Status,ModifiedDate,ModifiedBy)
			  values (@Id,1,@Id,'No', 'No', 'No', 'No', 'No', 'No', 0,'Yes', 'No',@StatusTypeActive,@CreatedDate,@UserId);
			  SELECT @ApprovalEmailIds = STRING_AGG(u.EmailAddress, ',') FROM emp.EmpLimit e inner join emp.UserMaster u on u.PersonId =e.EmployeeId  
				where  (e.MPCMember = 'Y' or e.IsHigherAuthorityMember = 'Y')
		   END

		      --Insert into log table--
		INSERT INTO emp.EpoCreationLog(EpoCreationTableId
			,EId
			,PId
			,GId
			,LId
			,EPOId
			,EmployeeVendor
			,Vendor
			,VendorId
			,Street1
			,Street2
			,Country
			,State
			,City
			,Zip
			,Phone
			,Fax
			,GstNo
			,Email
			,PurposeOfPurchase
			,EpoType
			,ReceiptMethod
			,EpoPurchaseStatus
			,PaymentType
			,PurchaseNotes
			,InternalNotes
			,SubTotal
			,GrandTotal
			,EmployeeName
			,DateOfPaymentCheckRequestOrCashAdvance
			,DaysOfPaymentPayWhenBilled
			,SelfApproval
			,ManagerApproval
			,MPCApproval
			,MPCHigherApproval
			,SelfDate
			,ManagerApprovalDate
			,MPCApprovalDate
			,MPCHigherApproval_date
			,QtyStatus
			,revised_date
			,ApprovedBy
			,CurrencyType
			,IsAttachmentPending
			,Status
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,EmployeeId
			,DollarPrice
			,TotalSgst
			,TotalCgst
			,TotalIgst
			,IsWitinGujarat
			)
		SELECT @Id
			,EId
			,PId
			,GId
			,LId
			,EPOId
			,EmployeeVendor
			,Vendor
			,VendorId
			,Street1
			,Street2
			,Country
			,State
			,City
			,Zip
			,Phone
			,Fax
			,GstNo
			,Email
			,PurposeOfPurchase
			,EpoType
			,ReceiptMethod
			,EpoPurchaseStatus
			,PaymentType
			,PurchaseNotes
			,InternalNotes
			,SubTotal
			,GrandTotal
			,EmployeeName
			,DateOfPaymentCheckRequestOrCashAdvance
			,DaysOfPaymentPayWhenBilled
			,SelfApproval
			,ManagerApproval
			,MPCApproval
			,MPCHigherApproval
			,SelfDate
			,ManagerApprovalDate
			,MPCApprovalDate
			,MPCHigherApproval_date
			,QtyStatus
			,revised_date
			,ApprovedBy
			,CurrencyType
			,IsAttachmentPending
			,Status
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,EmployeeId
			,DollarPrice 
			,TotalSgst
			,TotalCgst
			,TotalIgst
			,IsWitinGujarat
			FROM emp.EpoCreation
		WHERE  id=@Id;

		    DELETE FROM  [emp].[EpoLineItems] where EpoNo = @Id
		   	INSERT INTO [emp].[EpoLineItems] (
			EpoNo
			,[lineNo]
			,PID
			,ProductDescription
			,ProductGroup
			,Qty
			,UnitPrice
			,Uom
			,Department
			,AccountNo
			,IsWitinGujarat
			,Tax
			,Discount
			,UnitPriceQty
			,QtyUnitPriceTax
			,TotalCostDiscount
			,Gst
			,Cgst
			,Sgst
			,Igst
			,ExtendedCost
			,Purchase
			,DueDate
			,CgstValue
			,SgstValue
			,TaxValue
			,IgstValue
			,CurrencyType
			,[Status]
			,ModifiedBy
			,ModifiedDate
		)
		(SELECT
			@Id
			,eli.[lineNo]
			,@UserId 
			,eli.ProductDescription
			,eli.ProductGroup
			,eli.Qty
			,eli.UnitPrice
			,eli.Uom
			,eli.Department
			,eli.AccountNo
			,eli.gujarat
			,eli.Tax
			,eli.Discount
			,eli.UnitPriceQty
			,eli.QtyUnitPriceTax
			,eli.TotalCostDiscount
			,eli.Gst
			,eli.Cgst
			,eli.Sgst
			,eli.Igst
			,eli.ExtendedCost
			,eli.Purchase
			,eli.DueDate
			,eli.CgstValue
			,eli.SgstValue
			,eli.TaxValue
			,eli.IgstValue
			,eli.CurrencyType
			,@StatusTypeActive
			,@UserId
			,@CreatedDate
		FROM
			@EpoLineItemsList AS eli)
		
		---Log item table
		INSERT INTO emp.EpoLineItemsLog(
		EpoNo
		,[lineNo]
		,PID
		,ProductDescription
		,ProductGroup
		,Qty
		,UnitPrice
		,Uom
		,Department
		,AccountNo
		,IsWitinGujarat
		,Tax
		,Discount
		,UnitPriceQty
		,QtyUnitPriceTax
		,TotalCostDiscount
		,Gst
		,Cgst
		,Sgst
		,Igst
		,ExtendedCost
		,Purchase
		,DueDate
		,CgstValue
		,SgstValue
		,TaxValue
		,IgstValue
		,CurrencyType
		,Status
		,CreatedBy
		,CreatedDate
		,ModifiedBy
		,ModifiedDate)
		SELECT EpoNo
			,[lineNo]
			,PID
			,ProductDescription
			,ProductGroup
			,Qty
			,UnitPrice
			,Uom
			,Department
			,AccountNo
			,IsWitinGujarat
			,Tax
			,Discount
			,UnitPriceQty
			,QtyUnitPriceTax
			,TotalCostDiscount
			,Gst
			,Cgst
			,Sgst
			,Igst
			,ExtendedCost
			,Purchase
			,DueDate
			,CgstValue
			,SgstValue
			,TaxValue
			,IgstValue
			,CurrencyType
			,Status
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate FROM emp.EpoLineItems
		WHERE  EpoNo=@Id;

		DELETE FROM  emp.EpoAttachmentMst where EpoNo = @Id
		INSERT INTO emp.EpoAttachmentMst(
			MID
			,EmployeeId
			,UserId
			,AttachedFileName
			,AttachmentDate
			--,Title
			,approval
			,AttachmentType
			,EPONo
			,Status
			,ModifiedBy
			,ModifiedDate
		)
		(SELECT 
			1
			,@personId
			,@UserId
			,AttachedFileName
			,@CreatedDate
			--,Title
			,approval
			,'ECN'
			,@Id
			,@StatusTypeActive
			,@UserId
			,@CreatedDate
		FROM 
			@EpoAttachmentMstList)

			SET @InsertedId = @Id
		END
		COMMIT

			Select 
				@EpoApprovalMessage as EpoApprovalMessage,
				@InsertedId as EpoID,
				@PersonName as CreatedBy,
				@GrandTotal as GrandTotal,
				@PurposeOfPurchase as PurposeOfPurchase,
				@personEmailId As PersonEmailId,
				@ApprovalEmailIds as ApprovalEmailIds,
				@IsAttachmentPending as IsAttachmentPending,
				@CurrencyType as CurrencyType
    END TRY
	BEGIN CATCH
		Rollback
		DECLARE
		@stringmsg VARCHAR(MAX),
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
			set  @ErrorNumber =  ERROR_NUMBER();
			set  @ErrorSeverity= ERROR_SEVERITY();
			set  @ErrorState =  ERROR_STATE();
			set  @ErrorMessage =   ERROR_MESSAGE();
			set  @ErrorLine =  ERROR_LINE();
			 set @stringmsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;

			 THROW 51000,@stringmsg , 1; 
	END CATCH;
End;
