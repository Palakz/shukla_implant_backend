﻿  
/*------------------------------------------------------------------------------------------------------------    
Procedure : [emp].[CreateAttendanceFTO]    
Comments : 23-05-2022 | Amit| This procedure is used to Add New Attendance FTO Request.    
    
Test Execution :	DECLARE @Currentdate1 datetime =getdate()
					--SELECT @Currentdate1
					EXEC   [emp].[CreateAttendanceFTO]   
							@UserID =41,
							@EmployeeID ="emp2",
							@EmployeeName ='Shivam Dubey' ,
							@StartDate ='2022-05-29' ,
							@StartDateDayType = 1 ,
							@EndDate ='2022-05-30 ' ,
							@EndDateDayType  =1 ,
							@LeaveTypeID =1 ,
							@OtherLeaveTypeId =null,
							@EmployeeComment =null,
							@ApprovalTypePending =2 ,
							@StatusTypeActive =1,
							@CurrentDate =@Currentdate1
--------------------------------------------------------------------------------------------------------------*/    
ALTER PROCEDURE [emp].[CreateAttendanceFTO]    
(    
	@UserID INT,
	@EmployeeID NVARCHAR(50),
	@EmployeeName NVARCHAR(500) ,
	@StartDate DATE,
	@StartDateDayType TINYINT ,
	@EndDate DATE,
	@EndDateDayType TINYINT,
	@LeaveTypeID INT,
	@OtherLeaveTypeId INT,
	@EmployeeComment NVARCHAR (MAX),
	@ApprovalTypePending INT,
	@StatusTypeActive INT,
	@CurrentDate DATETIME
)    
AS    
BEGIN    
	SET NOCOUNT ON;    
	BEGIN TRY    
		Declare @ReportingUnder VARCHAR(100),
			   @SupervisorId INT,
			   @InsertedId INT,
			   @HRUserId INT,
			   @UserName VARCHAR(1000),
			   @EmailIdsForFtoNotification VARCHAR(1000),
			   @EmailIdForFtoApproval VARCHAR(100);

		SELECT
			@ReportingUnder = ReportingUnder 
		FROM 
			emp.VMSEmployeeDetails
		WHERE 
			UserId = @UserId
		
		SELECT 
			@SupervisorId = userID 
		FROM 
			emp.VMSEmployeeDetails 
		WHERE
			PersonName = @ReportingUnder

		INSERT INTO [emp].[AttendanceFTO]    
		( 
			UserID,
			EmployeeID,
			EmployeeName,
			StartDate,
			StartDateDayType,
			EndDate,
			EndDateDayType,
			LeaveTypeID,
			OtherLeaveTypeId,
			EmployeeComment,
			SupervisorName,
			SupervisorUserId,
			SupervisorApproval,
			HRApproval,
			MPCApproval,
			Approval,
			[Status],
			CreatedBy,
			CreatedDate
		)    
		VALUES    
		( 
			@UserID,
			@EmployeeID,
			@EmployeeName,
			@StartDate,
			@StartDateDayType,
			@EndDate,
			@EndDateDayType,
			@LeaveTypeID,
			@OtherLeaveTypeId,
			@EmployeeComment,
			@ReportingUnder,
			@SupervisorId,
			@ApprovalTypePending,
			@ApprovalTypePending,
			@ApprovalTypePending,
			@ApprovalTypePending,
			@StatusTypeActive,
			@UserID,
			@CurrentDate
		)    

		SET @InsertedId = SCOPE_IDENTITY();

		SELECT Top 1
			@HRUserId = userID  
		FROM 
			emp.VMSEmployeeDetails 
		WHERE 
			department = 'HR'

		SELECT 
			@EmailIdsForFtoNotification = STRING_AGG(EmailAddress,',')
		FROM 
			emp.UserMaster 
		WHERE 
			Id IN (@UserID,@SupervisorId,@HRUserId) ;
		
		SELECT
			@UserName = vms.PersonName,
			@EmailIdForFtoApproval = um.EmailAddress 
		FROM 
			emp.UserMaster um
			INNER JOIN emp.VMSEmployeeDetails vms ON um.Id = vms.UserId
		WHERE 
			um.Id IN (@UserID);

		SELECT 
			af.Id AS FtoId,
			af.StartDate,
			af.StartDateDayType,
			af.EndDate,
			af.EndDateDayType,
			Lt.[Name] AS LeaveTypeName,
			@UserName AS CreatedBy,
			@EmailIdsForFtoNotification AS EmailIdsForFtoNotification, 
			@EmailIdForFtoApproval AS EmailIdForFtoApproval 
		FROM
			emp.AttendanceFto AS af
			INNER JOIN master.LeaveType lt ON lt.Id = af.LeaveTypeId
		WHERE	
			af.ID = @InsertedId

    END TRY    
	BEGIN CATCH    
		DECLARE    
		@StringMsg VARCHAR(MAX),    
		@ErrorNumber VARCHAR(MAX),    
		@ErrorSeverity VARCHAR(MAX),    
		@ErrorState VARCHAR(MAX),    
		@ErrorLine VARCHAR(MAX),    
		@ErrorMessage VARCHAR(MAX)    
		SET  @ErrorNumber =  ERROR_NUMBER();    
		SET  @ErrorSeverity= ERROR_SEVERITY();    
		SET  @ErrorState =  ERROR_STATE();    
		SET  @ErrorMessage =   ERROR_MESSAGE();    
		SET  @ErrorLine =  ERROR_LINE();    
		SET @StringMsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;    
    
		THROW 51000,@stringmsg , 1;     
    END CATCH;    
End; 