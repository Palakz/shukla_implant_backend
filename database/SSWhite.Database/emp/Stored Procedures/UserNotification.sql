﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: EXEC  [emp].[GetEpoDetailsByEpoId]
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
USE [db_a5dc4b_sswhite]
 TEst Execution :	 [emp].[GetMpcVoteDetailsByEpoId] 178
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE  [emp].[GetMpcVoteDetailsByEpoId]
(
	@EpoId int
)
AS
BEGIN
	SET NOCOUNT ON;
	CREATE TABLE #TempTable (PersonName VARCHAR(500), MpcNumber VARCHAR(500), MpcApproval INT);

	
	INSERT INTO
		#TempTable 
	SELECT 
		PersonName,
		MpcNumber,
		NULL 
	FROM 
		emp.VMSEmployeeDetails vms INNER JOIN emp.EmpLimit el ON vms.userID = el.userID 
	WHERE MPCMember = 'Y'
	
	DECLARE @Mpc1Approval INT, @Mpc2Approval INT, @Mpc3Approval INT, @Mpc4Approval INT, @Mpc5Approval INT, @Mpc6Approval INT;
	
	SELECT 
		@Mpc1Approval = Mpc1Approval, 
		@Mpc2Approval = Mpc2Approval, 
		@Mpc3Approval = Mpc3Approval, 
		@Mpc4Approval = Mpc4Approval, 
		@Mpc5Approval = Mpc5Approval, 
		@Mpc6Approval = Mpc6Approval
	FROM 
		emp.EpoMpcVote 
	WHERE
		EpoCreationId = @EpoId
	
	UPDATE #TempTable SET MpcApproval = @Mpc1Approval WHERE MpcNumber = 'MPC1';
	UPDATE #TempTable SET MpcApproval = @Mpc2Approval WHERE MpcNumber = 'MPC2';
	UPDATE #TempTable SET MpcApproval = @Mpc3Approval WHERE MpcNumber = 'MPC3';
	UPDATE #TempTable SET MpcApproval = @Mpc4Approval WHERE MpcNumber = 'MPC4';
	UPDATE #TempTable SET MpcApproval = @Mpc5Approval WHERE MpcNumber = 'MPC5';
	UPDATE #TempTable SET MpcApproval = @Mpc6Approval WHERE MpcNumber = 'MPC6';

	SELECT 
		PersonName,
		MpcNumber,
		MpcApproval
	FROM 
		#TempTable
																			 
	DROP TABLE #TempTable 
END 
