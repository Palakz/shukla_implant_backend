﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[CheckIfEmployeeIdExists]
Comments	: 13-04-2022 | Kartik Bariya | This procedure is used to validate user while login into the application.

Test Execution : EXEC [emp].[CheckIfEmployeeIdExists] '101'
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[CheckIfEmployeeIdExists]
(
	@EmployeeId nvarchar(100)
)
AS
BEGIN
	SET NOCOUNT ON;
	Declare @empExists as varchar(100);

	IF EXISTS(SELECT PersonId from emp.UserMaster Where PersonId = @EmployeeId)
    BEGIN
        SET  @empExists = 'EmployeeId Already Exists';
	END;
	select @empExists as [Message];
End;
