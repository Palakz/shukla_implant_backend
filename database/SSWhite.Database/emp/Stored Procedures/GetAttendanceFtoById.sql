﻿/*------------------------------------------------------------------------------------------------------------
Name			: [emp].[GetDepartmentFtoByUserId]
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	: [emp].[GetAttendanceFtoById]
					@UserId = 41,
					@FtoId = 1,
					@StatusTypeActive = 13
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [emp].[GetAttendanceFtoById]
(
	@UserId INT,
	@FtoId INT,
	@StatusTypeActive INT
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		Id,
		EmployeeID,
		EmployeeName,
		StartDate,
		StartDateDayType,
		EndDate,
		EndDateDayType,
		LeaveTypeID,
		OtherLeaveTypeId,
		EmployeeComment,
		SupervisorNotes,
		HRNotes
	FROM 
		[emp].[AttendanceFto] ef
	WHERE
		ef.Id = @FtoId
		AND ef.[Status] = 1
END