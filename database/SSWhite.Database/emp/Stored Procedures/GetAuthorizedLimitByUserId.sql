﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[GetAuthorized LimitByUserId] 
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
USE [db_a5dc4b_sswhite]
 Test executio : EXEC [emp].[GetAuthorizedLimitByUserId] 47
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetAuthorizedLimitByUserId] 
(
	@UserId int
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
		EmpLimitNew
	FROM 
		emp.EmpLimit e
		INNER JOIN emp.usermaster u ON  u.PersonId = e.EmployeeId 
	WHERE 
		u.Id = @UserId
END 