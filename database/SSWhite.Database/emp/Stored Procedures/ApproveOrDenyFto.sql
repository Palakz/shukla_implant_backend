﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: EXEC  [emp].[GetEpoDetailsByEpoId]
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
USE [db_a5dc4b_sswhite]  
Test Execution :	--hr id  = 68
					DECLARE @Currentdate1 datetime =getdate()
					EXEC [emp].[ApproveOrDenyFto]
					@UserId = 68,
					@FtoId = 11,
					@IsApprove = 1,
					@ApprovalTypeApproved = 1,
					@ApprovalTypeDenied = 8,
					@ApprovalTypePending= 2,
					@Note = 'HRNotes',
					@CurrentDate = @Currentdate1
					select * from emp.attendancefto where id = 11
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE  [emp].[ApproveOrDenyFto]
(
	@UserId INT,
	@FtoId INT,
	@IsApprove BIT,
	@ApprovalTypeApproved INT,
	@ApprovalTypeDenied INT,
	@ApprovalTypePending INT,
	@Note VARCHAR(1000),
	@CurrentDate DATETIME
)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
	DECLARE @Department VARCHAR(100), 
			@UserEmailAddress VARCHAR(100),
			@HrEmailAddress VARCHAR(100),
			@HRUserId INT;

	SELECT Top 1
		@HRUserId = userID  
	FROM 
		emp.VMSEmployeeDetails 
	WHERE 
		department = 'HR'

	SELECT Top 1
		@Department = department
	FROM 
		emp.VMSEmployeeDetails 
	WHERE 
		UserId = @UserId

	IF EXISTS (SELECT Id FROM emp.AttendanceFTO WHERE Id = @FtoId AND SupervisorUserId = @UserId )
	BEGIN
		IF (@IsApprove = 1)
		BEGIN
			UPDATE 
				[emp].[AttendanceFTO] 
			Set 
				SupervisorNotes = @Note,
				SupervisorApproval = @ApprovalTypeApproved,
				Approval = @ApprovalTypePending,
				Modifiedby = @UserId,
				ModifiedDate = @CurrentDate
			WHERE 
				Id = @FtoId

			SELECT 
				@HrEmailAddress = EmailAddress
			FROM 
				emp.UserMaster 
			WHERE 
				Id IN (@HRUserId) ;
		END
		ELSE
		BEGIN
			UPDATE 
				[emp].[AttendanceFTO] 
			Set 
				SupervisorNotes = @Note,
				SupervisorApproval = @ApprovalTypeDenied,
				Approval = @ApprovalTypePending,
				Modifiedby = @UserId,
				ModifiedDate = @CurrentDate
			WHERE 
				Id = @FtoId
		END
	END

	IF EXISTS (SELECT Id FROM emp.AttendanceFTO WHERE Id = @FtoId AND @Department = 'HR')
	BEGIN
		IF (@IsApprove = 1)
		BEGIN
			UPDATE 
				[emp].[AttendanceFTO] 
			Set 
				HRNotes = @Note,
				HRApproval = @ApprovalTypeApproved,
				Approval = @ApprovalTypePending,
				Modifiedby = @UserId,
				ModifiedDate = @CurrentDate
			WHERE 
				Id = @FtoId
		END
		ELSE
		BEGIN
			UPDATE 
				[emp].[AttendanceFTO] 
			Set 
				HRNotes = @Note,
				HRApproval = @ApprovalTypeDenied,
				Approval = @ApprovalTypePending,
				Modifiedby = @UserId,
				ModifiedDate = @CurrentDate
			WHERE 
				Id = @FtoId
		END
	END

	IF EXISTS (SELECT Id FROM emp.AttendanceFTO WHERE Id = @FtoId AND SupervisorApproval = @ApprovalTypeApproved AND HRApproval = @ApprovalTypeApproved)
	BEGIN
		UPDATE 
				[emp].[AttendanceFTO] 
			Set 
				Approval = @ApprovalTypeApproved
			WHERE 
				Id = @FtoId
	END

	IF EXISTS (SELECT Id FROM emp.AttendanceFTO WHERE Id = @FtoId AND SupervisorApproval = @ApprovalTypeDenied AND HRApproval = @ApprovalTypeDenied)
	BEGIN
		UPDATE 
				[emp].[AttendanceFTO] 
			Set 
				Approval = @ApprovalTypeDenied
			WHERE 
				Id = @FtoId
	END

	SELECT 
		@UserEmailAddress = um.EmailAddress
	FROM 
		emp.UserMaster um
		INNER JOIN [emp].[AttendanceFTO] af ON af.CreatedBy = um.Id 
	WHERE 
		af.Id IN (@FtoId) ;

	SELECT
		af.Id AS FtoId,
		af.StartDate,
		af.StartDateDayType,
		af.EndDate,
		af.EndDateDayType,
		Lt.[Name] AS LeaveTypeName,
		af.CreatedBy,
		@UserEmailAddress AS UserEmailAddress,
		@HrEmailAddress AS HrEmailAddress
	FROM	
		emp.AttendanceFto AS af
		INNER JOIN master.LeaveType lt ON lt.Id = af.LeaveTypeId
	WHERE 
		af.Id = @FtoId

	END TRY
	BEGIN CATCH
		DECLARE
		@stringmsg VARCHAR(MAX),
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
			set  @ErrorNumber =  ERROR_NUMBER();
			set  @ErrorSeverity= ERROR_SEVERITY();
			set  @ErrorState =  ERROR_STATE();
			set  @ErrorMessage =   ERROR_MESSAGE();
			set  @ErrorLine =  ERROR_LINE();
			 set @stringmsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;

			 THROW 51000,@stringmsg , 1; 
	END CATCH;
END 


