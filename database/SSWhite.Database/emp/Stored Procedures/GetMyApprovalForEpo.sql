﻿/*------------------------------------------------------------------------------------------------------------
Name			: [emp].[GetMySubordinateEpos]
Comments		: 26-03-2021 | Amit | This procedure is used to get Get All Employee Limit.

Test Execution	: EXEC emp.GetMyApprovalForEpo
					@UserId = 41,
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL,
					@ApprovalTypeApproved = 1,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetMyApprovalForEpo]
(
	@UserId INT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@ApprovalTypeApproved INT,
	@TotalRecords INT OUT
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT  
			ec.Id,
			ec.CreatedDate,
			ec.PurposeOfPurchase,
			vms.PersonName AS CreatedBy,
			ec.GrandTotal ,
			ec.SelfApproval 
		INTO #TempTable
		FROM [emp].[EpoCreation] ec
	INNER JOIN 
			emp.EpoApprovalList ev ON ev.EpoNo = ec.Id
	INNER JOIN 
		emp.UserMaster um ON um.id = ec.CreatedBy
	INNER JOIN 
		[emp].[VMSEmployeeDetails] vms ON vms.PersonID = um.PersonId
	WHERE
			--ec.CreatedBy IN (select userid from emp.EmpLimit where MPCMember = 'Y')
			 ec.[Status] = 1
			 AND ev.UserId  = @UserId
			 AND ApprovalType != @ApprovalTypeApproved
			AND vms.Department = (SELECT  [Department] FROM emp.[VMSEmployeeDetails] WHERE UserId = @userid)
			AND 
			(
					ec.Id LIKE '%' + ISNULL(@SearchKeyword, ec.Id) +'%'
					OR ec.PurposeOfPurchase LIKE '%' + ISNULL(@SearchKeyword,ec.PurposeOfPurchase) +'%'
					OR vms.PersonName  LIKE '%' + ISNULL(@SearchKeyword,vms.PersonName ) +'%'
					OR ec.GrandTotal  LIKE '%' + ISNULL(@SearchKeyword,ec.GrandTotal ) +'%'
			);

	--SELECT * from #TempTable
		SELECT @TotalRecords = COUNT(Id)  FROM #TempTable 

		SELECT
			Id,
			CreatedDate,
			PurposeOfPurchase,
			CreatedBy,
			GrandTotal ,
			SelfApproval AS ApprovalType 
		FROM 
			#TempTable AS tmp
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN Id END desc,
			CASE WHEN @SortExpression = 'Id asc' THEN Id END ASC,
			CASE WHEN @SortExpression = 'Id desc' THEN Id END DESC,
			CASE WHEN @SortExpression = 'CreatedDate asc' THEN CreatedDate  END ASC,
			CASE WHEN @SortExpression = 'CreatedDate desc' THEN CreatedDate  END DESC,
			CASE WHEN @SortExpression = 'PurposeOfPurchase asc' THEN PurposeOfPurchase END ASC,
			CASE WHEN @SortExpression = 'PurposeOfPurchase desc' THEN PurposeOfPurchase END DESC,
			CASE WHEN @SortExpression = 'CreatedBy asc' THEN  CreatedBy END ASC,
			CASE WHEN @SortExpression = 'CreatedBy desc' THEN CreatedBy END DESC,
			CASE WHEN @SortExpression = 'GrandTotal asc' THEN  GrandTotal END ASC,
			CASE WHEN @SortExpression = 'GrandTotal desc' THEN GrandTotal END DESC
			--CASE WHEN @SortExpression = 'SelfApproval asc' THEN  SelfApproval END ASC,
			--CASE WHEN @SortExpression = 'SelfApproval desc' THEN SelfApproval END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;
	DROP TABLE #TempTable;
	END

	--update emp.epocreation set CreatedBy =60  where id =153  Maintenance

	--select * from emp.epocreation 

	--select department,userid,PersonName from update [emp].[VMSEmployeeDetails] set Department = 'Maintenance' where userid = 60
	-- update [emp].[VMSEmployeeDetails] set Department = 'CNC' where userid = 60