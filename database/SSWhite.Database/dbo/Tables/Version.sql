﻿CREATE TABLE [dbo].[Version] (
    [TID]           INT            NOT NULL,
    [ID]            INT            NOT NULL,
    [Versionno]     NVARCHAR (500) NULL,
    [VersionUpdate] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Version] PRIMARY KEY CLUSTERED ([TID] ASC, [ID] ASC)
);

