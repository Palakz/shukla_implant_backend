﻿CREATE TABLE [dbo].[InOutPunchDetails] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [UserId]      INT           NOT NULL,
    [PunchTime]   DATETIME      NULL,
    [InOutStatus] NVARCHAR (10) NULL,
    [PunchType]   SMALLINT      NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

