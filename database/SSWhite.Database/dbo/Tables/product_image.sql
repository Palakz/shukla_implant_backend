﻿CREATE TABLE [dbo].[product_image] (
    [MID]           INT            NOT NULL,
    [ID]            INT            NOT NULL,
    [PID]           INT            NOT NULL,
    [Product_name]  NVARCHAR (500) NULL,
    [Product_image] NVARCHAR (500) NULL,
    CONSTRAINT [PK_product_image] PRIMARY KEY CLUSTERED ([MID] ASC, [ID] ASC, [PID] ASC)
);

