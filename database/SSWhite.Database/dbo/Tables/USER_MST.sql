﻿CREATE TABLE [dbo].[USER_MST] (
    [TenentID]          INT            NOT NULL,
    [USER_ID]           INT            NOT NULL,
    [LOCATION_ID]       INT            NOT NULL,
    [CRUP_ID]           INT            NOT NULL,
    [FIRST_NAME]        NVARCHAR (50)  NOT NULL,
    [LAST_NAME]         NVARCHAR (50)  NULL,
    [FIRST_NAME1]       NVARCHAR (50)  NULL,
    [LAST_NAME1]        NVARCHAR (50)  NULL,
    [FIRST_NAME2]       NVARCHAR (50)  NULL,
    [LAST_NAME2]        NVARCHAR (50)  NULL,
    [LOGIN_ID]          NVARCHAR (100) NOT NULL,
    [PASSWORD]          NVARCHAR (150) NULL,
    [USER_TYPE]         INT            NULL,
    [REMARKS]           NVARCHAR (500) NULL,
    [ACTIVE_FLAG]       VARCHAR (1)    NOT NULL,
    [LAST_LOGIN_DT]     DATETIME       NULL,
    [USER_DETAIL_ID]    INT            NULL,
    [ACC_LOCK]          VARCHAR (1)    NULL,
    [FIRST_TIME]        VARCHAR (1)    NULL,
    [PASSWORD_CHNG]     NVARCHAR (150) NULL,
    [THEME_NAME]        NVARCHAR (50)  NULL,
    [APPROVAL_DT]       DATETIME       NULL,
    [VERIFICATION_CD]   NVARCHAR (40)  NULL,
    [EmailAddress]      NVARCHAR (50)  NULL,
    [Till_DT]           DATETIME       NULL,
    [IsSignature]       BIT            NULL,
    [SignatureImage]    VARCHAR (1000) NULL,
    [Avtar]             VARCHAR (1000) NULL,
    [CompId]            INT            NULL,
    [EmpID]             INT            NULL,
    [CheckinSwitch]     BIT            NULL,
    [LoginActive]       BIGINT         NULL,
    [ACTIVEUSER]        BIT            NULL,
    [USERDATE]          DATE           NULL,
    [Language1]         NVARCHAR (50)  NULL,
    [Language2]         NVARCHAR (50)  NULL,
    [Language3]         NVARCHAR (50)  NULL,
    [DateOfBirth]       DATETIME       NULL,
    [EmployeePosition]  NVARCHAR (50)  NULL,
    [salary]            NVARCHAR (50)  NULL,
    [DateOfJoining]     DATETIME       NULL,
    [phone_no]          NVARCHAR (50)  NULL,
    [pincode]           NVARCHAR (50)  NULL,
    [Address1]          NVARCHAR (MAX) NULL,
    [Address2]          NVARCHAR (MAX) NULL,
    [img]               NVARCHAR (500) NULL,
    [personID]          INT            NULL,
    [Library_menu]      NVARCHAR (50)  NULL,
    [Library_reader]    NVARCHAR (MAX) NULL,
    [Library_menu_dept] NVARCHAR (500) NULL,
    CONSTRAINT [PK_USER_MST] PRIMARY KEY CLUSTERED ([TenentID] ASC, [USER_ID] ASC, [LOCATION_ID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ayo Till date', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'USER_MST', @level2type = N'COLUMN', @level2name = N'Till_DT';

