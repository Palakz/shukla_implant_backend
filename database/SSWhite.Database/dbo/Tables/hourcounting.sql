﻿CREATE TABLE [dbo].[hourcounting] (
    [gpno]         NVARCHAR (50)  NOT NULL,
    [suppliername] NVARCHAR (MAX) NULL,
    [hourcount]    NVARCHAR (50)  NULL,
    CONSTRAINT [PK_hourcounting] PRIMARY KEY CLUSTERED ([gpno] ASC)
);

