﻿CREATE TABLE [dbo].[UserAttendance] (
    [Id]                      INT      IDENTITY (1, 1) NOT NULL,
    [UserId]                  INT      NOT NULL,
    [PunchTime]               DATETIME NOT NULL,
    [AttendancePunchTypeId]   INT      NULL,
    [AttendanceInOutStatusId] INT      NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

