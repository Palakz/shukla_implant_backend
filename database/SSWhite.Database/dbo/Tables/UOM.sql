﻿CREATE TABLE [dbo].[UOM] (
    [id]       INT           NOT NULL,
    [mid]      INT           NOT NULL,
    [uom_name] NVARCHAR (50) NULL,
    CONSTRAINT [PK_UOM] PRIMARY KEY CLUSTERED ([id] ASC, [mid] ASC)
);

