﻿CREATE TABLE [dbo].[SSW_Appliances_Main] (
    [ID]                 INT            NOT NULL,
    [PID]                INT            NOT NULL,
    [MID]                INT            NOT NULL,
    [SID]                INT            NOT NULL,
    [Type_of_appliance]  NVARCHAR (100) NOT NULL,
    [Appliance_Type]     NVARCHAR (MAX) NULL,
    [Appliance_Sub_Type] NVARCHAR (MAX) NULL,
    [Appliance_Name]     NVARCHAR (MAX) NULL,
    [Appliance_Number]   NVARCHAR (100) NULL,
    [Active]             NVARCHAR (2)   NULL,
    [Location]           NVARCHAR (500) NULL,
    [IP]                 NVARCHAR (100) NULL,
    [other_detail]       NVARCHAR (MAX) NULL,
    [mount_stand]        NVARCHAR (100) NULL,
    [Window_Version]     NVARCHAR (200) NULL,
    [PC_Configuration]   NVARCHAR (MAX) NULL,
    [Keyboard_Mouse]     NVARCHAR (100) NULL,
    [MAC_Address]        NVARCHAR (500) NULL,
    [Item_image]         NVARCHAR (200) NULL,
    CONSTRAINT [PK_SSW_Appliances_Main_1] PRIMARY KEY CLUSTERED ([ID] ASC, [PID] ASC, [MID] ASC, [SID] ASC, [Type_of_appliance] ASC)
);

