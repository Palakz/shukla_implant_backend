﻿CREATE TABLE [dbo].[Supplier_New_Data] (
    [ID]            INT            NOT NULL,
    [MID]           INT            NOT NULL,
    [Supplier_ID]   NVARCHAR (100) NOT NULL,
    [Supp_name]     NVARCHAR (MAX) NULL,
    [Supp_add1]     NVARCHAR (MAX) NULL,
    [Supp_add2]     NVARCHAR (MAX) NULL,
    [Supp_add3]     NVARCHAR (MAX) NULL,
    [City]          NVARCHAR (MAX) NULL,
    [State]         NVARCHAR (500) NULL,
    [postal_code]   NVARCHAR (MAX) NULL,
    [country]       NVARCHAR (500) NULL,
    [phone]         NVARCHAR (500) NULL,
    [email_id]      NVARCHAR (500) NULL,
    [GSTIN]         NVARCHAR (MAX) NULL,
    [Register_Type] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Supplier_New_Data] PRIMARY KEY CLUSTERED ([ID] ASC, [MID] ASC, [Supplier_ID] ASC)
);

