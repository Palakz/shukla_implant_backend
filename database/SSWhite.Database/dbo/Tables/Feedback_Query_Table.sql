﻿CREATE TABLE [dbo].[Feedback_Query_Table] (
    [ID]                     INT            NOT NULL,
    [MID]                    INT            NOT NULL,
    [Query_Title]            NVARCHAR (MAX) NULL,
    [Query_Reason]           NVARCHAR (MAX) NULL,
    [Query_Application_Name] NVARCHAR (MAX) NULL,
    [Query_generate_date]    DATETIME       NULL,
    [active]                 NVARCHAR (10)  NULL,
    [status]                 NVARCHAR (100) NULL,
    [query_complition_date]  DATETIME       NULL,
    [remark_on_completion]   NVARCHAR (MAX) NULL,
    [PersonID]               NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Feedback_Query_Table] PRIMARY KEY CLUSTERED ([ID] ASC, [MID] ASC)
);

