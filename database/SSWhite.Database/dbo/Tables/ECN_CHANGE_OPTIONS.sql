﻿CREATE TABLE [dbo].[ECN_CHANGE_OPTIONS] (
    [ID]       INT            NOT NULL,
    [MID]      INT            NOT NULL,
    [ECNID]    INT            NOT NULL,
    [PID]      INT            NOT NULL,
    [ECN_Name] NVARCHAR (500) NULL,
    CONSTRAINT [PK_ECN_CHANGE_OPTIONS] PRIMARY KEY CLUSTERED ([ID] ASC, [MID] ASC, [ECNID] ASC, [PID] ASC)
);

