﻿CREATE TABLE [dbo].[sswhite_gallery] (
    [MID]        INT            NOT NULL,
    [ImageID]    INT            NOT NULL,
    [ID]         INT            NOT NULL,
    [Active]     BIT            NULL,
    [Deleted]    BIT            NULL,
    [Title]      NVARCHAR (50)  NULL,
    [aTitle]     NVARCHAR (50)  NULL,
    [Type]       NVARCHAR (50)  NULL,
    [image]      NVARCHAR (500) NULL,
    [image_name] NVARCHAR (500) NULL,
    CONSTRAINT [PK_sswhite_gallery] PRIMARY KEY CLUSTERED ([MID] ASC, [ImageID] ASC, [ID] ASC)
);

