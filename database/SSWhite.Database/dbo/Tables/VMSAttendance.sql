﻿CREATE TABLE [dbo].[VMSAttendance] (
    [ID]         VARCHAR (50)   NULL,
    [datetime]   DATETIME       NULL,
    [date]       DATE           NULL,
    [time]       TIME (7)       NULL,
    [Status]     VARCHAR (50)   NULL,
    [Device]     VARCHAR (255)  NULL,
    [DeviceNo]   VARCHAR (255)  NULL,
    [PersonName] VARCHAR (255)  NULL,
    [CardNumber] VARCHAR (255)  NULL,
    [Approval]   NVARCHAR (100) NULL
);

