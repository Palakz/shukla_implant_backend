﻿CREATE TABLE [dbo].[tblCityStatesCounty] (
    [CityID]        INT            NOT NULL,
    [StateID]       INT            NOT NULL,
    [COUNTRYID]     INT            NOT NULL,
    [CityEnglish]   NVARCHAR (250) NOT NULL,
    [CityArabic]    NVARCHAR (250) NULL,
    [CityOther]     NVARCHAR (250) NULL,
    [LandLine]      NVARCHAR (20)  NULL,
    [ACTIVE1]       CHAR (1)       NULL,
    [ACTIVE2]       CHAR (1)       NULL,
    [CRUP_ID]       BIGINT         NULL,
    [AssignedRoute] NVARCHAR (50)  NULL,
    [SHORTCODE]     NVARCHAR (50)  NULL,
    [ZONE]          NVARCHAR (50)  NULL,
    [UploadDate]    DATETIME       NULL,
    [Uploadby]      NVARCHAR (50)  NULL,
    [SyncDate]      DATETIME       NULL,
    [Syncby]        NVARCHAR (50)  NULL,
    [SynID]         INT            NULL,
    [Deleted]       BIT            NULL,
    CONSTRAINT [PK_tblCityStatesCounty] PRIMARY KEY CLUSTERED ([CityID] ASC, [StateID] ASC, [COUNTRYID] ASC)
);

