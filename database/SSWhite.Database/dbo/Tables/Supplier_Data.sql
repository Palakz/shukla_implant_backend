﻿CREATE TABLE [dbo].[Supplier_Data] (
    [ID]          INT            NOT NULL,
    [MID]         INT            NOT NULL,
    [Supp_ID]     NVARCHAR (100) NOT NULL,
    [Supp_Name]   NVARCHAR (MAX) NULL,
    [Supp_Add1]   NVARCHAR (MAX) NULL,
    [Supp_Add2]   NVARCHAR (MAX) NULL,
    [Supp_Add3]   NVARCHAR (MAX) NULL,
    [City]        NVARCHAR (MAX) NULL,
    [State]       NVARCHAR (MAX) NULL,
    [country]     NVARCHAR (100) NULL,
    [phone]       NVARCHAR (100) NULL,
    [email_Id]    NVARCHAR (MAX) NULL,
    [Pan_no]      NVARCHAR (MAX) NULL,
    [GST_No]      NVARCHAR (MAX) NULL,
    [postal_code] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Supplier_Data] PRIMARY KEY CLUSTERED ([ID] ASC, [MID] ASC, [Supp_ID] ASC)
);

