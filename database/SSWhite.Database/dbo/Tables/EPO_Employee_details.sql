﻿CREATE TABLE [dbo].[EPO_Employee_details] (
    [ID]            INT            NOT NULL,
    [PID]           INT            NOT NULL,
    [employee_ID]   INT            NULL,
    [employee_name] NVARCHAR (500) NULL,
    [mobile_no]     NVARCHAR (50)  NULL,
    [status]        NVARCHAR (10)  NULL,
    CONSTRAINT [PK_EPO_Employee_details] PRIMARY KEY CLUSTERED ([ID] ASC, [PID] ASC)
);

