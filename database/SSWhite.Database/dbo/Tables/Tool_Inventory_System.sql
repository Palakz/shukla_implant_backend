﻿CREATE TABLE [dbo].[Tool_Inventory_System] (
    [ID]                        INT            NOT NULL,
    [MID]                       INT            NOT NULL,
    [PID]                       INT            NOT NULL,
    [Tool_ID]                   INT            NOT NULL,
    [Part_name]                 NVARCHAR (MAX) NULL,
    [Part_Dscription]           NVARCHAR (MAX) NULL,
    [Tool_Type]                 NVARCHAR (500) NULL,
    [Product_Class]             NVARCHAR (500) NULL,
    [Product_group]             NVARCHAR (500) NULL,
    [UOM]                       NVARCHAR (50)  NULL,
    [Qty_as_per_physical_stock] INT            NULL,
    [min_qty]                   INT            NULL,
    [item_qty_in_box]           INT            NULL,
    [active]                    NVARCHAR (50)  NULL,
    [Remark]                    NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Tool_Inventory_System] PRIMARY KEY CLUSTERED ([ID] ASC, [MID] ASC, [PID] ASC, [Tool_ID] ASC)
);

