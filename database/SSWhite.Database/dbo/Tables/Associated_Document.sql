﻿CREATE TABLE [dbo].[Associated_Document] (
    [ID]                INT            NOT NULL,
    [MID]               INT            NOT NULL,
    [PID]               INT            NOT NULL,
    [document_ID]       INT            NULL,
    [Rev]               INT            NULL,
    [Title]             NVARCHAR (MAX) NULL,
    [document_ID_Title] NVARCHAR (500) NULL,
    CONSTRAINT [PK_Associated_Document] PRIMARY KEY CLUSTERED ([ID] ASC, [MID] ASC, [PID] ASC)
);

