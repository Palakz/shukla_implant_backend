﻿CREATE TABLE [dbo].[machine] (
    [MID]         INT            NOT NULL,
    [ID]          INT            NOT NULL,
    [machinename] NVARCHAR (MAX) NULL,
    [type]        NVARCHAR (50)  NULL,
    CONSTRAINT [PK_machine] PRIMARY KEY CLUSTERED ([MID] ASC, [ID] ASC)
);

