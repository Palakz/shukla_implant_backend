﻿CREATE TABLE [dbo].[late_coming_img_table] (
    [id]       INT            NOT NULL,
    [month]    NVARCHAR (50)  NULL,
    [img_path] NVARCHAR (500) NULL,
    CONSTRAINT [PK_late_coming_img_table] PRIMARY KEY CLUSTERED ([id] ASC)
);

