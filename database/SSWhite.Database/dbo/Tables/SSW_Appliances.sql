﻿CREATE TABLE [dbo].[SSW_Appliances] (
    [ID]         INT            NOT NULL,
    [PID]        INT            NOT NULL,
    [MID]        INT            NOT NULL,
    [Refname]    NVARCHAR (MAX) NULL,
    [subrefname] NVARCHAR (MAX) NULL,
    [refname1]   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_SSW_Appliances] PRIMARY KEY CLUSTERED ([ID] ASC, [PID] ASC, [MID] ASC)
);

