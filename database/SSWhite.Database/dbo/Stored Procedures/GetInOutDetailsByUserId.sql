﻿
/*------------------------------------------------------------------------------------------------------------
Procedure	: [dbo].[GetInOutDetailsByUserId]
Comments	: 13-04-2022 | Kartik Bariya | This procedure is used to validate user while login into the application.

Test Execution : EXEC [dbo].[GetInOutDetailsByUserId]
					@UserId = 12,
					@StartDate = '2022-05-01 00:00:00.063',
					@EndDate = '2022-05-15 23:59:00.063'
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetInOutDetailsByUserId]
(
@UserId int,
	@StartDate DATETIME,
	@EndDate DATETIME
)
AS
BEGIN
	SET NOCOUNT ON;
	
	--SELECT  
	--	Id, CONVERT(date, PunchTime) AS [DATE], 
	--	PunchTime , AttendanceInOutStatusId AS InOutStatus,
	--	AttendancePunchTypeId AS PunchType 
	--FROM 
	--	dbo.UserAttendance 
	--WHERE PunchTime BETWEEN @StartDate and @EndDate  AND UserId = @UserId AND AttendancePunchTypeId
	-- =1

	SELECT  
		Id, CONVERT(date, PunchDateTime) AS [DATE], 
		PunchDateTime AS PunchTime , InOutStatus AS InOutStatus,
		PunchType AS PunchType 
	FROM 
		emp.VMSDataFromDashboard
	WHERE PunchDateTime BETWEEN @StartDate and @EndDate  AND EmployeeId  =@UserId AND PunchType=1

End;