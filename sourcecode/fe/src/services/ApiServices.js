/* eslint-disable */
import axios from 'axios';
import { getToken } from '../utils/Storage';

let baseUrl = process.env.REACT_APP_API_KEY;
class ApiService {
  authGet = (route, params = {}) => {
    return new Promise((resolve, reject) => {
      axios
        .get(baseUrl + route, { params })
        .then((response) => {
          resolve(response);
        })
        .catch(({ response }) => {
          if (response && response.status === 400) {
            resolve(response);
          }
          if (response && response.status === 401) {
            window.location.assign('/auth/login');
            reject(response);
          }
          reject(response);
        });
    });
  };
  get = (route,  params = {}) => {
    return new Promise((resolve, reject) => {
      axios
        .get(baseUrl + route, {
          params,
          headers: {
            ['auth-token']: getToken(),
          },
        })
        .then((response) => {
          resolve(response);
        })
        .catch(({ response }) => {
          if (response && response.status === 400) {
            resolve(response);
          }
          if (response && response.status === 401) {
            localStorage.clear();
            window.location.assign('/auth/login');
            resolve(response);
          }
          reject(response);
        });
    });
  };
  authPost = (route, params) => {
    return new Promise((resolve, reject) => {
      axios
        .post(baseUrl + route, params)
        .then((response) => {
          resolve(response);
        })
        .catch(({ response }) => {
          if (response && response.status === 400) {
            resolve(response);
          }
          if (response && response.status === 401) {
            // localStorage.clear();
            // window.location.assign('/auth/login');
            resolve(response);
          }
          resolve(response);
        });
    });
  };
  post = (route, params={}) => {
    return new Promise((resolve, reject) => {
      axios
        .post(baseUrl + route, params, {
          headers: {
            ['auth-token']: getToken(),
          },
        })
        .then((response) => {
          resolve(response);
        })
        .catch(({ response }) => {
          if (response && response.status === 400) {
            resolve(response);
          }
          if (response && response.status === 401) {
            localStorage.clear();
            window.location.assign('/auth/login');
            reject(response);
          }
        });
    });
  };
  put = (route, params) => {
    return new Promise((resolve, reject) => {
      axios
        .put(baseUrl + route, params, {
          headers: {
            ['auth-token']: getToken(),
          },
        })
        .then((response) => {
          resolve(response);
        })
        .catch(({ response }) => {
          if (response && response.status === 400) {
            resolve(response);
          }
          if (response && response.status === 401) {
            localStorage.clear();
            window.location.assign('/auth/login');
            reject(response);
          }
        });
    });
  };
  delete = (route, params) => {
    return new Promise((resolve, reject) => {
      axios
        .delete(baseUrl + route, {
          data: params, 
          headers: {
            ['auth-token']: getToken(),
          },
        })
        .then((response) => {
          resolve(response);
        })
        .catch(({ response }) => {
          if (response && (response.status === 400 || response.status === 403)) {
            resolve(response);
          }
          if (response && response.status === 401) {
            // localStorage.clear();
            // window.location.assign('/auth/login');
            reject(response);
          }
          reject(response);
        });
    });
  };
  patch = (route, params) => {
    return new Promise((resolve, reject) => {
      axios
        .patch(baseUrl + route, params, {
          headers: {
            ['auth-token']: getToken(),
          },
        })
        .then((response) => {
          resolve(response);
        })
        .catch(({ response }) => {
          if (response.status === 400) {
            resolve(response);
          }
          if (response.status === 401) {
            localStorage.clear();
            window.location.assign('/auth/login');
            reject(response);
          }
          resolve(response);
        });
    });
  };

  getWithToken = (route, token) => {
    return new Promise((resolve, reject) => {
      axios
        .get(baseUrl + route, {
          headers: {
            ['auth-token']: getToken(),
          },
        })
        .then((response) => {
          resolve(response);
        })
        .catch(({ response }) => {
          if (response && response.status === 400) {
            resolve(response);
          }
          if (response && response.status === 401) {
            localStorage.clear();
            window.location.assign('/auth/login');
            reject(response);
          }
          reject(response);
        });
    });
  };
  deleteWithToken = (route, token) => {
    return new Promise((resolve, reject) => {
      axios
        .delete(baseUrl + route, {
          headers: {
            ['auth-token']: getToken(),
          },
        })
        .then((response) => {
          resolve(response);
        })
        .catch(({ response }) => {
          if (response && (response.status === 400 || response.status === 403)) {
            resolve(response);
          }
          if (response && response.status === 401) {
            localStorage.clear();
            window.location.assign('/auth/login');
            reject(response);
          }
          reject(response);
        });
    });
  };
}

const instance = new ApiService();
export default instance;
