const AUTHENTICATION = "authenticate/";
const ADMIN_COMMON = "admin/common/";

const urlMapper = {
  login: `${AUTHENTICATION}token`,
  logOut: `${AUTHENTICATION}logout`,
  signUp: `${ADMIN_COMMON}AddNewUser`,
  getOrganization : `${ADMIN_COMMON}GetOrganization`,
  AddOrganization : `${ADMIN_COMMON}AddOrganization`,
  UpdateOrganization : `${ADMIN_COMMON}UpdateOrganization`,
  DeleteOccupations : `${ADMIN_COMMON}DeleteOccupations`,
  getOccupations : `${ADMIN_COMMON}GetOccupations`,
  AddOccupations : `${ADMIN_COMMON}AddOccupations`,
  UpdateOccupations : `${ADMIN_COMMON}UpdateOccupations`,
  DeleteOrganization : `${ADMIN_COMMON}DeleteOrganization`,
  AddOrEditPickList : `${ADMIN_COMMON}AddOrEditPickList`,
  GetAllPickList : `${ADMIN_COMMON}GetAllPickList`,
  DeletePickList : `${ADMIN_COMMON}DeletePickList`,
  AddOrEditPickListValue : `${ADMIN_COMMON}AddOrEditPickListValue`,
  GetPickListValueById : `${ADMIN_COMMON}GetPickListValueById`,
  DeletePickListValue : `${ADMIN_COMMON}DeletePickListValue`,
};
export default urlMapper;
