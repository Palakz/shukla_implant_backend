import apiService from '../../services/ApiServices';
import { AUTH_TOKEN } from '../constants';
import urlMapper from '../../services/urlMapper'

export const login = (params) => {
    return () => apiService.authPost(urlMapper.login, params)
};

export const logout = () => {
    return () => apiService.post(urlMapper.logOut);
}

export const signUp = (params) => {
    return () => apiService.authPost(urlMapper.signUp, params)
}

// ///////////////////////////////////////////
// Axios part Reducers End
// //////////////////////////////////////////

export const demo = (data) => ({
  type: AUTH_TOKEN,
  payload: data,
});
