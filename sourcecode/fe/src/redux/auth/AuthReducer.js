import { AUTH_TOKEN } from '../constants';

const INIT_STATE = {
  authUser: null,
  auth: null,
};

const AuthReducer = (state = INIT_STATE, action) => {
  switch (action.type) {
    case AUTH_TOKEN:
      return {
        ...state,
        auth: action.payload,
      };

    default:
      return state;
  }
};

export default AuthReducer;
