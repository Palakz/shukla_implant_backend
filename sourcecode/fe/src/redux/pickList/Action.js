/* eslint-disable */  
import apiService from '../../services/ApiServices';
import {
  SELECTED_PICKLIST,
  SEARCH_PICKLIST,
  UPDATE_PICKLIST,
  DELETE_PICKLIST,
  ADD_PICKLIST,
  FETCH_PICKLIST_SUCCESS,
} from '../constants';
import urlMapper from '../../services/urlMapper'
import axios from 'axios'

export const AddOrEditPickList = (params) => {
  return () => apiService.post(urlMapper.AddOrEditPickList, params)
};

export const GetAllPickList = () => {
  return () => apiService.get(urlMapper.GetAllPickList)
};

export const DeletePickList = (params) => {
  return () => apiService.post(urlMapper.DeletePickList, params)
};

export const AddOrEditPickListValue = (params) => {
  return () => apiService.post(urlMapper.AddOrEditPickListValue, params)
};

export const GetPickListValueById = (params) => {
  return () => apiService.get(urlMapper.GetPickListValueById, params)
};

export const DeletePickListValue = (params) => {
  return () => apiService.post(urlMapper.DeletePickListValue, params)
};



// ///////////////////////////////////////////
// Axios part Reducers End
// //////////////////////////////////////////

export const fetchPickList = () => (dispatch) => {
  axios
    .get('/api/data/pickList/pickListData')
    .then((response) => {
      dispatch({
        type: FETCH_PICKLIST_SUCCESS,
        pickLists: response.data,
      });
    })
    .catch((err) => err);
};


export const openPickList = (id) => ({
  type: SELECTED_PICKLIST,
  id,
});

export const pickListSearch = (searchTerm) => ({
  type: SEARCH_PICKLIST,
  searchTerm,
});
export const deletePickList = (id) => ({
  type: DELETE_PICKLIST,
  id,
});
export const updatePickList = (id, field, value) => ({
  type: UPDATE_PICKLIST,
  id,
  field,
  value,
});

export const addPickList = (payload) => ({
  type: ADD_PICKLIST,
  // eslint-disable-next-line no-param-reassign
  id: payload.id++,
  color: (theme) => theme.palette.primary.main,
  name: payload.name,
});

