import apiService from '../../services/ApiServices';
import { AUTH_TOKEN } from '../constants';
import urlMapper from '../../services/urlMapper'

export const getOccupations = () => {
    return () => apiService.authGet(urlMapper.getOccupations)
};

export const addOccupations = (params) => {
  return () => apiService.post(urlMapper.AddOccupations, params)
};

export const UpdateOccupations = (params) => {
  return () => apiService.post(urlMapper.UpdateOccupations, params)
};

export const DeleteOccupations = (params) => {
  return () => apiService.delete(urlMapper.DeleteOccupations, params)
};

export const getOrganization = () => {
    return () => apiService.authGet(urlMapper.getOrganization);
}

export const AddOrganization = (params) => {
  return () => apiService.post(urlMapper.AddOrganization, params)
};

export const UpdateOrganization = (params) => {
  return () => apiService.post(urlMapper.UpdateOrganization, params)
};

export const DeleteOrganization = (params) => {
  return () => apiService.delete(urlMapper.DeleteOrganization, params)
};

// ///////////////////////////////////////////
// Axios part Reducers End
// //////////////////////////////////////////

export const demo = (data) => ({
  type: AUTH_TOKEN,
  payload: data,
});

