import { ADD_OCCUPATION, ADD_ORGANIZATION } from '../constants';

const INIT_STATE = {
  occupationList: [],
  organizationList: [] ,
};

const CommonReducer = (state = INIT_STATE, action) => {
  switch (action.type) {
    case ADD_OCCUPATION:
      return {
        ...state,
        occupationList: action.payload,
      };

    case ADD_ORGANIZATION:
      return {
        ...state,
        organizationList: action.payload,
      };

    default:
      return state;
  }
};

export default CommonReducer;
