import { configureStore } from '../redux/Store';
import React from 'react';

const withReducer = (key, reducer) => WrappedComponent =>
	class extends React.PureComponent {
		constructor(props) {
			super(props);
			configureStore(key, reducer);
		}

		render() {
			return <WrappedComponent {...this.props} />;
		}
	};

export default withReducer;
