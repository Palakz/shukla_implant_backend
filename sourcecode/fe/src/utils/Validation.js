/*eslint-disable */

export const normalizePhone = (maxLength) => (value, previousValue) => {
    if (!value) {
        return value;
    }
    if (value.toString().trim().length <= maxLength) {
        return value;
    }
    return previousValue;
};

const URL_REGEX = new RegExp(/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi);
const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

export const phoneNumber = value => value && !/^(0|[1-9][0-9]{9})$/i.test(value) ? 'Invalid phone number, must be 10 digits' : undefined

// export const required = (value) => (value ? undefined : "This field is required");
export const required = (value) => {
    if (typeof value === 'string') {
        value = value.trim();
    }
    else if (Array.isArray(value)) {
        value = Object.keys(value).length > 0 ? value : null;
    }
    return value ? undefined : "This field is required";
};

export const checkIsNumber = value => value && isNaN(Number(value)) ? 'Must be a number' : undefined

export const notZero = value => value && value <= 0 ? 'Must be a number' : undefined

export const checkUrl = (value) =>
    value && !value.match(URL_REGEX)
        ? 'Invalid url (eg: http://exampleurl)' : undefined;

export const numberOnly = value => value && !/^[0-9]+$/i.test(value)
    ? 'Invalid value' : undefined;

export const IFSC = (value) =>
    value && !/^[A-Za-z]{4}[a-zA-Z0-9]{7}$/i.test(value)
        ? "Invalid IFSC code"
        : undefined;

export const minimumDate = (value, allValues) => {
    if (value <= allValues.start_date) {
        return 'End date cannot less than or equal to the start date';
    }
    return undefined;
};

export const characterLimit = (limit) => (value, previousValue) => {
    if (!value) {
        return value;
    }
    if (value.toString().trim().length <= limit) {
        return value;
    }
    return previousValue;
}

export const trimmed = () => (value) => {
    return value.toString().trim();
}

export const email = (value) => {
    return value.match(EMAIL_REGEX) ? undefined : "Please enter a valid email";
}