/*eslint-disable */
import React from 'react';
import PropTypes from "prop-types";
// @material-ui/core components
import FormControl from '@mui/material/FormControl';
// import InputLabel from '@mui/material/InputLabel';
// import Input from '@mui/material/Input';
import FormHelperText from '@mui/material/FormHelperText';
import CustomTextField from '../../../components/forms/custom-elements/CustomTextField';
import CustomFormLabel from '../../../components/forms/custom-elements/CustomFormLabel';




export default function CustomInput(props) {
  const { meta: { touched, error }, labelText, id, inputProps, input, inputRef } = props;
  return (
    <FormControl
      variant="standard" className='formControl'
      error={touched && Boolean(error)}
    >
      {labelText !== undefined ? (
        <CustomFormLabel htmlFor={id}>{labelText}</CustomFormLabel>
      ) : null}
              <CustomTextField
                id={id}
                placeholder={inputProps.placeholder}
                variant="outlined"
                fullWidth
                {...inputProps}
                error={touched && Boolean(error)}
              />
      {touched && error ? (
        <FormHelperText style={{ color: 'red' }}>{touched && error}</FormHelperText>
      ) : null}
    </FormControl>
  );
}


CustomInput.propTypes = {
  labelText: PropTypes.node,
  labelProps: PropTypes.object,
  id: PropTypes.string,
  inputProps: PropTypes.object,
  error: PropTypes.bool,
  success: PropTypes.bool,
  input: PropTypes.object,
};
