/* eslint-disable no-unused-vars */
import React, { useEffect, useState, useContext } from 'react';
import {
  IconButton,
  ListItemText,
  Box,
  Divider,
  Stack,
  Typography,
  Tooltip,
} from '@mui/material';
import { useSelector, useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { LoaderContext } from "utils/LoaderContext";
import FeatherIcon from 'feather-icons-react';
import Scrollbar from '../../custom-scroll/Scrollbar';
import { GetAllPickList, openPickList, DeletePickList, pickListSearch } from '../../../redux/pickList/Action';
import CustomTextField from '../../forms/custom-elements/CustomTextField';
import {FETCH_PICKLIST_SUCCESS} from '../../../redux/constants/index';


const NoteList = () => {
  const dispatch = useDispatch();
  const pickListData = useSelector((state) => state.PickListReducer.pickLists);
  const pickListsContent = useSelector((state) => state.PickListReducer.pickListsContent);

  const { toggalLoader } = useContext(LoaderContext);
  // const searchTerm = useSelector((state) => state.PickListReducer.pickListSearch);


  const fetchAllPickList = () => {
    toggalLoader(true)
    dispatch(GetAllPickList()).then((response) => {
      toggalLoader(false)
      if(response.status === 200) {
        dispatch({
          type: FETCH_PICKLIST_SUCCESS,
          pickLists: response.data,
        });
      }
      
    })
    .catch((err) => err);;
  }

  // const filterNotes = (notes, nSearch) => {
  //   if (nSearch !== '')
  //     return notes.filter(
  //       (t) =>
  //         !t.deleted &&
  //         t.title.toLocaleLowerCase().concat(' ').includes(nSearch.toLocaleLowerCase()),
  //     );
  //   return notes.filter((t) => !t.deleted);
  // };

  // const notes = useSelector((state) =>
  //   filterNotes(state.PickListReducer.pickLists, state.PickListReducer.pickListSearch),
  // );

  const deletePickList = (_id) => {
    toggalLoader(true)
    dispatch(DeletePickList({id : _id}))
        .then(response => {
          toggalLoader(false)
            if (response.status === 200) {
              fetchAllPickList()
              toast.success(`Picklist deleted succesfully`);
            }
        }).catch((error) => error);
  }

  useEffect(() => {
    fetchAllPickList()
  }, []);

  return (
    <Box>
      <Box sx={{ p: '15px' }}>
        <CustomTextField
          id="search"
          // value={searchTerm}
          placeholder="Search PickList"
          inputProps={{ 'aria-label': 'Search PickList' }}
          size="small"
          type="search"
          variant="outlined"
          fullWidth
          // onChange={(e) => dispatch(pickListSearch(e.target.value))}
        />
      </Box>
      <Divider />
      <Box sx={{ height: { lg: 'calc(100vh - 365px)', sm: '100vh' } }}>
        <Scrollbar>
          {pickListData && pickListData.length
            ? pickListData.map((note) => (
                <div key={note.id}>
                  <Box
                    p={2}
                    sx={{
                      position: 'relative',
                      cursor: 'pointer',
                      backgroundColor: pickListsContent === note.id ? 'rgba(230,244,255,0.3)' : '',
                    }}
                    onClick={() => {
                      if(pickListsContent !== note.id){
                        dispatch(openPickList(note.id))
                      }
                      
                    }}
                  >
                    <Box
                      sx={{
                        position: 'absolute',
                        width: '5px',
                        height: '100%',
                        left: 0,
                        top: 0,
                        // backgroundColor:
                        //   note.color === 'secondary'
                        //     ? (theme) => theme.palette.secondary.main
                        //     : note.color === 'error'
                        //     ? (theme) => theme.palette.error.main
                        //     : note.color === 'warning'
                        //     ? (theme) => theme.palette.warning.main
                        //     : note.color === 'success'
                        //     ? (theme) => theme.palette.success.main
                        //     : note.color === 'primary'
                        //     ? (theme) => theme.palette.primary.main
                        //     : (theme) => theme.palette.primary.main,
                      }}
                    />
                    <Typography variant="h5" sx={{ width: '240px' }} noWrap>
                      
                      <Stack
                      direction="row"
                      justifyContent="space-between"
                      alignItems="center"
                      spacing={2}
                    >
                      {note.name}
                      <Tooltip title="Delete">
                        <IconButton
                          aria-label="delete"
                          size="small"
                          sx={{ flexShrink: '0' }}
                          onClick={() => deletePickList(note.id)}
                        >
                          <FeatherIcon icon="trash-2" width="16" />
                        </IconButton>
                      </Tooltip>
                    </Stack>
                    </Typography>
                    
                  </Box>
                  <Divider />
                </div>
              ))
            : 'No PickList Found'}
        </Scrollbar>
      </Box>
    </Box>
  );
};

export default NoteList;
