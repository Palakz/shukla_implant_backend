/* eslint-disable */
import React, { useState, useContext, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Box,
  Divider,
  Button,
  Typography,
  Stack,
  TableCell,
  TableRow,
  Grid,
  FormControl,
  Tooltip,
  IconButton
} from '@mui/material';
import FeatherIcon from 'feather-icons-react';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import { LoaderContext } from "utils/LoaderContext";
import { AddOrEditPickListValue, GetPickListValueById, GetAllPickList, DeletePickListValue } from '../../../redux/pickList/Action';
import Scrollbar from '../../custom-scroll/Scrollbar';
import FormDialog from './components/FormDialog';
import FieldArrayForm from './components/FieldArrayForm';


const NoteContent = ({ toggleNoteSidebar }) => {
  const dispatch = useDispatch();
  const pickListValData = useSelector((state) => state.PickListReducer.pickListData);
  const isFormEdit = useSelector((state) => state.PickListReducer.isFormEdit);
  const { toggalLoader } = useContext(LoaderContext);
  const [open, setOpen] = useState(false);
  const pickListDetails = useSelector((state) =>
    state.PickListReducer.pickLists.find(
      (val) => state.PickListReducer.pickListsContent === val.id,
    ),
  );

  const [initialValues, setInitialValues] = useState(  {
    name: '',
    dataType: '',
    description: '',
    isValueNameSame: true,
    isValueAutoId: true,
    displayOrder: 0,
    orderBy: 'string',
  });
  // {
  //   name: '',
  //   dataType: '',
  //   description: '',
  //   isValueNameSame: true,
  //   isValueAutoId: true,
  //   displayOrder: 0,
  //   orderBy: 'string',
  // }

  const fetchAllPickList = () => {
    toggalLoader(true)
    dispatch(GetAllPickList()).then((response) => {
      toggalLoader(false)
      if(response.status === 200) {
        dispatch({
          type: "FETCH_PICKLIST_SUCCESS",
          pickLists: response.data,
        });
      }
      
    })
    .catch((err) => err);;
  }

  const stringToHTML = (str) => {
    var parser = new DOMParser();
    var doc = parser.parseFromString(str, 'text/html');
    return doc.body;
  };

  const handleSubmit = (data) => {
    
    console.log("Datas", data)
    const payloadData = {
      ...data,
      "isEdit": isFormEdit
    }

    dispatch(AddOrEditPickListValue(payloadData))
    .then((response) => {
      if (response.status === 200) {
        toast.success(`Picklist value ${(initialValues?.id) ? 'updated' : 'added'} succesfully`);
        fetchAllPickList() 
      } 
      else {
        toast.error("Something went wrong");
      }
    })
    .catch((error) => error);

  }



  const addFormData = (data) => {
    setOpen(data);
    setInitialValues({
      name: '',
      dataType: '',
      description: '',
      isValueNameSame: true,
      isValueAutoId: true,
      displayOrder: 0,
      orderBy: 'string',
    });
  };
  const editFormData = (data) => {
   
    console.log("editFormData",data);
    setInitialValues(data);
    setOpen(true);
  };

  const fetchPickListValue = async () => {
    toggalLoader(true)
    await dispatch(GetPickListValueById({PickListId : pickListDetails?.id})).then(response => {
      toggalLoader(false)
      if(response.status === 200) {
        dispatch({
          type: "FETCH_PICKLIST_VALUE_SUCCESS",
          data: response.data,
        });
      }
      
    })
  }

  const deletePickListValue = (_id) => {
    const payload = {
      id: [_id]
    }
    toggalLoader(true)
    dispatch(DeletePickListValue(payload)).then(response => {
      toggalLoader(false)
      fetchPickListValue()
      
    })
    console.log("deletePickListValue", payload)
  }

  useEffect(async () => {
    if(pickListDetails?.id){
      fetchPickListValue()
    }
  }, [pickListDetails?.id])

  

  return (
    <Box>
      <Box display="flex" alignItems="center" p={2}>
        <Box sx={{ display: { xs: 'block', md: 'block', lg: 'none' }, mr: '10px' }}>
          <FeatherIcon icon="menu" width="18" onClick={toggleNoteSidebar} />
        </Box>
        {/* <input
          style={{ display: 'none' }}
          type="text"
          name="title"
          id="title"
          ref={(node) => {
            title = node;
          }}
        />
        <input
          style={{ display: 'none' }}
          type="text"
          name="color"
          id="color"
          ref={(node) => {
            color = node;
          }}
        /> */}
        <Button
          variant="contained"
          disableElevation
          color="primary"
          style={{ marginLeft: 'auto' }}
          onClick={(e) => {
            e.preventDefault();
            addFormData(true);
          }}
        >
          Add PickList
        </Button>
      </Box>
      <Divider />
      {pickListDetails ? (
        <Box p={2}>
          <Typography variant="h3" fontWeight="600">
            {pickListDetails.name}
            <IconButton
                aria-label="Add"
                size="medium"
                sx={{ flexShrink: '0' }}
                onClick={(e) => {
                  e.preventDefault();
                  // dispatch(addNote(id, title.value, color.value));
                  // title.value = '';
                  // color.value = '#1a97f5';
                  editFormData(pickListDetails);
                }}
              >
                <FeatherIcon icon="edit" width="16" />
              </IconButton>
          </Typography>
          <Divider />
          <br />
          <Stack
            direction="row"
            // justifyContent="space-between"
            alignItems="center"
            spacing={2}
          >
            <Typography variant="h5" fontWeight="600">
              PickList Value
            </Typography>
            {/* <Tooltip title="Add">
              <IconButton
                aria-label="Add"
                size="medium"
                sx={{ flexShrink: '0' }}
              >
                <FeatherIcon icon="plus-square" width="16" />
              </IconButton>
            </Tooltip> */}
          </Stack>

          <Box sx={{ height: { lg: 'calc(100vh - 365px)', sm: '100vh' } }}>
            <Scrollbar>
              <Grid
                aria-label="simple table"
                sx={{
                  whiteSpace: 'nowrap',
                }}
              >
                  <Box>
                    <TableCell sx={{ m: 1, minWidth: 50 }}>
                      <Typography variant="h5"></Typography>
                    </TableCell>
                    <TableCell sx={{ m: 1, minWidth: 200 }}>
                      <Typography variant="h5">Name*</Typography>
                    </TableCell>
                    <TableCell sx={{ m: 1, minWidth: 200 }}>
                      <Typography variant="h5">Value*</Typography>
                    </TableCell>
                    {/* <TableCell sx={{ m: 1, minWidth: 200 }}>
                      <Typography variant="h5">isDefault</Typography>
                    </TableCell> */}
                  </Box>
                  {
                    pickListValData &&
                 
                <FieldArrayForm 
                    pickListValData={pickListValData}
                    pickListDetails={pickListDetails}
                    handleSubmit={(val) => handleSubmit(val)}
                    handleDelete={(id) => deletePickListValue(id)}
                    />
                  } 
                </Grid>
            </Scrollbar>
          </Box>
        </Box>
      ) : (
        <Box sx={{ textAlign: 'center', fontSize: '24px', mt: 2 }}>Select a PickList</Box>
      )}
      <FormDialog 
        initialValues={initialValues} 
        open={open} 
        setOpen={(data) => addFormData(data)} 
        getList={() => fetchAllPickList()}
      />
    </Box>
  );
};

NoteContent.propTypes = {
  toggleNoteSidebar: PropTypes.func,
};

export default NoteContent;
