/* eslint-disable */
import React from 'react';
import { FieldArray, Form, Formik, getIn, } from 'formik';
import * as Yup from 'yup';
import ReactQuill from 'react-quill';
import { Box, Button, Typography, Grid, FormControl } from '@mui/material';
import CustomTextField from '../../../forms/custom-elements/CustomTextField';
import CustomCheckbox from '../../../forms/custom-elements/CustomCheckbox';

import 'react-quill/dist/quill.snow.css';
import './Quill.css';

const validationSchema = Yup.object().shape({
  pickListValue: Yup.array().of(
    Yup.object().shape({
      name: Yup.string().required('Name is required'),
      value: Yup.string().required('Value is required'),
    }),
  ),
});

const FieldArrayForm = (props) => {
  const { pickListValData, pickListDetails, handleSubmit, handleDelete } = props;
  let initData = pickListValData?.pickListValue?.length > 0 ? (
    { pickListValue : [...pickListValData.pickListValue] }
  )
  : ({ pickListValue : [{
    pickListId: pickListDetails.id,
    isDefault: false,
    displayOrder: 1,
    name: '',
    value: '',
    note: 'string',
  }] })
  return (
    <Formik
      initialValues={initData}
      validationSchema={validationSchema}
      onSubmit={(values) => {
          console.log("handleSubmitDatas", values);
        handleSubmit(values);
      }}
    >
      {({ values, touched, errors, handleChange, handleBlur, isValid, setFieldValue  }) => (
        <>
          <Form noValidate autoComplete="off">
            <FieldArray name="pickListValue">
              {({ push, remove  }) => (
                <>
                  {values.pickListValue.length > 0 &&
                    values?.pickListValue?.map((list, index) => {
                      const name = `pickListValue[${index}].name`;
                      const touchedName = getIn(touched, name);
                      const errorName = getIn(errors, name);

                      const value = `pickListValue[${index}].value`;
                      const touchedValue = getIn(touched, value);
                      const errorValue = getIn(errors, value);

                      return (
                        <Grid key={list.id}>
                          {/* <FormControl sx={{ m: 1, minWidth: 50 }}>
                              <CustomCheckbox
                                onChange={()=>{}}
                                color="primary"
                                />
                          </FormControl> */}
                          <FormControl sx={{ m: 1, minWidth: 120 }}>
                            <CustomTextField
                              id="name"
                              name={name}
                              value={list.name}
                              type="text"
                              fullWidth
                              placeholder="Enter name"
                              variant="outlined"
                              size="small"
                              required
                              inputProps={{ 'aria-label': 'Enter name' }}
                              sx={{
                                mb: 1,
                              }}
                              helperText={touchedName && errorName ? errorName : ''}
                              error={Boolean(touchedName && errorName)}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </FormControl>
                          <FormControl sx={{ m: 1, minWidth: 120 }}>
                            {
                                pickListDetails.dataType == 'Html'
                                ?
                                <ReactQuill
                                id="name"
                                name={value}
                                value={list.value}
                                onChange={v => setFieldValue(value, v)} 
                                placeholder="Type here..."
                                />
                                :
                                <CustomTextField
                                id="name"
                                name={value}
                                value={list.value}
                                type="text"
                                fullWidth
                                placeholder="Enter value"
                                variant="outlined"
                                size="small"
                                required
                                inputProps={{ 'aria-label': 'Enter name' }}
                                sx={{
                                    mb: 1,
                                }}
                                helperText={touchedValue && errorValue ? errorValue : ''}
                                error={Boolean(touchedValue && errorValue)}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                />
                            }
                          </FormControl>
                          <FormControl sx={{ m: 1, minWidth: 120 }}>
                            <Button
                              margin="normal"
                              type="button"
                              color="secondary"
                              variant="outlined"
                              onClick={() => {
                                  remove(index);
                                  handleDelete(list.id)
                              }}
                            >
                              x
                            </Button>
                          </FormControl>
                        </Grid>
                      );
                    })}
                  <Box sx={{ textAlign: 'left', fontSize: '24px', mt: 2 }}>
                    <Button
                      type="button"
                      variant="outlined"
                      onClick={() =>
                        push({
                        //   id: 1000 + Math.floor(Math.random() * 100),
                          pickListId: pickListDetails.id,
                          isDefault: false,
                          displayOrder: 1,
                          name: '',
                          value: '',
                          note: 'string',
                        })
                      }
                    >
                      Add
                    </Button>
                  </Box>
                </>
              )}
            </FieldArray>
            <br />
            <Grid item xs={8} sm={8} lg={8}>
                {console.log("pickListValData", pickListValData)}
            <Box sx={{ textAlign: 'right', fontSize: '24px', mt: 2 }}>
              <Button
                type="submit"
                color="primary"
                variant="contained"
                // disabled={!isValid || values.people.length === 0}
              >
                submit
              </Button>
            </Box>
            </Grid>
            
          </Form>
        </>
      )}
    </Formik>
  );
};

export default FieldArrayForm;
