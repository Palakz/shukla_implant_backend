import React from 'react';
import { Outlet, Navigate } from 'react-router-dom';
import { isLogin } from '../../utils/Storage'

const BlankLayout = () => {

  if (isLogin()) {
    return <Navigate to="/dashboards" />;
  }

  return (
    <>
      <Outlet />
    </>
  );
}

export default BlankLayout;
