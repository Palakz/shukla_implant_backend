import mock from './mock';
import './chats/ChatData';
import './notes/NotesData';
import './pickList/pickListData';

mock.onAny().passThrough();
