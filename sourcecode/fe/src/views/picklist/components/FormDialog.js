/* eslint-disable */  
import React, {useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { MenuItem, FormHelperText } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import * as Yup from "yup";
import { Formik, Form, Field, ErrorMessage } from "formik";
import CustomTextField from '../../../components/forms/custom-elements/CustomTextField';
import CustomSelect from '../../../components/forms/custom-elements/CustomSelect';

import * as pickList from '../../../redux/pickList/Action';
import { toast } from 'react-toastify';

const  FormDialog = (props) => {
    const dispatch = useDispatch();
    const { getList, open, setOpen, initialValues } = props;
    const pickListData = useSelector(state => state.PickListReducer.pickList);
    const validationSchema = Yup.object({
      name: Yup.string().required(),
      description: Yup.string().required()
    });

    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };

    const onSubmit = (payload) => {
      // dispatch(pickList.addMockPickList(payload))
      
      // dispatch(pickList.AddOrEditPickList(payload))
      // .then((response) => {
      //   if (response.status === 200) {
      //     toast.success(`Picklist ${(initialValues?.id) ? 'updated' : 'added'} succesfully`);
      //     handleClose()
      //     resetForm()
      //     getList() 
      //   } 
      //   else {
      //     toast.error("Something went wrong");
      //   }
      // })
      // .catch((error) => error);
    };

    useEffect(() => {
      console.log("pickListData", pickListData)
    }, [pickListData])
    

  return (
    <div>
      <Button variant="contained" className='primaryButton nextBtn' onClick={handleClickOpen} >+ Add Organization</Button >
      <Dialog fullWidth open={open} onClose={handleClose}>
        <DialogTitle>PickList</DialogTitle>
        <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={async (values, { resetForm }) => {
        await onSubmit(values);
      }}
    >
      <Form>
        <DialogContent>
        <Field name="name">
             {({
               field, // { name, value, onChange, onBlur }
               form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
               meta,  
             }) => (
               <>
                 <CustomTextField
                    id="name"
                    name="name"
                    type="text"
                    fullWidth
                    placeholder="Enter name"
                    variant="outlined"
                    size="small"
                    {...field}
                    inputProps={{ 'aria-label': 'Enter name' }}
                    sx={{
                      mb: 1,
                    }}
                    error={meta.touched && Boolean(meta.error)}
                  />
                  {
                    meta.touched &&  Boolean(meta.error) ? (
                      <FormHelperText style={{ color: 'red' }}>{meta.error}</FormHelperText>
                    ) : null
                  }
               </>
             )}
           </Field>
           <Field name="description">
             {({
               field, // { name, value, onChange, onBlur }
               form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
               meta,
             }) => (
               <>
                  <CustomSelect
                    id="description"
                    name="description"
                    placeholder="Please select description"
                    variant="outlined"
                    {...field}
                    inputProps={{ 'aria-label': 'Please select description' }}
                    size="small"
                    fullWidth
                    error={meta.touched && Boolean(meta.error)}
                  >
                    <MenuItem value='string' >String</MenuItem>
                    <MenuItem value='ckeditor' >ckeditor</MenuItem>
                  </CustomSelect>
                  {
                    meta.touched &&  Boolean(meta.error) ? (
                      <FormHelperText style={{ color: 'red' }}>{meta.error}</FormHelperText>
                    ) : null
                  }
               </>
             )}
           </Field>
        </DialogContent>
        <DialogActions>
          <Button variant="contained"
          color="secondary"
          size="large"
          sx={{
            width: '100%',
            display: 'block',
          }} onClick={handleClose}>Cancel</Button>
          <Button variant="contained"
          type="submit"
          color="primary"
          size="large"
          sx={{
            width: '100%',
            display: 'block',
          }} >Submit</Button>
          
        </DialogActions>
        </Form>
    </Formik>
      </Dialog>
    </div>
  );
}

export default FormDialog;
