/*eslint-disable */

import React from 'react';
import { reduxForm, Field } from 'redux-form';
import { required, email } from '../../../utils/Validation';
import CustomInput from '../../../components/custom/CustomInput/CustomInput';

const password = (value) =>
  value && !/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,}$/i.test(value)
    ? 'Password must have five characters, at least one letter, one number and one special character(!@#$%^&*)'
    : undefined;

function Form(props) {

  return (
    <>
        <Field
          name="email"
          component={CustomInput}
          id="email"
          labelText="Email"
          formControlProps={{
            fullWidth: true,
          }}
          validate={[required, email]}
          inputProps={{
            placeholder: 'Enter Email Address',
            type: 'text',
          }}
        />
        <Field
          name="password"
          component={CustomInput}
          id="password"
          labelText="Password"
          formControlProps={{
            fullWidth: true,
          }}
          validate={[required]}
          inputProps={{
            placeholder: 'Enter Password',
            type: 'password',
          }}
        />
    </>
  );
}

const ReduxFormMapped = reduxForm({
  form: 'LoginForm', // a unique identifier for this form
  enableReinitialize: false,
})(Form);
export default ReduxFormMapped;
