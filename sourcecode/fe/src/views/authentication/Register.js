/* eslint-disable */
import React, { useState, useEffect } from 'react';
import { Grid, Box, Typography, Button, MenuItem } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import * as Auth from '../../redux/auth/Action';
import * as Common from '../../redux/common/Action';

import CustomTextField from '../../components/forms/custom-elements/CustomTextField';
import CustomFormLabel from '../../components/forms/custom-elements/CustomFormLabel';
import PageContainer from '../../components/container/PageContainer';
import CustomSelect from '../../components/forms/custom-elements/CustomSelect';

import img1 from '../../assets/images/backgrounds/login-bg.svg';
import LogoIcon from '../../assets/images/logos/sslogo.png';

import { toast } from 'react-toastify';

const Register = () => {
  const dispatch = useDispatch();
  const navigate  = useNavigate();
  const [registerData, setRegisterData] = useState({});
//   {
//     "email": "raj01@yopmail.com",
//     "password": "Admin@123",
//     "FirstName": "raj",
//     "LastName": "kumar",
//     "UserName": "raj01",
//     "MobileNumber": "9877765430",
//     "occupationId": 4,
//     "organizationId": 3
// }
  const [error, setError] = useState({});
  const [occupationData, setOccupationData] = useState([]);
  const [organizationData, setOrganizationData] = useState([]);

  const handleChange = (e) => {
    setRegisterData({ ...registerData, [e.target.name]: e.target.value });
    setError({ ...error, [`${e.target.name}`]: '' });
  };

  const getOccupations = () => {
    dispatch(Common.getOccupations())
        .then(response => {
            if (response.status === 200) {
              setOccupationData(response.data)
            }
        }).catch((error) => error);
  }

  const getOrganization = () => {
    dispatch(Common.getOrganization())
        .then(response => {
            if (response.status === 200) {
              setOrganizationData(response.data)
            }
        }).catch((error) => error);
  }

  useEffect(() => {
    getOrganization()
    getOccupations()
  },[])

  const validate = () =>{
    let FirstName, LastName, UserName, email, password, MobileNumber, occupationId, organizationId = '';

    if(!registerData.FirstName){
      FirstName = "First Namer is required.";
    }
    if(!registerData.LastName){
      LastName = "Last Name is required.";
    }
    if(!registerData.UserName){
      UserName = "User Name is required.";
    }
    if(!registerData.email){
      email = "Email Name is required.";
    }
    if(!registerData.password){
      password = "Password is required.";
    }
    if(!registerData.MobileNumber){
      MobileNumber = "Mobile Number is required.";
    }
    if(!registerData.occupationId){
      occupationId = "Occupation is required.";
    }
    if(!registerData.organizationId){
      organizationId = "Organization is required.";
    }
    if(FirstName || LastName || UserName || email || password || MobileNumber, occupationId, organizationId){
      setError({...error, FirstName, LastName, UserName, email, password, MobileNumber, occupationId, organizationId})
    return false;
    }
    return true;
    }

  const submitRegisterForm = (e) => {
    e.preventDefault();

    if(!validate()){
      return
    }
    setError({})
    const payload = registerData

    dispatch(Auth.signUp(payload))
        .then(response => {
            if (response.status === 201) {
              toast.success('Register succesfully');
              navigate('/');
            }
            else if(response.status === 304){
              toast.error("Email already exists");
            }
            else if(response.status === 409){
              toast.error(" Email yet not verified");
            }
        }).catch((error) => error);
  };

  return (
    <PageContainer title="Register" description="this is Register page">
      <Grid container spacing={0} sx={{ height: '100vh', justifyContent: 'center' }}>
        <Grid
          item
          xs={12}
          sm={12}
          lg={6}
          sx={{
            background: (theme) => `${theme.palette.mode === 'dark' ? '#1c1f25' : '#ffffff'}`,
          }}
        >
          <Box
            sx={{
              position: 'relative',
            }}
          >
            <Box
              display="flex"
              alignItems="center"
              justifyContent="center"
              sx={{
                position: {
                  xs: 'relative',
                  lg: 'absolute',
                },
                height: { xs: 'auto', lg: '100vh' },
                right: { xs: 'auto', lg: '-50px' },
                margin: '0 auto',
              }}
            >
              <img
                src={img1}
                alt="bg"
                style={{
                  width: '100%',
                  maxWidth: '812px',
                }}
              />
            </Box>

            <Box
              display="flex"
              alignItems="center"
              sx={{
                p: 4,
                position: 'absolute',
                top: '0',
              }}
            >
              <img src={LogoIcon} width="100px" />
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} sm={8} lg={6} display="flex" alignItems="center">
          <Grid container spacing={0} display="flex" justifyContent="center">
            <Grid item xs={12} lg={9} xl={6}>
              <Box
                sx={{
                  p: 4,
                }}
              >
                <Typography fontWeight="700" variant="h2">
                  Welcome to Sukla Implant
                </Typography>
                <Box display="flex" alignItems="center">
                  <Typography
                    color="textSecondary"
                    variant="h6"
                    fontWeight="400"
                    sx={{
                      mr: 1,
                    }}
                  >
                    Already have an Account?
                  </Typography>
                  <Typography
                    component={Link}
                    to="/auth/login"
                    fontWeight="500"
                    sx={{
                      display: 'block',
                      textDecoration: 'none',
                      color: 'primary.main',
                    }}
                  >
                    Sign In
                  </Typography>
                </Box>
                <Box
                  sx={{
                    mt: 4,
                  }}
                >
                  <CustomFormLabel htmlFor="FirstName">First Name</CustomFormLabel>
                  <CustomTextField
                    id="FirstName"
                    name="FirstName"
                    value={registerData.FirstName}
                    onChange={(e) => handleChange(e)}
                    variant="outlined"
                    fullWidth
                    error={error?.FirstName?.length}
                    helperText={error?.FirstName}
                  />

                  <CustomFormLabel htmlFor="LastName">Last Name</CustomFormLabel>
                  <CustomTextField
                    id="LastName"
                    name="LastName"
                    value={registerData.LastName}
                    onChange={(e) => handleChange(e)}
                    variant="outlined"
                    fullWidth
                    error={error?.LastName?.length}
                    helperText={error?.LastName}
                  />

                  <CustomFormLabel htmlFor="UserName">User Name</CustomFormLabel>
                  <CustomTextField
                    id="UserName"
                    name="UserName"
                    value={registerData.UserName}
                    onChange={(e) => handleChange(e)}
                    variant="outlined"
                    fullWidth
                    error={error?.UserName?.length}
                    helperText={error?.UserName}
                  />

                  <CustomFormLabel htmlFor="email">Email Adddress</CustomFormLabel>
                  <CustomTextField
                    id="email"
                    name="email"
                    value={registerData.email}
                    onChange={(e) => handleChange(e)}
                    variant="outlined"
                    fullWidth
                    error={error?.email?.length}
                    helperText={error?.email}
                  />

                  <CustomFormLabel htmlFor="password">Password</CustomFormLabel>
                  <CustomTextField
                    name="password"
                    value={registerData.password}
                    onChange={(e) => handleChange(e)}
                    id="password"
                    type="password"
                    variant="outlined"
                    fullWidth
                    error={error?.password?.length}
                    helperText={error?.password}
                  />

                  <CustomFormLabel htmlFor="MobileNumber">Mobile Number</CustomFormLabel>
                  <CustomTextField
                    id="MobileNumber"
                    name="MobileNumber"
                    value={registerData.MobileNumber}
                    onChange={(e) => handleChange(e)}
                    variant="outlined"
                    fullWidth
                    error={error?.MobileNumber?.length}
                    helperText={error?.MobileNumber}
                  />

                  <CustomFormLabel htmlFor="OccupationId">Occupation</CustomFormLabel>
                  <CustomSelect
                    labelId="OccupationId"
                    id="OccupationId"
                    name="occupationId"
                    value={registerData.occupationId}
                    onChange={(e) => handleChange(e)}
                    fullWidth
                    error={error?.occupationId?.length}
                    helperText={error?.occupationId}
                  >
                    {
                      occupationData.length > 0 &&
                      occupationData.map((val)=>(<MenuItem value={val.id}>{val.name}</MenuItem>)) 
                    }
                  </CustomSelect>

                  <CustomFormLabel htmlFor="OccupationId">Organization</CustomFormLabel>
                  <CustomSelect
                    labelId="OrganizationId"
                    id="OrganizationId"
                    name="organizationId"
                    value={registerData.organizationId}
                    onChange={(e) => handleChange(e)}
                    fullWidth
                    error={error?.organizationId?.length}
                    helperText={error?.organizationId}
                  >
                    {
                      organizationData.length > 0 &&
                      organizationData.map((val)=>(<MenuItem value={val.id}>{val.name}</MenuItem>)) 
                    }
                  </CustomSelect>

                  <Button
                    color="secondary"
                    variant="contained"
                    size="large"
                    fullWidth
                    sx={{
                      mt: '20px',
                      pt: '10px',
                      pb: '10px',
                    }}
                    onClick={(e) => submitRegisterForm(e)}
                  >
                    Sign Up
                  </Button>
                  <Box
                    sx={{
                      position: 'relative',
                      textAlign: 'center',
                      mt: '20px',
                      mb: '20px',
                      '&::before': {
                        content: '""',
                        background: (theme) =>
                          `${theme.palette.mode === 'dark' ? '#42464d' : '#ecf0f2'}`,
                        height: '1px',
                        width: '100%',
                        position: 'absolute',
                        left: '0',
                        top: '13px',
                      },
                    }}
                  >
                  </Box>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </PageContainer>
  );
};

export default Register;
