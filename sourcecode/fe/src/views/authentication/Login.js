/* eslint-disable */
import React, { useState } from 'react';
import { Grid, Box, Typography, FormGroup, FormControlLabel, Button } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import * as Actions from '../../redux/auth/Action';
import { AUTH_TOKEN } from '../../redux/constants/index';

import CustomCheckbox from '../../components/forms/custom-elements/CustomCheckbox';
import CustomTextField from '../../components/forms/custom-elements/CustomTextField';
import CustomFormLabel from '../../components/forms/custom-elements/CustomFormLabel';
import PageContainer from '../../components/container/PageContainer';

import img1 from '../../assets/images/backgrounds/login-bg.svg';
import LogoIcon from '../../assets/images/logos/sslogo.png';
import { toast } from 'react-toastify';


const required = (value) => (value ? undefined : 'Required');

const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [userName, setUserName] = useState(''); //Pings12
  const [password, setPassword] = useState(''); //Admin@123
  const [error, setError] = useState({userNameError : '', passwordError : ''});

  const validate = () =>{
    let userNameError = "";
    let passwordError = "";

    if(!userName){
    userNameError = "UserName is Invalid ";
    }
    if(!password){
    passwordError = "Password is required";
    }
    if(userNameError || passwordError){
      setError({...error, userNameError, passwordError})
    return false;
    }
    return true;
    }
  const authLogin = () => {
    const payload = {
      userName,
      password,
    };

    if(!validate()){
      return
    }
    setError({userNameError : '', passwordError : ''})

    dispatch(Actions.login(payload))
      .then((response) => {
        if (response.status === 200) {
          localStorage.setItem('token', response.data.authToken);
          dispatch({
            type: AUTH_TOKEN,
            payload: response.data,
          });
          navigate('/dashboards/dashboard1');
          toast.success('Login succesfully');
        } 
        else if(response.status === 409){
          toast.error(" Email yet not verified");
        }
        else {
          localStorage.removeItem('token');
          dispatch({
            type: AUTH_TOKEN,
            payload: null,
          });
          toast.error("Invalid username of password");
        }
      })
      .catch((error) => error);
  };

  return (
    <PageContainer title="Login" description="this is Login page">
      <Grid container spacing={0} sx={{ height: '100vh', justifyContent: 'center' }}>
        <Grid
          item
          xs={12}
          sm={12}
          lg={6}
          sx={{
            background: (theme) => `${theme.palette.mode === 'dark' ? '#1c1f25' : '#ffffff'}`,
          }}
        >
          <Box
            sx={{
              position: 'relative',
            }}
          >
            <Box
              display="flex"
              alignItems="center"
              justifyContent="center"
              sx={{
                position: {
                  xs: 'relative',
                  lg: 'absolute',
                },
                height: { xs: 'auto', lg: '100vh' },
                right: { xs: 'auto', lg: '-50px' },
                margin: '0 auto',
              }}
            >
              <img
                src={img1}
                alt="bg"
                style={{
                  width: '100%',
                  maxWidth: '812px',
                }}
              />
            </Box>

            <Box
              sx={{
                p: 4,
                position: 'absolute',
                top: '0',
              }}
            >
              <img src={LogoIcon} width="100px" />
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} sm={8} lg={6} display="flex" alignItems="center">
          <Grid container spacing={0} display="flex" justifyContent="center">
            <Grid item xs={12} lg={9} xl={6}>
              <Box
                sx={{
                  p: 4,
                }}
              >
                <Typography fontWeight="700" variant="h2">
                  Welcome to Sukla Implant
                </Typography>
                <Box display="flex" alignItems="center">
                  <Typography
                    color="textSecondary"
                    variant="h6"
                    fontWeight="500"
                    sx={{
                      mr: 1,
                    }}
                  >
                    New to Sukla Implant?
                  </Typography>
                  <Typography
                    component={Link}
                    to="/auth/register"
                    fontWeight="500"
                    sx={{
                      display: 'block',
                      textDecoration: 'none',
                      color: 'primary.main',
                    }}
                  >
                    Create an account
                  </Typography>
                </Box>
                <Box
                  sx={{
                    mt: 4,
                  }}
                >
                  <CustomFormLabel htmlFor="UserName">User Name</CustomFormLabel>
                  <CustomTextField
                        name="userName"
                        value={userName}
                        onChange={(e) => 
                          setUserName(e.target.value)
                        }
                        id="UserName"
                        variant="outlined"
                        fullWidth
                        error={error?.userNameError.length}
                        helperText={error?.userNameError}
                        
                      />

                  <CustomFormLabel htmlFor="password">Password</CustomFormLabel>
                  <CustomTextField
                    name="password"
                    value={password}
                    onChange={(e) => 
                      setPassword(e.target.value)
                    }
                    id="password"
                    type="password"
                    variant="outlined"
                    fullWidth
                    sx={{
                      mb: 3,
                    }}

                    error={error?.passwordError.length}
                    helperText={error?.passwordError}
                  />
                  <Box
                    sx={{
                      display: {
                        xs: 'block',
                        sm: 'flex',
                        lg: 'flex',
                      },
                      alignItems: 'center',
                    }}
                  >
                    {/* <FormGroup>
                      <FormControlLabel
                        control={<CustomCheckbox defaultChecked />}
                        label="Remeber this Device"
                        sx={{
                          mb: 2,
                        }}
                      />
                    </FormGroup> */}
                    {/* <Box
                      sx={{
                        ml: 'auto',
                      }}
                    >
                      <Typography
                        component={Link}
                        to="/auth/reset-password"
                        fontWeight="500"
                        sx={{
                          display: 'block',
                          textDecoration: 'none',
                          mb: '16px',
                          color: 'primary.main',
                        }}
                      >
                        Forgot Password ?
                      </Typography>
                    </Box> */}
                  </Box>

                  <Button
                    color="secondary"
                    variant="contained"
                    size="large"
                    fullWidth
                    sx={{
                      pt: '10px',
                      pb: '10px',
                    }}
                    onClick={() => authLogin()}
                  >
                    Sign In
                  </Button>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </PageContainer>
  );
};

export default Login;
