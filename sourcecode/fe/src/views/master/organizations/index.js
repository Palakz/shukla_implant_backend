/* eslint-disable */
import React, { useEffect, useState, useContext } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from "react-redux";
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import {
  Card,
  CardContent,
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableFooter,
  TablePagination,
  Chip,
  Grid,
  IconButton,
} from '@mui/material';
import * as Common from '../../../redux/common/Action';

import Breadcrumb from '../../../layouts/full-layout/breadcrumb/Breadcrumb';
import PageContainer from '../../../components/container/PageContainer';
import FormDialog from './components/FormDialog';
import { toast } from 'react-toastify';
import { LoaderContext } from "utils/LoaderContext";

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const BCrumb = [
  {
    to: '/',
    title: 'Dashboard',
  },
  {
    title: 'Organization',
  },
];

const BasicTable = () => {
  const dispatch = useDispatch();
  const { toggalLoader } = useContext(LoaderContext);

  const getOrganizationList = useSelector(state => state.CommonReducer?.organizationList)
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - getOrganizationList.length) : 0;

  const [open, setOpen] = useState(false);
  const [initialValues, setInitialValues] = useState({
    name: ""
  })
  
  const getOrganization = () => {
    toggalLoader(true)
    dispatch(Common.getOrganization())
        .then(response => {
          toggalLoader(false)
            if (response.status === 200) {
              dispatch({ type: "ADD_ORGANIZATION", payload: response.data });
            }
        }).catch((error) => error);
  }

  const DeleteOrganization = (_id) => {
    toggalLoader(true)
    dispatch(Common.DeleteOrganization({id : _id}))
        .then(response => {
          toggalLoader(false)
            if (response.status === 200) {
              getOrganization()
              toast.success(`Organization deleted succesfully`);
            }
        }).catch((error) => error);
  }

  useEffect(()=>{
    getOrganization()
  },[])

  const editFormData = (data) => {
    setOpen(true)
    setInitialValues(data)
  }

  const addFormData = (data) => {
    setOpen(data)
    setInitialValues({
      name: ""
    })
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return(
    <PageContainer title="Organization" description="this is Organization Table page">
      {/* breadcrumb */}
      <Breadcrumb title="Organization" items={BCrumb} />
      {/* end breadcrumb */}
      <Card>
        <CardContent>
          <Box
            sx={{
              overflow: {
                xs: 'auto',
                sm: 'unset',
              },
            }}
          >
            <Grid  className='pagetitle-container' style={{ float: 'right' }}>
                    <FormDialog
                    initialValues={initialValues} 
                    open={open} 
                    setOpen={(data)=> addFormData(data)} 
                    getList={()=>{getOrganization()}} 
                    />
                </Grid>
            <Table
              aria-label="simple table"
              sx={{
                whiteSpace: 'nowrap',
              }}
            >
              <TableHead>
                <TableRow>
                  <TableCell>
                    <Typography variant="h5">Id</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="h5">Title</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="h5">Action</Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {(rowsPerPage > 0
                  ? getOrganizationList.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  : getOrganizationList
                ).map((org) => (
                  <TableRow key={org.id}>
                    <TableCell>
                      <Typography color="textSecondary" variant="h6" fontWeight="400">
                        {org.id}
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography color="textSecondary" variant="h6" fontWeight="400">
                        {org.name}
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Chip
                        sx={{
                          backgroundColor: (theme) => theme.palette.success.light,
                          color: (theme) => theme.palette.success.main,
                          borderRadius: '6px',
                          pl: '3px',
                          pr: '3px',
                        }}
                        size="small"
                        label='Edit'
                        onClick={() => editFormData(org)}
                      />
                      {' '}
                      <Chip
                        sx={{
                          backgroundColor: (theme) => theme.palette.error.light,
                          color: (theme) => theme.palette.success.dark,
                          borderRadius: '6px',
                          pl: '3px',
                          pr: '3px',
                        }}
                        size="small"
                        label='Delete'
                        onClick={() => DeleteOrganization(org.id)}
                      />
                    </TableCell>
                  </TableRow>
                ))}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                    colSpan={6}
                    count={getOrganizationList.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputprops: {
                        'aria-label': 'rows per page',
                      },
                      native: true,
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </Box>
        </CardContent>
      </Card>
    </PageContainer>
  )
};

export default BasicTable;
