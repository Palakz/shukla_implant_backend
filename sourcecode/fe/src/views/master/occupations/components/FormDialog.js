/* eslint-disable */  
import React, {useState} from 'react';
import { useDispatch } from 'react-redux';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import FormHelperText from '@mui/material/FormHelperText';
import DialogTitle from '@mui/material/DialogTitle';
import * as Yup from "yup";
import { Formik, Form, Field, ErrorMessage } from "formik";
import CustomTextField from '../../../../components/forms/custom-elements/CustomTextField';
import * as Common from '../../../../redux/common/Action';
import { toast } from 'react-toastify';

const  FormDialog = (props) => {
    const dispatch = useDispatch();
    const { getList, open, setOpen, initialValues } = props;
    const validationSchema = Yup.object({
      name: Yup.string().required()
    });

    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };

    const onSubmit = (payload) => {

      dispatch(initialValues?.id ? Common.UpdateOccupations(payload) : Common.addOccupations(payload))
      .then((response) => {
        if (response.status === 200) {
          toast.success(`Occupation ${(initialValues?.id) ? 'updated' : 'added'} succesfully`);
          handleClose()
          resetForm()
          getList() 
        } 
        else {
          toast.error("Something went wrong");
        }
      })
      .catch((error) => error);
    };

  return (
    <div>
      <Button variant="contained" className='primaryButton nextBtn' onClick={handleClickOpen} >+ Add Organization</Button >
      <Dialog fullWidth open={open} onClose={handleClose}>
        <DialogTitle>Occupation</DialogTitle>
        <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={async (values, { resetForm }) => {
        await onSubmit(values, resetForm);
      }}
    >
      <Form>
        <DialogContent>
        <Field name="name">
             {({
               field, // { name, value, onChange, onBlur }
               form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
               meta,
             }) => (
               <>
                 <CustomTextField
                    id="name"
                    type="text"
                    fullWidth
                    placeholder="Enter name"
                    variant="outlined"
                    size="small"
                    {...field}
                    inputProps={{ 'aria-label': 'Enter name' }}
                    sx={{
                      mb: 1,
                    }}
                    error={meta.touched && Boolean(meta.error)}
                    helperText={meta.error}
                  />
               </>
             )}
           </Field>
        </DialogContent>
        <DialogActions>
          <Button variant="contained"
          color="secondary"
          size="large"
          sx={{
            width: '100%',
            display: 'block',
          }} onClick={handleClose}>Cancel</Button>
          <Button variant="contained"
          type="submit"
          color="primary"
          size="large"
          sx={{
            width: '100%',
            display: 'block',
          }} >Submit</Button>
          
        </DialogActions>
        </Form>
    </Formik>
      </Dialog>
    </div>
  );
}

export default FormDialog;
