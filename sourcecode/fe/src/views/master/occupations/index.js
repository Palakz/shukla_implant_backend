/* eslint-disable */
import React, { useEffect, useState, useContext } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from "react-redux";
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import {
  Card,
  CardContent,
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableFooter,
  TablePagination,
  Chip,
  Grid,
  IconButton,
} from '@mui/material';
import * as Common from '../../../redux/common/Action';

import Breadcrumb from '../../../layouts/full-layout/breadcrumb/Breadcrumb';
import PageContainer from '../../../components/container/PageContainer';
import FormDialog from './components/FormDialog';
import { toast } from 'react-toastify';
import { LoaderContext } from "utils/LoaderContext";


function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};


const BCrumb = [
  {
    to: '/',
    title: 'Dashboard',
  },
  {
    title: 'Occupation',
  },
];

const BasicTable = () => {
  const dispatch = useDispatch();
  const { toggalLoader } = useContext(LoaderContext);

  const getOccupationList = useSelector(state => state.CommonReducer?.occupationList)

  const [open, setOpen] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - getOccupationList.length) : 0;

  const [initialValues, setInitialValues] = useState({
    name: ""
  })

  const getOccupations = () => {
    toggalLoader(true);
    dispatch(Common.getOccupations())
        .then(response => {
          toggalLoader(false);
            if (response.status === 200) {
              dispatch({ type: "ADD_OCCUPATION", payload: response.data });
              // setOccupationData(response.data)
            }
        }).catch((error) => error);
  }

  const DeleteOccupations = (_id) => {
    toggalLoader(true);
    dispatch(Common.DeleteOccupations({id : _id}))
        .then(response => {
            if (response.status === 200) {
              toast.success(`Occupation deleted succesfully`);
              getOccupations()
            }
            toggalLoader(false);
        }).catch((error) => error);
  }

  useEffect(()=>{
    getOccupations()
  },[])

  const editFormData = (data) => {
    setOpen(true)
    setInitialValues(data)
  }

  const addFormData = (data) => {
    setOpen(data)
    setInitialValues({
      name: ""
    })
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <PageContainer title="Occupation" description="this is Occupation Table page">
      {/* breadcrumb */}
      <Breadcrumb title="Occupation" items={BCrumb} />
      {/* end breadcrumb */}
      <Card>
        <CardContent>
          <Box
            sx={{
              overflow: {
                xs: 'auto',
                sm: 'unset',
              },
            }}
          >
            <Grid  className='pagetitle-container' style={{ float: 'right' }}>
                    <FormDialog 
                      initialValues={initialValues} 
                      open={open} 
                      setOpen={(data)=> addFormData(data)} 
                      getList={()=>{getOccupations()}} 
                    />
                </Grid>
            <Table
              aria-label="simple table"
              sx={{
                whiteSpace: 'nowrap',
              }}
            >
              <TableHead>
                <TableRow>
                  <TableCell>
                    <Typography variant="h5">Id</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="h5">Title</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="h5">Action</Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {(rowsPerPage > 0
                  ? getOccupationList.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  : getOccupationList
                ).map((occ) => (
                  <TableRow key={occ.id}>
                    <TableCell>
                      <Typography color="textSecondary" variant="h6" fontWeight="400">
                        {occ.id}
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography color="textSecondary" variant="h6" fontWeight="400">
                        {occ.name}
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Chip
                        sx={{
                          backgroundColor: (theme) => theme.palette.success.light,
                          color: (theme) => theme.palette.success.main,
                          borderRadius: '6px',
                          pl: '3px',
                          pr: '3px',
                        }}
                        size="small"
                        label='Edit'
                        onClick={() => editFormData(occ)}
                      />
                      {' '}
                      <Chip
                        sx={{
                          backgroundColor: (theme) => theme.palette.error.light,
                          color: (theme) => theme.palette.success.dark,
                          borderRadius: '6px',
                          pl: '3px',
                          pr: '3px',
                        }}
                        size="small"
                        label='Delete'
                        onClick={() => DeleteOccupations(occ.id)}
                      />
                    </TableCell>
                  </TableRow>
                ))}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                    colSpan={6}
                    count={getOccupationList.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputprops: {
                        'aria-label': 'rows per page',
                      },
                      native: true,
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </Box>
        </CardContent>
      </Card>
    </PageContainer>
  );
};

export default BasicTable;
