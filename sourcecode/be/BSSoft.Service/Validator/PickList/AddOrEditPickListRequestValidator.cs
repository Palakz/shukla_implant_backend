﻿namespace SSWhite.Service.Validator.PickList
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using FluentValidation;
    using FluentValidation.Results;
    using SSWhite.Core.Common.Session;
    using SSWhite.Domain.Request.PickList;
    using SSWhite.ResourceFile;
    using SSWhite.Service.Interface.PickList;

    public class AddOrEditPickListRequestValidator : AbstractValidator<AddOrEditPickListRequest>
    {
        private readonly IPickListService _pickListService;
        private List<string> _list = new List<string>();

        public AddOrEditPickListRequestValidator()
        {
            _pickListService = (IPickListService)SessionGetterSetter.GetServiceProvider(typeof(IPickListService));
            _list = _pickListService.GetAllPickList().Result.Select(x => x.Name).ToList();
            RuleFor(x => x.Name)
               .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name));

            RuleFor(x => x.Description)
               .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Description));

            RuleFor(x => x.IsValueNameSame)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, Validations.IsValueNameSame));

            RuleFor(x => x.IsValueAutoId)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.IsValueAutoId));

            RuleFor(x => x.DisplayOrder)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.DisplayOrder))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, Labels.Id, 0));

            RuleFor(x => x.OrderBy)
               .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.OrderBy));

            When(x => x.Id != null, () =>
            {
                RuleFor(x => x.Name)
                    .Custom((context, option) =>
                    {
                        _list.Remove(context);
                        if (_list.Contains(context, StringComparer.OrdinalIgnoreCase))
                        {
                            option.AddFailure(option.MessageFormatter.BuildMessage(string.Format("Name already exists")));
                        }
                    });
            }).Otherwise(() =>
            {
                RuleFor(x => x.Name)
                    .Custom((context, option) =>
                    {
                        if (_list.Contains(context, StringComparer.OrdinalIgnoreCase))
                        {
                            option.AddFailure(option.MessageFormatter.BuildMessage(string.Format("Name already exists")));
                        }
                    });
            });
        }

        ////public async override Task<ValidationResult> ValidateAsync(ValidationContext<AddOrEditPickListRequest> context, CancellationToken cancellation = default)
        ////{
        ////    var session = SessionGetterSetter.Get();
        ////    var s = await _pickListService.GetAllPickList();
        ////    return await base.ValidateAsync(context, cancellation);
        ////}
    }
}
