﻿namespace SSWhite.Service.Validator.PickList
{
    using FluentValidation;
    using SSWhite.Domain.Request.PickList;
    using SSWhite.ResourceFile;

    public class AddPickListValueImageRequestValidator : AbstractValidator<AddPickListValueImageRequest>
    {
        public AddPickListValueImageRequestValidator()
        {
            RuleFor(x => x.PicklistValueId)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.Id))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, Labels.Id, 0));
        }
    }
}
