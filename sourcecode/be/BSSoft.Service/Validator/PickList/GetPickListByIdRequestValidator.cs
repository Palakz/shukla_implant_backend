﻿namespace SSWhite.Service.Validator.PickList
{
    using FluentValidation;
    using SSWhite.Domain.Request.PickList;
    using SSWhite.ResourceFile;

    public class GetPickListByIdRequestValidator : AbstractValidator<GetPickListByIdRequest>
    {
        public GetPickListByIdRequestValidator()
        {
            RuleFor(x => x.Id)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.Id))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, Labels.Id, 0));
        }
    }
}
