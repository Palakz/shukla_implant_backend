﻿namespace SSWhite.Service.Validator.PickList
{
    using FluentValidation;
    using SSWhite.Domain.Request.PickList;
    using SSWhite.ResourceFile;

    public class PickListValueValidator : AbstractValidator<PickListValue>
    {
        public PickListValueValidator(bool isEdit = false)
        {
            ////RuleFor(x => x.Id)
            ////  .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Id))
            ////  .When(x => isEdit);

            RuleFor(x => x.PickListId)
               .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.PickListId));

            RuleFor(x => x.DisplayOrder)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.DisplayOrder))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, Labels.Id, 0));

            RuleFor(x => x.Name)
               .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name));

            RuleFor(x => x.Value)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.Value));
        }
    }
}
