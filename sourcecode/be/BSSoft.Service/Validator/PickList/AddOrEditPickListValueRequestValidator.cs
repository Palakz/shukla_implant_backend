﻿namespace SSWhite.Service.Validator.PickList
{
    using FluentValidation;
    using SSWhite.Domain.Request.PickList;

    public class AddOrEditPickListValueRequestValidator : AbstractValidator<AddOrEditPickListValueRequest>
    {
        public AddOrEditPickListValueRequestValidator()
        {
            When(x => x.IsEdit == true, () =>
            {
                RuleForEach(x => x.PickListValue).SetValidator(new PickListValueValidator(true));
            }).Otherwise(() =>
            {
                RuleForEach(x => x.PickListValue).SetValidator(new PickListValueValidator(false));
            });
        }
    }
}
