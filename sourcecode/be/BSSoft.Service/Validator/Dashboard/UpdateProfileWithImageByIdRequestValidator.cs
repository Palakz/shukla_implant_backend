﻿namespace SSWhite.Service.Validator.Dashboard
{
    using System;
    using FluentValidation;
    using SSWhite.Core.Extensions;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.Dashboard;
    using SSWhite.ResourceFile;

    public class UpdateProfileWithImageByIdRequestValidator : AbstractValidator<UpdateProfileWithImageByIdRequest>
    {
        public UpdateProfileWithImageByIdRequestValidator()
        {
            When(x => x.Image != null, () =>
            {
                RuleFor(x => x.Image.Length).NotNull().LessThanOrEqualTo(5242880)
                .WithMessage("File size is larger than allowed. Allowed file size is 5 MB");

                RuleFor(x => x.Image.ContentType).NotNull().Must(x => x.Equals("image/jpeg") || x.Equals("image/jpg") || x.Equals("image/png"))
                .WithMessage("File type not allowed");
            });
        }
    }
}
