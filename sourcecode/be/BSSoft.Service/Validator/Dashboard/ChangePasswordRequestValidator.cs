﻿namespace SSWhite.Service.Validator.Dashboard
{
    using System;
    using FluentValidation;
    using SSWhite.Core.Extensions;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.Dashboard;
    using SSWhite.ResourceFile;

    public class ChangePasswordRequestValidator : AbstractValidator<ChangePasswordRequest>
    {
        public ChangePasswordRequestValidator()
        {
            RuleFor(x => x.CurrentPassword)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, "Current Password"));

            RuleFor(x => x.NewPassword)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, "New Password"))
                .MinimumLength(8).MaximumLength(100);

            RuleFor(x => x.VerifyNewPassword)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, "Verify New Password"))
                .MinimumLength(8).MaximumLength(100);

            When(x => !x.VerifyNewPassword.IsNullOrWhiteSpace() && !x.NewPassword.IsNullOrWhiteSpace(), () =>
            {
                RuleFor(x => x.NewPassword.Trim())
                    .Equal(x => x.VerifyNewPassword.Trim())
                    .WithMessage("new password and verify new password are not same");
            });
        }
    }
}
