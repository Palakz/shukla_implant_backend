﻿namespace SSWhite.Service.Validator.Organization.User
{
    using FluentValidation;
    using SSWhite.Core.Common;
    using SSWhite.Domain.Common.Organization.User;

    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            // RuleFor(x => x.FirstName)
            //    .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.FirstName))
            //    .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.FirstName, 100));

            // RuleFor(x => x.LastName)
            //    .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.LastName))
            //    .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.LastName, 100));

            // RuleFor(x => x.UserName)
            //    .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.UserName))
            //    .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.UserName, 100));

            // RuleFor(x => x.MobileNo)
            //    .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.MobileNumber))
            //    .MinimumLength(10).WithMessage(string.Format(Validations.MinLength, Labels.MobileNumber, 10));

            // RuleFor(x => x.EmailAddress)
            //    .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Email))
            //    .Matches(RegexExpression.EMAIL).WithMessage(string.Format(Validations.Invalid, Labels.Email))
            //    .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Email, 100));

            // RuleFor(x => x.Address)
            //    .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Address))
            //    .MaximumLength(200).WithMessage(string.Format(Validations.MaxLength, Labels.Address, 200));

            ////RuleFor(x => x.Entities)
            ////    .NotEmpty().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Site));
        }
    }
}
