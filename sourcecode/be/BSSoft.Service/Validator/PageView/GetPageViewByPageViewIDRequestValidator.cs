﻿namespace SSWhite.Service.Validator.PickList
{
    using System;
    using FluentValidation;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.Common;
    using SSWhite.Domain.Request.PageView;
    using SSWhite.Domain.Request.PickList;
    using SSWhite.ResourceFile;

    public class GetPageViewByPageViewIDRequestValidator : AbstractValidator<GetPageViewByPageViewIDRequest>
    {
        public GetPageViewByPageViewIDRequestValidator()
        {
            RuleFor(x => x.PageViewID)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.Id))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, Labels.Id, 0));
        }
    }
}
