﻿namespace SSWhite.Service.Validator.Admin
{
    using System;
    using FluentValidation;
    using SSWhite.Domain.Request.Admin;

    public class AddNewEmployeeRequestValidator : AbstractValidator<AddUserRequest>
    {
        // public AddNewEmployeeRequestValidator()
        // {
        //    RuleFor(x => x.PersonID)
        //        .NotEmpty().WithMessage("Person Id Is Required")
        //        .MinimumLength(3)
        //        .MaximumLength(10);

        // RuleFor(x => x.Organization)
        //        .NotEmpty().WithMessage("Organization Name Is Required");

        // RuleFor(x => x.FirstName)
        //        .NotEmpty().WithMessage("First Name Is Required")
        //        .MinimumLength(3)
        //        .MaximumLength(50);

        // RuleFor(x => x.LastName)
        //        .NotEmpty().WithMessage("Last Name Is Required")
        //        .MinimumLength(3)
        //        .MaximumLength(50);

        // RuleFor(x => x.Gender)
        //        .NotEmpty().WithMessage("Gender Is Required");

        // RuleFor(x => x.DOB)
        //        .NotEmpty().WithMessage("Date Of Birth Is Required")
        //        .LessThanOrEqualTo(new DateTime(2005, 1, 1, 00, 0, 0, 000));

        // RuleFor(x => x.DOJ)
        //        .NotEmpty().WithMessage("Date Of Joining Is Required");

        // RuleFor(x => x.Address1)
        //       .NotEmpty().WithMessage("Address 1 Is Required")
        //       .MinimumLength(3)
        //        .MaximumLength(200);

        // RuleFor(x => x.Address2)
        //       .NotEmpty().WithMessage("Address 2 Is Required")
        //       .MinimumLength(3)
        //        .MaximumLength(200);

        // RuleFor(x => x.BloodGroup)
        //       .NotEmpty().WithMessage("Blood Group Is Required");

        // RuleFor(x => x.EmailAddress)
        //       .NotEmpty().WithMessage("Email Address Is Required")
        //       .EmailAddress().WithMessage("A valid email is required")
        //       .MinimumLength(10)
        //        .MaximumLength(100);

        // RuleFor(x => x.EmpPosition)
        //       .NotEmpty().WithMessage("Employee Position Is Required");

        // RuleFor(x => x.ReportingUnder)
        //       .NotEmpty().WithMessage("Reporting Under Is Required");

        // RuleFor(x => x.Department)
        //       .NotEmpty().WithMessage("Department Is Required");

        // RuleFor(x => x.PhoneNo)
        //       .NotEmpty().WithMessage("Phone Number Is Required")
        //       .MinimumLength(10)
        //        .MaximumLength(10);

        // RuleFor(x => x.AvatarImage)
        //       .NotNull().WithMessage("AvatarImage Is Required");

        // RuleFor(x => x.SignatureImage)
        //       .NotNull().WithMessage("SignatureImage Is Required");
        // }
    }
}
