﻿namespace SSWhite.Service.Validator.Common
{
    using System;
    using FluentValidation;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.Common;
    using SSWhite.ResourceFile;

    public class InsertPickListRequestValidator : AbstractValidator<InsertPickListRequest>
    {
        public InsertPickListRequestValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name));

            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Description));
        }
    }
}
