﻿namespace SSWhite.Service.Validator.Implant
{
    using FluentValidation;
    using SSWhite.Domain.Request.Implant;
    using SSWhite.ResourceFile;

    public class GetImplantTypeByIdRequestValidator : AbstractValidator<GetImplantTypeByIdRequest>
    {
        public GetImplantTypeByIdRequestValidator()
        {
            RuleFor(x => x.Id)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.Id))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, Labels.Id, 0));
        }
    }
}
