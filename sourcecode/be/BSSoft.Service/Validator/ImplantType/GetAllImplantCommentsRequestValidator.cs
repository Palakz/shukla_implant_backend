﻿namespace SSWhite.Service.Validator.Implant
{
    using System;
    using FluentValidation;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.Common;
    using SSWhite.Domain.Request.Implant;
    using SSWhite.ResourceFile;

    public class GetAllImplantCommentsRequestValidator : AbstractValidator<GetAllImplantCommentsRequest>
    {
        public GetAllImplantCommentsRequestValidator()
        {
            RuleFor(x => x.ImplantId)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, "ImplantID"))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, "ImplantID", 0));
        }
    }
}
