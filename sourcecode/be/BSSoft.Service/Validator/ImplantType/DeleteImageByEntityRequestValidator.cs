﻿namespace SSWhite.Service.Validator.Implant
{
    using FluentValidation;
    using SSWhite.Domain.Request.Implant;
    using SSWhite.ResourceFile;

    public class DeleteImageByEntityRequestValidator : AbstractValidator<DeleteImageByEntityRequest>
    {
        public DeleteImageByEntityRequestValidator()
        {
            RuleFor(x => x.EntityId)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, "EntityId"))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, "EntityId", 0));

            RuleFor(x => x.EntityName)
               .NotEmpty().WithMessage(string.Format(Validations.IsRequired, "EntityName"));

            RuleFor(x => x.ImagePath)
               .NotEmpty().WithMessage(string.Format(Validations.IsRequired, "ImagePath"));

            RuleFor(x => x.EntityColumnName)
               .NotEmpty().WithMessage(string.Format(Validations.IsRequired, "EntityColumnName"));
        }
    }
}
