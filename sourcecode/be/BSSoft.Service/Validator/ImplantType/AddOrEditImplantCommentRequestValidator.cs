﻿namespace SSWhite.Service.Validator.Implant
{
    using FluentValidation;
    using SSWhite.Domain.Request.Implant;
    using SSWhite.ResourceFile;

    public class AddOrEditImplantCommentRequestValidator : AbstractValidator<AddOrEditImplantCommentRequest>
    {
        public AddOrEditImplantCommentRequestValidator()
        {
            RuleFor(x => x.ImplantId)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, "ImplantId"))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, "ImplantId", 0));

            RuleFor(x => x.Comment)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, "Comment"));
        }
    }
}
