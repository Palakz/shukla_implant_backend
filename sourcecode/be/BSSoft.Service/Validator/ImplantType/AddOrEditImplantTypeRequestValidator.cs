﻿namespace SSWhite.Service.Validator.Implant
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using FluentValidation;
    using FluentValidation.Results;
    using SSWhite.Core.Common.Session;
    using SSWhite.Domain.Request.Implant;
    using SSWhite.ResourceFile;
    using SSWhite.Service.Interface.Implant;

    public class AddOrEditImplantTypeRequestValidator : AbstractValidator<AddOrEditImplantTypeRequest>
    {
        private readonly IImplantService _implantService;
        private List<string> _list = new List<string>();

        public AddOrEditImplantTypeRequestValidator()
        {
            _implantService = (IImplantService)SessionGetterSetter.GetServiceProvider(typeof(IImplantService));
            _list = _implantService.GetAllImplantType().Result.Select(x => x.Name).ToList();

            RuleFor(x => x.Name)
               .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
               .MinimumLength(3);

            When(x => x.IsEdit == true, () =>
            {
                RuleFor(x => x.Id)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.Id))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, Labels.Id, 0));

                RuleFor(x => x.Name)
                .Custom((context, option) =>
                {
                    _list.Remove(context);
                    if (_list.Contains(context, StringComparer.OrdinalIgnoreCase))
                    {
                        ////option.AddFailure(option.MessageFormatter.BuildMessage(string.Format(Validations.Unique, Labels.Name)));
                        option.AddFailure(option.MessageFormatter.BuildMessage(string.Format("Duplicate Name")));
                    }
                });
            }).Otherwise(() =>
            {
                RuleFor(x => x.Name)
                .Custom((context, option) =>
                {
                    if (_list.Contains(context, StringComparer.OrdinalIgnoreCase))
                    {
                        ////option.AddFailure(option.MessageFormatter.BuildMessage(string.Format(Validations.Unique, Labels.Name)));
                        option.AddFailure(option.MessageFormatter.BuildMessage(string.Format("Duplicate Name")));
                    }
                });
            });
        }
    }
}
