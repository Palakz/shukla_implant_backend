﻿namespace SSWhite.Service.Validator.Implant
{
    using FluentValidation;
    using SSWhite.Domain.Request.Implant;
    using SSWhite.ResourceFile;

    public class LikeOrDislikeImplantCommentRequestValidator : AbstractValidator<LikeOrDislikeImplantCommentRequest>
    {
        public LikeOrDislikeImplantCommentRequestValidator()
        {
            RuleFor(x => x.CommentId)
               .NotNull().WithMessage(string.Format(Validations.IsRequired, "CommentId"))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, "CommentId", 0));

            RuleFor(x => x.IsLiked)
                .NotNull().WithMessage(string.Format(Validations.IsRequired, "ImplIsLikedantTypeId"));
        }
    }
}
