﻿namespace SSWhite.Service.Implementation.Account
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Account;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.Organization.Account;
    using SSWhite.Domain.Response.Account;
    using SSWhite.Service.Interface.Account;

    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly AppSetting _appSetting;

        public AccountService(IAccountRepository accountRepository, IOptions<AppSetting> appSetting)
        {
            _accountRepository = accountRepository;
            _appSetting = appSetting.Value;
        }

        public async Task ForgotPassword(ServiceRequest<ForgotPasswordRequest> request)
        {
            try
            {
                await _accountRepository.ForgotPassword(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ServiceResponse<LogInResponse>> ValidateUserForLogIn(ServiceRequest<LogInRequest> request)
        {
            var response = new ServiceResponse<LogInResponse>();
            try
            {
                response.Data = await _accountRepository.ValidateUserForLogIn(request);
                if (response.Data != null)
                {
                    response.Data.ProfileImage = response.Data.ProfileImage == null ? response.Data.ProfileImage : $"{_appSetting.ProfileImagePathToGetImage}{response.Data.ProfileImage}";
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
