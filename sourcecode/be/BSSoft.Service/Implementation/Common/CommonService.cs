﻿namespace SSWhite.Service.Implementation.Common
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Data.Interface.Common;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.Common;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Domain.Response.Common;
    using SSWhite.Service.Interface.Common;

    public class CommonService : ICommonService
    {
        private readonly ICommonRepository _commonRepository;

        public CommonService(ICommonRepository commonRepository)
        {
            _commonRepository = commonRepository;
        }

        public async Task<List<GetOrganizationResponse>> GetOrganization()
        {
            try
            {
                return await _commonRepository.GetOrganization();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetOccupationsResponse>> GetOccupations()
        {
            try
            {
                return await _commonRepository.GetOccupations();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task InsertPickList(ServiceRequest<InsertPickListRequest> request)
        {
            try
            {
                 await _commonRepository.InsertPickList(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task InsertPickListValue(ServiceRequest<InsertPickListValueRequest> request)
        {
            try
            {
                 await _commonRepository.InsertPickListValue(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<SelectPickListsResponse>> SelectPickLists()
        {
            try
            {
                return await _commonRepository.SelectPickLists();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task UpdateOccupations(ServiceRequest<UpdateOccupationsRequest> request)
        {
            try
            {
                 await _commonRepository.UpdateOccupations(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task UpdateOrganization(ServiceRequest<UpdateOrganizationRequest> request)
        {
            try
            {
                await _commonRepository.UpdateOrganization(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task AddOccupations(ServiceRequest<AddOccupationsRequest> request)
        {
            try
            {
                 await _commonRepository.AddOccupations(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task AddOrganization(ServiceRequest<AddOrganizationRequest> request)
        {
            try
            {
                await _commonRepository.AddOrganization(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteOccupations(ServiceRequest<DeleteOccupationsRequest> request)
        {
            try
            {
                await _commonRepository.DeleteOccupations(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteOrganization(ServiceRequest<DeleteOrganisationsRequest> request)
        {
            try
            {
                await _commonRepository.DeleteOrganization(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task ChangeOrganizationStatus(ServiceRequest<ChangeOrganizationStatusRequest> request)
        {
            try
            {
                await _commonRepository.ChangeOrganizationStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task ChangeOccupationStatus(ServiceRequest<ChangeOccupationStatusRequest> request)
        {
            try
            {
                await _commonRepository.ChangeOccupationStatus(request);
            }
            catch (Exception exception)
            {

                throw exception;
            }
        }
    }
}
