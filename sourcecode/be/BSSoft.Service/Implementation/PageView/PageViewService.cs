﻿namespace SSWhite.Service.Implementation.PageView
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Data.Interface.PageView;
    using SSWhite.Domain.Request.PageView;
    using SSWhite.Domain.Response.PageView;
    using SSWhite.Service.Interface.PageView;

    public class PageViewService : IPageViewService
    {

        private readonly IPageViewRepository _pageViewRepository;

        public PageViewService(IPageViewRepository pageViewRepository)
        {
            _pageViewRepository = pageViewRepository;
        }

        public async Task AddOrEditPageView(ServiceRequest<AddOrEditPageViewRequest> request)
        {
            await _pageViewRepository.AddOrEditPageView(request);
        }

        public async Task AddOrEditPageViewContainer(ServiceRequest<AddOrEditPageViewContainerRequest> request)
        {
            await _pageViewRepository.AddOrEditPageViewContainer(request);
        }

        public async Task AddOrEditPageViewProperty(ServiceRequest<AddOrEditPageViewPropertyRequest> request)
        {
            await _pageViewRepository.AddOrEditPageViewProperty(request);
        }

        public async Task AddOrEditPageViewFields(ServiceRequest<AddOrEditPageViewFieldsRequest> request)
        {
            await _pageViewRepository.AddOrEditPageViewFields(request);
        }

        public async Task DeletePageViewFields(ServiceRequest<DeletePageViewFieldsRequest> request)
        {
            await _pageViewRepository.DeletePageViewFields(request);
        }

        public async Task DeletePageView(ServiceRequest<DeletePageViewRequest> request)
        {
            await _pageViewRepository.DeletePageView(request);
        }

        public async Task DeletePageViewContainer(ServiceRequest<DeletePageViewContainerRequest> request)
        {
            await _pageViewRepository.DeletePageViewContainer(request);
        }

        public async Task<List<GetAllPageViewFieldsResponse>> GetAllPageViewFields()
        {
            return await _pageViewRepository.GetAllPageViewFields();
        }

        public async Task<List<GetAllPageViewResponse>> GetAllPageViewByImplantType(ServiceRequest<GetAllPageViewByImplantTypeRequest> request)
        {
            return await _pageViewRepository.GetAllPageViewByImplantType(request);
        }

        public async Task<List<GetAllPageViewPropertyResponse>> GetAllPageViewProperty()
        {
            return await _pageViewRepository.GetAllPageViewProperty();
        }

        public async Task DeletePageViewProperty(ServiceRequest<DeletePageViewPropertyRequest> request)
        {
            await _pageViewRepository.DeletePageViewProperty(request);
        }

        public async Task<List<GetAllContainerByPageViewIDResponse>> GetAllContainerByPageViewID(GetAllContainerByPageViewIDRequest request)
        {
            return await _pageViewRepository.GetAllContainerByPageViewID(request);
        }

        public async Task UpdateIsVisiblePageViewProperty(ServiceRequest<UpdateIsVisiblePageViewPropertyRequest> request)
        {
            await _pageViewRepository.UpdateIsVisiblePageViewProperty(request);
        }

        public async Task UpdateIsReadOnlyPageViewProperty(ServiceRequest<UpdateIsReadOnlyPageViewPropertyRequest> request)
        {
            await _pageViewRepository.UpdateIsReadOnlyPageViewProperty(request);
        }

        public async Task UpdateIsActivePageViewProperty(ServiceRequest<UpdateIsActivePageViewPropertyRequest> request)
        {
            await _pageViewRepository.UpdateIsActivePageViewProperty(request);
        }

        public async Task<GetPageViewByPageViewIDResponse> GetPageViewByPageViewID(GetPageViewByPageViewIDRequest request)
        {
            try
            {
                return await _pageViewRepository.GetPageViewByPageViewID(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetPageViewByPageViewIDResponse> GetPageViewByImplantType(ServiceRequest<GetPageViewByImplantTypeRequest> request)
        {
            try
            {
                return await _pageViewRepository.GetPageViewByImplantType(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<dynamic> GetAllRecentlyViewed(ServiceRequest<GetAllRecentlyViewedRequest> request)
        {
            try
            {
                return await _pageViewRepository.GetAllRecentlyViewed(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteRecentlyViewedById(DeleteRecentlyViewedByIdRequest request)
        {
            try
            {
                 await _pageViewRepository.DeleteRecentlyViewedById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteAllRecentlyViewedByImplantId(ServiceRequest<DeleteAllRecentlyViewedByImplantIdRequest> request)
        {
            try
            {
                 await _pageViewRepository.DeleteAllRecentlyViewedByImplantId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


    }
}
