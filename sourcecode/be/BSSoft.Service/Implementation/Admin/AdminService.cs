﻿namespace SSWhite.Service.Implementation.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Admin;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Service.Interface.Admin;

    public class AdminService : IAdminService
    {
        private readonly IAdminRepository _adminRepository;

        public AdminService(IAdminRepository adminRepository)
        {
            _adminRepository = adminRepository;
        }

        public async Task AddNewUser(ServiceRequest<AddUserRequest> request)
        {
            try
            {
                await _adminRepository.AddNewUser(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task AddNewUserWithImage(ServiceRequest<AddNewUserWithImageRequest> request)
        {
            try
            {
                await _adminRepository.AddNewUserWithImage(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllEmployeeLimitsResponse>> GetAllEmployeeLimits(GetAllEmployeeLimitsRequest request)
        {
            return await _adminRepository.GetAllEmployeeLimits(request);
        }

        public async Task UpdateToken(UpdateTokenByIdRequest updateEmployeeLimitByIdRequest)
        {
            try
            {
                await _adminRepository.UpdateToken(updateEmployeeLimitByIdRequest);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ServiceSearchResponse<GetAllUserDetailsByEmailResponse>> GetUserDetailsByEmail(GetAllUserDetailsByEmailRequest request)
        {
            try
            {
                return await _adminRepository.GetUserDetailsByEmail(request);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<CheckIfEmployeeIdExistsResponse> CheckIfEmployeeIdExists(CheckIfEmployeeIdExistsRequest checkIfEmployeeIdExistsRequest)
        {
            return await _adminRepository.CheckIfEmployeeIdExists(checkIfEmployeeIdExistsRequest);
        }

        public async Task<List<GetAllUserResponse>> GetAllUser()
        {
            try
            {
                return await _adminRepository.GetAllUser();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<GetAllUserDetailsResponse>> GetAllUserDetails()
        {
            try
            {
                return await _adminRepository.GetAllUserDetails();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task UpdateIsAdminApproveByUserId(ServiceRequest<UpdateIsAdminApproveByUserIdRequest> request)
        {
            try
            {
                await _adminRepository.UpdateIsAdminApproveByUserId(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task UpdateDocumentApprovalList(List<UpdateDocumentApprovalListRequest> request)
        {
            await _adminRepository.UpdateDocumentApprovalList(request);
        }

        public async Task ConfirmEmail(ConfirmEmailRequest ConfirmEmailRequest)
        {
            await _adminRepository.ConfirmEmail(ConfirmEmailRequest);
        }

        public async Task UpdateIsHoldStatusByUserId(UpdateIsHoldStatusByUserIdRequest request)
        {
            try
            {
                await _adminRepository.UpdateIsHoldStatusByUserId(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task UpdateOccupationRightsByOccupationId(ServiceRequest<UpdateOccupationRightsByOccupationIdRequest> request)
        {
            try
            {
                await _adminRepository.UpdateOccupationRightsByOccupationId(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task UpdateUserRightsByUserId(ServiceRequest<UpdateUserRightsByUserIdRequest> request)
        {
            try
            {
                await _adminRepository.UpdateUserRightsByUserId(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<GetUsersDetailsForDropDownResponse>> GetUsersDetailsForDropDown()
        {
            try
            {
                return await _adminRepository.GetUsersDetailsForDropDown();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<GetModuleRightsByOccupationIdResponse>> GetModuleRightsByOccupationId(GetModuleRightsByOccupationIdRequest request)
        {
            try
            {
                return await _adminRepository.GetModuleRightsByOccupationId(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<GetModuleRightsByUserIdResponse>> GetModuleRightsByUserId(GetModuleRightsByUserIdRequest request)
        {
            try
            {
                return await _adminRepository.GetModuleRightsByUserId(request);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
