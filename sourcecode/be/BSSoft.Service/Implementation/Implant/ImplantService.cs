﻿namespace SSWhite.Service.Implementation.Implant
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Implant;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.Implant;
    using SSWhite.Domain.Response.Implant;
    using SSWhite.Notification.Interface;
    using SSWhite.Notification.Model;
    using SSWhite.Service.Interface.Implant;

    public class ImplantService : IImplantService
    {
        private readonly IImplantRepository _implantRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ISendMailService _sendMailService;
        private readonly AppSetting _appSetting;

        public ImplantService(IImplantRepository implantRepository, IWebHostEnvironment webHostEnvironment, ISendMailService sendMailService, IOptions<AppSetting> appSetting)
        {
            _implantRepository = implantRepository;
            _webHostEnvironment = webHostEnvironment;
            _sendMailService = sendMailService;
            _appSetting = appSetting.Value;
        }

        public async Task AddOrEditImplantType(ServiceRequest<AddOrEditImplantTypeRequest> request)
        {
            try
            {
                await _implantRepository.AddOrEditImplantType(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task UpdateIsHoldStatusByUserId(ServiceRequest<ChangeImplantTypeStatusRequest> request)
        {
            try
            {
                await _implantRepository.ChangeImplantTypeStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task ChangeImplantFieldStatus(ServiceRequest<ChangeImplantFieldStatusRequest> request)
        {
            try
            {
                await _implantRepository.ChangeImplantFieldStatus(request);
            }
            catch (Exception exception)
            {

                throw exception;
            }
        }

        public async Task ChangeImplantStatus(ServiceRequest<ChangeImplantStatusRequest> request)
        {
            try
            {
                await _implantRepository.ChangeImplantStatus(request);
            }
            catch (Exception exception)
            {

                throw exception;
            }
        }
        public async Task DeleteImplantType(ServiceRequest<DeleteImplantTypeRequest> request)
        {
            try
            {
                await _implantRepository.DeleteImplantType(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteHistory(ServiceRequest<DeleteHistoryRequest> request)
        {
            try
            {
                await _implantRepository.DeleteHistory(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetAllImplantTypeResponse>> GetAllImplantType()
        {
            try
            {
                return await _implantRepository.GetAllImplantType();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetImplantTypeForDropdownResponse>> GetImplantTypeForDropdown()
        {
            try
            {
                return await _implantRepository.GetImplantTypeForDropdown();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetImplantTypeByIdResponse> GetImplantTypeById(GetImplantTypeByIdRequest request)
        {
            try
            {
                return await _implantRepository.GetImplantTypeById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetAllDataTypesResponse>> GetAllDataTypes()
        {
            try
            {
                return await _implantRepository.GetAllDataTypes();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetAllImplantFieldsResponse>> GetAllImplantFields()
        {
            try
            {
                return await _implantRepository.GetAllImplantFields();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetAllImplantFieldsResponse>> GetImplantFieldsByImplantType(int implantTypeId, bool includeAllImplantTypeFields = true)
        {
            try
            {
                return await _implantRepository.GetImplantFieldsByImplantType(implantTypeId, includeAllImplantTypeFields);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public async Task<GetAllImplantFieldsByIdResponse> GetAllImplantFieldsById(GetAllImplantFieldsByIdRequest request)
        {
            try
            {
                return await _implantRepository.GetAllImplantFieldsById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task AddOrEditImplantFields(ServiceRequest<AddOrEditImplantFieldsRequest> request)
        {
            try
            {
                await _implantRepository.AddOrEditImplantFields(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<dynamic> GetAllImplantByImplantTypeId(ServiceRequest<GetAllImplantByImplantTypeIdRequest> request)
        {
            try
            {
                return await _implantRepository.GetAllImplantByImplantTypeId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<Dictionary<string, string>> AddOrEditImplantForm(ServiceRequest<Dictionary<string, string>> request)
        {
            Dictionary<string, string> errorValues = new Dictionary<string, string>();
            try
            {
                var fieldMetaData = await _implantRepository.GetAllImplantFields();

                if (request.Data["IsFinalSave"] == "true")
                {
                    foreach (var item in request.Data)
                    {
                        var fieldDetail = fieldMetaData.FirstOrDefault(x => x.FieldName.Equals(item.Key, StringComparison.InvariantCultureIgnoreCase));
                        if (fieldDetail != null)
                        {
                            string errorMessage = "";
                            if (fieldDetail.IsRequired.HasValue && fieldDetail.IsRequired.Value && string.IsNullOrWhiteSpace(item.Value.Equals("null", StringComparison.OrdinalIgnoreCase) ? null : item.Value))
                            {
                                errorMessage = errorMessage + $"{fieldDetail.DisplayName} is required.";
                            }
                            else if (fieldDetail.MinLength.HasValue && fieldDetail.MinLength.Value > 0 && (string.IsNullOrWhiteSpace(item.Value) || item.Value.Length < fieldDetail.MinLength.Value))
                            {
                                errorMessage = errorMessage + $"{fieldDetail.DisplayName} minimum length must be {fieldDetail.MinLength.Value}.";
                            }
                            else if (fieldDetail.MaxLength.HasValue && fieldDetail.MaxLength.Value > 0 && (string.IsNullOrWhiteSpace(item.Value) || item.Value.Length > fieldDetail.MaxLength.Value))
                            {
                                errorMessage = errorMessage + $"{fieldDetail.DisplayName} maximum length must be {fieldDetail.MaxLength.Value}.";
                            }

                            if (!string.IsNullOrWhiteSpace(fieldDetail.Regex) && !string.IsNullOrWhiteSpace(item.Value) && !Regex.IsMatch(item.Value, fieldDetail.Regex))
                            {
                                errorMessage = errorMessage + $"Invalid data for the {fieldDetail.DisplayName} field.";
                            }

                            if (!string.IsNullOrEmpty(errorMessage))
                            {
                                errorValues.Add(item.Key, errorMessage);
                            }
                        }
                    }
                }
                if (errorValues.Count <= 0)
                    await _implantRepository.AddOrEditImplantForm(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return errorValues;
        }

        public async Task AddOrRemoveFavorite(ServiceRequest<AddOrRemoveFavoriteRequest> request)
        {
            try
            {
                await _implantRepository.AddOrRemoveFavorite(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<dynamic> GetAllFavoriteImplant(ServiceRequest<GetAllFavoriteImplantRequest> request)
        {
            try
            {
                return await _implantRepository.GetAllFavoriteImplant(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetAllHistoryImplantResponse>> GetAllHistory(ServiceRequest<GetAllHistoryRequest> request)
        {
            try
            {
                return await _implantRepository.GetAllHistory(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<dynamic> SearchImplant(ServiceRequest<SearchImplantRequest> request)
        {
            try
            {
                return await _implantRepository.SearchImplant(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<dynamic> GetAllUnapprovedImplant()
        {
            try
            {
                return await _implantRepository.GetAllUnapprovedImplant();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task ApproveImplant(ServiceRequest<ApproveImplantRequest> request)
        {
            try
            {
                var response = await _implantRepository.ApproveImplant(request);
                if (response.EmailAddress != null)
                {
                    await SendMail(response);
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task RejectImplant(ServiceRequest<ApproveImplantRequest> request)
        {
            try
            {
                var response = await _implantRepository.RejectImplant(request);
                if (response.EmailAddress != null)
                {
                    await SendMail(response);
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public async Task<dynamic> GetImplantById(GetImplantByIdRequest request)
        {
            try
            {
                return await _implantRepository.GetImplantById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteAllHistoryByUserId(ServiceRequest request)
        {
            try
            {
                await _implantRepository.DeleteAllHistoryByUserId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<dynamic> GetAllImplant()
        {
            try
            {
                return await _implantRepository.GetAllImplant();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<dynamic> GetRelatedImplantOfShuklaParts(GetRelatedImplantOfShuklaPartsRequest request)
        {
            try
            {
                return await _implantRepository.GetRelatedImplantOfShuklaParts(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task AddOrEditImplantComment(ServiceRequest<AddOrEditImplantCommentRequest> request)
        {
            try
            {
                await _implantRepository.AddOrEditImplantComment(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetAllImplantCommentsResponse>> GetAllImplantComments(ServiceRequest<GetAllImplantCommentsRequest> request)
        {
            try
            {
                return await _implantRepository.GetAllImplantComments(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task LikeOrDislikeImplantComment(ServiceRequest<LikeOrDislikeImplantCommentRequest> request)
        {
            try
            {
                await _implantRepository.LikeOrDislikeImplantComment(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetImplantNotificationResponse>> GetImplantNotification(ServiceRequest request)
        {
            try
            {
                return await _implantRepository.GetImplantNotification(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task MarkReadImplantNotification(ServiceRequest<MarkReadImplantNotificationRequest> request)
        {
            try
            {
                await _implantRepository.MarkReadImplantNotification(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteImageByEntity(DeleteImageByEntityRequest request)
        {
            try
            {
                await _implantRepository.DeleteImageByEntity(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task ChangeImplantTypeStatus(ServiceRequest<ChangeImplantTypeStatusRequest> request)
        {
            try
            {
                await _implantRepository.ChangeImplantTypeStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private async Task SendMail(ApproveImplantResponse request)
        {
            string emailBody = System.IO.File.ReadAllText(Path.Combine(_webHostEnvironment.WebRootPath, "EmailTemplates", "ApproveOrRejectImplant.html"));
            emailBody = emailBody.Replace("{ImplantId}", request.ImplantId.ToString());
            emailBody = emailBody.Replace("{ImplantName}", request.ImplantName);
            emailBody = emailBody.Replace("{IsApproved}", request.IsApprovedOrRejected);
            emailBody = emailBody.Replace("{ImagePath}", _appSetting.EmailSSwhiteShuklaLogo);
            SendMail sendMail = new SendMail() { ToEmails = request.EmailAddress, Body = emailBody, IsHtml = true, Subject = $"Implant {request.IsApprovedOrRejected}" };
            await _sendMailService.SendMailAsync(sendMail);
        }


    }
}
