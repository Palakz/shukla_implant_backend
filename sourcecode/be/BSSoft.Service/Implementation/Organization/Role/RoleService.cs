﻿namespace SSWhite.Service.Implementation.Organization.Role
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Organization.Role;
    using SSWhite.Domain.Common.Organization.Role;
    using SSWhite.Domain.Request.Organization.Role;
    using SSWhite.Domain.Response.Organization.Role;
    using SSWhite.Service.Interface.Organization.Role;

    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;

        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public async Task<ServiceResponse<int>> AddEditRole(ServiceRequest<Role> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _roleRepository.AddEditRole(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeRoleStatus(ServiceRequest<int> request)
        {
            try
            {
                await _roleRepository.ChangeRoleStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteRole(ServiceRequest<int> request)
        {
            try
            {
                await _roleRepository.DeleteRole(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllRolesResponse>> GetAllRoles(ServiceSearchRequest<GetAllRoles> request)
        {
            var response = new ServiceSearchResponse<GetAllRolesResponse>();
            try
            {
                response = await _roleRepository.GetAllRoles(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<Role>> GetRoleById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<Role>();
            try
            {
                response.Data = await _roleRepository.GetRoleById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
