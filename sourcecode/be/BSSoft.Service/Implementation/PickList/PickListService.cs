﻿namespace SSWhite.Service.Implementation.PickList
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Data.Interface.Common;
    using SSWhite.Data.Interface.PickList;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.Common;
    using SSWhite.Domain.Request.PickList;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Domain.Response.Common;
    using SSWhite.Domain.Response.PickList;
    using SSWhite.Service.Interface.Common;
    using SSWhite.Service.Interface.PickList;

    public class PickListService : IPickListService
    {
        private readonly IPickListRepository _pickListRepository;

        public PickListService(IPickListRepository picklistRepository)
        {
            _pickListRepository = picklistRepository;
        }

        public async Task AddOrEditPickList(ServiceRequest<AddOrEditPickListRequest> request)
        {
            try
            {
                await _pickListRepository.AddOrEditPickList(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeletePickList(ServiceRequest<DeletePickListRequest> request)
        {
            try
            {
                await _pickListRepository.DeletePickList(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetAllPickListResponse>> GetAllPickList()
        {
            try
            {
                return await _pickListRepository.GetAllPickList();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetPickListByIdResponse> GetPickListById(GetPickListByIdRequest request)
        {
            try
            {
                return await _pickListRepository.GetPickListById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetPickListValueByIdResponse>> GetPickListValueById(GetPickListValueByIdRequest request)
        {
            try
            {
                return await _pickListRepository.GetPickListValueById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeletePickListValue(ServiceRequest<DeletePickListValueRequest> request)
        {
            try
            {
                await _pickListRepository.DeletePickListValue(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task AddOrEditPickListValue(ServiceRequest<AddOrEditPickListValueRequest> request)
        {
            try
            {
                await _pickListRepository.AddOrEditPickListValue(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task AddPickListValueImage(ServiceRequest<AddPickListValueImageRequest> request)
        {
            try
            {
                await _pickListRepository.AddPickListValueImage(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task ChangePickListStatus(ServiceRequest<ChangePickListStatusRequest> request)
        {
            try
            {
                await _pickListRepository.ChangePickListStatus(request);
            }
            catch (Exception exception)
            {

                throw exception;
            }
        }
    }
}
