﻿namespace SSWhite.Service.Implementation.Account
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Account;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.Dashboard;
    using SSWhite.Domain.Response.Account;
    using SSWhite.Domain.Response.Dashboard;
    using SSWhite.Service.Interface.Account;
    using SSWhite.Service.Interface.Dashboard;

    public class DashboardService : IDashboardService
    {
        private readonly IDashboardRepository _dashboardRepository;
        private readonly IConfiguration _configuration;
        private readonly AppSetting _appSetting;


        public DashboardService(IDashboardRepository dashboardRepository, IConfiguration configuration, IOptions<AppSetting> appSetting)
        {
            _dashboardRepository = dashboardRepository;
            _configuration = configuration;
            _appSetting = appSetting.Value;
        }

        public async Task<List<GetSSWhiteTeamDetailsResponse>> GetSSWhiteTeamDetails()
        {
            try
            {
                var result = await _dashboardRepository.GetSSWhiteTeamDetails();
                foreach (var item in result)
                {
                    item.Image = $"{_configuration.GetValue<string>("AppSetting:AvatarRootPath")}{item.Image}";
                }

                return result;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<UserDetail> GetUserDetailsById(int id)
        {
            ////var response = new ServiceResponse<LogInResponse>();
            try
            {
                var result = await _dashboardRepository.GetUserDetailsById(id);
                result.Picture = $"{_configuration.GetValue<string>("AppSetting:AvatarRootPath")}{result.Picture}";
                return result;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<UserNotificationResponse>> UserNotification(ServiceRequest request)
        {
            try
            {
                var response = await _dashboardRepository.UserNotification(request);
                return response;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<int> ChangePassword(ServiceRequest<ChangePasswordRequest> request)
        {
            try
            {
                return await _dashboardRepository.ChangePassword(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task UpdateProfileById(ServiceRequest<UpdateProfileByIdRequest> request)
        {
            try
            {
                await _dashboardRepository.UpdateProfileById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task UpdateProfileWithImageById(ServiceRequest<UpdateProfileWithImageByIdRequest> request)
        {
            try
            {
                await _dashboardRepository.UpdateProfileWithImageById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetProfileByIdResponse> GetProfileById(ServiceRequest request)
        {
            var response = new ServiceResponse<GetProfileByIdResponse>();
            try
            {
                response.Data = await _dashboardRepository.GetProfileById(request);
                if (response.Data != null)
                {
                    response.Data.ProfileImage = response.Data.ProfileImage == null ? response.Data.ProfileImage : $"{_appSetting.ProfileImagePathToGetImage}{response.Data.ProfileImage}";
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response.Data;
        }

        public async Task<GetUserDetailsByUserIdResponse> GetUserDetailsByUserId(ServiceRequest request)
        {
            try
            {
                return await _dashboardRepository.GetUserDetailsByUserId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
