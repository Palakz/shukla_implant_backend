﻿namespace SSWhite.Service.Interface.Dashboard
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.Dashboard;
    using SSWhite.Domain.Response.Account;
    using SSWhite.Domain.Response.Dashboard;

    public interface IDashboardService
    {
        Task<List<GetSSWhiteTeamDetailsResponse>> GetSSWhiteTeamDetails();

        Task<UserDetail> GetUserDetailsById(int id);

        Task<List<UserNotificationResponse>> UserNotification(ServiceRequest request);

        Task<int> ChangePassword(ServiceRequest<ChangePasswordRequest> request);

        Task UpdateProfileById(ServiceRequest<UpdateProfileByIdRequest> request);

        Task UpdateProfileWithImageById(ServiceRequest<UpdateProfileWithImageByIdRequest> request);

        Task<GetUserDetailsByUserIdResponse> GetUserDetailsByUserId(ServiceRequest request);

        Task<GetProfileByIdResponse> GetProfileById(ServiceRequest request);


    }
}
