﻿namespace SSWhite.Service.Interface.Organization.Role
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Organization.Role;
    using SSWhite.Domain.Request.Organization.Role;
    using SSWhite.Domain.Response.Organization.Role;

    public interface IRoleService
    {
        Task<ServiceSearchResponse<GetAllRolesResponse>> GetAllRoles(ServiceSearchRequest<GetAllRoles> request);

        Task<ServiceResponse<int>> AddEditRole(ServiceRequest<Role> request);

        Task ChangeRoleStatus(ServiceRequest<int> request);

        Task<ServiceResponse<Role>> GetRoleById(ServiceRequest<int> request);

        Task DeleteRole(ServiceRequest<int> request);
    }
}
