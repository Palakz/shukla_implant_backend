﻿namespace SSWhite.Service.Interface.PickList
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Domain.Request.Common;
    using SSWhite.Domain.Request.PickList;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Domain.Response.Common;
    using SSWhite.Domain.Response.PickList;

    public interface IPickListService
    {
        Task<List<GetAllPickListResponse>> GetAllPickList();

        Task<GetPickListByIdResponse> GetPickListById(GetPickListByIdRequest request);

        Task DeletePickList(ServiceRequest<DeletePickListRequest> request);

        Task AddOrEditPickList(ServiceRequest<AddOrEditPickListRequest> request);

        Task<List<GetPickListValueByIdResponse>> GetPickListValueById(GetPickListValueByIdRequest request);

        Task DeletePickListValue(ServiceRequest<DeletePickListValueRequest> request);

        Task AddOrEditPickListValue(ServiceRequest<AddOrEditPickListValueRequest> request);

        Task AddPickListValueImage(ServiceRequest<AddPickListValueImageRequest> request);

        Task ChangePickListStatus(ServiceRequest<ChangePickListStatusRequest> request);
    }
}
