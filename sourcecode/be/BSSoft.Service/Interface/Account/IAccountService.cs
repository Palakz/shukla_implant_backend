﻿namespace SSWhite.Service.Interface.Account
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.Organization.Account;
    using SSWhite.Domain.Response.Account;

    public interface IAccountService
    {
        Task<ServiceResponse<LogInResponse>> ValidateUserForLogIn(ServiceRequest<LogInRequest> request);

        Task ForgotPassword(ServiceRequest<ForgotPasswordRequest> request);
    }
}
