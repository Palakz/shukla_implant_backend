﻿namespace SSWhite.Service.Interface.Admin
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Response.Admin;

    public interface IAdminService
    {
        Task<ServiceSearchResponse<GetAllEmployeeLimitsResponse>> GetAllEmployeeLimits(GetAllEmployeeLimitsRequest request);

        Task<CheckIfEmployeeIdExistsResponse> CheckIfEmployeeIdExists(CheckIfEmployeeIdExistsRequest checkIfEmployeeIdExistsRequest);

        Task<List<GetAllUserResponse>> GetAllUser();

        Task UpdateDocumentApprovalList(List<UpdateDocumentApprovalListRequest> request);

        // Shukla medical
        Task AddNewUser(ServiceRequest<AddUserRequest> addNewUserRequest);

        Task AddNewUserWithImage(ServiceRequest<AddNewUserWithImageRequest> request);

        Task UpdateToken(UpdateTokenByIdRequest updateTokenByIdRequest);

        Task<ServiceSearchResponse<GetAllUserDetailsByEmailResponse>> GetUserDetailsByEmail(GetAllUserDetailsByEmailRequest request);

        Task ConfirmEmail(ConfirmEmailRequest ConfirmEmailRequest);

        Task<List<GetAllUserDetailsResponse>> GetAllUserDetails();

        Task UpdateIsAdminApproveByUserId(ServiceRequest<UpdateIsAdminApproveByUserIdRequest> request);

        Task UpdateIsHoldStatusByUserId(UpdateIsHoldStatusByUserIdRequest request);

        Task UpdateOccupationRightsByOccupationId(ServiceRequest<UpdateOccupationRightsByOccupationIdRequest> request);

        Task UpdateUserRightsByUserId(ServiceRequest<UpdateUserRightsByUserIdRequest> request);

        Task<List<GetUsersDetailsForDropDownResponse>> GetUsersDetailsForDropDown();

        Task<List<GetModuleRightsByOccupationIdResponse>> GetModuleRightsByOccupationId(GetModuleRightsByOccupationIdRequest request);

        Task<List<GetModuleRightsByUserIdResponse>> GetModuleRightsByUserId(GetModuleRightsByUserIdRequest request);
    }
}
