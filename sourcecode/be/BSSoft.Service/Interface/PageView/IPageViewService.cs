﻿namespace SSWhite.Service.Interface.PageView
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Domain.Request.PageView;
    using SSWhite.Domain.Response.PageView;

    public interface IPageViewService
    {
        Task AddOrEditPageViewFields(ServiceRequest<AddOrEditPageViewFieldsRequest> request);

        Task AddOrEditPageView(ServiceRequest<AddOrEditPageViewRequest> request);

        Task AddOrEditPageViewContainer(ServiceRequest<AddOrEditPageViewContainerRequest> request);

        Task AddOrEditPageViewProperty(ServiceRequest<AddOrEditPageViewPropertyRequest> request);

        Task<List<GetAllPageViewFieldsResponse>> GetAllPageViewFields();

        Task DeletePageViewFields(ServiceRequest<DeletePageViewFieldsRequest> request);

        Task DeletePageView(ServiceRequest<DeletePageViewRequest> request);

        Task DeletePageViewContainer(ServiceRequest<DeletePageViewContainerRequest> request);

        Task<List<GetAllPageViewResponse>> GetAllPageViewByImplantType(ServiceRequest<GetAllPageViewByImplantTypeRequest> request);

        Task<List<GetAllPageViewPropertyResponse>> GetAllPageViewProperty();

        Task DeletePageViewProperty(ServiceRequest<DeletePageViewPropertyRequest> request);

        Task<List<GetAllContainerByPageViewIDResponse>> GetAllContainerByPageViewID(GetAllContainerByPageViewIDRequest request);

        Task UpdateIsVisiblePageViewProperty(ServiceRequest<UpdateIsVisiblePageViewPropertyRequest> request);

        Task UpdateIsReadOnlyPageViewProperty(ServiceRequest<UpdateIsReadOnlyPageViewPropertyRequest> request);

        Task UpdateIsActivePageViewProperty(ServiceRequest<UpdateIsActivePageViewPropertyRequest> request);

        Task<GetPageViewByPageViewIDResponse> GetPageViewByPageViewID(GetPageViewByPageViewIDRequest request);

        Task<GetPageViewByPageViewIDResponse> GetPageViewByImplantType(ServiceRequest<GetPageViewByImplantTypeRequest> request);

        Task<dynamic> GetAllRecentlyViewed(ServiceRequest<GetAllRecentlyViewedRequest> request);

        Task DeleteRecentlyViewedById(DeleteRecentlyViewedByIdRequest request);

        Task DeleteAllRecentlyViewedByImplantId(ServiceRequest<DeleteAllRecentlyViewedByImplantIdRequest> request);
    }
}
