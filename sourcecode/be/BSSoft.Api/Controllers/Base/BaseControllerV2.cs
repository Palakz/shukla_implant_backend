﻿namespace SSWhite.Api.Controllers.Base
{
    using System;
    using Microsoft.AspNetCore.Mvc;
    using SSWhite.Component.Interface.Session;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Request;

    public class BaseControllerV2 : ControllerBase
    {
        protected static AppSetting _appSetting;
        private static ISession _session;

        public BaseControllerV2()
        {
        }

        public BaseControllerV2(ISession session)
        {
            _session = session;
        }

        /// <summary>
        /// This Method Cast ViewModel Request to RequestViewModel.<T> request.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <returns>Object of RequestViewModel</returns>
        protected ServiceRequest CreateServiceRequest()
        {
            return new ServiceRequest()
            {
                Session = GetSessionDetails(),
            };
        }

        protected ServiceRequest<T> CreateServiceRequest<T>(T data)
        {
            var request = new ServiceRequest<T>()
            {
                Session = GetSessionDetails(),
                Data = Activator.CreateInstance<T>()
            };
            request.Data = data;
            return request;
        }

        /// <summary>
        /// This method is used to cast Session Details from Session Array to Session Entity.
        /// </summary>
        protected SessionDetail GetSessionDetails()
        {
            return SessionGetterSetter.Get();
        }

        protected void SetInMemorySessionDetails(Core.Response.ServiceResponse<Domain.Response.Account.LogInResponse> response, string authToken)
        {
            _session.SetInMemorySessionDetails(
                                new SessionDetail()
                                {
                                    SubscriberId = response.Data.SubscriberId,
                                    UserId = response.Data.UserId,
                                    UserName = response.Data.UserName,
                                }, authToken);

            HttpContext.Request.Headers.Add("Authorization", $"bearer {authToken}");
        }
    }
}
