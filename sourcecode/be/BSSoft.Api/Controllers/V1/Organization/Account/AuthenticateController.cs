﻿namespace SSWhite.Api.Controllers.V1.Organization.Account
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Api.Services;
    using SSWhite.Component.Interface.Session;
    using SSWhite.Core.Common.Application;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.Organization.Account;
    using SSWhite.Notification.Interface;
    using SSWhite.Service.Interface.Account;

    [Route("/v1.0/authenticate/")]
    public class AuthenticateController : BaseControllerV2
    {
        private static IWebHostEnvironment _webHostEnvironment;
        private readonly IAccountService _accountService;
        private readonly ISendMailService _sendMailService;
        private readonly IConfiguration _config;
        private readonly IJwtService _jwtService;
        private readonly ILogger _logger;
        private readonly ISession _session;

        public AuthenticateController(ILogger<AuthenticateController> logger, IAccountService accountService, ISession session, IOptions<AppSetting> appsetting, IOptions<IPAddress> ipAddress, IWebHostEnvironment webHostEnvironment, ISendMailService sendMailService, IConfiguration config, IOptions<AppSetting> appSetting, IJwtService jwtService)
            : base(session)
        {
            _logger = logger;
            _appSetting = appsetting.Value;
            _accountService = accountService;
            _webHostEnvironment = webHostEnvironment;
            _sendMailService = sendMailService;
            _config = config;
            _jwtService = jwtService;
            _session = session;
        }

        [AllowAnonymous]
        [HttpPost("token")]
        public async Task<JsonResult> Authenticate([FromBody] LogInRequest request)
        {
            var serviceRequest = CreateServiceRequest(request);
            var response = await _accountService.ValidateUserForLogIn(serviceRequest);

            if (response.Data == null)
            {
                Response.StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status401Unauthorized;
                return new JsonResult(new { error = "Invalid Username or Password" });
            }
            else if (response.Data.IsActive == false)
            {
                Response.StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status409Conflict;
                return new JsonResult(new { error = "Account yet not verified. Please check your register emailId and Verified Account" });
            }
            else
            {
                int? expiringInMinutes = 0;
                var authToken = _jwtService.GenerateSecurityToken(response.Data);
                SetInMemorySessionDetails(response, authToken);
                return new JsonResult(new { authToken, expiringInMinutes, response.Data });
            }
        }

        [Microsoft.AspNetCore.Authorization.Authorize]
        [HttpPost("logOut")]
        public IActionResult LogOut()
        {
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost("ForgotPassword")]
        public async Task ForgotPassword(ForgotPasswordRequest request)
        {
            await _accountService.ForgotPassword(CreateServiceRequest(request));
        }
    }
}
