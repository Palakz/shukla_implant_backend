﻿namespace SSWhite.Api.Controllers.V1.Dashboard
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Interface.Session;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Dashboard;
    using SSWhite.Domain.Response.Dashboard;
    using SSWhite.Service.Interface.Dashboard;

    [Microsoft.AspNetCore.Authorization.Authorize]
    [Route("v1.0/[controller]")]
    [ApiController]
    public class DashboardController : BaseControllerV2
    {
        private readonly IDashboardService _dashboardService;
        private readonly AppSetting _appSetting;

        public DashboardController(IDashboardService dashboardService, ISession session, IOptions<AppSetting> appSetting, IOptions<IPAddress> ipAddress)
        {
            _dashboardService = dashboardService;
            _appSetting = appSetting.Value;
        }

        ////[HttpGet("GetSSWhiteTeamDetails")]
        ////public async Task<List<GetSSWhiteTeamDetailsResponse>> GetSSWhiteTeamDetails()
        ////{
        ////    return await _dashboardService.GetSSWhiteTeamDetails();
        ////}

        ////[HttpGet("GetUserDetailsById")]
        ////public async Task<UserDetail> GetUserDetailsById()
        ////{
        ////    var session = await GetSessionDetails();
        ////    //var serviceRequest = CreateServiceRequest(session.UserId);
        ////    var response = await _dashboardService.GetUserDetailsById(session.UserId);
        ////    return response;
        ////}

        ////[HttpGet("UserNotification")]
        ////public async Task<List<UserNotificationResponse>> UserNotification()
        ////{
        ////    return await _dashboardService.UserNotification(CreateServiceRequest());
        ////}

        [HttpGet("GetProfileDetails")]
        public async Task<GetProfileByIdResponse> GetProfileById()
        {
            return await _dashboardService.GetProfileById(CreateServiceRequest());
        }

        [HttpPost("ChangePassword")]
        public async Task<ActionResult> ChangePassword(ChangePasswordRequest request)
        {
            var response = await _dashboardService.ChangePassword(CreateServiceRequest(request));

            if (response == -1)
            {
                Response.StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status406NotAcceptable;

                return new JsonResult(new { error = "Current Password Is Not Correct" });
            }
            else
            {
                ////await DeleteSession();
                Response.StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status200OK;
                return new JsonResult(new { message = "Success" });
            }
        }

        [HttpPost("UpdateProfileById")]
        public async Task UpdateProfileById(UpdateProfileByIdRequest request)
        {
            await _dashboardService.UpdateProfileById(CreateServiceRequest(request));
        }

        [HttpPost("UpdateProfileWithImageById")]
        public async Task UpdateProfileWithImageById([FromForm] UpdateProfileWithImageByIdRequest request)
        {
            if (request.Image?.Length > 0)
            {
                request.IsImageUpdate = true;
                Guid guid = Guid.NewGuid();
                string path = _appSetting.ProfileImagePath;

                if (!Directory.Exists($"{path}"))
                {
                    Directory.CreateDirectory($"{path}");
                }

                var attachmentPath = $"{guid}_{request.Image.FileName}";

                using (FileStream fileStream = System.IO.File.Create(path + attachmentPath))
                {
                    request.Image.CopyTo(fileStream);
                }

                ////attachmentPath = attachmentPath.Replace(@"\", @"/");
                request.ProfileImage = attachmentPath;

                var getUserDetailsByUserIdResponse = await _dashboardService.GetUserDetailsByUserId(CreateServiceRequest());

                if (getUserDetailsByUserIdResponse != null)
                {
                    string imagePath = $"{_appSetting.ProfileImagePathToGetImage}{getUserDetailsByUserIdResponse.ProfileImage.Trim()}";
                    if (System.IO.File.Exists(imagePath))
                    {
                        System.IO.File.Delete(imagePath);
                    }
                }
            }

            await _dashboardService.UpdateProfileWithImageById(CreateServiceRequest(request));
        }
    }
}
