﻿namespace SSWhite.Api.Controllers.V1.Implant
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Attributes;
    using SSWhite.Core.Common.Application;
    using SSWhite.Domain.Request.Implant;
    using SSWhite.Domain.Response.Implant;
    using SSWhite.Service.Interface.Implant;

    [Microsoft.AspNetCore.Authorization.Authorize]
    [Route("/v1.0/implant")]
    [ApiController]
    public class ImplantController : BaseControllerV2
    {
        private readonly IImplantService _implantService;
        private readonly AppSetting _appSetting;

        public ImplantController(IImplantService implantService, IOptions<AppSetting> appSetting)
        {
            _implantService = implantService;
            _appSetting = appSetting.Value;
        }

        [HttpGet("GetAllImplantType")]
        public async Task<List<GetAllImplantTypeResponse>> GetAllImplantType()
        {
            return await _implantService.GetAllImplantType();
        }

        [HttpGet("GetImplantTypeForDropdown")]
        public async Task<List<GetImplantTypeForDropdownResponse>> GetImplantTypeForDropdown()
        {
            return await _implantService.GetImplantTypeForDropdown();
        }

        [HttpGet("GetImplantTypeById")]
        public async Task<GetImplantTypeByIdResponse> GetImplantTypeById([FQuery] GetImplantTypeByIdRequest request)
        {
            return await _implantService.GetImplantTypeById(request);
        }

        [HttpPost("DeleteImplantType")]
        public async Task DeleteImplantType(DeleteImplantTypeRequest request)
        {
            await _implantService.DeleteImplantType(CreateServiceRequest(request));
        }

        [HttpPost("AddOrEditImplantType")]
        public async Task InsertImplantType(AddOrEditImplantTypeRequest request)
        {
            await _implantService.AddOrEditImplantType(CreateServiceRequest(request));
        }

        [HttpPost("ChangeImplantTypeStatus")]

        public async Task ChangeImplantTypeStatus(ChangeImplantTypeStatusRequest request)
        {
            await _implantService.ChangeImplantTypeStatus(CreateServiceRequest(request));
        }

        [HttpPost("ChangeImplantStatus")]

        public async Task ChangeImplantStatus(ChangeImplantStatusRequest request)
        {
            await _implantService.ChangeImplantStatus(CreateServiceRequest(request));
        }

        [HttpPost("ChangeImplantFieldStatus")]

        public async Task ChangeImplantFieldStatus(ChangeImplantFieldStatusRequest request)
        {
            await _implantService.ChangeImplantFieldStatus(CreateServiceRequest(request));
        }

        [HttpGet("GetAllDataTypes")]
        public async Task<List<GetAllDataTypesResponse>> GetAllDataTypes()
        {
            return await _implantService.GetAllDataTypes();
        }

        [HttpGet("GetAllImplantFields")]
        public async Task<List<GetAllImplantFieldsResponse>> GetAllImplantFields()
        {
            return await _implantService.GetAllImplantFields();
        }

        [HttpGet("GetImplantFieldsByImplantType")]
        public async Task<List<GetAllImplantFieldsResponse>> GetImplantFieldsByImplantType(int implantTypeId, bool includeAllImplantTypeFields = true)
        {
            return await _implantService.GetImplantFieldsByImplantType(implantTypeId, includeAllImplantTypeFields);
        }

        [HttpGet("GetAllImplantFieldsById")]
        public async Task<GetAllImplantFieldsByIdResponse> GetAllImplantFieldsById([FQuery] GetAllImplantFieldsByIdRequest request)
        {
            return await _implantService.GetAllImplantFieldsById(request);
        }

        [HttpPost("AddOrEditImplantFields")]
        public async Task AddOrEditImplantFields(AddOrEditImplantFieldsRequest request)
        {
            await _implantService.AddOrEditImplantFields(CreateServiceRequest(request));
        }

        [HttpGet("GetAllImplantByImplantTypeId")]
        public async Task<dynamic> GetAllImplantByImplantTypeId([FQuery] GetAllImplantByImplantTypeIdRequest request)
        {
            return await _implantService.GetAllImplantByImplantTypeId(CreateServiceRequest(request));
        }

        [HttpPost("AddOrEditImplantForm")]
        public async Task<IActionResult> AddOrEditImplantForm(Microsoft.AspNetCore.Http.IFormCollection formdata)
        {
            try
            {
                Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
                foreach (var item in formdata)
                {
                    keyValuePairs.Add(item.Key, item.Value);
                }

                Dictionary<string, string> keyValuePairsImages = new Dictionary<string, string>();
                var files = HttpContext.Request.Form.Files;
                ////var s = files.ToDictionary(x => x.Name, x => x);

                if (files?.Count > 0)
                {
                    foreach (var item in files)
                    {
                        if (item.Length > 0)
                        {
                            Guid guid = Guid.NewGuid();
                            string path = _appSetting.ImplantImagesToStoreImages;

                            if (!Directory.Exists($"{path}{item.Name}\\"))
                            {
                                Directory.CreateDirectory($"{path}{item.Name}\\");
                            }

                            var attachmentPath = $"{item.Name}\\{item.Name}_{guid}_{item.FileName}";

                            using (FileStream fileStream = System.IO.File.Create(path + attachmentPath))
                            {
                                item.CopyTo(fileStream);
                            }

                            if (keyValuePairs.ContainsKey(item.Name))
                            {
                                attachmentPath = attachmentPath.Replace(@"\", @"/");
                                keyValuePairs[item.Name] = $" {keyValuePairs[item.Name]},{attachmentPath}";
                            }
                            else
                            {
                                attachmentPath = attachmentPath.Replace(@"\", @"/");
                                keyValuePairs.Add(item.Name, attachmentPath);
                            }
                        }
                    }
                }

                var errorValues = await _implantService.AddOrEditImplantForm(CreateServiceRequest(keyValuePairs));

                if (errorValues != null && errorValues.Count > 0)
                {
                    Response.StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest;
                    return new JsonResult(new { errorValues = errorValues });
                }
                else
                {
                    Response.StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status200OK;
                    return new JsonResult(new { message = "Success" });
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost("AddOrRemoveFavorite")]
        public async Task AddOrRemoveFavorite(AddOrRemoveFavoriteRequest request)
        {
            await _implantService.AddOrRemoveFavorite(CreateServiceRequest(request));
        }

        [HttpPost("SearchImplant")]
        public async Task<dynamic> SearchImplant(SearchImplantRequest request)
        {
            return await _implantService.SearchImplant(CreateServiceRequest(request));
        }

        [HttpGet("GetAllFavoriteImplant")]
        public async Task<dynamic> GetAllFavoriteImplant([FQuery] GetAllFavoriteImplantRequest request)
        {
            return await _implantService.GetAllFavoriteImplant(CreateServiceRequest(request));
        }

        [HttpPost("DeleteHistory")]
        public async Task DeleteHistory(DeleteHistoryRequest request)
        {
            await _implantService.DeleteHistory(CreateServiceRequest(request));
        }

        [HttpGet("GetAllHistory")]
        public async Task<List<GetAllHistoryImplantResponse>> GetAllHistory([FQuery] GetAllHistoryRequest request)
        {
            return await _implantService.GetAllHistory(CreateServiceRequest(request));
        }

        [HttpGet("GetAllUnapprovedImplant")]
        public async Task<dynamic> GetAllUnapprovedImplant()
        {
            return await _implantService.GetAllUnapprovedImplant();
        }

        [HttpPost("ApproveImplant")]
        public async Task ApproveImplant(ApproveImplantRequest request)
        {
            await _implantService.ApproveImplant(CreateServiceRequest(request));
        }

        [HttpPost("RejectImplant")]
        public async Task RejectImplant(ApproveImplantRequest request)
        {
            await _implantService.RejectImplant(CreateServiceRequest(request));
        }

        [HttpGet("GetImplantById")]
        public async Task<dynamic> GetImplantById([FQuery] GetImplantByIdRequest request)
        {
            return await _implantService.GetImplantById(request);
        }

        [HttpPost("DeleteAllHistoryByUserId")]
        public async Task DeleteAllHistoryByUserId()
        {
            await _implantService.DeleteAllHistoryByUserId(CreateServiceRequest());
        }

        [HttpGet("GetAllImplant")]
        public async Task<dynamic> GetAllImplant()
        {
            return await _implantService.GetAllImplant();
        }

        [HttpPost("GetRelatedImplantOfShuklaParts")]
        public async Task<dynamic> GetRelatedImplantOfShuklaParts(GetRelatedImplantOfShuklaPartsRequest request)
        {
            return await _implantService.GetRelatedImplantOfShuklaParts(request);
        }

        [HttpPost("AddOrEditImplantComment")]
        public async Task AddOrEditImplantComment(AddOrEditImplantCommentRequest request)
        {
            await _implantService.AddOrEditImplantComment(CreateServiceRequest(request));
        }

        [HttpPost("GetAllImplantComments")]
        public async Task<List<GetAllImplantCommentsResponse>> GetAllImplantComments(GetAllImplantCommentsRequest request)
        {
            return await _implantService.GetAllImplantComments(CreateServiceRequest(request));
        }

        [HttpPost("LikeOrDislikeImplantComment")]
        public async Task LikeOrDislikeImplantComment(LikeOrDislikeImplantCommentRequest request)
        {
            await _implantService.LikeOrDislikeImplantComment(CreateServiceRequest(request));
        }

        [HttpGet("GetImplantNotification")]
        public async Task<List<GetImplantNotificationResponse>> GetImplantNotification()
        {
            return await _implantService.GetImplantNotification(CreateServiceRequest());
        }

        [HttpPost("MarkReadImplantNotification")]
        public async Task MarkReadImplantNotification(MarkReadImplantNotificationRequest request)
        {
            await _implantService.MarkReadImplantNotification(CreateServiceRequest(request));
        }

        [HttpPost("DeleteImageByEntity")]
        public async Task DeleteImageByEntity(DeleteImageByEntityRequest request)
        {

            if (string.Equals(request.EntityName, "implant", StringComparison.OrdinalIgnoreCase))
            {
                if (request.ImagePath != null)
                {
                    string path = $"{_appSetting.ImplantImagesToStoreImages}{request.ImagePath.Trim()}";
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                }
            }

            await _implantService.DeleteImageByEntity(request);
        }
    }
}