﻿namespace SSWhite.Api.Controllers.V1.PageView
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Attributes;
    using SSWhite.Component.Interface.Session;
    using SSWhite.Core.Common.Application;
    using SSWhite.Domain.Request.PageView;
    using SSWhite.Domain.Response.PageView;
    using SSWhite.Service.Interface.PageView;

    [Microsoft.AspNetCore.Authorization.Authorize]
    [Route("/v1.0/pageview")]
    [ApiController]
    public class PageViewController : BaseControllerV2
    {
        private readonly IPageViewService _pageViewFieldsService;

        public PageViewController(IPageViewService pageViewFieldsService, ISession session, IOptions<AppSetting> appsetting, IOptions<IPAddress> ipAddress)
        {
            _pageViewFieldsService = pageViewFieldsService;
        }

        [HttpPost("AddOrEditPageView")]
        public async Task AddOrEditPageView(AddOrEditPageViewRequest request)
        {
            await _pageViewFieldsService.AddOrEditPageView(CreateServiceRequest(request));
        }

        [HttpPost("AddOrEditPageViewContainer")]
        public async Task AddOrEditPageViewContainer(AddOrEditPageViewContainerRequest request)
        {
            await _pageViewFieldsService.AddOrEditPageViewContainer(CreateServiceRequest(request));
        }

        [HttpPost("AddOrEditPageViewProperty")]
        public async Task AddOrEditPageViewProperty(AddOrEditPageViewPropertyRequest request)
        {
            await _pageViewFieldsService.AddOrEditPageViewProperty(CreateServiceRequest(request));
        }

        [HttpPost("GetAllPageViewByImplantType")]
        public async Task<List<GetAllPageViewResponse>> GetAllPageViewByImplantType(GetAllPageViewByImplantTypeRequest request)
        {
            return await _pageViewFieldsService.GetAllPageViewByImplantType(CreateServiceRequest(request));
        }

        [HttpPost("GetAllPageView")]
        public async Task<List<GetAllPageViewResponse>> GetAllPageView(GetAllPageViewByImplantTypeRequest request)
        {
            return await _pageViewFieldsService.GetAllPageViewByImplantType(CreateServiceRequest(request));
        }

        [HttpPost("AddOrEditPageViewFields")]
        public async Task AddOrEditPageViewFields(AddOrEditPageViewFieldsRequest request)
        {
            await _pageViewFieldsService.AddOrEditPageViewFields(CreateServiceRequest(request));
        }

        [HttpPost("GetAllPageViewFields")]
        public async Task<List<GetAllPageViewFieldsResponse>> GetAllPageViewFields()
        {
            return await _pageViewFieldsService.GetAllPageViewFields();
        }

        [HttpPost("DeletePageViewFields")]
        public async Task DeletePageViewFields(DeletePageViewFieldsRequest request)
        {
            await _pageViewFieldsService.DeletePageViewFields(CreateServiceRequest(request));
        }

        [HttpPost("DeletePageView")]
        public async Task DeletePageView(DeletePageViewRequest request)
        {
            await _pageViewFieldsService.DeletePageView(CreateServiceRequest(request));
        }

        [HttpPost("DeletePageViewContainer")]
        public async Task DeletePageViewContainer(DeletePageViewContainerRequest request)
        {
            await _pageViewFieldsService.DeletePageViewContainer(CreateServiceRequest(request));
        }

        [HttpGet("GetAllPageViewProperty")]
        public async Task<List<GetAllPageViewPropertyResponse>> GetAllPageViewProperty()
        {
            return await _pageViewFieldsService.GetAllPageViewProperty();
        }

        [HttpPost("DeletePageViewProperty")]
        public async Task DeletePageViewProperty(DeletePageViewPropertyRequest request)
        {
            await _pageViewFieldsService.DeletePageViewProperty(CreateServiceRequest(request));
        }

        [HttpPost("GetAllContainerByPageViewID")]
        public async Task<List<GetAllContainerByPageViewIDResponse>> GetAllContainerByPageViewID(GetAllContainerByPageViewIDRequest request)
        {
            return await _pageViewFieldsService.GetAllContainerByPageViewID(request);
        }

        [HttpPost("UpdateIsVisiblePageViewProperty")]
        public async Task UpdateIsVisiblePageViewProperty(UpdateIsVisiblePageViewPropertyRequest request)
        {
            await _pageViewFieldsService.UpdateIsVisiblePageViewProperty(CreateServiceRequest(request));
        }

        [HttpPost("UpdateIsReadOnlyPageViewProperty")]
        public async Task UpdateIsReadOnlyPageViewProperty(UpdateIsReadOnlyPageViewPropertyRequest request)
        {
            await _pageViewFieldsService.UpdateIsReadOnlyPageViewProperty(CreateServiceRequest(request));
        }

        [HttpPost("UpdateIsActivePageViewProperty")]
        public async Task UpdateIsActivePageViewProperty(UpdateIsActivePageViewPropertyRequest request)
        {
            await _pageViewFieldsService.UpdateIsActivePageViewProperty(CreateServiceRequest(request));
        }

        [HttpPost("GetPageViewByPageViewID")]
        public async Task<GetPageViewByPageViewIDResponse> GetPageViewByPageViewID(GetPageViewByPageViewIDRequest request)
        {
            return await _pageViewFieldsService.GetPageViewByPageViewID(request);
        }

        [HttpPost("GetPageViewByImplantType")]
        public async Task<GetPageViewByPageViewIDResponse> GetPageViewByImplantType(GetPageViewByImplantTypeRequest request)
        {
            return await _pageViewFieldsService.GetPageViewByImplantType(CreateServiceRequest(request));
        }

        [HttpGet("GetAllRecentlyViewed")]
        public async Task<dynamic> GetAllRecentlyViewed([FQuery] GetAllRecentlyViewedRequest request)
        {
            return await _pageViewFieldsService.GetAllRecentlyViewed(CreateServiceRequest(request));
        }

        [HttpPost("DeleteRecentlyViewedById")]
        public async Task DeleteRecentlyViewedById(DeleteRecentlyViewedByIdRequest request)
        {
            await _pageViewFieldsService.DeleteRecentlyViewedById(request);
        }

        [HttpPost("DeleteAllRecentlyViewedByImplantId")]
        public async Task DeleteAllRecentlyViewedByImplantId(DeleteAllRecentlyViewedByImplantIdRequest request)
        {
            await _pageViewFieldsService.DeleteAllRecentlyViewedByImplantId(CreateServiceRequest(request));
        }
    }
}
