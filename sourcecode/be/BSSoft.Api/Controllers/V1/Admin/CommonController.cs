﻿namespace SSWhite.Api.Controllers.V1.Admin
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Attributes;
    using SSWhite.Component.Interface.Session;
    using SSWhite.Core.Common.Application;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.Common;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Domain.Response.Common;
    using SSWhite.Service.Interface.Common;

    [Microsoft.AspNetCore.Authorization.Authorize]
    [Route("/v1.0/admin/common")]
    [ApiController]
    public class CommonController : BaseControllerV2
    {
        private readonly ICommonService _commonService;

        public CommonController(ICommonService commonService, ISession session, IOptions<AppSetting> appsetting, IOptions<IPAddress> ipAddress)
        {
            _commonService = commonService;
        }

        [AllowAnonymous]
        [HttpGet("GetOrganization")]
        public async Task<List<GetOrganizationResponse>> GetOrganization()
        {
            return await _commonService.GetOrganization();
        }

        [AllowAnonymous]
        [HttpPost("UpdateOrganization")]
        public async Task UpdateOrganization(UpdateOrganizationRequest request)
        {
            await _commonService.UpdateOrganization(CreateServiceRequest(request));
        }

        [AllowAnonymous]
        [HttpPost("AddOrganization")]
        public async Task AddOrganization(AddOrganizationRequest request)
        {
            await _commonService.AddOrganization(CreateServiceRequest(request));
        }

        [AllowAnonymous]
        [HttpPost("ChangeOrganizationStatus")]
        public async Task ChangeOrganizationStatus(ChangeOrganizationStatusRequest request)
        {
            await _commonService.ChangeOrganizationStatus(CreateServiceRequest(request));
        }

        [HttpGet("GetOccupations")]
        [AllowAnonymous]
        public async Task<List<GetOccupationsResponse>> GetOccupations()
        {
            return await _commonService.GetOccupations();
        }

        [HttpPost("UpdateOccupations")]
        public async Task UpdateOccupations(UpdateOccupationsRequest request)
        {
            await _commonService.UpdateOccupations(CreateServiceRequest(request));
        }

        [HttpPost("AddOccupations")]
        public async Task AddOccupations(AddOccupationsRequest request)
        {
            await _commonService.AddOccupations(CreateServiceRequest(request));
        }

        [AllowAnonymous]
        [HttpPost("ChangeOccupationStatus")]
        public async Task ChangeOccupationStatus(ChangeOccupationStatusRequest request)
        {
            await _commonService.ChangeOccupationStatus(CreateServiceRequest(request));
        }

        [HttpDelete("DeleteOccupations")]
        public async Task DeleteOccupations(DeleteOccupationsRequest request)
        {
            await _commonService.DeleteOccupations(CreateServiceRequest(request));
        }

        [HttpDelete("DeleteOrganization")]
        public async Task DeleteOrganization(DeleteOrganisationsRequest request)
        {
            await _commonService.DeleteOrganization(CreateServiceRequest(request));
        }

        // [HttpPost("AddOrRemoveFavorite")]
        // public async Task AddOrRemoveFavorite(AddOrRemoveFavoriteRequest request)
        // {
        //     await _commonService.AddOrRemoveFavorite(CreateServiceRequest(request));
        // }

        // [HttpGet("GetAllFavoriteImplant")]
        // public async Task GetAllFavoriteImplant(GetAllFavoriteImplant request)
        // {
        //     await _commonService.GetAllFavoriteImplant();
        // }

        ////[HttpGet("SelectPickLists")]
        ////public async Task<List<SelectPickListsResponse>> SelectPickLists()
        ////{
        ////    var response = await _commonService.SelectPickLists();
        ////    return response;
        ////}

        ////[HttpPost("InsertPickListValue")]
        ////public async Task InsertPickListValue(InsertPickListValueRequest request)
        ////{
        ////    await _commonService.InsertPickListValue(CreateServiceRequest(request));
        ////}

        ////[HttpPost("InsertPickList")]
        ////public async Task InsertPickList(InsertPickListRequest request)
        ////{
        ////    await _commonService.InsertPickList(CreateServiceRequest(request));
        ////}
    }
}
