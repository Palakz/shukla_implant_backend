﻿namespace SSWhite.Api.Controllers.V1.Admin
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.Organization.Account;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Notification.Interface;
    using SSWhite.Notification.Model;
    using SSWhite.Service.Interface.Admin;

    [Microsoft.AspNetCore.Authorization.Authorize]
    [Route("/v1.0/admin/common")]
    [ApiController]
    public class AdminController : BaseControllerV2
    {
        public static IWebHostEnvironment _webHostEnvironment;
        private readonly IAdminService _adminService;
        private readonly IConfiguration _configuration;
        private readonly AppSetting _appSetting;
        private readonly ISendMailService _sendMailService;

        public AdminController(IAdminService adminService, IConfiguration configuration, IWebHostEnvironment webHostEnvironment, Component.Interface.Session.ISession session, IOptions<IPAddress> ipAddress, ISendMailService sendMailService, IOptions<AppSetting> appSetting)
        {
            _adminService = adminService;
            _webHostEnvironment = webHostEnvironment;
            _configuration = configuration;
            _appSetting = appSetting.Value;
            _sendMailService = sendMailService;
        }

        [AllowAnonymous]
        [HttpPost("AddNewUser")]
        public async Task<StatusCodeResult> AddNewUser(AddUserRequest request)
        {
            var user = await _adminService.GetUserDetailsByEmail(new GetAllUserDetailsByEmailRequest()
            {
                Email = request.Email,
                UserName = request.UserName
            });

            if (user.Result.Count == 0)
            {
                await _adminService.AddNewUser(CreateServiceRequest(request));

                var token = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Encrypt();

                ////var conformationLink = Url.Action(nameof(ConfirmEmail), "Admin", new { token, request.Email }, Request.Scheme);
                var conformationLink = $"{_appSetting.EmailConfirmBaseUrl}{token}&Email={request.Email}";

                await SendMail(new SendEmail() { Templatename = "EmailConfirmation.html", ToEmail = request.Email, Data = conformationLink, FirstName = request.FirstName });

                return StatusCode(201);
            }
            else
            {
                if (user.Result[0].IsEmailVerified == false)
                {
                    // if emailId already exist but yet to verified
                    return StatusCode(409);
                }
                else
                {
                    // Email Id Alre
                    return StatusCode(304);
                }
            }
        }

        [AllowAnonymous]
        [HttpPost("AddNewUserWithImage")]
        public async Task<ActionResult> AddNewUserWithImage([FromForm] AddNewUserWithImageRequest request)
        {
            var user = await _adminService.GetUserDetailsByEmail(new GetAllUserDetailsByEmailRequest()
            {
                Email = request.Email,
                UserName = request.UserName
            });

            if (user.Result.Count == 0)
            {
                if (request.Image?.Length > 0)
                {
                    Guid guid = Guid.NewGuid();
                    string path = _appSetting.ProfileImagePath;

                    if (!Directory.Exists($"{path}"))
                    {
                        Directory.CreateDirectory($"{path}");
                    }

                    var attachmentPath = $"{guid}_{request.Image.FileName}";

                    using (FileStream fileStream = System.IO.File.Create(path + attachmentPath))
                    {
                        request.Image.CopyTo(fileStream);
                    }

                    ////attachmentPath = attachmentPath.Replace(@"\", @"/");
                    request.ProfileImage = attachmentPath;
                }

                await _adminService.AddNewUserWithImage(CreateServiceRequest(request));

                var token = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Encrypt();

                ////var conformationLink = Url.Action(nameof(ConfirmEmail), "Admin", new { token, request.Email }, Request.Scheme);
                var conformationLink = $"{_appSetting.EmailConfirmBaseUrl}{token}&Email={request.Email}";

                await SendMail(new SendEmail() { Templatename = "EmailConfirmation.html", ToEmail = request.Email, Data = conformationLink, FirstName = request.FirstName });

                return StatusCode(201);
            }
            else
            {
                if (user.Result[0].IsEmailVerified == false)
                {
                    // if emailId already exist but yet to verified
                    return StatusCode(409);
                }
                else
                {
                    // Email Id Alre
                    return StatusCode(304);
                }
            }
        }

        [AllowAnonymous]
        [HttpGet("ResendActivation")]
        public async Task<StatusCodeResult> ResendActivation(string email)
        {
            var user = await _adminService.GetUserDetailsByEmail(new GetAllUserDetailsByEmailRequest() { Email = email });
            if (user.Result.Count != 0)
            {
                var token = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Encrypt();

                var conformationLink = Url.Action(nameof(ConfirmEmail), "Admin", new { token, email }, Request.Scheme);

                await SendMail(new SendEmail() { Templatename = "EmailConfirmation.html", ToEmail = email, Data = conformationLink, FirstName = user.Result[0].FirstName });
                return StatusCode(200);
            }
            else
            {
                return StatusCode(304);
            }
        }

        [AllowAnonymous]
        [HttpGet("ConfirmEmail")]
        public async Task ConfirmEmail(string token, string email)
        {
            var user = (await _adminService.GetUserDetailsByEmail(new GetAllUserDetailsByEmailRequest() { Email = email })).Result[0];
            await _adminService.ConfirmEmail(new ConfirmEmailRequest() { Email = email });
        }

        [HttpGet("GetUserByEmail")]
        public async Task<GetAllUserDetailsByEmailResponse> GetUserByEmail(string email)
        {
            return (await _adminService.GetUserDetailsByEmail(new GetAllUserDetailsByEmailRequest() { Email = email })).Result[0];
        }

        [HttpGet("GetAllEmployeeLimit")]
        public async Task<ServiceSearchResponse<GetAllEmployeeLimitsResponse>> GetAllEmployeeLimits([FromQuery] GetAllEmployeeLimitsRequest request)
        {
            ////var serviceRequest = CreateServiceRequest(request);
            return await _adminService.GetAllEmployeeLimits(request);
        }

        [HttpGet("CheckIfEmployeeIdExists")]
        public async Task<CheckIfEmployeeIdExistsResponse> CheckIfEmployeeIdExists([FromQuery] CheckIfEmployeeIdExistsRequest checkIfEmployeeIdExistsRequest)
        {
            return await _adminService.CheckIfEmployeeIdExists(checkIfEmployeeIdExistsRequest);
        }

        [AllowAnonymous]
        [HttpGet("GetAllUser")]
        public async Task<List<GetAllUserResponse>> GetAllUser()
        {
            return await _adminService.GetAllUser();
        }

        [HttpPost("UpdateDocumentApprovalList")]
        public async Task UpdateDocumentApprovalList(List<UpdateDocumentApprovalListRequest> request)
        {
            await _adminService.UpdateDocumentApprovalList(request);
        }

        [HttpGet("GetAllUserDetails")]
        public async Task<List<GetAllUserDetailsResponse>> GetAllUserDetails()
        {
            return await _adminService.GetAllUserDetails();
        }

        [HttpPost("UpdateIsAdminApproveByUserId")]
        public async Task UpdateIsAdminApproveByUserId(UpdateIsAdminApproveByUserIdRequest request)
        {
            await _adminService.UpdateIsAdminApproveByUserId(CreateServiceRequest(request));
        }

        [AllowAnonymous]
        [HttpPost("SendForgetPasswordLink")]
        public async Task<StatusCodeResult> SendForgetPasswordLink(string emailId)
        {
            var user = await _adminService.GetUserDetailsByEmail(new GetAllUserDetailsByEmailRequest() { Email = emailId });
            if (user.Result.Count != 0)
            {
                var token = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Encrypt();

                ////var conformationLink = Url.Action("ForgotPassword", "Authenticate", new { token, emailId }, Request.Scheme);
                var conformationLink = $"{_appSetting.ForgotEmailBaseUrl}Email={emailId}";
                await SendMail(new SendEmail() { Templatename = "ChangePassword.html", ToEmail = emailId, Data = conformationLink, FirstName = user.Result[0].FirstName });
                return StatusCode(200);
            }
            else
            {
                return StatusCode(304);
            }
        }

        [HttpPost("UpdateIsHoldStatusByUserId")]
        public async Task UpdateIsHoldStatusByUserId(UpdateIsHoldStatusByUserIdRequest request)
        {
            await _adminService.UpdateIsHoldStatusByUserId(request);
        }

        [HttpPost("UpdateOccupationRightsByOccupationId")]
        public async Task UpdateOccupationRightsByOccupationId(UpdateOccupationRightsByOccupationIdRequest request)
        {
            await _adminService.UpdateOccupationRightsByOccupationId(CreateServiceRequest(request));
        }

        [HttpPost("UpdateUserRightsByUserId")]
        public async Task UpdateUserRightsByUserId(UpdateUserRightsByUserIdRequest request)
        {
            await _adminService.UpdateUserRightsByUserId(CreateServiceRequest(request));
        }

        [HttpGet("GetUsersDetailsForDropDown")]
        public async Task<List<GetUsersDetailsForDropDownResponse>> GetUsersDetailsForDropDown()
        {
            return await _adminService.GetUsersDetailsForDropDown();
        }

        [HttpGet("GetModuleRightsByOccupationId")]
        public async Task<List<GetModuleRightsByOccupationIdResponse>> GetModuleRightsByOccupationId([FromQuery] GetModuleRightsByOccupationIdRequest request)
        {
            return await _adminService.GetModuleRightsByOccupationId(request);
        }

        [HttpGet("GetModuleRightsByUserId")]
        public async Task<List<GetModuleRightsByUserIdResponse>> GetModuleRightsByUserId([FromQuery] GetModuleRightsByUserIdRequest request)
        {
            return await _adminService.GetModuleRightsByUserId(request);
        }

        private async Task SendMail(SendEmail sendEmail)
        {
            string emailBody = System.IO.File.ReadAllText(Path.Combine(_webHostEnvironment.WebRootPath, "EmailTemplates", sendEmail.Templatename));
            emailBody = emailBody.Replace("{ConfirmLink}", sendEmail.Data);
            emailBody = emailBody.Replace("{NAME}", sendEmail.FirstName);
            emailBody = emailBody.Replace("{ImagePath}", _appSetting.EmailSSwhiteShuklaLogo);
            SendMail sendMail = new SendMail() { ToEmails = sendEmail.ToEmail, Body = emailBody, IsHtml = true };
            await _sendMailService.SendMailAsync(sendMail);
        }
    }
}