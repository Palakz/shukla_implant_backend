﻿namespace SSWhite.Api.Controllers.V1.PickList
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Attributes;
    using SSWhite.Component.Interface.Session;
    using SSWhite.Core.Common.Application;
    using SSWhite.Domain.Request.PickList;
    using SSWhite.Domain.Response.PickList;
    using SSWhite.Service.Interface.PickList;

    [Microsoft.AspNetCore.Authorization.Authorize]
    [Route("/v1.0/admin/common")]
    [ApiController]
    public class PickListController : BaseControllerV2
    {
        private readonly IPickListService _pickListService;
        private readonly AppSetting _appSetting;

        public PickListController(IPickListService pickListService, ISession session, IOptions<AppSetting> appSetting, IOptions<IPAddress> ipAddress)
        {
            _pickListService = pickListService;
            _appSetting = appSetting.Value;
        }

        [HttpGet("GetAllPickList")]
        public async Task<List<GetAllPickListResponse>> GetAllPickList()
        {
            return await _pickListService.GetAllPickList();
        }

        [HttpGet("GetPickListById")]
        public async Task<GetPickListByIdResponse> GetPickListById([FQuery] GetPickListByIdRequest request)
        {
            return await _pickListService.GetPickListById(request);
        }

        [HttpPost("DeletePickList")]
        public async Task InsertPickListValue(DeletePickListRequest request)
        {
            await _pickListService.DeletePickList(CreateServiceRequest(request));
        }

        [HttpPost("AddOrEditPickList")]
        public async Task InsertPickList(AddOrEditPickListRequest request)
        {
            ////var validator = new AddOrEditPickListRequestValidator();
            ////var validatorResponse = validator.ValidateAsync(request);
            await _pickListService.AddOrEditPickList(CreateServiceRequest(request));
        }

        [HttpPost("ChangePickListStatus")]
        public async Task ChangePickListStatus(ChangePickListStatusRequest request)
        {
            await _pickListService.ChangePickListStatus(CreateServiceRequest(request));
        }

        [HttpGet("GetPickListValueById")]
        public async Task<List<GetPickListValueByIdResponse>> GetPickListValueById([FQuery] GetPickListValueByIdRequest request)
        {
            return await _pickListService.GetPickListValueById(request);
        }

        [HttpPost("DeletePickListValue")]
        public async Task DeletePickListValue(DeletePickListValueRequest request)
        {
            await _pickListService.DeletePickListValue(CreateServiceRequest(request));
        }

        [HttpPost("AddOrEditPickListValue")]
        public async Task AddOrEditPickListValue(AddOrEditPickListValueRequest request)
        {
            await _pickListService.AddOrEditPickListValue(CreateServiceRequest(request));
        }

        [HttpPost("AddPickListValueImage")]
        public async Task AddPickListValueImage([FromForm] AddPickListValueImageRequest request)
        {

            if (request.PickListValueImage?.Count > 0)
            {
                int i = 0;
                foreach (var item in request.PickListValueImage)
                {
                    if (item.Length > 0)
                    {
                        Guid guid = Guid.NewGuid();
                        string path = _appSetting.PickListValueImagesToStoreImages;

                        if (!Directory.Exists($"{path}{item.Name}\\"))
                        {
                            Directory.CreateDirectory($"{path}{item.Name}\\");
                        }

                        var attachmentPath = $"{item.Name}\\{item.Name}_{guid}_{item.FileName}";

                        using (FileStream fileStream = System.IO.File.Create(path + attachmentPath))
                        {
                            item.CopyTo(fileStream);
                        }

                        if (i == 0)
                        {
                            attachmentPath = attachmentPath.Replace(@"\", @"/");
                            request.ImagePath = attachmentPath;
                            i++;
                        }
                        else
                        {
                            attachmentPath = attachmentPath.Replace(@"\", @"/");
                            request.ImagePath = $" {request.ImagePath },{attachmentPath}";
                        }
                    }
                }
            }

            await _pickListService.AddPickListValueImage(CreateServiceRequest(request));
        }
    }
}