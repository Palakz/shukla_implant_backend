﻿namespace SSWhite.Api.Services
{
    using SSWhite.Domain.Response.Account;

    public interface IJwtService
    {
        string GenerateSecurityToken(LogInResponse user);
    }
}
