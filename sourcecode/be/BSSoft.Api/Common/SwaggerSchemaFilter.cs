﻿namespace SSWhite.Api.Common
{
    using System;
    using System.Linq;
    using System.Reflection;
    using Microsoft.OpenApi.Models;
    using SSWhite.Core.Attributes;
    using Swashbuckle.AspNetCore.SwaggerGen;

    public class SwaggerSchemaFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            if (schema?.Properties == null || context.Type == null)
            {
                return;
            }

            var excludedProperties = context.Type.GetProperties()
                                            .Where(t => t.GetCustomAttribute<Internal>() != null);

            foreach (var excludedProperty in excludedProperties)
            {
                schema.Properties = schema.Properties.Where(p => !p.Key.Equals(excludedProperty.Name, StringComparison.CurrentCultureIgnoreCase))
                                          .ToDictionary(x => x.Key, x => x.Value);
            }
        }
    }
}
