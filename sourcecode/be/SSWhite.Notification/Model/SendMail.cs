﻿namespace SSWhite.Notification.Model
{
    public class SendMail
    {
        public string Subject { get; set; }

        public bool IsHtml { get; set; }

        public string Body { get; set; }

        public string ToEmails { get; set; }

        public string BCCEmails { get; set; }

        public string CCEmails { get; set; }
    }
}
