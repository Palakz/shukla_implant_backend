﻿namespace SSWhite.Core.Request
{
    using System.Collections.Generic;

    public class RequestEntities
    {
        public List<RequestEntity> Ids { get; set; }
    }
}
