﻿namespace SSWhite.Core.Attributes
{
    using System;

    public class GroupAttribute : Attribute
    {
        public string[] Groups { get; set; }

        public GroupAttribute()
        {
        }

        public GroupAttribute(params string[] groups)
        {
            Groups = groups;
        }
    }
}
