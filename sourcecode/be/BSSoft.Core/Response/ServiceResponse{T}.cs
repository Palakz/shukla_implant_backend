﻿namespace SSWhite.Core.Response
{
    using System.Collections.Generic;

    /// <summary>
    /// This class is used to create GenericResponse.
    /// It will bind any request parameter of any model.
    /// </summary>
    public class ServiceResponse<T> : ServiceResponse
    {
        public T Data { get; set; }

        public List<T> DataList { get; set; }
    }
}
