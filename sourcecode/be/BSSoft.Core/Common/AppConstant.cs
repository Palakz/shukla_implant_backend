﻿namespace SSWhite.Core.Common
{
    public class AppConstant
    {
        public const string SESSION = "Session";
        public const string DATA = "Data";
        public const string DATA_LIST = "DataList";
        public const string SERVICE_REQUEST = "ServiceRequest";
        public const string SERVICE_SEARCH_REQUEST = "ServiceSearchRequest";
        public const string SSWhite_API = "SSWhiteApi";
    }
}
