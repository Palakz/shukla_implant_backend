﻿namespace SSWhite.Core.Common.Session
{
    using System;
    using Microsoft.AspNetCore.Http;

    public static class SessionGetterSetter
    {
        private static SessionDetail Session { get; set; }

        private static IHttpContextAccessor _httpContextAccessor;

        public static void Configure(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public static HttpContext Context => _httpContextAccessor.HttpContext;

        public static void Set(SessionDetail session)
        {
            Session = session;
        }

        public static SessionDetail Get()
        {
            return Session;
        }

        public static object GetServiceProvider(Type t)
        {
            return Context.RequestServices.GetService(t);
        }
    }
}
