﻿namespace SSWhite.Core.Common
{
    // using SSWhite.Core.Enums;
    public class Rights
    {
        public int ModuleGroupRightsId { get; set; }

        public string Name { get; set; }

        public string Acronym { get; set; }

        public bool SelectedRight { get; set; }
    }
}
