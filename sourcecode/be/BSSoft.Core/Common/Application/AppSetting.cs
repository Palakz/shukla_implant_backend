﻿namespace SSWhite.Core.Common.Application
{
    public class AppSetting
    {
        public int Pagination { get; set; }

        public string DefaultPath { get; set; }

        public string DefaultCompanyLogoPath { get; set; }

        public string DefaultExceptionFilePath { get; set; }

        public bool IsLogRequest { get; set; }

        public string AvatarRootPathToStoreImage { get; set; }

        public string AttachmentRootPathToStoreImage { get; set; }

        public string AttachmentRootPathToGetImage { get; set; }

        public string DocumentAttachmentRootPathImage { get; set; }

        public Pdf Pdf { get; set; }

        public NotificationCredentials NotificationCredentials { get; set; }

        public string ImplantImagesToStoreImages { get; set; }

        public string PickListValueImagesToStoreImages { get; set; }

        public string ProfileImagePath { get; set; }

        public string EmailConfirmBaseUrl { get; set; }

        public string ForgotEmailBaseUrl { get; set; }

        public string EmailSSwhiteShuklaLogo { get; set; }

        public string ShuklaImagesPath { get; set; }

        public string ProfileImagePathToGetImage { get; set; }

        public bool IsRedisCacheSession { get; set; }
    }
}
