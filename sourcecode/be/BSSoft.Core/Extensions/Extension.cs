﻿namespace SSWhite.Core.Extensions
{
    public static class Extension
    {
        public static bool IsNullOrWhiteSpace(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }
    }
}
