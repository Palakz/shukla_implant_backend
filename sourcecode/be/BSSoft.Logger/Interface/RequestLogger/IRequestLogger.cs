﻿namespace SSWhite.Logger.Interface.RequestLogger
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Logger.Request.Request;

    public interface IRequestLogger
    {
        public Task LogToDatabase(ServiceRequest<LogRequest> request);
    }
}
