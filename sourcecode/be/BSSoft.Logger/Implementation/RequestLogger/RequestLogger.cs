﻿namespace SSWhite.Logger.Implementation.RequestLogger
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Logger.Interface.Common;
    using SSWhite.Logger.Interface.RequestLogger;
    using SSWhite.Logger.Request.Request;

    public class RequestLogger : IRequestLogger
    {
        private readonly ILoggerRepository _databaseRepository;

        public RequestLogger(ILoggerRepository databaseRepository)
        {
            _databaseRepository = databaseRepository;
        }

        public async Task LogToDatabase(ServiceRequest<LogRequest> request)
        {
            try
            {
                await _databaseRepository.LogToDatabase(request);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
