﻿namespace SSWhite.Logger.Implementation.Common
{
    using System.Data;
    using System.Threading.Tasks;
    using Dapper;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Logger.Common;
    using SSWhite.Logger.Interface.Common;
    using SSWhite.Logger.Request.Exception;
    using SSWhite.Logger.Request.Request;

    public class LoggerRepository : ILoggerRepository
    {
        private readonly IBaseRepository _baseRepository;
        private readonly IPAddress _ipAddress;

        public LoggerRepository(IBaseRepository baseRepository, IOptions<IPAddress> ipAddress)
        {
            _baseRepository = baseRepository;
            _ipAddress = ipAddress.Value;
        }

        public async Task LogToDatabase(ServiceRequest<LogException> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session?.SubscriberId, DbType.Int32);
            parameters.Add("Source", request.Data.Source, DbType.String);
            parameters.Add("StackTrace", request.Data.StrackTrace, DbType.String);
            parameters.Add("InnerException", request.Data.InnerException, DbType.String);
            parameters.Add("Message", request.Data.Message, DbType.String);
            parameters.Add("Data", request.Data.Data, DbType.String);
            parameters.Add("FileName", request.Data.FileName, DbType.String);
            parameters.Add("IpAddress", _ipAddress.RequestIpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.LOGGING_LOG_DATA_EXCEPTION, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task LogToDatabase(LogException request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.FileName, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.LOGGING_LOG_EXCEPTION, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task LogToDatabase(ServiceRequest<LogRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session?.SubscriberId, DbType.Int32);
            parameters.Add("UserId", request.Session?.UserId, DbType.Int32);
            parameters.Add("RequestId", request.Data.RequestId, DbType.Guid);
            parameters.Add("Data", request.Data.Data, DbType.String);
            parameters.Add("Action", request.Data.Action, DbType.String);
            parameters.Add("FileName", request.Data.FileName, DbType.String);
            parameters.Add("Method", request.Data.Method, DbType.String);
            parameters.Add("IpAddress", _ipAddress.RequestIpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.LOGGING_LOG_REQUEST, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
