﻿namespace SSWhite.DI.Registrator
{
    using System;
    using DryIoc;
    using SSWhite.Component.Implementation.Session;
    using SSWhite.Component.Interface.Session;
    using SSWhite.Data.Implementation.Account;
    using SSWhite.Data.Implementation.Admin;
    using SSWhite.Data.Implementation.Common;
    using SSWhite.Data.Implementation.Implant;
    using SSWhite.Data.Implementation.Organization.Role;
    using SSWhite.Data.Implementation.Organization.User;
    using SSWhite.Data.Implementation.PageView;
    using SSWhite.Data.Implementation.PickList;
    using SSWhite.Data.Interface.Account;
    using SSWhite.Data.Interface.Admin;
    using SSWhite.Data.Interface.Common;
    using SSWhite.Data.Interface.Implant;
    using SSWhite.Data.Interface.Organization.Role;
    using SSWhite.Data.Interface.Organization.User;
    using SSWhite.Data.Interface.PageView;
    using SSWhite.Data.Interface.PickList;
    using SSWhite.Logger.Implementation.Common;
    using SSWhite.Logger.Implementation.ExceptionLogger;
    using SSWhite.Logger.Implementation.RequestLogger;
    using SSWhite.Logger.Interface.Common;
    using SSWhite.Logger.Interface.ExceptionLogger;
    using SSWhite.Logger.Interface.RequestLogger;
    using SSWhite.Notification.Implementation;
    using SSWhite.Notification.Interface;
    using SSWhite.Service.Implementation.Account;
    using SSWhite.Service.Implementation.Admin;
    using SSWhite.Service.Implementation.Common;
    using SSWhite.Service.Implementation.Implant;
    using SSWhite.Service.Implementation.Organization.Role;
    using SSWhite.Service.Implementation.Organization.User;
    using SSWhite.Service.Implementation.PageView;
    using SSWhite.Service.Implementation.PickList;
    using SSWhite.Service.Interface.Account;
    using SSWhite.Service.Interface.Admin;
    using SSWhite.Service.Interface.Common;
    using SSWhite.Service.Interface.Dashboard;
    using SSWhite.Service.Interface.Implant;
    using SSWhite.Service.Interface.Organization.Role;
    using SSWhite.Service.Interface.Organization.User;
    using SSWhite.Service.Interface.PageView;
    using SSWhite.Service.Interface.PickList;
    using BaseRepository = SSWhite.Data.Implementation.Base.BaseRepository;
    using IBaseRepository = SSWhite.Data.Interface.Base.IBaseRepository;

    public class DIRegistrator
    {
        public DIRegistrator(IRegistrator registrator, IServiceProvider serviceProvider)
        {
            // Services
            registrator.Register<IRequestLogger, RequestLogger>();
            registrator.Register<IExceptionLogger, ExceptionLogger>();
            registrator.Register<ISession, Session>();
            registrator.Register<IAccountService, AccountService>();
            registrator.Register<IUserService, UserService>();
            registrator.Register<IRoleService, RoleService>();
            registrator.Register<IDashboardService, DashboardService>();
            registrator.Register<IAdminService, AdminService>();
            registrator.Register<ICommonService, CommonService>();
            registrator.Register<IPickListService, PickListService>();
            registrator.Register<IImplantService, ImplantService>();
            registrator.Register<IPageViewService, PageViewService>();

            // Repositories
            registrator.Register<IBaseRepository, BaseRepository>();
            registrator.Register<Logger.Interface.Common.IBaseRepository, Logger.Implementation.Common.BaseRepository>();
            registrator.Register<ILoggerRepository, LoggerRepository>();
            registrator.Register<ICommonRepository, CommonRepository>();
            registrator.Register<IAccountRepository, AccountRepository>();
            registrator.Register<IUserRepository, UserRepository>();
            registrator.Register<IRoleRepository, RoleRepository>();
            registrator.Register<IDashboardRepository, DashboardRepository>();
            registrator.Register<IAdminRepository, AdminRepository>();
            registrator.Register<IPickListRepository, PickListRepository>();
            registrator.Register<IImplantRepository, ImplantRepository>();
            registrator.Register<IPageViewRepository, PageViewRepository>();


            // Notification
            registrator.Register<ISendMailService, SendMailService>();
        }
    }
}
