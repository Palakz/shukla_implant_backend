﻿namespace SSWhite.Domain.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public enum ApprovedStatus : short
    {
        Pending = 1,
        Approved = 2,
        Deny = 3
    }
}
