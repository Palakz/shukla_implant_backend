﻿namespace SSWhite.Domain.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public enum StatusType : short
    {
        Active = 1,
        InActive = 2,
        Deleted = 3
    }
}
