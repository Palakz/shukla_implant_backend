﻿namespace SSWhite.Domain.Response.PageView
{
    using System.Collections.Generic;

    public class PageViewContainer
    {
        public PageViewContainer()
        {
            PageViewProperty = new List<GetAllPageViewPropertyResponse>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public int PageViewId { get; set; }

        public bool IsSystemGenerated { get; set; }

        public int DisplayColumnNumber { get; set; }

        public List<GetAllPageViewPropertyResponse> PageViewProperty { get; set; }
    }
}
