﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Response.PageView
{
    public class GetAllPageViewResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int PageViewType { get; set; }

        public int ImplantType { get; set; }
  
        public string URL { get; set; }

        public bool IsDefault { get; set; }

        public bool IsActive { get; set; }
    }
}
