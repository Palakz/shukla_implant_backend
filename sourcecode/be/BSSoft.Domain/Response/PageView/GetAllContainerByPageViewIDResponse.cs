﻿namespace SSWhite.Domain.Response.PageView
{
    public class GetAllContainerByPageViewIDResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int PageViewId { get; set; }

        public string ContainerIcon { get; set; }

        public string ContainerIconPosition { get; set; }

        public int? DisplayColumnNumber { get; set; }
    }
}
