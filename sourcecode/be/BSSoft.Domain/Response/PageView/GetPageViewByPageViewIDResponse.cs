﻿namespace SSWhite.Domain.Response.PageView
{
    using SSWhite.Domain.Enums;
    using System.Collections.Generic;

    public class GetPageViewByPageViewIDResponse
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public string URL { get; set; }

        public List<PageViewContainer> Container { get; set; }

        public dynamic Implant { get; set; }

        public StatusType? Status { get; set; }

    }
}
