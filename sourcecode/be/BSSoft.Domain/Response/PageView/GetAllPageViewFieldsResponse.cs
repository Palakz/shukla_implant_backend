﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Response.PageView
{
    public class GetAllPageViewFieldsResponse
    {
        public int Id { get; set; }

        public int PageViewContainerId { get; set; }

        public int FieldId { get; set; }

        public string OnChangeJavaScriptFunc { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; }

        public int CreatedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

    }
}
