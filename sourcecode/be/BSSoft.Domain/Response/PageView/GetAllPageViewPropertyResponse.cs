﻿using SSWhite.Domain.Response.Implant;
using System.Collections.Generic;

namespace SSWhite.Domain.Response.PageView
{
    public class GetAllPageViewPropertyResponse
    {
        public int Id { get; set; }

        public int FieldId { get; set; }

        public string DisplayName { get; set; }

        public int ContainerId { get; set; }

        public bool IsActive { get; set; }

        public bool IsReadOnly { get; set; }

        public bool IsVisible { get; set; }

        public string CssClass { get; set; }

        public string Script { get; set; }

        public string CurrentValue { get; set; }

        public string FieldControlType { get; set; }

        public GetAllImplantFieldsResponse ImplantFields { get; set; }
    }
}
