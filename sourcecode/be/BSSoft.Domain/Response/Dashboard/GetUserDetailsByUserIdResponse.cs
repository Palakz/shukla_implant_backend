﻿namespace SSWhite.Domain.Response.Dashboard
{
    public class GetUserDetailsByUserIdResponse
    {
        public string ProfileImage { get; set; }
    }
}
