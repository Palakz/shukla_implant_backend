﻿namespace SSWhite.Domain.Response.Dashboard
{
    public class UserNotificationResponse
    {
        public int EpoId { get; set; }

        public bool IsAttachmentPending { get; set; }
    }
}
