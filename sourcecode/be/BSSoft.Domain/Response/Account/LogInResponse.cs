﻿namespace SSWhite.Domain.Response.Account
{
    using System.Collections.Generic;
    using SSWhite.Core.Common;
    using SSWhite.Domain.Enums;

    public class LogInResponse
    {
        public LogInResponse()
        {
            Entities = new List<UserEntity>();
            Rights = new List<Rights>();
        }

        public int SubscriberId { get; set; }

        public int UserId { get; set; }

        public string UserId2 { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public string MobileNumber { get; set; }

        // public UserType UserType { get; set; }
        public bool IsPasswordReset { get; set; }

        public bool IsActive { get; set; }

        public bool IsEmailVerified { get; set; }

        public bool IsAdmin { get; set; }

        public int OccupationId { get; set; }

        public int OrganisationId { get; set; }

        public string Organisation { get; set; }

        public string Occupation { get; set; }

        public string ProfileImage { get; set; }

        public ApprovedStatus ApprovedStatus { get; set; }

        public bool HoldStatus { get; set; }

        public List<UserEntity> Entities { get; set; }

        public List<Rights> Rights { get; set; }
    }
}
