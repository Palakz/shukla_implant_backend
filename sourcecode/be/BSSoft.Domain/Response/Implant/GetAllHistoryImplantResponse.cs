﻿namespace SSWhite.Domain.Response.Implant
{
    using System;

    public class GetAllHistoryImplantResponse
    {
        public int Id { get; set; }

        public string SearchName { get; set; }

        public string SearchPayload { get; set; }

        public int SearchBy { get; set; }

        public DateTime SearchDate { get; set; }
    }
}
