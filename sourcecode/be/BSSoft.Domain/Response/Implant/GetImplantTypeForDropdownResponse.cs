﻿namespace SSWhite.Domain.Response.Implant
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SSWhite.Domain.Enums;

    public class GetImplantTypeForDropdownResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public StatusType Status { get; set; }
    }
}
