﻿namespace SSWhite.Domain.Response.Implant
{
    public class GetImplantTypeByIdResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
