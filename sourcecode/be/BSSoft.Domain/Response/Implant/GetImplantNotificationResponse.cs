﻿namespace SSWhite.Domain.Response.Implant
{
    using System;

    public class GetImplantNotificationResponse
    {
        public int Id { get; set; }

        public int ImplantId { get; set; }

        public string ImplantName { get; set; }

        public bool IsApprovedOrRejected { get; set; }
    }
}
