﻿using SSWhite.Domain.Enums;
using System.Collections.Generic;

namespace SSWhite.Domain.Response.Implant
{
    public class GetAllImplantFieldsResponse
    {
        public GetAllImplantFieldsResponse()
        {
            PickListValue = new List<PickListValueDictionary>();
        }

        public int Id { get; set; }

        public int ImplantType { get; set; }

        public string ImplantTypeName { get; set; }

        public string FieldName { get; set; }

        public string DisplayName { get; set; }

        public int? DataTypeId { get; set; }

        public string DataTypeName { get; set; }

        public int? PickListId { get; set; }

        public string DefaultValue { get; set; }

        public int? MaxLength { get; set; }

        public int? MinLength { get; set; }

        public bool? IsRequired { get; set; }

        public string Regex { get; set; }

        public int? RerversePropertyId { get; set; }

        public int? DisplayOrder { get; set; }

        public bool? IsPartOfFilter { get; set; }

        public int? FilterDisplayOrder { get; set; }

        public bool? IsMultiSelectOnCreate { get; set; }

        public bool? IsMultiSelectOnFilter { get; set; }

        public bool? IsSystemDefined { get; set; }

        public bool? IsApplicableToAllImplant { get; set; }

        public List<PickListValueDictionary> PickListValue { get; set; }

        public StatusType? Status { get; set; }

    }

}
