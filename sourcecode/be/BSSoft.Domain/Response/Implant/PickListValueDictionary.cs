﻿using System.Collections.Generic;

namespace SSWhite.Domain.Response.Implant
{
    public class PickListValueDictionary
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }
    }
}
