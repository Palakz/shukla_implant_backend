﻿namespace SSWhite.Domain.Response.Implant
{
    using System;

    public class GetAllImplantCommentsResponse
    {
        public int Id { get; set; }

        public string Comment { get; set; }

        public DateTime CreatedOn { get; set; }

        public int CreatedById { get; set; }

        public string CreatedBy { get; set; }

        public bool? Liked { get; set; }

        public bool? IsCommentAddedFromWorkbench { get; set; }

        public int TotalLikedCount { get; set; }

        public int TotalDisLikedCount { get; set; }
    }
}
