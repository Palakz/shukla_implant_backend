﻿namespace SSWhite.Domain.Response.Implant
{
    using System;

    public class ApproveImplantResponse
    {
        public int ImplantId { get; set; }

        public string ImplantName { get; set; }

        public string EmailAddress { get; set; }

        public string IsApprovedOrRejected { get; set; }
    }
}
