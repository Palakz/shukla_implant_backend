﻿namespace SSWhite.Domain.Response.Implant
{
    public class GetAllDataTypesResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
