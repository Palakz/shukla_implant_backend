﻿namespace SSWhite.Domain.Response.Implant
{
    using System.Collections.Generic;
    using SSWhite.Domain.Enums;

    public class GetAllImplantTypeResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public StatusType? Status { get; set; }
    }
}
