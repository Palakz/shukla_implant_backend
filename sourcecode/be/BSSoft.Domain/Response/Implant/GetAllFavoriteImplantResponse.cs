﻿namespace SSWhite.Domain.Response.Implant
{
    using System;

    public class GetAllFavoriteImplantResponse
    {
        public int Id { get; set; }

        public int ImplantId { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
