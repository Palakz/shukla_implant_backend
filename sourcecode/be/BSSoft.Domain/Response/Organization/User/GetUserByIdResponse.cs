﻿namespace SSWhite.Domain.Response.Organization.User
{
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Common.Organization.User;

    public class GetUserByIdResponse : User
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }
    }
}
