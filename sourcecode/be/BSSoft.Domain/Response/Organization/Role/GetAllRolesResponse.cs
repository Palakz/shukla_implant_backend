﻿namespace SSWhite.Domain.Response.Organization.Role
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;

    public class GetAllRolesResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        // public StatusType Status { get; set; }
    }
}
