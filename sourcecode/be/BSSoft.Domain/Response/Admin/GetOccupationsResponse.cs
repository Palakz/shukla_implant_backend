﻿namespace SSWhite.Domain.Response.Admin
{
    using SSWhite.Domain.Enums;

    public class GetOccupationsResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public StatusType? Status { get; set; }
    }
}
