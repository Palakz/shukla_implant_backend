﻿namespace SSWhite.Domain.Response.Admin
{
    using SSWhite.Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetUsersDetailsForDropDownResponse
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public bool IsAdmin { get; set; }
    }
}
