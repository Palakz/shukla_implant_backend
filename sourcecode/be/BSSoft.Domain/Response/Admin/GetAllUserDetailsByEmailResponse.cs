﻿namespace SSWhite.Domain.Response.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetAllUserDetailsByEmailResponse
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string MobileNumber { get; set; }

        public string EmailAddress { get; set; }

        public bool IsEmailVerified { get; set; }

        public string EmailVerificationToken { get; set; }
    }
}
