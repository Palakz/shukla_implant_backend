﻿namespace SSWhite.Domain.Response.Admin
{
    using SSWhite.Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetAllUserDetailsResponse
    {
        public int UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public bool IsAdmin { get; set; }

        public string MobileNumber { get; set; }

        public int OccupationId { get; set; }

        public int OrganisationId { get; set; }

        public ApprovedStatus ApprovedStatus { get; set; }

        public bool HoldStatus { get; set; }
    }
}
