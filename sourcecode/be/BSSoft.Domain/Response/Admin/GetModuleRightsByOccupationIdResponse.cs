﻿namespace SSWhite.Domain.Response.Admin
{
    using SSWhite.Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetModuleRightsByOccupationIdResponse
    {
        public GetModuleRightsByOccupationIdResponse()
        {
            ModuleGroupRights = new List<ModuleGroupRights>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Acronym { get; set; }

        public List<ModuleGroupRights> ModuleGroupRights { get; set; }
    }
}
