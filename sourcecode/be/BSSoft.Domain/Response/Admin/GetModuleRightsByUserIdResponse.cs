﻿namespace SSWhite.Domain.Response.Admin
{
    using System.Collections.Generic;

    public class GetModuleRightsByUserIdResponse
    {
        public GetModuleRightsByUserIdResponse()
        {
            ModuleGroupRights = new List<ModuleGroupRights>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Acronym { get; set; }

        public List<ModuleGroupRights> ModuleGroupRights { get; set; }
    }
}
