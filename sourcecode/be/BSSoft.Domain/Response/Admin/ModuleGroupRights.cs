﻿namespace SSWhite.Domain.Response.Admin
{
    public class ModuleGroupRights
    {
        public int ModuleGroupRightsId { get; set; }

        public int ModuleGroupId { get; set; }

        public string Name { get; set; }

        public string Acronym { get; set; }

        public bool SelectedRight { get; set; }
    }
}
