﻿namespace SSWhite.Domain.Response.PickList
{
    public class GetPickListByIdResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool? IsValueNameSame { get; set; }

        public bool? IsValueAutoId { get; set; }

        public int? DisplayOrder { get; set; }

        public string OrderBy { get; set; }

        public string DataType { get; set; }
    }
}
