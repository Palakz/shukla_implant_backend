﻿namespace SSWhite.Domain.Response.PickList
{
    public class GetPickListValueByIdResponse
    {
        public int Id { get; set; }

        public int PickListId { get; set; }

        public bool? IsDefault { get; set; }

        public int? DisplayOrder { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public string Note { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }
    }
}
