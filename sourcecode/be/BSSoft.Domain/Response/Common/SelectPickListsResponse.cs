﻿namespace SSWhite.Domain.Response.Common
{
    public class SelectPickListsResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool? IsValueNameSame { get; set; }

        public bool? IsValueAutoId { get; set; }
    }
}
