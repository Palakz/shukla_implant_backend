﻿namespace SSWhite.Domain.Request.Implant
{
    public class MarkReadImplantNotificationRequest
    {
        public int? Id { get; set; }

        public bool IsReadAll { get; set; }
    }
}
