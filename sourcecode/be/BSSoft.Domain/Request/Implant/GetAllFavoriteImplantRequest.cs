﻿namespace SSWhite.Domain.Request.Implant
{
    public class GetAllFavoriteImplantRequest
    {
        public int ImplantTypeId { get; set; }
    }
}
