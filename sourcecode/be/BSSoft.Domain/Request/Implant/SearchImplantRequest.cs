﻿namespace SSWhite.Domain.Request.Implant
{
    public class SearchImplantRequest
    {
        public string Name { get; set; }

        public string JsonSearchPayload { get; set; }

        public string ImplantTypeId { get; set; }

        public int? Status { get; set; }
    }
}
