﻿namespace SSWhite.Domain.Request.Implant
{
    public class GetAllImplantByImplantTypeIdRequest
    {
        public string ImplantType { get; set; }
    }
}
