﻿namespace SSWhite.Domain.Request.Implant
{
    public class AddOrEditImplantFieldsRequest
    {
        public int? Id { get; set; }

        public int? ImplantType { get; set; }

        public string FieldName { get; set; }

        public string DisplayName { get; set; }

        public int? DataTypeId { get; set; }

        public int? PickListId { get; set; }

        public string DefaultValue { get; set; }

        public int? MaxLength { get; set; }

        public int? MinLength { get; set; }

        public bool? IsRequired { get; set; }

        public string Regex { get; set; }

        public int? RerversePropertyId { get; set; }

        public int? DisplayOrder { get; set; }

        public bool? IsPartOfFilter { get; set; }

        public int? FilterDisplayOrder { get; set; }

        public bool? IsMultiSelectOnCreate { get; set; }

        public bool? IsMultiSelectOnFilter { get; set; }

        public bool? IsSystemDefined { get; set; }

        public int? MaxImageAllowed { get; set; }

        public int? MaxTotalSize { get; set; }

        public int? MaxSingleSize { get; set; }

        public bool? IsEdit { get; set; }

        public bool? IsApplicableToAllImplant { get; set; }
    }
}
