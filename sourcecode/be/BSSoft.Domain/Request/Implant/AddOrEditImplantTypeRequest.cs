﻿namespace SSWhite.Domain.Request.Implant
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class AddOrEditImplantTypeRequest
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public bool IsEdit { get; set; }
    }
}
