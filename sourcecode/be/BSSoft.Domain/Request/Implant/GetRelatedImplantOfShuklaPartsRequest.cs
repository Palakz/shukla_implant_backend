﻿namespace SSWhite.Domain.Request.Implant
{
    public class GetRelatedImplantOfShuklaPartsRequest
    {
        public int ImplantID { get; set; }

        public int ImplantTypeId { get; set; }

        public string SearchToolValue { get; set; }
    }
}
