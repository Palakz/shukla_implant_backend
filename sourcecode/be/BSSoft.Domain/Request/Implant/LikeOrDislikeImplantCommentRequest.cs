﻿namespace SSWhite.Domain.Request.Implant
{
    public class LikeOrDislikeImplantCommentRequest
    {
        public int? CommentId { get; set; }

        public bool? IsLiked { get; set; }
    }
}
