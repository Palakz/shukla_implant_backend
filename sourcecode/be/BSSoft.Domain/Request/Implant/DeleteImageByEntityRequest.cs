﻿namespace SSWhite.Domain.Request.Implant
{
    public class DeleteImageByEntityRequest
    {
        public int? EntityId { get; set; }

        public string EntityName { get; set; }

        public string ImagePath { get; set; }

        public string EntityColumnName { get; set; }
    }
}
