﻿namespace SSWhite.Domain.Request.Implant
{
    public class AddOrEditImplantCommentRequest
    {
        public int? Id { get; set; }

        public int? ImplantId { get; set; }

        public string Comment { get; set; }

        public bool? IsCommentAddedFromWorkbench { get; set; }
    }
}
