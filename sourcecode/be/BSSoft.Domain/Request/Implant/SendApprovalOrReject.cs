﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Request.Implant 
{
    public class SendApprovalOrReject
    {
        public string FirstName { get; set; }
        public string Data { get; set; }
        public string Templatename { get; set; }
        public string ToEmail { get; set; }
    }
}
