﻿namespace SSWhite.Domain.Request.Implant
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class DeleteImplantTypeRequest
    {
        public int? Id { get; set; }
    }
}
