﻿namespace SSWhite.Domain.Request.Implant
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class AddOrRemoveFavoriteRequest
    {
        public bool AddAsFavourite { get; set; }

        public int ImplantId { get; set; }
    }
}
