﻿namespace SSWhite.Domain.Request.Implant
{
    public class GetImplantByIdRequest
    {
        public int? Id { get; set; }
    }
}
