﻿namespace SSWhite.Domain.Request.Implant
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class GetAllImplantFieldsByIdRequest
    {
        public int? Id { get; set; }
    }
}
