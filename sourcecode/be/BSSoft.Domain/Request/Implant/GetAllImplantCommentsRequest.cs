﻿namespace SSWhite.Domain.Request.Implant
{
    public class GetAllImplantCommentsRequest
    {
        public int? ImplantId { get; set; }
    }
}
