﻿using System.Collections.Generic;

namespace SSWhite.Domain.Request.Admin
{
    public class GetModuleRightsByOccupationIdRequest
    {
        public int OccupationId { get; set; }
    }
}
