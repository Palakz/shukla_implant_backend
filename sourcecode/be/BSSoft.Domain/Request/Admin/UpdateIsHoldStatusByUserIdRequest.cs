﻿using SSWhite.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Request.Admin
{
    public class UpdateIsHoldStatusByUserIdRequest
    {
        public int UserId { get; set; }

        public bool HoldStatus { get; set; }
    }
}
