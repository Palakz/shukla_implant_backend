﻿using System.Collections.Generic;

namespace SSWhite.Domain.Request.Admin
{
    public class UpdateUserRightsByUserIdRequest
    {
        public List<int> RightIdList { get; set; }

        public int UserIdForRights { get; set; }
    }
}
