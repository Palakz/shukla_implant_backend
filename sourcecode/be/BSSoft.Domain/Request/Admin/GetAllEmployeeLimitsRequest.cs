﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SSWhite.Core.Attributes;

    public class GetAllEmployeeLimitsRequest
    {
        public int Start { get; set; }

        public int Length { get; set; }

        public string SortExpression { get; set; }

        [Trim]
        public string SearchKeyword { get; set; }
    }
}
