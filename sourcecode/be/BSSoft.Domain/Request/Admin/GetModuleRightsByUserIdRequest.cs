﻿using System.Collections.Generic;

namespace SSWhite.Domain.Request.Admin
{
    public class GetModuleRightsByUserIdRequest
    {
        public int UserId { get; set; }
    }
}
