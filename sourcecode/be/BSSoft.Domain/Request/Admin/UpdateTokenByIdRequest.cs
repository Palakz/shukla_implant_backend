﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class UpdateTokenByIdRequest
    {
        public Guid Id { get; set; }

        public string Token { get; set; }

        public DateTime TokenGeneratedTime { get; set; }
    }
}
