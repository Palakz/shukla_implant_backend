﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class AddNewUserWithImageRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public string MobileNumber { get; set; }

        public int OccupationId { get; set; }

        public int OrganizationId { get; set; }

        public string ProfileImage { get; set; }

        public IFormFile Image { get; set; }
    }
}
