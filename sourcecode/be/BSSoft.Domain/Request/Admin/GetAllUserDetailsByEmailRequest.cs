﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetAllUserDetailsByEmailRequest
    {
        public string Email { get; set; }

        public string UserName { get; set; }
    }
}
