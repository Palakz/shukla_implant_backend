﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ConfirmEmailRequest
    {
        public string Email { get; set; }
    }
}
