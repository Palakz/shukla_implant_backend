﻿using SSWhite.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Request.Admin
{
    public class UpdateOccupationRightsByOccupationIdRequest
    {
        public List<int> RightIdList { get; set; }

        public int OccupationId { get; set; }
    }
}
