﻿using SSWhite.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Request.Admin
{
    public class UpdateIsAdminApproveByUserIdRequest
    {
        public int UserId { get; set; }

        public ApprovedStatus ApprovedStatus { get; set; }
    }
}
