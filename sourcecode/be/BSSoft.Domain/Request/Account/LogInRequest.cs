﻿namespace SSWhite.Domain.Request.Account
{
    using SSWhite.Core.Attributes;

    public class LogInRequest
    {
        [Trim]
        public string UserName { get; set; }

        [Trim]
        public string Password { get; set; }
    }
}
