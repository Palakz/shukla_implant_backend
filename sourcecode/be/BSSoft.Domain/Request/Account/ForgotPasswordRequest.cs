﻿namespace SSWhite.Domain.Request.Organization.Account
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ForgotPasswordRequest
    {
        public string EmailAddress { get; set; }

        public string Password { get; set; }
    }
}
