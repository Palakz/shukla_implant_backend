﻿namespace SSWhite.Domain.Request.Common
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class InsertPickListRequest
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool? IsValueNameSame { get; set; }

        public bool? IsValueAutoId { get; set; }

        public int? DisplayOrder { get; set; }
    }
}
