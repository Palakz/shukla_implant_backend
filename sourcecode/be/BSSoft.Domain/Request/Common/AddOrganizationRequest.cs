﻿namespace SSWhite.Domain.Request.Common
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class AddOrganizationRequest
    {
        public string Name { get; set; }
    }
}
