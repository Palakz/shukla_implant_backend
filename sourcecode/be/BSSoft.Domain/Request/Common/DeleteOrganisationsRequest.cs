﻿namespace SSWhite.Domain.Request.Common
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class DeleteOrganisationsRequest
    {
        public int Id { get; set; }
    }
}
