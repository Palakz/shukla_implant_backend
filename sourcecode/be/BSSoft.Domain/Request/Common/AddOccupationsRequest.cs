﻿namespace SSWhite.Domain.Request.Common
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class AddOccupationsRequest
    {
        public string Name { get; set; }
    }
}
