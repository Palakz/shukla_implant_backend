﻿namespace SSWhite.Domain.Request.Common
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class UpdateOccupationsRequest
    {
        public int? Id { get; set; }

        public string Name { get; set; }
    }
}
