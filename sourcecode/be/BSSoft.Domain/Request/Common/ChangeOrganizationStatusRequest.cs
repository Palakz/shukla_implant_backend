﻿namespace SSWhite.Domain.Request.Common
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ChangeOrganizationStatusRequest
    {
        public int Id { get; set; }
    }
}
