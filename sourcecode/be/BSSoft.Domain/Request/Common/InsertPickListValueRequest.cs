﻿namespace SSWhite.Domain.Request.Common
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class InsertPickListValueRequest
    {
        public int? PickListId { get; set; }

        public bool? IsDefault { get; set; }

        public int? DisplayOrder { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public string Note { get; set; }
    }
}
