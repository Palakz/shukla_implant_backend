﻿namespace SSWhite.Domain.Request.Common
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class UpdateOrganizationRequest
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}