﻿namespace SSWhite.Domain.Request.PickList
{
    using System.Collections.Generic;

    public class AddOrEditPickListValueRequest
    {
        public List<PickListValue> PickListValue { get; set; }

        public bool IsEdit { get; set; }
    }
}
