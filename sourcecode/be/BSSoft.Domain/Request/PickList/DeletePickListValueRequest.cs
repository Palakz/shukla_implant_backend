﻿namespace SSWhite.Domain.Request.PickList
{
    using System.Collections.Generic;

    public class DeletePickListValueRequest
    {
        public List<int?> Id { get; set; }
    }
}
