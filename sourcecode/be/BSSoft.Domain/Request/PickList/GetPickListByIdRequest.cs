﻿namespace SSWhite.Domain.Request.PickList
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class GetPickListByIdRequest
    {
        public int? Id { get; set; }
    }
}
