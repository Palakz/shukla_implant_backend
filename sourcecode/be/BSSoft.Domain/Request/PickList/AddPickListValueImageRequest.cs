﻿namespace SSWhite.Domain.Request.PickList
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;

    public class AddPickListValueImageRequest
    {
        public int? PicklistValueId { get; set; }

        public string ImagePath { get; set; }

        public List<IFormFile> PickListValueImage { get; set; }

    }
}
