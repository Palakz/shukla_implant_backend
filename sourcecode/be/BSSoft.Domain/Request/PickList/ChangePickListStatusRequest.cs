﻿namespace SSWhite.Domain.Request.PickList
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ChangePickListStatusRequest
    {
        public int Id { get; set; }
    }
}
