﻿namespace SSWhite.Domain.Request.PickList
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class DeletePickListRequest
    {
        public int? Id { get; set; }
    }
}
