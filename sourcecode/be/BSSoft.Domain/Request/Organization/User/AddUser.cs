﻿namespace SSWhite.Domain.Request.Organization.User
{
    using SSWhite.Domain.Common.Organization.User;

    public class AddUser : User
    {
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}
