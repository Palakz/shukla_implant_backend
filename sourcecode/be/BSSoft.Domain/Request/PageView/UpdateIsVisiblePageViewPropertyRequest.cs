﻿namespace SSWhite.Domain.Request.PageView
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class UpdateIsVisiblePageViewPropertyRequest
    {
        public int Id { get; set; }

        public bool IsVisible { get; set; }

    }
}
