﻿namespace SSWhite.Domain.Request.PageView
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class GetAllRecentlyViewedRequest
    {
        public int ImplantTypeId { get; set; }
    }
}
