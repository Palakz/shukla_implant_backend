﻿namespace SSWhite.Domain.Request.PageView
{
   public class AddOrEditPageViewRequest
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public int PageViewType { get; set; }

        public int ImplantType { get; set; }

        public string Url { get; set; }

        public bool IsActive { get; set; }
    }
}
