﻿namespace SSWhite.Domain.Request.PageView
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;
    using SSWhite.Domain.Enums;

    public class GetPageViewByImplantTypeRequest
    {
        public int ImpantTypeId { get; set; }

        public int PageviewTypeId { get; set; }

        public int? ImplantId { get; set; }

        public string ImplantSearchText { get; set; }


    }
}
