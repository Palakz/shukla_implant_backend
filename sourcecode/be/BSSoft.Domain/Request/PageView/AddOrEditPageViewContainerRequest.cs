﻿namespace SSWhite.Domain.Request.PageView
{
   public class AddOrEditPageViewContainerRequest
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public int PageViewId { get; set; }

        public string ContainerIcon { get; set; }

        public string ContainerIconPosition { get; set; }

        public int? DisplayColumnNumber { get; set; }
    }
}
