﻿namespace SSWhite.Domain.Request.PageView
{
   public class GetAllPageViewByImplantTypeRequest
    {
         public int ImplantType { get; set; }
    }
}
