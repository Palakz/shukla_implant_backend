﻿namespace SSWhite.Domain.Request.PageView
{
   public class AddOrEditPageViewPropertyRequest
    {
        public int? Id { get; set; }

        public int FieldId { get; set; }

        public string DisplayName { get; set; }

        public int ContainerId { get; set; }

        public bool IsActive { get; set; }

        public bool IsReadOnly { get; set; }

        public bool IsVisible { get; set; }

        public string CssClass { get; set; }

        public string Script { get; set; }

        public string FieldControlType { get; set; }
    }
}
