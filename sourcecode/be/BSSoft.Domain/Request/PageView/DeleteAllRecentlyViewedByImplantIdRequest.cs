﻿namespace SSWhite.Domain.Request.PageView
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class DeleteAllRecentlyViewedByImplantIdRequest
    {
        public int? ImplantId { get; set; }
    }
}
