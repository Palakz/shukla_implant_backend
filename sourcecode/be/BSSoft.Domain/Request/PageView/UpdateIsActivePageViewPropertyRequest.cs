﻿namespace SSWhite.Domain.Request.PageView
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class UpdateIsActivePageViewPropertyRequest
    {
        public int Id { get; set; }

        public bool IsActive { get; set; }

    }
}
