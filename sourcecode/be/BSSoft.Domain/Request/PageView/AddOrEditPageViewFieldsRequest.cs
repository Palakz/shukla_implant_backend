﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Request.PageView
{
   public class AddOrEditPageViewFieldsRequest
    {
        public int? Id { get; set; }

        public int PageViewContainerId { get; set; }

        public int FieldId { get; set; }

        public string OnChangeJavaScriptFunc { get; set; }

    }
}
