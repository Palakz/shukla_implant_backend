﻿using System.Collections.Generic;

namespace SSWhite.Domain.Request.Dashboard
{
    public class GetProfileByIdRequest
    {
        public int UserId { get; set; }
    }
}
