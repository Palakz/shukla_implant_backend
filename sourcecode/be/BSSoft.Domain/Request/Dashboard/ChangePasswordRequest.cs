﻿namespace SSWhite.Domain.Request.Dashboard
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class ChangePasswordRequest
    {
        public string CurrentPassword { get; set; }

        public string NewPassword { get; set; }

        public string VerifyNewPassword { get; set; }
    }
}
