﻿namespace SSWhite.Domain.Request.Dashboard
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class UpdateProfileByIdRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MobileNumber { get; set; }

        public int OccupationId { get; set; }

        public int OrganisationId { get; set; }

    }
}
