﻿namespace SSWhite.Domain.Common.Organization.User
{
    using System.Collections.Generic;

    public class User
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string MobileNo { get; set; }

        public string EmailAddress { get; set; }

        public string Address { get; set; }

        public string ProfilePicPath { get; set; }

        public List<int> EntityIds { get; set; }

        public List<int> RoleIds { get; set; }
    }
}
