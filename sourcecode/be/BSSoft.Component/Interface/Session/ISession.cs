﻿namespace SSWhite.Component.Interface.Session
{
    using System.Threading.Tasks;
    using SSWhite.Core.Common.Session;

    public interface ISession
    {
        ////public Task SetSessionDetails(SessionDetail data, string authToken);

        ////public Task<SessionDetail> GetSessionDetails();

        ////public Task DeleteSession();

        ////Task<(string, int?)> GetSessionDetails(string userName);

        public void SetInMemorySessionDetails(SessionDetail data, string authToken);

        public SessionDetail GetInMemorySessionDetails();

        public void DeleteInMemorySession();

        public (string, int?) GetInMemorySessionDetails(string userName);

    }
}
