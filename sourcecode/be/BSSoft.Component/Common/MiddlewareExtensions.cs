﻿namespace SSWhite.Component.Common
{
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Routing;

    public static class MiddlewareExtensions
    {
        public static async Task<string> GetRequestData(this HttpContext httpContext)
        {
            var data = string.Empty;

            // Log query string parameters (GET request)
            if (httpContext.Request.QueryString.HasValue)
            {
                foreach (var (key, value) in httpContext.Request.Query)
                {
                    data += key + ":" + value;
                }
            }

            // Log route data if available
            var routeData = httpContext.GetRouteData();
            if (routeData != null)
            {
                foreach (var (key, value) in routeData.Values)
                {
                    data += key + ":" + value;
                }
            }

            // Log request body for POST and PUT requests
            if (httpContext.Request.Method == HttpMethods.Post || httpContext.Request.Method == HttpMethods.Put)
            {
                httpContext.Request.EnableBuffering();

                using (var reader = new StreamReader(httpContext.Request.Body, Encoding.UTF8, detectEncodingFromByteOrderMarks: false, leaveOpen: true))
                {
                    var body = await reader.ReadToEndAsync();
                    httpContext.Request.Body.Position = 0; // Reset the stream position for the next middleware
                    data += body;
                }

                // Log form data if available
                if (httpContext.Request.HasFormContentType)
                {
                    var form = await httpContext.Request.ReadFormAsync();
                    foreach (var key in form.Keys)
                    {
                        var value = form[key];
                        data += key + ":" + value;
                    }
                }
            }

            return data;
        }
    }
}
