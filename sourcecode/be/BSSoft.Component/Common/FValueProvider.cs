﻿namespace SSWhite.Component.Common
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Microsoft.Extensions.Primitives;
    using SSWhite.Core.Utilities;

    public class FValueProvider : BindingSourceValueProvider
    {
        private readonly IValueProvider _valueProvider;
        private readonly HashSet<string> _encryptedProperties;

        public FValueProvider(BindingSource bindingSource, IValueProvider valueProvider, HashSet<string> encryptedProperties)
            : base(bindingSource)
        {
            _valueProvider = valueProvider;
            _encryptedProperties = encryptedProperties;
        }

        public override bool ContainsPrefix(string prefix)
        {
            return _valueProvider.ContainsPrefix(prefix);
        }

        public override ValueProviderResult GetValue(string key)
        {
            var values = _valueProvider.GetValue(key);

            if (values.FirstValue != null && _encryptedProperties.Contains(key))
            {
                if (values.Values.Count == 1)
                {
                    return new ValueProviderResult(EncryptDecrypt.Decrypt(values.FirstValue), values.Culture);
                }

                var decryptedValues = new string[values.Values.Count];

                for (int i = 0; i < values.Values.Count; i++)
                {
                    decryptedValues[i] = EncryptDecrypt.Decrypt(values.Values[i]);
                }

                return new ValueProviderResult(new StringValues(decryptedValues), values.Culture);
            }

            return values;
        }
    }
}
