﻿namespace SSWhite.Component.Common
{
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;

    public class FValueProviderFactory : IValueProviderFactory
    {
        public Task CreateValueProviderAsync(ValueProviderFactoryContext context)
        {
            IValueProvider value = null;
            BindingSource source = null;

            if (context.ActionContext.HttpContext.Request.Method == "GET")
            {
                value = context.ValueProviders.OfType<QueryStringValueProvider>().FirstOrDefault();
                source = BindingSource.Query;
            }
            else if (context.ActionContext.HttpContext.Request.Method == "POST")
            {
                value = context.ValueProviders.OfType<FormValueProvider>().FirstOrDefault();
                source = BindingSource.Form;
            }

            if (value != null)
            {
                var encryptor = string.Empty;
                var type = context.ActionContext.ActionDescriptor.Parameters.First().ParameterType;
                var properties = (from property in type.GetProperties()
                                  let convertType = property.GetCustomAttribute<JsonConverterAttribute>()?.ConverterType
                                  where convertType == typeof(EncryptPropertyConverter)
                                  select property.Name).ToHashSet();

                context.ValueProviders.Insert(0, new FValueProvider(source, value, properties));
            }

            return Task.CompletedTask;
        }
    }
}
