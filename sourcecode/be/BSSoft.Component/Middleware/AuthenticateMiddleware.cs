﻿namespace SSWhite.Component.Middleware
{
    using System.Net;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using SSWhite.Component.Interface.Cache;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Extensions;

    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class AuthenticateMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IOptions<AppSetting> appSetting;
        private readonly ICache _cache;
        private readonly AppSetting _appSetting;

        public AuthenticateMiddleware(RequestDelegate next, IOptions<AppSetting> appSetting, ICache cache)
        {
            _next = next;
            this.appSetting = appSetting;
            _cache = cache;
            _appSetting = appSetting.Value;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var userid = httpContext.User.Identity.Name;
            string sourceType = httpContext.Request.Headers["source-type"].ToString();
            if (httpContext.Request.Path.Value.Contains("swagger"))
            {
                await _next(httpContext);
            }
            else if (httpContext.Request.Path.Value.Contains("token"))
            {
                await _next(httpContext);
            }
            else if (httpContext.Request.Path.Value.Contains("ForgotPassword") || httpContext.Request.Path.ToString().Contains("ConfirmEmail") || httpContext.Request.Path.ToString().Contains("GetOrganization") || httpContext.Request.Path.ToString().Contains("GetOccupations") || httpContext.Request.Path.ToString().Contains("AddNewUser") || httpContext.Request.Path.ToString().Contains("ForgotPassword") || httpContext.Request.Path.ToString().Contains("GetAllUser") || httpContext.Request.Path.ToString().Contains("ForgotPassword") || httpContext.Request.Path.ToString().Contains("SendForgetPasswordLink") || httpContext.Request.Path.ToString().Contains("ResendActivation"))
            {
                await _next(httpContext);
            }
            else
            {
                if (httpContext.Request.Headers["referer"].ToString().Contains("swagger"))
                {
                    // sourceType = httpContext.Request.Headers["source-type"] = SourceType.Swagger.ToString();
                }

                var authToken = httpContext.Request.Headers["auth-token"].ToString();

                if (string.IsNullOrEmpty(authToken) && (!httpContext.Request.Path.ToString().Contains("ConfirmEmail") && !httpContext.Request.Path.ToString().Contains("GetOrganization") && !httpContext.Request.Path.ToString().Contains("GetOccupations") && !httpContext.Request.Path.ToString().Contains("AddNewUser") && !httpContext.Request.Path.ToString().Contains("ForgotPassword")) && !httpContext.Request.Path.ToString().Contains("GetAllUser") && !httpContext.Request.Path.ToString().Contains("ForgotPassword") && !httpContext.Request.Path.ToString().Contains("SendForgetPasswordLink") && !httpContext.Request.Path.ToString().Contains("ResendActivation"))
                {
                    httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    return;
                }
                else
                {
                    if (_appSetting.IsRedisCacheSession)
                    {
                    }
                    else
                    {
                        var value = _cache.GetData<string>(authToken);
                        if (value == null)
                        {
                            httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            return;
                        }

                        var session = JsonConvert.DeserializeObject<SessionDetail>(value);

                        if (session == null)
                        {
                            httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            return;
                        }
                        else
                        {
                            var userAuth = _cache.GetData<string>($"{httpContext.Request.Host.Value}_{session.UserName}");

                            if (authToken != userAuth)
                            {
                                _cache.RemoveData(authToken);
                                httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                                return;
                            }
                            else
                            {
                                await _next(httpContext);
                            }
                        }
                    }
                }
            }
        }
    }
}
