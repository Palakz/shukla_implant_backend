﻿namespace SSWhite.Component.Middleware
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Options;
    using SSWhite.Component.Interface.Cache;
    using SSWhite.Core.Common.Application;

    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class SessionInfoMiddleware
    {
        private static Interface.Session.ISession _session;
        private readonly RequestDelegate _next;
        private readonly IOptions<AppSetting> appSetting;
        private readonly ICache _cache;
        private readonly AppSetting _appSetting;

        public SessionInfoMiddleware(RequestDelegate next, IOptions<AppSetting> appSetting, ICache cache)
        {
            _next = next;
            this.appSetting = appSetting;
            _cache = cache;
            _appSetting = appSetting.Value;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            _session ??= (Interface.Session.ISession)httpContext.RequestServices.GetService(typeof(Interface.Session.ISession));
            var requestId = Guid.NewGuid();
            httpContext.Items["RequestId"] = requestId;

            await _next(httpContext);
        }
    }
}
