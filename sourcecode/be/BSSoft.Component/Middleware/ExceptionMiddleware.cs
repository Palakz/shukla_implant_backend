﻿namespace SSWhite.Component.Middleware
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Component.Common;
    using SSWhite.Core.Request;
    using SSWhite.Logger.Interface.ExceptionLogger;
    using ISession = SSWhite.Component.Interface.Session.ISession;

    public class ExceptionMiddleware
    {
        private static IExceptionLogger _exceptionLogger;
        private static ISession _session;
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                httpContext.Response.Headers["RequestId"] = httpContext.Items["RequestId"].ToString();
                await _next(httpContext);
            }
            catch (Exception appException)
            {
                try
                {
                    await LogException(appException, httpContext);
                    throw appException;
                }
                catch (Exception exception)
                {
                    _exceptionLogger = (IExceptionLogger)(_exceptionLogger ?? httpContext.RequestServices.GetService(typeof(IExceptionLogger)));
                    await _exceptionLogger.LogToFile(appException, exception);
                    throw appException;
                }
            }
        }

        private async Task LogException(Exception exception, HttpContext httpContext)
        {
            _session ??= (ISession)httpContext.RequestServices.GetService(typeof(ISession));
            _exceptionLogger = (IExceptionLogger)(_exceptionLogger ?? httpContext.RequestServices.GetService(typeof(IExceptionLogger)));
            var request = new ServiceRequest<object>()
            {
                Session = _session.GetInMemorySessionDetails(),
                Data = await httpContext.GetRequestData()
            };
            await _exceptionLogger.LogToDatabase(request, exception);
        }
    }
}
