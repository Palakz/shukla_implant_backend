﻿namespace SSWhite.Component.Middleware
{
    using System;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Routing;
    using Newtonsoft.Json;
    using SSWhite.Component.Common;
    using SSWhite.Core.Request;
    using SSWhite.Logger.Interface.RequestLogger;
    using SSWhite.Logger.Request.Request;
    using ISession = SSWhite.Component.Interface.Session.ISession;

    public class RequestMiddleware
    {
        private static IRequestLogger _requestLogger;
        private static ISession _session;
        private readonly RequestDelegate _next;

        public RequestMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                var data = await httpContext.GetRequestData();
                await _next(httpContext);
                _session ??= (ISession)httpContext.RequestServices.GetService(typeof(ISession));
                _requestLogger = (IRequestLogger)(_requestLogger ?? httpContext.RequestServices.GetService(typeof(IRequestLogger)));
                var serviceRequest = new ServiceRequest<LogRequest>()
                {
                    Session = _session.GetInMemorySessionDetails(),
                    Data = PrepareLogRequest(httpContext, data),
                };
                await _requestLogger.LogToDatabase(serviceRequest);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static LogRequest PrepareLogRequest(HttpContext httpContext, string data)
        {
            return new LogRequest()
            {
                RequestId = (Guid)httpContext.Items["RequestId"],
                Data = JsonConvert.SerializeObject(data),
                FileName = httpContext.Request.Path,
                Method = httpContext.Request.Method,
                Action = httpContext.Request.Path.Value.Split('/')[^1],
            };
        }
    }
}
