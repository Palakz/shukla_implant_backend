﻿namespace SSWhite.Data.Common
{
    public static class StoredProcedures
    {
        // Common shulka
        public const string DBO_SELECT_PICK_LISTS = "dbo.SelectPickLists";
        public const string DBO_INSERT_PICK_LIST = "dbo.InsertPickList";
        public const string DBO_INSERT_PICK_LIST_VALUE = "dbo.InsertPickListValue";
        public const string MASTER_UPDATE_OCCUPATIONS = "master.UpdateOccupations";
        public const string MASTER_UPDATE_ORGANIZATION = "master.UpdateOrganisations";
        public const string MASTER_ADD_OCCUPATIONS = "master.AddOccupations";
        public const string MASTER_ADD_ORGANIZATION = "master.AddOrganisations";
        public const string MASTER_DELETE_OCCUPATIONS = "master.DeleteOccupation";
        public const string MASTER_DELETE_ORGANIZATION = "master.DeleteOrganisations";
        public const string DBO_CHANGE_ORGANIZATION_STATUS = "dbo.ChangeOrganizationStatus";
        public const string DBO_CHANGE_OCCUPATION_STATUS = "dbo.ChangeOccupationStatus";

        //Picklist
        public const string DBO_GET_ALL_PICKLIST = "dbo.GetAllPickList";
        public const string DBO_GET_PICKLIST_BY_ID = "dbo.GetPickListById";
        public const string DBO_DELETE_PICKLIST = "dbo.DeletePickList";
        public const string DBO_ADD_OR_EDIT_PICKLIST = "dbo.AddOrEditPickList";
        public const string DBO_GET_PICKLISTVALUE_BY_ID = "dbo.GetPickListValueById";
        public const string DBO_DELETE_PICKLISTVALUE = "dbo.DeletePickListValue";
        public const string DBO_ADD_OR_EDIT_PICKLISTVALUE = "dbo.AddOrEditPickListValue";
        public const string DBO_ADD_PICKLIST_VALUE_IMAGE = "dbo.AddPickListValueImage";
        public const string DBO_CHANGE_PICKLIST_STATUS = "dbo.ChangePickListStatus";

        //PageView
        public const string DBO_ADD_OR_EDIT_PAGEVIEWFIELDS = "dbo.InsertPageViewFields";
        public const string DBO_GET_ALL_PAGEVIEWFIELDS = "dbo.GetAllPageViewFields";
        public const string DBO_ADD_OR_EDIT_PAGEVIEW = "dbo.InsertPageView";
        public const string DBO_GET_ALL_PAGEVIEWBYIMPLANTID = "dbo.GetAllPageViewByImplantType";
        public const string DBO_GET_ALL_Delete_Page_View = "dbo.DeletePageView";
        public const string DBO_GET_ALL_Delete_Page_View_Fields = "dbo.DeletePageViewFields";
        public const string DBO_ADD_OR_EDIT_PAGE_VIEW_CONTAINER = "dbo.InsertPageViewContainer";
        public const string DBO_GET_ALL_Delete_Page_View_Container = "dbo.DeletePageViewContainer";
        public const string DBO_GET_ALL_PAGE_VIEW_PROPERTY = "dbo.GetAllPageViewProperty";
        public const string DBO_DELETE_PAGE_VIEW_PROPERTY = "dbo.DeletePageViewProperty";
        public const string DBO_ADD_OR_EDIT_PAGEVIEW_PROPERTY = "dbo.InsertPageViewProperty";
        public const string DBO_GET_ALL_CONTAINER_BY_PAGE_VIEW_ID = "dbo.GetAllContainerByPageViewID";
        public const string DBO_UPDATE_ISACTIVE_PAGEVIEW_PROPERTYT = "dbo.UpdateIsActivePageViewProperty";
        public const string DBO_UPDATE_ISREADONLY_PAGEVIEW_PROPERTYT = "dbo.UpdateIsReadOnlyPageViewProperty";
        public const string DBO_UPDATE_ISVISIBLE_PAGEVIEW_PROPERTYT = "dbo.UpdateIsVisiblePageViewProperty";
        public const string DBO_GET_PAGE_VIEW_BY_PAGE_VIEW_ID = "dbo.GetPageViewByPageViewID";
        public const string DBO_GET_PAGE_VIEW_BY_IMPANT_TYPE_ID = "dbo.GetPageViewByImplantType";
        public const string DBO_GET_IMPLANT_PROPERT_COUNT_BY_FILTER = "dbo.GetImplantPropertCountByFilter";
        public const string DBO_GET_GET_ALL_RECENTLY_VIEWED = "dbo.GetAllRecentlyViewed";
        public const string DBO_GET_DELETE_RECENTLY_VIEWED_BY_ID = "dbo.DeleteRecentlyViewedById";
        public const string DBO_GET_DELETE_ALL_RECENTLY_VIEWED_BY_IMPLANT_ID = "dbo.DeleteAllRecentlyViewedByImplantId";

        //shukla Implant
        public const string DBO_ADD_OR_EDIT_IMPLANTTYPE = "dbo.AddOrEditImplantType";
        public const string DBO_DELETE_IMPLANTTYPE = "dbo.DeleteImplantType";
        public const string DBO_GET_ALL_IMPLANTTYPE = "dbo.GetAllImplantType";
        public const string DBO_GET_IMPLANTTYPE_FOR_DROPDOWN = "dbo.GetImplantTypeForDropdown";
        public const string DBO_GET_IMPLANTTYPE_BY_ID = "dbo.GetImplantTypeById";
        public const string DBO_GET_ALL_DATATYPES = "dbo.GetAllDataTypes";
        public const string DBO_GET_ALL_IMPLANTFIELDS = "dbo.GetAllImplantFields";
        public const string DBO_GET_ALL_IMPLANTFIELDS_By_IMPLANTTYPE = "dbo.GetImplantFieldsByImplantType";
        public const string DBO_GET_ALL_IMPLANTFIELDS_BY_ID = "dbo.GetAllImplantFieldsById";
        public const string DBO_ADD_OR_EDIT_IMPLANTFIELDS = "dbo.AddOrEditImplantFields";
        public const string DBO_GET_IMPLANT_BY_IMPLANTTYPE = "dbo.GetAllImplantByImplantTyppeId";
        public const string DBO_ADD_OR_REMOVE_FAVORITE = "dbo.AddOrRemoveFavorite";
        public const string DBO_SEARCH_IMPLANT = "dbo.SearchImplant";
        public const string DBO_DELETE_HISTORY = "dbo.DeleteHistory";
        public const string GET_ALL_ALL_FAVORITE_IMPLANT = "dbo.GetAllFavoriteImplant";
        public const string DBO_GET_ALL_HISTORY = "dbo.GetAllHistory";
        public const string DBO_GET_ALL_UNAPPROVED_IMPLANT = "dbo.GetAllUnapprovedImplant";
        public const string DBO_APPROVE_IMPLANT = "dbo.ApproveImplant";
        public const string DBO_REJECT_IMPLANT = "dbo.RejectImplant";
        public const string DBO_GET_IMPLANT_BY_ID = "dbo.GetImplantById";
        public const string DBO_GET_ALL_IMPLANT = "dbo.GetAllImplant";
        public const string DBO_GET_RELATED_IMPLANT_OF_SHUKLAPARTS = "dbo.GetRelatedImplantOfShuklaParts";
        public const string DBO_DELETE_ALL_HISTORY_BY_USERID = "dbo.DeleteAllHistoryByUserId";
        public const string DBO_GET_ALL_IMPLANT_COMMENTS = "dbo.GetAllImplantComments";
        public const string DBO_ADD_OR_EDIT_IMPLANT_COMMENT = "dbo.AddOrEditImplantComment";
        public const string DBO_LIKE_OR_DISLIKE_IMPLANT_COMMENT = "dbo.LikeOrDislikeImplantComment";
        public const string DBO_GET_IMPLANT_NOTIFICATION = "dbo.GetImplantNotification";
        public const string DBO_MARK_READ_IMPLANT_NOTIFICATION = "dbo.MarkReadImplantNotification";
        public const string DBO_DELETE_IMAGE_BY_ENTITY = "dbo.DeleteImageByEntity";
        public const string DBO_CHANGE_IMPLANTTYPE_BY_STATUS = "dbo.ChangeImplantTypeStatus";
        public const string DBO_CHANGE_IMPLANTFIELD_BY_STATUS = "dbo.ChangeImplantFieldStatus";
        public const string DBO_CHANGE_IMPLANT_BY_STATUS = "dbo.ChangeImplantStatus";


        // Dashboard
        public const string MASTER_GET_SSWHITE_TEAM_DETAILS = "master.GetSSWhiteTeamDetails";
        public const string EMP_USER_NOTIFICATION = "emp.UserNotification";
        public const string EMP_CHANGE_PASSWORD = "master.ChangePassword";
        public const string MASTER_GET_USER_DETAILS_BY_USER_ID = "master.GetUserDetailsByUserId";
        public const string MASTER_UPDATE_PROFILE_BY_ID = "master.UpdateProfileById";
        public const string MASTER_UPDATE_PROFILE_WITH_IMAGE_BY_ID = "master.UpdateProfileWithImageById";
        public const string GET_PROFILE_BY_ID = "GetProfileById";
        // Admin
        public const string MASTER_GET_ORGANIZATION = "master.GetAllOrganisations";
        public const string MASTER_GET_Occupations = "master.GetAllOccupations";

        public const string MASTER_GET_DEPARTMENT_BY_DEPARTMENT_SUPERVISOR_NAME = "master.GetDepartmentByDepartmentSupervisorName";
        public const string MASTER_GET_REPORTING_UNDER = "master.GetReportingUnder";
        public const string MASTER_GET_EMPLOYEE_JOB_DESCRIPTION = "master.GetEmployeeJobDescription";
        public const string EMP_ADD_NEW_EMPLOYEE = "emp.AddNewEmployee";
        public const string EMPLOYEE_GET_ALL_EMPLOYEE_LIMIT = "emp.GetAllEmployeeLimit";
        public const string EMPLOYEE_UPDATE_EMPLOYEE_LIMIT = "emp.UpdateEmployeeLimit";

        // public const string EMPLOYEE_GET_DOCUMENT_APPROVAL_LIST = "emp.GetDocumentApprovalList";
        public const string EMP_CHECK_IF_EMPLOYEEID_EXISTS = "emp.CheckIfEmployeeIdExists";
        public const string EMP_INSERT_PUNCH_DETAILS = "emp.InsertPunchDetails";
        public const string EMP_UPDATE_DOCUMENT_APPROVAL_LIST = "emp.UpdateDocumentApprovalList";
        public const string EMP_VOID_EPO_BY_USERID = "emp.VoidEpoByUserId";
        public const string EMP_APPROVE_OR_DENY_EPO = "emp.ApproveOrDenyEpo";

        // Admin        // Shukla Implant
        public const string DBO_ADD_NEW_USER = "dbo.AddNewUser";
        public const string DBO_ADD_NEW_USER_WITH_IMAGE = "dbo.AddNewUserWithImage";
        public const string DBO_UPDATE_TOKEN_BY_ID = "dbo.UpdateTokenById";
        public const string DBO_GET_USER_DETAILS_BY_EMAIL = "dbo.GetUserDetailsByEmail";
        public const string EMP_Confirm_Email = "dbo.ConfirmEmial";
        public const string DBO_GET_ALL_USERS = "dbo.GetAllUser";
        public const string DBO_GET_ALL_USERS_DETAILS = "dbo.GetAllUserDetails";
        public const string DBO_UPDATE_ISADMIN_APPROVE_BY_USERID = "dbo.UpdateIsAdminApproveByUserId";

        public const string DBO_UPDATE_ISHOLDSTATUSBYUSERID = "dbo.UpdateIsHoldStatusByUserId";
        public const string DBO_UPDATE_OCCUPATIONRIGHTSBYOCCUPATIONID = "dbo.UpdateOccupationRightsByOccupationId";
        public const string DBO_UPDATE_USERRIGHTSBYUSERID = "dbo.UpdateUserRightsByUserId";
        public const string DBO_GET_USERSDETAILSFORDROPDOWN = "dbo.GetUsersDetailsForDropDown";
        public const string DBO_GET_MODULERIGHTSBYOCCUPATIONID = "dbo.GetModuleRightsByOccupationId";
        public const string DBO_GET_MODULERIGHTSBYUSERID = "dbo.GetModuleRightsByUserId";

        // EPO
        public const string EMP_GET_ALL_EPO_DROPDOWNS = "emp.GetAllEPODropDowns";
        public const string EMP_ADD_OR_EDIT_EPO = "emp.AddOrEditEpo";
        public const string EMP_GET_AUTHORIZED_LIMIT_BY_USER_ID = "emp.GetAuthorizedLimitByUserId";
        public const string EMP_GET_EPO_DETAILS_BY_EPOID = "emp.GetEpoDetailsByEpoId";
        public const string EMP_GET_ALL_EPOS = "emp.GetAllEpos";
        public const string EMP_GET_MPC_VOTE_FOR_EPO = "emp.GetMpcVoteForEpo";
        public const string EMP_GET_ALL_MPC_VOTE_FOR_EPO = "emp.GetAllMpcVoteForEpo";
        public const string EMP_GET_MY_SUBORDINATE_EPOS = "emp.GetMySubordinateEpos";
        public const string EMP_GET_MY_APPROVAL_FOR_EPO = "emp.GetMyApprovalForEpo";

        // Document
        public const string EMP_ADD_OR_EDIT_DOCUMENT = "emp.AddOrEditDocument";

        // Attendance Change
        public const string EMP_CREATE_ATTENDANCE_FTO = "emp.CreateAttendanceFto";
        public const string EMP_APPROVE_OR_DENY_FTO = "emp.ApproveOrDenyFto";
        public const string EMP_GET_EPO_MODULES_COUNTS = "emp.GetEpoModulesCounts";
        public const string EMP_GET_DEPARTMENT_FTO_BY_USERID = "emp.GetDepartmentFtoByUserId";
        public const string EMP_GET_ATTENDANCE_FTO_BY_ID = "emp.GetAttendanceFtoById";
        public const string EMP_GET_ALL_DEPARTMENT_OR_USER_FTO_BY_USERID = "emp.GetAllDepartmentOrUserFtoByUserId";
        public const string EMP_GET_MPC_VOTE_DETAILS_BY_EPOID = "emp.GetMpcVoteDetailsByEpoId";

        // Subscriber Schema -> Account
        public const string SUBSCRIBER_VALIDATE_USER_FOR_LOGIN = "subscriber.ValidateUserForLogIn";
        public const string MASTER_VALIDATE_USER_FOR_LOGIN = "master.ValidateUserForLogIn";
        public const string DBO_FORGOT_PASSWORD = "dbo.ForgotPassword";

        // Subscriber Schema -> User
        public const string SUBSCRIBER_GET_ALL_USERS = "subscriber.GetAllUsers";
        public const string SUBSCRIBER_INSERT_OR_UPDATE_USER = "subscriber.InsertOrUpdateUser";
        public const string SUBSCRIBER_CHANGE_USER_STATUS = "subscriber.ChangeUserStatus";
        public const string SUBSCRIBER_GET_USER_BY_ID = "subscriber.GetUserById";
        public const string SUBSCRIBER_DELETE_USER = "subscriber.DeleteUser";
        public const string DBO_GET_USER_DETAILS_BY_ID = "dbo.GetUserDetailsById";
        public const string DBO_INSERT_IN_OUT_DETAILS_BY_ID = "dbo.InsertInOutDetailsById";
        public const string DBO_GET_IN_OUT_DETAILS_BY_USER_ID = "dbo.GetInOutDetailsByUserId";
        public const string DBO_GET_USERS_IN_OUT_STATUS = "dbo.GetUsersInOutStatus";
        public const string DBO_GET_IN_OUT_STATUS_BY_PUNCHTYPE = "dbo.GetInOutStatusByPunchType";

        // Subscriber Schema -> Role
        public const string SUBSCRIBER_GET_ALL_ROLES = "subscriber.GetAllRoles";
        public const string SUBSCRIBER_INSERT_OR_UPDATE_ROLE = "subscriber.InsertOrUpdateRole";
        public const string SUBSCRIBER_CHANGE_ROLE_STATUS = "subscriber.ChangeRoleStatus";
        public const string SUBSCRIBER_GET_ROLE_BY_ID = "subscriber.GetRoleById";
        public const string SUBSCRIBER_DELETE_ROLE = "subscriber.DeleteRole";
        public const string SUBSCRIBER_GET_ROLES = "subscriber.GetRoles";

        // Subscriber Schema -> RightsGroup
        public const string SUBSCRIBER_GET_RIGHTS_GROUPS = "subscriber.GetRightsGroups";
        public const string SUBSCRIBER_GET_RIGHTS = "subscriber.GetRights";

        // Subscriber Schema -> Module
        public const string SUBSCRIBER_GET_MODULES = "subscriber.GetModules";

        // Master Schema -> Client
        public const string MASTER_GET_ALL_CLIENTS = "master.GetAllClients";
        public const string MASTER_INSERT_OR_UPDATE_CLIENT = "master.InsertOrUpdateClient";
        public const string MASTER_CHANGE_CLIENT_STATUS = "master.ChangeClientStatus";
        public const string MASTER_GET_CLIENT_BY_ID = "master.GetClientById";
        public const string MASTER_DELETE_CLIENT = "master.DeleteClient";
        public const string MASTER_GET_CLIENT_OPENING_BALANCE_DETAILS = "master.GetClientOpeningBalanceDetails";
        public const string MASTER_INSERT_OR_UPDATE_OPENING_BALANCE = "master.InsertOrUpdateOpeningBalance";

        // Master Schema -> Company
        public const string MASTER_GET_ALL_COMPANIES = "master.GetAllCompanies";
        public const string MASTER_INSERT_OR_UPDATE_COMPANY = "master.InsertOrUpdateCompany";
        public const string MASTER_CHANGE_COMPANY_STATUS = "master.ChangeCompanyStatus";
        public const string MASTER_GET_COMPANY_BY_ID = "master.GetCompanyById";
        public const string MASTER_DELETE_COMPANY = "master.DeleteCompany";
        public const string MASTER_GET_ENTITIES = "master.GetEntities";

        // Subscriber Schema -> Site
        public const string SUBSCRIBER_GET_ALL_SITES = "subscriber.GetAllSites";
        public const string SUBSCRIBER_INSERT_OR_UPDATE_SITE = "subscriber.InsertOrUpdateSite";
        public const string SUBSCRIBER_CHANGE_SITE_STATUS = "subscriber.ChangeSiteStatus";
        public const string SUBSCRIBER_GET_SITE_BY_ID = "subscriber.GetSiteById";
        public const string SUBSCRIBER_DELETE_SITE = "subscriber.DeleteSite";

        // Master Schema -> Tax
        public const string MASTER_GET_ALL_TAXES = "master.GetAllTaxes";
        public const string MASTER_INSERT_OR_UPDATE_TAXES = "master.InsertOrUpdateTax";
        public const string MASTER_CHANGE_TAX_STATUS = "master.ChangeTaxStatus";
        public const string MASTER_GET_TAX_BY_ID = "master.GetTaxById";
        public const string MASTER_DELETE_TAX = "master.DeleteTax";

        // Master Schema -> ItemGroup
        public const string MASTER_GET_ALL_ITEM_GROUPS = "master.GetAllItemGroups";
        public const string MASTER_INSERT_OR_UPDATE_ITEM_GROUP = "master.InsertOrUpdateItemGroup";
        public const string MASTER_CHANGE_ITEM_GROUP_STATUS = "master.ChangeItemGroupStatus";
        public const string MASTER_GET_ITEM_GROUP_BY_ID = "master.GetItemGroupById";
        public const string MASTER_DELETE_ITEM_GROUP = "master.DeleteItemGroup";

        // Master Schema -> ItemSubGroup
        public const string MASTER_GET_ALL_ITEM_SUB_GROUPS = "master.GetAllItemSubGroups";
        public const string MASTER_INSERT_OR_UPDATE_ITEM_SUB_GROUP = "master.InsertOrUpdateItemSubGroup";
        public const string MASTER_CHANGE_ITEM_SUB_GROUP_STATUS = "master.ChangeItemSubGroupStatus";
        public const string MASTER_GET_ITEM_SUB_GROUP_BY_ID = "master.GetItemSubGroupById";
        public const string MASTER_DELETE_ITEM_SUB_GROUP = "master.DeleteItemSubGroup";

        // Master Schema -> ItemCategory
        public const string MASTER_GET_ALL_ITEM_CATEGORIES = "master.GetAllItemCategories";
        public const string MASTER_INSERT_OR_UPDATE_ITEM_CATEGORY = "master.InsertOrUpdateItemCategory";
        public const string MASTER_CHANGE_ITEM_CATEGORY_STATUS = "master.ChangeItemCategoryStatus";
        public const string MASTER_GET_ITEM_CATEGORY_BY_ID = "master.GetItemCategoryById";
        public const string MASTER_DELETE_ITEM_CATEGORY = "master.DeleteItemCategory";
        public const string MASTER_GET_ITEM_CATEGORIES = "master.GetItemCategories";

        // Master Schema -> ItemSubCategory
        public const string MASTER_GET_ALL_ITEM_SUB_CATEGORIES = "master.GetAllItemSubCategories";
        public const string MASTER_INSERT_OR_UPDATE_ITEM_SUB_CATEGORY = "master.InsertOrUpdateItemSubCategory";
        public const string MASTER_CHANGE_ITEM_SUB_CATEGORY_STATUS = "master.ChangeItemSubCategoryStatus";
        public const string MASTER_GET_ITEM_SUB_CATEGORY_BY_ID = "master.GetItemSubCategoryById";
        public const string MASTER_DELETE_ITEM_SUB_CATEGORY = "master.DeleteItemSubCategory";

        // Master Schema -> ItemType
        public const string MASTER_GET_ALL_ITEM_TYPES = "master.GetAllItemTypes";
        public const string MASTER_INSERT_OR_UPDATE_ITEM_TYPE = "master.InsertOrUpdateItemType";
        public const string MASTER_CHANGE_ITEM_TYPE_STATUS = "master.ChangeItemTypeStatus";
        public const string MASTER_GET_ITEM_TYPE_BY_ID = "master.GetItemTypeById";
        public const string MASTER_DELETE_ITEM_TYPE = "master.DeleteItemType";

        // Master Schema -> Uqc
        public const string MASTER_GET_ALL_UQCS = "master.GetAllUqcs";
        public const string MASTER_INSERT_OR_UPDATE_UQC = "master.InsertOrUpdateUqc";
        public const string MASTER_CHANGE_UQC_STATUS = "master.ChangeUqcStatus";
        public const string MASTER_GET_UQC_BY_ID = "master.GetUqcById";
        public const string MASTER_DELETE_UQC = "master.DeleteUqc";

        // Master Schema -> Item
        public const string MASTER_GET_ALL_ITEMS = "master.GetAllItems";
        public const string MASTER_INSERT_OR_UPDATE_ITEM = "master.InsertOrUpdateItem";
        public const string MASTER_CHANGE_ITEM_STATUS = "master.ChangeItemStatus";
        public const string MASTER_GET_ITEM_BY_ID = "master.GetItemById";
        public const string MASTER_DELETE_ITEM = "master.DeleteItem";
        public const string MASTER_GET_ITEM_SUB_GROUP_BY_GROUP_ID = "master.GetItemSubGroupByGroupId";
        public const string MASTER_GET_ITEM_SUB_CATEGORY_BY_CATEGORY_ID = "master.GetItemSubCategoryByCategoryId";

        // Master Schema -> ItemRate
        public const string MASTER_GET_ALL_ITEM_RATES = "master.GetAllItemRates";
        public const string MASTER_INSERT_OR_UPDATE_ITEM_RATE = "master.InsertOrUpdateItemRate";
        public const string MASTER_CHANGE_ITEM_RATE_STATUS = "master.ChangeItemRateStatus";
        public const string MASTER_GET_ITEM_RATE_BY_ID = "master.GetItemRateById";
        public const string MASTER_DELETE_ITEM_RATE = "master.DeleteItemRate";
        public const string MASTER_GET_ITEM_BY_GROUP_ID = "master.GetItemByGroupId";

        // Master Schema -> Crate
        public const string MASTER_GET_ALL_CRATES = "master.GetAllCrates";
        public const string MASTER_INSERT_OR_UPDATE_CRATE = "master.InsertOrUpdateCrate";
        public const string MASTER_CHANGE_CRATE_STATUS = "master.ChangeCrateStatus";
        public const string MASTER_GET_CRATE_BY_ID = "master.GetCrateById";
        public const string MASTER_DELETE_CRATE = "master.DeleteCrate";
        public const string MASTER_GET_CRATES = "master.GetCrates";

        // Master Schema -> State
        public const string APP_GET_ALL_STATES_FOR_MASTER = "app.GetAllStatesForMaster";
        public const string APP_INSERT_OR_UPDATE_STATE = "app.InsertOrUpdateState";
        public const string APP_CHANGE_STATE_STATUS = "app.ChangeStateStatus";
        public const string APP_GET_STATE_BY_ID = "app.GetStateById";
        public const string APP_DELETE_STATE = "app.DeleteState";

        // Master Schema -> City
        public const string APP_GET_ALL_CITIES = "app.GetAllCities";
        public const string APP_INSERT_OR_UPDATE_CITY = "app.InsertOrUpdateCity";
        public const string APP_CHANGE_CITY_STATUS = "app.ChangeCityStatus";
        public const string APP_GET_CITY_BY_ID = "app.GetCityById";
        public const string APP_DELETE_CITY = "app.DeleteCity";

        // Master Schema -> Headers
        public const string MASTER_GET_ALL_HEADERS = "master.GetAllHeaders";
        public const string MASTER_INSERT_OR_UPDATE_HEADER = "master.InsertOrUpdateHeader";
        public const string MASTER_CHANGE_HEADER_STATUS = "master.ChangeHeaderStatus";
        public const string MASTER_GET_HEADER_BY_ID = "master.GetHeaderById";
        public const string MASTER_DELETE_HEADER = "master.DeleteHeader";
        public const string MASTER_GET_HEADERS = "master.GetHeaders";
        public const string MASTER_GET_HEADER_BY_CLIENT_ID = "master.GetHeaderByClientId";

        // Application Schema
        public const string APP_GET_ALL_STATES = "app.GetAllStates";
        public const string APP_GET_CITY_BY_STATE_ID = "app.GetCityByStateId";

        // Master Schema -> Item Types
        public const string MASTER_GET_ITEM_TYPES = "master.GetItemTypes";
        public const string MASTER_GET_ITEM_GROUPS = "master.GetItemGroups";
        public const string MASTER_GET_UQCS = "master.GetUqcs";
        public const string MASTER_GET_TAXES = "master.GetTaxes";
        public const string MASTER_GET_ITEMS = "master.GetItems";
        public const string MASTER_GET_ITEM_RATES = "master.GetItemRates";
        public const string MASTER_GET_ITEM_SUB_GROUPS = "master.GetItemSubGroups";
        public const string MASTER_GET_ITEM_SUB_CATEGORIES = "master.GetItemSubCategories";
        public const string MASTER_GET_CRATES_BY_ITEM_ID = "master.GetCratesByItemId";
        public const string MASTER_GET_UNITS_BY_ITEM_ID = "master.GetUnitsByItemId";

        // Master Schema -> Regions
        public const string MASTER_GET_ALL_REGIONS = "master.GetAllRegions";
        public const string MASTER_INSERT_OR_UPDATE_REGION = "master.InsertOrUpdateRegion";
        public const string MASTER_CHANGE_REGION_STATUS = "master.ChangeRegionStatus";
        public const string MASTER_GET_REGION_BY_ID = "master.GetRegionById";
        public const string MASTER_DELETE_REGION = "master.DeleteRegion";
        public const string MASTER_GET_REGIONS = "master.GetRegions";

        // Master Schema -> LedgerGroups
        public const string MASTER_GET_LEDGER_GROUPS = "master.GetLedgerGroups";

        // Master Schema -> Countries
        public const string MASTER_GET_ALL_COUNTRIES = "master.GetAllCountries";
        public const string MASTER_INSERT_OR_UPDATE_COUNTRY = "master.InsertOrUpdateCountry";
        public const string MASTER_CHANGE_COUNTRY_STATUS = "master.ChangeCountryStatus";
        public const string MASTER_GET_COUNTRY_BY_ID = "master.GetCountryById";
        public const string MASTER_DELETE_COUNTRY = "master.DeleteCountry";
        public const string MASTER_GET_COUNTRIES = "master.GetCountries";

        // Master Schema -> PackingTypes
        public const string MASTER_GET_ALL_PACKING_TYPES = "master.GetAllPackingTypes";
        public const string MASTER_INSERT_OR_UPDATE_PACKING_TYPE = "master.InsertOrUpdatePackingType";
        public const string MASTER_CHANGE_PACKING_TYPE_STATUS = "master.ChangePackingTypeStatus";
        public const string MASTER_GET_PACKING_TYPE_BY_ID = "master.GetPackingTypeById";
        public const string MASTER_DELETE_PACKING_TYPE = "master.DeletePackingType";
        public const string MASTER_GET_PACKING_TYPES = "master.GetPackingTypes";

        // Master Schema -> SubUnit
        public const string MASTER_GET_ALL_SUB_UNITS = "master.GetAllSubUnits";
        public const string MASTER_INSERT_OR_UPDATE_SUB_UNIT = "master.InsertOrUpdateSubUnit";
        public const string MASTER_CHANGE_SUB_UNIT_STATUS = "master.ChangeSubUnitStatus";
        public const string MASTER_GET_SUB_UNIT_BY_ID = "master.GetSubUnitById";
        public const string MASTER_DELETE_SUB_UNIT = "master.DeleteSubUnit";
        public const string MASTER_GET_SUB_UNITS = "master.GetSubUnits";

        // Susbcriber Schema
        public const string MASTER_GET_CLIENTS = "master.GetClients";

        // Document Schema -> Purchase
        public const string DOCUMENT_GET_ALL_PURCHASE_DOCUMENTS = "document.GetAllPurchaseDocuments";
        public const string DOCUMENT_GET_ALL_PENDING_PURCHASE_DOCUMENTS = "document.GetAllPendingPurchaseDocuments";
        public const string DOCUMENT_INSERT_OR_UPDATE_PURCHASE_DOCUMENT = "document.InsertOrUpdatePurchaseDocument";
        public const string DOCUMENT_CHANGE_PURCHASE_DOCUMENT_STATUS = "document.ChangePurchaseDocumentStatus";
        public const string DOCUMENT_GET_PURCHASE_DOCUMENT_BY_ID = "document.GetPurchaseDocumentById";
        public const string DOCUMENT_DELETE_PURCHASE_DOCUMENT = "document.DeletePurchaseDocument";

        // Document Schema
        public const string DOCUMENT_GET_DOCUMENT_NUMBERS = "document.GetDocumentNumbers";

        // Subscriber Schema
        public const string SUBSCRIBER_GET_NUMBER_CONFIGURATION = "subscriber.GetNumberConfiguration";

        // Document Schema -> Report -> InwardSlip
        public const string DOCUMENT_GET_ALL_INWARD_SLIPS_FOR_REPORT = "document.GetAllInwardSlipsForReport";

        // Master Schema -> LedgerGroup
        public const string MASTER_GET_ALL_LEDGER_GROUPS = "master.GetAllLedgerGroups";
        public const string MASTER_INSERT_OR_UPDATE_LEDGER_GROUP = "master.InsertOrUpdateLedgerGroup";
        public const string MASTER_CHANGE_LEDGER_GROUP_STATUS = "master.ChangeLedgerGroupStatus";
        public const string MASTER_GET_LEDGER_GROUP_BY_ID = "master.GetLedgerGroupById";
        public const string MASTER_DELETE_LEDGER_GROUP = "master.DeleteLedgerGroup";

        // Master Schema -> Brand
        public const string MASTER_GET_ALL_BRANDS = "master.GetAllBrands";
        public const string MASTER_INSERT_OR_UPDATE_BRAND = "master.InsertOrUpdateBrand";
        public const string MASTER_CHANGE_BRAND_STATUS = "master.ChangeBrandStatus";
        public const string MASTER_GET_BRAND_BY_ID = "master.GetBrandById";
        public const string MASTER_DELETE_BRAND = "master.DeleteBrand";

        // Document Schema -> Sales
        public const string DOCUMENT_GET_ALL_SALES_DOCUMENTS = "document.GetAllSalesDocuments";
        public const string DOCUMENT_GET_ALL_PENDING_RATE_SALES_DOCUMENTS = "document.GetAllPendingRateSalesDocuments";
        public const string DOCUMENT_INSERT_OR_UPDATE_SALES_DOCUMENT = "document.InsertOrUpdateSalesDocument";
        public const string DOCUMENT_CHANGE_SALES_DOCUMENT_STATUS = "document.ChangeSalesDocumentStatus";
        public const string DOCUMENT_GET_SALES_DOCUMENT_BY_ID = "document.GetSalesDocumentById";
        public const string DOCUMENT_DELETE_SALES_DOCUMENT = "document.DeleteSalesDocument";

        // Document Schema -> Receipt
        public const string DOCUMENT_GET_ALL_RECEIPTS = "document.GetAllReceipts";
        public const string DOCUMENT_INSERT_OR_UPDATE_RECEIPT = "document.InsertOrUpdateReceipt";
        public const string DOCUMENT_CHANGE_RECEIPT_STATUS = "document.ChangeReceiptStatus";
        public const string DOCUMENT_GET_RECEIPT_BY_ID = "document.GetReceiptById";
        public const string DOCUMENT_DELETE_RECEIPT = "document.DeleteReceipt";

        // Document Schema -> Payment
        public const string DOCUMENT_GET_ALL_PAYMENTS = "document.GetAllPayments";
        public const string DOCUMENT_INSERT_OR_UPDATE_PAYMENT = "document.InsertOrUpdatePayment";
        public const string DOCUMENT_CHANGE_PAYMENT_STATUS = "document.ChangePaymentStatus";
        public const string DOCUMENT_GET_PAYMENT_BY_ID = "document.GetPaymentById";
        public const string DOCUMENT_DELETE_PAYMENT = "document.DeletePayment";

        // Document Schema -> Payment
        public const string DOCUMENT_GET_ALL_JOURNAL_VOUCHERS = "document.GetAllJournalVouchers";
        public const string DOCUMENT_INSERT_OR_UPDATE_JOURNAL_VOUCHER = "document.InsertOrUpdateJournalVoucher";
        public const string DOCUMENT_CHANGE_JOURNAL_VOUCHER_STATUS = "document.ChangeJournalVoucherStatus";
        public const string DOCUMENT_GET_JOURNAL_VOUCHER_BY_ID = "document.GetJournalVoucherById";
        public const string DOCUMENT_DELETE_JOURNAL_VOUCHER = "document.DeleteJournalVoucher";

        // Document Schema -> Common
        public const string DOCUMENT_GET_LOT_NUMBERS = "document.GetLotNumbers";
        public const string DOCUMENT_GET_ITEM_DETAIL_BY_LOT_NUMBER = "document.GetItemDetailByLotNumber";
        public const string DOCUMENT_CHECK_IF_LOT_NUMBER_IS_ASSIGNED = "document.CheckIfLotNumberIsAssigned";

        // Document Schema -> Report -> Ledger
        public const string DOCUMENT_GET_LEDGER_REPORT = "document.GetLedgerReport";
    }
}
