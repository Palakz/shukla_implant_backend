﻿namespace SSWhite.Data.Interface.PickList
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Domain.Request.PickList;
    using SSWhite.Domain.Response.PickList;

    public interface IPickListRepository
    {
        Task<List<GetAllPickListResponse>> GetAllPickList();

        Task<GetPickListByIdResponse> GetPickListById(GetPickListByIdRequest request);

        Task DeletePickList(ServiceRequest<DeletePickListRequest> request);

        Task AddOrEditPickList(ServiceRequest<AddOrEditPickListRequest> request);

        Task<List<GetPickListValueByIdResponse>> GetPickListValueById(GetPickListValueByIdRequest request);

        Task DeletePickListValue(ServiceRequest<DeletePickListValueRequest> request);

        Task AddOrEditPickListValue(ServiceRequest<AddOrEditPickListValueRequest> request);

        Task AddPickListValueImage(ServiceRequest<AddPickListValueImageRequest> request);

        Task ChangePickListStatus(ServiceRequest<ChangePickListStatusRequest> request);
    }
}
