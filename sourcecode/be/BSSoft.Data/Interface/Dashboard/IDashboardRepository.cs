﻿namespace SSWhite.Data.Interface.Account
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Domain.Request.Dashboard;
    using SSWhite.Domain.Response.Dashboard;

    public interface IDashboardRepository
    {
        Task<List<GetSSWhiteTeamDetailsResponse>> GetSSWhiteTeamDetails();

        Task<UserDetail> GetUserDetailsById(int id);

        Task<List<UserNotificationResponse>> UserNotification(ServiceRequest request);

        Task<int> ChangePassword(ServiceRequest<ChangePasswordRequest> request);

        Task UpdateProfileById(ServiceRequest<UpdateProfileByIdRequest> request);

        Task UpdateProfileWithImageById(ServiceRequest<UpdateProfileWithImageByIdRequest> request);

        Task<GetUserDetailsByUserIdResponse> GetUserDetailsByUserId(ServiceRequest request);
       
        Task<GetProfileByIdResponse> GetProfileById(ServiceRequest request);

    }
}
