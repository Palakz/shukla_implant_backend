﻿namespace SSWhite.Data.Interface.Implant
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Implant;
    using SSWhite.Domain.Response.Implant;

    public interface IImplantRepository
    {
        Task<List<GetAllImplantTypeResponse>> GetAllImplantType();

        Task<List<GetImplantTypeForDropdownResponse>> GetImplantTypeForDropdown();

        Task<GetImplantTypeByIdResponse> GetImplantTypeById(GetImplantTypeByIdRequest request);

        Task DeleteImplantType(ServiceRequest<DeleteImplantTypeRequest> request);

        Task DeleteHistory(ServiceRequest<DeleteHistoryRequest> request);

        Task AddOrEditImplantType(ServiceRequest<AddOrEditImplantTypeRequest> request);

        Task ChangeImplantTypeStatus(ServiceRequest<ChangeImplantTypeStatusRequest> request);

        Task ChangeImplantFieldStatus(ServiceRequest<ChangeImplantFieldStatusRequest> request);

        Task ChangeImplantStatus(ServiceRequest<ChangeImplantStatusRequest> request);

        Task<List<GetAllDataTypesResponse>> GetAllDataTypes();

        Task<List<GetAllImplantFieldsResponse>> GetAllImplantFields();

        Task<GetAllImplantFieldsByIdResponse> GetAllImplantFieldsById(GetAllImplantFieldsByIdRequest request);

        Task AddOrEditImplantFields(ServiceRequest<AddOrEditImplantFieldsRequest> request);

        Task<List<GetAllImplantFieldsResponse>> GetImplantFieldsByImplantType(int implantTypeId, bool includeAllImplantTypeFields);

        Task<dynamic> GetAllImplantByImplantTypeId(ServiceRequest<GetAllImplantByImplantTypeIdRequest> request);

        Task AddOrEditImplantForm(ServiceRequest<Dictionary<string, string>> request);

        Task AddOrRemoveFavorite(ServiceRequest<AddOrRemoveFavoriteRequest> request);

        Task<dynamic> SearchImplant(ServiceRequest<SearchImplantRequest> request);

        Task<dynamic> GetAllFavoriteImplant(ServiceRequest<GetAllFavoriteImplantRequest> request);

        Task<List<GetAllHistoryImplantResponse>> GetAllHistory(ServiceRequest<GetAllHistoryRequest> request);

        Task<dynamic> GetAllUnapprovedImplant();

        Task<ApproveImplantResponse> ApproveImplant(ServiceRequest<ApproveImplantRequest> request);

        Task<ApproveImplantResponse> RejectImplant(ServiceRequest<ApproveImplantRequest> request);

        Task<dynamic> GetImplantById(GetImplantByIdRequest request);

        Task DeleteAllHistoryByUserId(ServiceRequest request);

        Task<dynamic> GetAllImplant();

        Task<dynamic> GetRelatedImplantOfShuklaParts(GetRelatedImplantOfShuklaPartsRequest request);

        Task AddOrEditImplantComment(ServiceRequest<AddOrEditImplantCommentRequest> request);

        Task<List<GetAllImplantCommentsResponse>> GetAllImplantComments(ServiceRequest<GetAllImplantCommentsRequest> request);

        Task LikeOrDislikeImplantComment(ServiceRequest<LikeOrDislikeImplantCommentRequest> request);

        Task<List<GetImplantNotificationResponse>> GetImplantNotification(ServiceRequest request);

        Task MarkReadImplantNotification(ServiceRequest<MarkReadImplantNotificationRequest> request);

        Task DeleteImageByEntity(DeleteImageByEntityRequest request);
    }
}
