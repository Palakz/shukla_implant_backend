﻿namespace SSWhite.Data.Interface.Common
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Domain.Request.Common;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Domain.Response.Common;

    public interface ICommonRepository
    {
        Task<List<GetOrganizationResponse>> GetOrganization();

        Task<List<GetOccupationsResponse>> GetOccupations();

        Task<List<SelectPickListsResponse>> SelectPickLists();

        Task InsertPickList(ServiceRequest<InsertPickListRequest> request);

        Task InsertPickListValue(ServiceRequest<InsertPickListValueRequest> request);

        Task UpdateOccupations(ServiceRequest<UpdateOccupationsRequest> request);

        Task UpdateOrganization(ServiceRequest<UpdateOrganizationRequest> request);

        Task AddOccupations(ServiceRequest<AddOccupationsRequest> request);

        Task AddOrganization(ServiceRequest<AddOrganizationRequest> request);

        Task DeleteOccupations(ServiceRequest<DeleteOccupationsRequest> request);

        Task DeleteOrganization(ServiceRequest<DeleteOrganisationsRequest> request);

        Task ChangeOrganizationStatus(ServiceRequest<ChangeOrganizationStatusRequest> request);

        Task ChangeOccupationStatus(ServiceRequest<ChangeOccupationStatusRequest> request);
    }
}
