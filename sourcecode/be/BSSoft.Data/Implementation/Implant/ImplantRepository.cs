﻿namespace SSWhite.Data.Implementation.Implant
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Newtonsoft.Json;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Implant;
    using SSWhite.Domain.Request.Implant;
    using SSWhite.Domain.Response.Implant;

    public class ImplantRepository : IImplantRepository
    {
        private readonly IBaseRepository _baseRepository;

        public ImplantRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task AddOrEditImplantType(ServiceRequest<AddOrEditImplantTypeRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("IsEdit", request.Data.IsEdit, DbType.Boolean);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_ADD_OR_EDIT_IMPLANTTYPE, parameters, commandType: CommandType.StoredProcedure);

        }

        public async Task DeleteImplantType(ServiceRequest<DeleteImplantTypeRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.String);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_DELETE_IMPLANTTYPE, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteHistory(ServiceRequest<DeleteHistoryRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_DELETE_HISTORY, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetAllImplantTypeResponse>> GetAllImplantType()
        {
            return (await _baseRepository.QueryAsync<GetAllImplantTypeResponse>(StoredProcedures.DBO_GET_ALL_IMPLANTTYPE, null, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetImplantTypeForDropdownResponse>> GetImplantTypeForDropdown()
        {
            return (await _baseRepository.QueryAsync<GetImplantTypeForDropdownResponse>(StoredProcedures.DBO_GET_IMPLANTTYPE_FOR_DROPDOWN, null, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<GetImplantTypeByIdResponse> GetImplantTypeById(GetImplantTypeByIdRequest request)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("Id", request.Id, DbType.String);

            return await _baseRepository.QueryFirstOrDefaultAsync<GetImplantTypeByIdResponse>(StoredProcedures.DBO_GET_IMPLANTTYPE_BY_ID, parameteres, commandType: CommandType.StoredProcedure);

        }

        public async Task<List<GetAllDataTypesResponse>> GetAllDataTypes()
        {
            return (await _baseRepository.QueryAsync<GetAllDataTypesResponse>(StoredProcedures.DBO_GET_ALL_DATATYPES, null, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetAllImplantFieldsResponse>> GetAllImplantFields()
        {
            return (await _baseRepository.QueryAsync<GetAllImplantFieldsResponse>(StoredProcedures.DBO_GET_ALL_IMPLANTFIELDS, null, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetAllImplantFieldsResponse>> GetImplantFieldsByImplantType(int implantTypeId, bool includeAllImplantTypeFields = true)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("ImplantType", implantTypeId, DbType.Int32);
            parameteres.Add("@IncludeAllImplantTypeFields", includeAllImplantTypeFields, DbType.Boolean);
            return (await _baseRepository.QueryAsync<GetAllImplantFieldsResponse>(StoredProcedures.DBO_GET_ALL_IMPLANTFIELDS_By_IMPLANTTYPE, parameteres, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<GetAllImplantFieldsByIdResponse> GetAllImplantFieldsById(GetAllImplantFieldsByIdRequest request)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("Id", request.Id, DbType.String);

            return await _baseRepository.QueryFirstOrDefaultAsync<GetAllImplantFieldsByIdResponse>(StoredProcedures.DBO_GET_ALL_IMPLANTFIELDS_BY_ID, parameteres, commandType: CommandType.StoredProcedure);

        }

        public async Task AddOrEditImplantFields(ServiceRequest<AddOrEditImplantFieldsRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ImplantType", request.Data.ImplantType, DbType.Int32);
            parameters.Add("FieldName", request.Data.FieldName, DbType.String);
            parameters.Add("DisplayName", request.Data.DisplayName, DbType.String);
            parameters.Add("DataTypeId", request.Data.DataTypeId, DbType.Int32);
            parameters.Add("PickListId", request.Data.PickListId, DbType.Int32);
            parameters.Add("DefaultValue", request.Data.DefaultValue, DbType.String);
            parameters.Add("MaxLength", request.Data.MaxLength, DbType.Int32);
            parameters.Add("MinLength", request.Data.MinLength, DbType.Int32);
            parameters.Add("IsRequired", request.Data.IsRequired, DbType.Boolean);
            parameters.Add("Regex", request.Data.Regex, DbType.String);
            parameters.Add("RerversePropertyId", request.Data.RerversePropertyId, DbType.Int32);
            parameters.Add("DisplayOrder", request.Data.DisplayOrder, DbType.Int32);
            parameters.Add("IsPartOfFilter", request.Data.IsPartOfFilter, DbType.Boolean);
            parameters.Add("FilterDisplayOrder", request.Data.FilterDisplayOrder, DbType.Int32);
            parameters.Add("IsMultiSelectOnCreate", request.Data.IsMultiSelectOnCreate, DbType.Boolean);
            parameters.Add("IsMultiSelectOnFilter", request.Data.IsMultiSelectOnFilter, DbType.Boolean);
            parameters.Add("IsSystemDefined", request.Data.IsSystemDefined, DbType.Boolean);
            parameters.Add("MaxImageAllowed", request.Data.MaxImageAllowed, DbType.Int32);
            parameters.Add("MaxTotalSize", request.Data.MaxTotalSize, DbType.Int32);
            parameters.Add("MaxSingleSize", request.Data.MaxSingleSize, DbType.Int32);
            parameters.Add("IsEdit", request.Data.IsEdit, DbType.Boolean);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("IsEdit", request.Data.IsEdit, DbType.Boolean);
            parameters.Add("IsApplicableToAllImplant", request.Data.IsApplicableToAllImplant, DbType.Boolean);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_ADD_OR_EDIT_IMPLANTFIELDS, parameters, commandType: CommandType.StoredProcedure);
        }


        public async Task ChangeImplantTypeStatus(ServiceRequest<ChangeImplantTypeStatusRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("ModifiedOn", DateTime.Now, DbType.DateTime);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_CHANGE_IMPLANTTYPE_BY_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task ChangeImplantStatus(ServiceRequest<ChangeImplantStatusRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("ModifiedOn", DateTime.Now, DbType.DateTime);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_CHANGE_IMPLANT_BY_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task ChangeImplantFieldStatus(ServiceRequest<ChangeImplantFieldStatusRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("ModifiedOn", DateTime.Now, DbType.DateTime);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_CHANGE_IMPLANTFIELD_BY_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<dynamic> GetAllImplantByImplantTypeId(ServiceRequest<GetAllImplantByImplantTypeIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("ImplantType", request.Data.ImplantType, DbType.String);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            var d = await _baseRepository.QueryAsync<dynamic>(StoredProcedures.DBO_GET_IMPLANT_BY_IMPLANTTYPE, parameters, commandType: CommandType.StoredProcedure);
            return d;
        }

        public async Task AddOrRemoveFavorite(ServiceRequest<AddOrRemoveFavoriteRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("ImplantId", request.Data.ImplantId, DbType.Int32);
            parameters.Add("AddAsFavourite", request.Data.AddAsFavourite, DbType.Int32);
            parameters.Add("CreatedOn", DateTime.Now, DbType.DateTime);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            await _baseRepository.QueryAsync<dynamic>(StoredProcedures.DBO_ADD_OR_REMOVE_FAVORITE, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<dynamic> GetAllFavoriteImplant(ServiceRequest<GetAllFavoriteImplantRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("ImplantTypeId", request.Data.ImplantTypeId, DbType.Int32);
            return (await _baseRepository.QueryAsync<dynamic>(StoredProcedures.GET_ALL_ALL_FAVORITE_IMPLANT, parameters, commandType: CommandType.StoredProcedure));
        }

        public async Task<List<GetAllHistoryImplantResponse>> GetAllHistory(ServiceRequest<GetAllHistoryRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("ImplantTypeId", request.Data.ImplantTypeId, DbType.Int32);
            return (await _baseRepository.QueryAsync<GetAllHistoryImplantResponse>(StoredProcedures.DBO_GET_ALL_HISTORY, parameters, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task AddOrEditImplantForm(ServiceRequest<Dictionary<string, string>> request)
        {

            string a = null, b = null;
            string sql, updatedSet = null;

            ////a = a.Remove(a.Length - 1);
            ////b = b.Remove(b.Length - 1);

            if (request.Data.ContainsKey("id"))
            {
                if (request.Data.ContainsKey("Images"))
                {
                    var imageToAppend = await _baseRepository.QueryFirstOrDefaultAsync<string>($"select Images from implant where id = {request.Data["id"]}");
                    request.Data["Images"] = $" {request.Data["Images"]},{imageToAppend}";
                }

                var idKey = request.Data["id"].ToString();
                var isCommentAddedFromWorkbench = Convert.ToBoolean(request.Data["IsCommentAddedFromWorkbench"]);
                request.Data.Remove("id");
                if (request.Data.ContainsKey("Comments") && request.Data["isCommentChanged"] == "true")
                {
                    var query = $"INSERT INTO ImplantComment(ImplantId, Comment, CreatedBy, CreatedOn, IsCommentAddedFromWorkbench) " +
                        $"VALUES ({idKey}, '{request.Data["Comments"]}', {request.Session.UserId}, '{DateTime.Now}', {Convert.ToInt32(isCommentAddedFromWorkbench)})";

                    await _baseRepository.ExecuteAsync(query);
                }

                request.Data.Remove("isCommentChanged");
                request.Data.Remove("IsCommentAddedFromWorkbench");

                foreach (var item in request.Data.Keys)
                {
                    updatedSet = $"{updatedSet} {item} = {PrepareValue(request.Data[item])} ,";
                }

                //updatedSet = updatedSet.Replace(@"'", @"''").Substring(0, updatedSet.Length - 1);
                //sql = $"Update Implant SET ModifyBy = '{request.Session.UserId}', ModifyDate = '{DateTime.Now}', {updatedSet} " +
                //    $"WHERE id = {Convert.ToInt32(idKey)}";
                updatedSet = updatedSet.Substring(0, updatedSet.Length - 1);

                int id = Convert.ToInt32(idKey);
                sql = $"Update Implant SET ModifyBy = '{request.Session.UserId}', ModifyDate = '{DateTime.Now}', isApproved = {0},{updatedSet} " +
                    $"WHERE id = {id}";
            }
            else
            {
                var id = await _baseRepository.QueryFirstOrDefaultAsync<int>("select Id from Implant WHERE Id = (SELECT MAX(Id) FROM Implant)");
                var isCommentAddedFromWorkbench = Convert.ToBoolean(request.Data["IsCommentAddedFromWorkbench"]);

                if (request.Data.ContainsKey("Comments") && request.Data["isCommentChanged"] == "true")
                {
                    var query = $"INSERT INTO ImplantComment(ImplantId, Comment, CreatedBy, CreatedOn,IsCommentAddedFromWorkbench) " +
                        $"VALUES ({id + 1}, '{request.Data["Comments"]}', {request.Session.UserId}, '{DateTime.Now}', {Convert.ToInt32(isCommentAddedFromWorkbench)})";

                    await _baseRepository.ExecuteAsync(query);
                }

                request.Data.Remove("isCommentChanged");
                request.Data.Remove("IsCommentAddedFromWorkbench");

                foreach (var item in request.Data.Keys)
                {
                    a = a + item + ',';
                    b = $"{b} {PrepareValue(request.Data[item])}{','}";
                }

                sql = $"insert into Implant ({a} CreatedBy, CreatedDate, IsApproved, IsDeleted)" +
                    $" values ({b}'{request.Session.UserId}','{DateTime.Now}',0 , 0)";
            }

            var ss = await _baseRepository.ExecuteAsync(sql);

        }

        public async Task<dynamic> SearchImplant(ServiceRequest<SearchImplantRequest> request)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            Dictionary<string, string> dictionaySearchJson = new Dictionary<string, string>();
            if (request.Data.JsonSearchPayload != null)
            {
                dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(request.Data.JsonSearchPayload);
                dictionaySearchJson = dict.Where(f => f.Value != null).ToDictionary(x => x.Key, x => x.Value);
            }

            string appendJsonString = null;
            if (string.IsNullOrEmpty(request.Data.Name) && string.IsNullOrWhiteSpace(request.Data.JsonSearchPayload))
            {
                var parameters = new DynamicParameters();
                parameters.Add("ImplantType", request.Data.ImplantTypeId, DbType.String);
                parameters.Add("UserId", request.Session.UserId, DbType.Int32);
                var d = await _baseRepository.QueryAsync<dynamic>(StoredProcedures.DBO_GET_IMPLANT_BY_IMPLANTTYPE, parameters, commandType: CommandType.StoredProcedure);
                return d;
            }
            else
            {
                var condition = request.Data.Status == null ? "NULL" : request.Data.Status.ToString();
                var implantCondition = string.IsNullOrEmpty(request.Data.ImplantTypeId) ? null : $"AND ImplantTypeId = '{request.Data.ImplantTypeId}'";
                appendJsonString = $"SELECT  I.*, (CASE WHEN fav.id IS NOT NULL AND fav.UserId = {request.Session.UserId} THEN 1 ELSE 0 END ) AS IsFavourite " +
                    $"FROM Implant I LEFT JOIN Favorite fav ON I.Id = fav.ImplantId AND fav.UserId = {request.Session.UserId}" + " " +
                    $"WHERE IsApproved = 1 {implantCondition} AND IsDeleted = 0 AND I.Status = ISNULL({condition},I.Status) " +
                    $"AND (Name LIKE '%{request.Data.Name}%'";

                foreach (var item in dictionaySearchJson)
                {
                    if (item.Value != null && item.Value.IndexOf(',') > -1)
                    {
                        appendJsonString = $"{appendJsonString} AND  {item.Key} IN ('{string.Join("','", item.Value.Split(','))}')";
                    }
                    else
                    {
                        appendJsonString = $"{appendJsonString} AND  {item.Key} = '{item.Value}'";
                    }
                }

                appendJsonString = $"{appendJsonString})";
                var parameters = new DynamicParameters();
                parameters.Add("UserId", request.Session.UserId, DbType.Int32);
                parameters.Add("Name", request.Data.Name, DbType.String);
                parameters.Add("JsonSearchPayload", request.Data.JsonSearchPayload, DbType.String);
                parameters.Add("AppendJsonString", appendJsonString, DbType.String);
                parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
                parameters.Add("ImplantTypeId", request.Data.ImplantTypeId, DbType.String);


                parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

                var response = (await _baseRepository.QueryAsync<dynamic>(StoredProcedures.DBO_SEARCH_IMPLANT, parameters, commandType: CommandType.StoredProcedure)).ToList();

                return response;
            }
        }

        public async Task<dynamic> GetAllUnapprovedImplant()
        {
            return await _baseRepository.QueryAsync<dynamic>(StoredProcedures.DBO_GET_ALL_UNAPPROVED_IMPLANT, commandType: CommandType.StoredProcedure);
        }

        public async Task<ApproveImplantResponse> ApproveImplant(ServiceRequest<ApproveImplantRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now.ToString(), DbType.String);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);

            return await _baseRepository.QueryFirstOrDefaultAsync<ApproveImplantResponse>(StoredProcedures.DBO_APPROVE_IMPLANT, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ApproveImplantResponse> RejectImplant(ServiceRequest<ApproveImplantRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now.ToString(), DbType.String);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);

            return await _baseRepository.QueryFirstOrDefaultAsync<ApproveImplantResponse>(StoredProcedures.DBO_REJECT_IMPLANT, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<dynamic> GetImplantById(GetImplantByIdRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Id, DbType.Int32);
            return await _baseRepository.QueryAsync<dynamic>(StoredProcedures.DBO_GET_IMPLANT_BY_ID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteAllHistoryByUserId(ServiceRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_DELETE_ALL_HISTORY_BY_USERID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<dynamic> GetAllImplant()
        {
            return await _baseRepository.QueryAsync<dynamic>(StoredProcedures.DBO_GET_ALL_IMPLANT, commandType: CommandType.StoredProcedure);
        }

        public async Task<dynamic> GetRelatedImplantOfShuklaParts(GetRelatedImplantOfShuklaPartsRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("ImplantID", request.ImplantID, DbType.String);
            parameters.Add("ImplantTypeId", request.ImplantTypeId, DbType.String);
            parameters.Add("SearchToolValue", request.SearchToolValue, DbType.String);
            return await _baseRepository.QueryAsync<dynamic>(StoredProcedures.DBO_GET_RELATED_IMPLANT_OF_SHUKLAPARTS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task AddOrEditImplantComment(ServiceRequest<AddOrEditImplantCommentRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ImplantId", request.Data.ImplantId, DbType.Int32);
            parameters.Add("Comment", request.Data.Comment, DbType.String);
            parameters.Add("IsCommentAddedFromWorkbench", request.Data.IsCommentAddedFromWorkbench, DbType.Boolean);
            parameters.Add("CurrentDate", DateTime.Now.ToString(), DbType.String);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_ADD_OR_EDIT_IMPLANT_COMMENT, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetAllImplantCommentsResponse>> GetAllImplantComments(ServiceRequest<GetAllImplantCommentsRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("ImplantId", request.Data.ImplantId, DbType.Int32);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            return (await _baseRepository.QueryAsync<GetAllImplantCommentsResponse>(StoredProcedures.DBO_GET_ALL_IMPLANT_COMMENTS, parameters, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task LikeOrDislikeImplantComment(ServiceRequest<LikeOrDislikeImplantCommentRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("CommentId", request.Data.CommentId, DbType.Int32);
            parameters.Add("IsLiked", request.Data.IsLiked, DbType.Boolean);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_LIKE_OR_DISLIKE_IMPLANT_COMMENT, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetImplantNotificationResponse>> GetImplantNotification(ServiceRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            return (await _baseRepository.QueryAsync<GetImplantNotificationResponse>(StoredProcedures.DBO_GET_IMPLANT_NOTIFICATION, parameters, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task MarkReadImplantNotification(ServiceRequest<MarkReadImplantNotificationRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("IsReadAll", request.Data.IsReadAll, DbType.Boolean);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_MARK_READ_IMPLANT_NOTIFICATION, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteImageByEntity(DeleteImageByEntityRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("EntityId", request.EntityId, DbType.Int32);
            parameters.Add("EntityName", request.EntityName, DbType.String);
            parameters.Add("ImagePath", request.ImagePath, DbType.String);
            parameters.Add("EntityColumnName", request.EntityColumnName, DbType.String);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_DELETE_IMAGE_BY_ENTITY, parameters, commandType: CommandType.StoredProcedure);
        }

        private object PrepareValue(object value, string dataType = null)
        {
            return $"'{value}'";
        }
    }
}
