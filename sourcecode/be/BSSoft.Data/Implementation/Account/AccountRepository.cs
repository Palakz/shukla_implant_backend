﻿namespace SSWhite.Data.Implementation.Account
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Account;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.Organization.Account;
    using SSWhite.Domain.Response.Account;

    public class AccountRepository : IAccountRepository
    {
        private readonly IBaseRepository _baseRepository;
        private readonly IPAddress _ipAddress;

        public AccountRepository(IBaseRepository baseRepository, IOptions<IPAddress> ipAddress)
        {
            _baseRepository = baseRepository;
            _ipAddress = ipAddress.Value;
        }

        public async Task ForgotPassword(ServiceRequest<ForgotPasswordRequest> request)
        {
            var paramters = new DynamicParameters();
            paramters.Add("EmailAddress", request.Data.EmailAddress, DbType.String);
            paramters.Add("Password", request.Data.Password.GenerateSHA256Hash(), DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_FORGOT_PASSWORD, paramters, commandType: CommandType.StoredProcedure);
        }

        public async Task<LogInResponse> ValidateUserForLogIn(ServiceRequest<LogInRequest> request)
        {
            var paramters = new DynamicParameters();
            paramters.Add("UserName", request.Data.UserName, DbType.String);
            paramters.Add("Password", request.Data.Password.GenerateSHA256Hash(), DbType.String);
            ////paramters.Add("IpAddress", _ipAddress.RequestIpAddress, DbType.String);
            paramters.Add("StatusTypeActive", /*StatusType.Active,*/ DbType.Int16);
            ////paramters.Add("UserTypeAdmin", UserType.Admin, DbType.Int16);

            var response = new LogInResponse();
            var entities = new List<UserEntity>();
            var rights = new List<Rights>();

            try
            {
                using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.MASTER_VALIDATE_USER_FOR_LOGIN, paramters, commandType: CommandType.StoredProcedure))
                {
                    response = await multi.ReadFirstOrDefaultAsync<LogInResponse>();
                    ////entities = (await multi.ReadAsync<UserEntity>()).ToList();
                    rights = (await multi.ReadAsync<Rights>()).ToList();
                }
            }
            catch (System.Exception ex)
            {
                throw;
            }

            if (response != null)
            {
                response.Entities = entities;
                response.Rights = rights;
            }

            return response;
        }
    }
}
