﻿namespace SSWhite.Data.Implementation.Organization.User
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Organization.User;
    using SSWhite.Domain.Common.Organization.User;
    using SSWhite.Domain.Request.Organization.User;
    using SSWhite.Domain.Response.Organization.User;

    public class UserRepository : IUserRepository
    {
        private readonly IBaseRepository _baseRepository;

        public UserRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<int> AddUser(ServiceRequest<AddUser> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", null, DbType.Int32);
            parameters.Add("Password", request.Data.Password, DbType.String);
            return await AddEditCommonParams(request.Data, request.Session, parameters);
        }

        public async Task ChangeUserStatus(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.SUBSCRIBER_CHANGE_USER_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteUser(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("StatusTypeDeleted", /*StatusType.Deleted*/ DbType.Int16);

            await _baseRepository.ExecuteAsync(StoredProcedures.SUBSCRIBER_DELETE_USER, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllUsersResponse>> GetAllUsers(ServiceSearchRequest<GetAllUsers> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("Status", /*request.Data.Status,*/ DbType.Int16);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllUsersResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllUsersResponse>(StoredProcedures.SUBSCRIBER_GET_ALL_USERS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetUserByIdResponse> GetUserById(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            var response = new GetUserByIdResponse();
            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.SUBSCRIBER_GET_USER_BY_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadSingleAsync<GetUserByIdResponse>();
                response.RoleIds = (await multi.ReadAsync<int>()).ToList();
                response.EntityIds = (await multi.ReadAsync<int>()).ToList();
            }

            return response;
        }

        public async Task<int> EditUser(ServiceRequest<EditUser> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("Password", null, DbType.String);
            return await AddEditCommonParams(request.Data, request.Session, parameters);
        }

        private async Task<int> AddEditCommonParams(User request, SessionDetail sessionDetail, DynamicParameters parameters)
        {
            parameters.Add("SubscriberId", sessionDetail.SubscriberId, DbType.Int32);
            parameters.Add("UserType", /*UserType.User,*/ DbType.Int16);
            parameters.Add("FirstName", request.FirstName, DbType.String);
            parameters.Add("LastName", request.LastName, DbType.String);
            parameters.Add("UserName", request.UserName, DbType.String);
            parameters.Add("MobileNumber", request.MobileNo, DbType.String);
            parameters.Add("EmailAddress", request.EmailAddress, DbType.String);
            parameters.Add("Address", request.Address, DbType.String);
            parameters.Add("ProfilePicPath", request.ProfilePicPath, DbType.String);
            if (request.EntityIds?.Any() == true)
            {
                parameters.Add("EntityIds", request.EntityIds.ToDataTable(), DbType.Object);
            }

            if (request.RoleIds?.Any() == true)
            {
                parameters.Add("RoleIds", request.RoleIds.ToDataTable(), DbType.Object);
            }

            parameters.Add("IpAddress", sessionDetail.IpAddress, DbType.String);
            parameters.Add("CreatedBy", sessionDetail.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("StatusTypeInActive", /*StatusType.Active,*/ DbType.Int16);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.ReturnValue);

            await _baseRepository.ExecuteAsync(StoredProcedures.SUBSCRIBER_INSERT_OR_UPDATE_USER, parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("ReturnValue");
        }
    }
}
