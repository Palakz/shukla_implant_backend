﻿namespace SSWhite.Data.Implementation.Common
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Request;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Common;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.Common;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Domain.Response.Common;

    public class CommonRepository : ICommonRepository
    {
        private readonly IBaseRepository _baseRepository;

        public CommonRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<List<GetOrganizationResponse>> GetOrganization()
        {
            var parameteres = new DynamicParameters();
            return (await _baseRepository.QueryAsync<GetOrganizationResponse>(StoredProcedures.MASTER_GET_ORGANIZATION, null, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task ChangeOrganizationStatus(ServiceRequest<ChangeOrganizationStatusRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("ModifiedOn", DateTime.Now, DbType.DateTime);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_CHANGE_ORGANIZATION_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetOccupationsResponse>> GetOccupations()
        {
            var parameteres = new DynamicParameters();
            return (await _baseRepository.QueryAsync<GetOccupationsResponse>(StoredProcedures.MASTER_GET_Occupations, null, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task ChangeOccupationStatus(ServiceRequest<ChangeOccupationStatusRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("ModifiedOn", DateTime.Now, DbType.DateTime);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_CHANGE_OCCUPATION_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task InsertPickList(ServiceRequest<InsertPickListRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("Description", request.Data.Description, DbType.String);
            parameters.Add("IsValueNameSame", request.Data.IsValueNameSame, DbType.Boolean);
            parameters.Add("IsValueAutoId", request.Data.IsValueAutoId, DbType.Boolean);
            parameters.Add("CreatedOn", DateTime.Now, DbType.DateTime);
            parameters.Add("DisplayOrder", request.Data.DisplayOrder, DbType.Int16);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_INSERT_PICK_LIST, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task InsertPickListValue(ServiceRequest<InsertPickListValueRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@PickListId", request.Data.PickListId, DbType.Int32);
            parameters.Add("IsDefault", request.Data.IsDefault, DbType.Boolean);
            parameters.Add("DisplayOrder", request.Data.DisplayOrder, DbType.Int16);
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("Value", request.Data.Value, DbType.String);
            parameters.Add("Note", request.Data.Note, DbType.String);
            parameters.Add("CreatedOn", DateTime.Now, DbType.DateTime);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_INSERT_PICK_LIST_VALUE, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<SelectPickListsResponse>> SelectPickLists()
        {
            return (await _baseRepository.QueryAsync<SelectPickListsResponse>(StoredProcedures.DBO_SELECT_PICK_LISTS, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task UpdateOccupations(ServiceRequest<UpdateOccupationsRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.String);
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("ModifiedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_UPDATE_OCCUPATIONS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task UpdateOrganization(ServiceRequest<UpdateOrganizationRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.String);
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("ModifiedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_UPDATE_ORGANIZATION, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task AddOccupations(ServiceRequest<AddOccupationsRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("Createdon", DateTime.Now, DbType.DateTime);
            parameters.Add("Createdby", request.Session.UserId, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_ADD_OCCUPATIONS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task AddOrganization(ServiceRequest<AddOrganizationRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("Createdon", DateTime.Now, DbType.DateTime);
            parameters.Add("Createdby", request.Session.UserId, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_ADD_ORGANIZATION, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteOccupations(ServiceRequest<DeleteOccupationsRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.String);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("ModifiedDate", DateTime.Now, DbType.DateTime);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_DELETE_OCCUPATIONS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteOrganization(ServiceRequest<DeleteOrganisationsRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.String);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("ModifiedDate", DateTime.Now, DbType.DateTime);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_DELETE_ORGANIZATION, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
