﻿namespace SSWhite.Data.Implementation.PageView
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Dapper;
    using Microsoft.AspNetCore.Routing;
    using SSWhite.Core.Request;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.PageView;
    using SSWhite.Domain.Request.PageView;
    using SSWhite.Domain.Response.Implant;
    using SSWhite.Domain.Response.PageView;
    using SSWhite.Domain.Response.PickList;

    public class PageViewRepository : IPageViewRepository
    {
        private readonly IBaseRepository _baseRepository;

        public PageViewRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task AddOrEditPageViewFields(ServiceRequest<AddOrEditPageViewFieldsRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("PageViewContainerId", request.Data.PageViewContainerId, DbType.Int32);
            parameters.Add("FieldId", request.Data.FieldId, DbType.Int32);
            parameters.Add("OnChangeJavaScriptFunc", request.Data.OnChangeJavaScriptFunc, DbType.String);
            parameters.Add("CreatedOn", DateTime.Now, DbType.DateTime);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_ADD_OR_EDIT_PAGEVIEWFIELDS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task AddOrEditPageView(ServiceRequest<AddOrEditPageViewRequest> request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("Id", request.Data.Id, DbType.Int32);
                parameters.Add("Name", request.Data.Name, DbType.String);
                parameters.Add("PageViewType", request.Data.PageViewType, DbType.Int32);
                parameters.Add("Url", request.Data.Url, DbType.String);
                parameters.Add("IsActive", request.Data.IsActive, DbType.Boolean);
                parameters.Add("ImplantType", request.Data.ImplantType, DbType.Int32);
                parameters.Add("CreatedOn", DateTime.Now, DbType.DateTime);
                parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);

                await _baseRepository.ExecuteAsync(StoredProcedures.DBO_ADD_OR_EDIT_PAGEVIEW, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
            }
        }

        public async Task AddOrEditPageViewContainer(ServiceRequest<AddOrEditPageViewContainerRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("ContainerIconPosition", request.Data.ContainerIconPosition, DbType.String);
            parameters.Add("ContainerIcon", request.Data.ContainerIcon, DbType.String);
            parameters.Add("PageViewId", request.Data.PageViewId, DbType.Int32);
            parameters.Add("DisplayColumnNumber", request.Data.DisplayColumnNumber, DbType.Int32);
            parameters.Add("CreatedOn", DateTime.Now, DbType.DateTime);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("ModifiedOn", DateTime.Now, DbType.DateTime);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_ADD_OR_EDIT_PAGE_VIEW_CONTAINER, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task AddOrEditPageViewProperty(ServiceRequest<AddOrEditPageViewPropertyRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("FieldId", request.Data.FieldId, DbType.Int32);
            parameters.Add("DisplayName", request.Data.DisplayName, DbType.String);
            parameters.Add("ContainerId", request.Data.ContainerId, DbType.Int32);
            parameters.Add("IsActive", request.Data.IsActive, DbType.Boolean);
            parameters.Add("IsReadOnly", request.Data.IsReadOnly, DbType.Boolean);
            parameters.Add("IsVisible", request.Data.IsVisible, DbType.Boolean);
            parameters.Add("CssClass", request.Data.CssClass, DbType.String);
            parameters.Add("Script", request.Data.Script, DbType.String);
            parameters.Add("FieldControlType", request.Data.FieldControlType, DbType.String);
            parameters.Add("CreatedOn", DateTime.Now, DbType.DateTime);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_ADD_OR_EDIT_PAGEVIEW_PROPERTY, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeletePageView(ServiceRequest<DeletePageViewRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ModifiedOn", DateTime.Now, DbType.DateTime);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_GET_ALL_Delete_Page_View, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeletePageViewContainer(ServiceRequest<DeletePageViewContainerRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ModifiedOn", DateTime.Now, DbType.DateTime);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_GET_ALL_Delete_Page_View_Container, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeletePageViewFields(ServiceRequest<DeletePageViewFieldsRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ModifiedOn", DateTime.Now, DbType.DateTime);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_GET_ALL_Delete_Page_View_Fields, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetAllPageViewFieldsResponse>> GetAllPageViewFields()
        {
            return (await _baseRepository.QueryAsync<GetAllPageViewFieldsResponse>(StoredProcedures.DBO_GET_ALL_PAGEVIEWFIELDS, null, commandType: CommandType.StoredProcedure)).AsList();
        }

        public async Task<List<GetAllPageViewResponse>> GetAllPageViewByImplantType(ServiceRequest<GetAllPageViewByImplantTypeRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("ImplantType", request.Data.ImplantType, DbType.Int32);
            return (await _baseRepository.QueryAsync<GetAllPageViewResponse>(StoredProcedures.DBO_GET_ALL_PAGEVIEWBYIMPLANTID, parameters, commandType: CommandType.StoredProcedure)).AsList();
        }

        public async Task<List<GetAllPageViewPropertyResponse>> GetAllPageViewProperty()
        {
            return (await _baseRepository.QueryAsync<GetAllPageViewPropertyResponse>(StoredProcedures.DBO_GET_ALL_PAGE_VIEW_PROPERTY, null, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task DeletePageViewProperty(ServiceRequest<DeletePageViewPropertyRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_DELETE_PAGE_VIEW_PROPERTY, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task UpdateIsVisiblePageViewProperty(ServiceRequest<UpdateIsVisiblePageViewPropertyRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("IsVisible", request.Data.IsVisible, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_UPDATE_ISVISIBLE_PAGEVIEW_PROPERTYT, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task UpdateIsReadOnlyPageViewProperty(ServiceRequest<UpdateIsReadOnlyPageViewPropertyRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("IsReadOnly", request.Data.IsReadOnly, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_UPDATE_ISREADONLY_PAGEVIEW_PROPERTYT, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task UpdateIsActivePageViewProperty(ServiceRequest<UpdateIsActivePageViewPropertyRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("IsActive", request.Data.IsActive, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_UPDATE_ISACTIVE_PAGEVIEW_PROPERTYT, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetAllContainerByPageViewIDResponse>> GetAllContainerByPageViewID(GetAllContainerByPageViewIDRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("PageViewID", request.PageViewID, DbType.Int32);
            return (await _baseRepository.QueryAsync<GetAllContainerByPageViewIDResponse>(StoredProcedures.DBO_GET_ALL_CONTAINER_BY_PAGE_VIEW_ID, parameters, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<GetPageViewByPageViewIDResponse> GetPageViewByPageViewID(GetPageViewByPageViewIDRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("PageViewID", request.PageViewID, DbType.String);

            var response = new GetPageViewByPageViewIDResponse();
            response.Container = new List<PageViewContainer>();
            var pageViewProperty = new List<GetAllPageViewPropertyResponse>();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.DBO_GET_PAGE_VIEW_BY_PAGE_VIEW_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstOrDefaultAsync<GetPageViewByPageViewIDResponse>();
                if (response != null)
                {
                    response.Container = (await multi.ReadAsync<PageViewContainer>()).ToList();
                }

                pageViewProperty = (await multi.ReadAsync<GetAllPageViewPropertyResponse>()).ToList();
            }

            if (pageViewProperty?.Any() == true)
            {
                response.Container.ForEach(x => x.PageViewProperty.AddRange(pageViewProperty.Where(z => z.ContainerId == x.Id)));
            }

            return response;
        }

        public async Task<GetPageViewByPageViewIDResponse> GetPageViewByImplantType(ServiceRequest<GetPageViewByImplantTypeRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("ImplantTypeID", request.Data.ImpantTypeId, DbType.Int32);
            parameters.Add("PageViewTypeID", request.Data.PageviewTypeId, DbType.Int32);
            parameters.Add("ImplantId", request.Data.ImplantId, DbType.Int32);
            parameters.Add("ImplantSearchText", request.Data.ImplantSearchText, DbType.String);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);

            var implantDetail = new RouteValueDictionary();
            ////implantDetail["name"] = "implantName";
            var response = new GetPageViewByPageViewIDResponse();
            response.Container = new List<PageViewContainer>();
            var pageViewProperty = new List<GetAllPageViewPropertyResponse>();
            var implantFields = new List<GetAllImplantFieldsResponse>();
            var pickListValue = new List<GetPickListValueByIdResponse>();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.DBO_GET_PAGE_VIEW_BY_IMPANT_TYPE_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstOrDefaultAsync<GetPageViewByPageViewIDResponse>();
                if (response != null)
                {
                    response.Container = (await multi.ReadAsync<PageViewContainer>()).ToList();
                }

                pageViewProperty = (await multi.ReadAsync<GetAllPageViewPropertyResponse>()).ToList();
                implantFields = (await multi.ReadAsync<GetAllImplantFieldsResponse>()).ToList();
                pickListValue = (await multi.ReadAsync<GetPickListValueByIdResponse>()).ToList();
                if (request.Data.ImplantId > 0)
                {
                    implantDetail = new RouteValueDictionary(await multi.ReadFirstOrDefaultAsync<dynamic>());
                    response.Implant = implantDetail;
                }
            }

            if (pageViewProperty?.Any() == true)
            {
                response.Container.ForEach(x => x.PageViewProperty.AddRange(pageViewProperty.Where(z => z.ContainerId == x.Id)));
            }

            if (implantFields.Any())
            {
                foreach (var pageProperty in response.Container.Select(x => x.PageViewProperty))
                {
                    pageProperty.ForEach(async x =>
                    {

                        x.ImplantFields = implantFields.Where(z => z.Id == x.FieldId).FirstOrDefault();
                        x.CurrentValue = (string)implantDetail[x.ImplantFields.FieldName];
                    });
                }
            }

            if (pickListValue.Any())
            {
                foreach (var item in implantFields)
                {
                    if (item.DataTypeName == "PickList")
                    {
                        var list = pickListValue.Select(x => x).Where(x => x.PickListId == item.PickListId).ToList();
                        if (request.Data.PageviewTypeId == 2)
                        {
                            var property = pageViewProperty.FirstOrDefault(x => x.ImplantFields.Id == item.Id);
                            if (property != null && !string.IsNullOrEmpty(property.CurrentValue))
                            {
                                var splitedValue = property.CurrentValue.Split(",").ToList();
                                var selectedValues = list.Where(x => splitedValue.IndexOf(x.Value) > -1 || splitedValue.IndexOf(x.Name) > -1);
                                if (selectedValues != null)
                                {
                                    foreach (var selectedValue in selectedValues)
                                    {
                                        item.PickListValue.Add(new PickListValueDictionary
                                        {
                                            Key = selectedValue.Value,
                                            Value = selectedValue.Name,
                                            Description = selectedValue.Description,
                                            Image = selectedValue.Image
                                        });
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (var item1 in list)
                            {
                                item.PickListValue.Add(new PickListValueDictionary
                                {
                                    Key = item1.Value,
                                    Value = item1.Name,
                                    Description = item1.Description,
                                    Image = item1.Image
                                });
                            }
                        }
                    }
                }
            }

            return response;
        }

        public async Task<dynamic> GetAllRecentlyViewed(ServiceRequest<GetAllRecentlyViewedRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("ImplantTypeId", request.Data.ImplantTypeId, DbType.Int32);
            return await _baseRepository.QueryAsync<dynamic>(StoredProcedures.DBO_GET_GET_ALL_RECENTLY_VIEWED, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteRecentlyViewedById(DeleteRecentlyViewedByIdRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Id, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_GET_DELETE_RECENTLY_VIEWED_BY_ID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteAllRecentlyViewedByImplantId(ServiceRequest<DeleteAllRecentlyViewedByImplantIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("ImplantId", request.Data.ImplantId, DbType.Int32);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_GET_DELETE_ALL_RECENTLY_VIEWED_BY_IMPLANT_ID, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
