﻿namespace SSWhite.Data.Implementation.Account
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Account;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.Dashboard;
    using SSWhite.Domain.Response.Account;
    using SSWhite.Domain.Response.Dashboard;

    public class DashboardRepository : IDashboardRepository
    {
        private readonly IBaseRepository _baseRepository;
        private readonly IPAddress _ipAddress;

        public DashboardRepository(IBaseRepository baseRepository, IOptions<IPAddress> ipAddress)
        {
            _baseRepository = baseRepository;
            _ipAddress = ipAddress.Value;
        }

        public async Task<List<GetSSWhiteTeamDetailsResponse>> GetSSWhiteTeamDetails()
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("Status", /*StatusType.Active,*/ DbType.Int32);

            return (await _baseRepository.QueryAsync<GetSSWhiteTeamDetailsResponse>(StoredProcedures.MASTER_GET_SSWHITE_TEAM_DETAILS, parameteres, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<UserDetail> GetUserDetailsById(int id)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Int16);

            var result = await _baseRepository.QueryFirstAsync<UserDetail>(StoredProcedures.DBO_GET_USER_DETAILS_BY_ID, parameters, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<List<UserNotificationResponse>> UserNotification(ServiceRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int16);

            var result = (await _baseRepository.QueryAsync<UserNotificationResponse>(StoredProcedures.EMP_USER_NOTIFICATION, parameters, commandType: CommandType.StoredProcedure)).ToList();
            return result;
        }

        public async Task<int> ChangePassword(ServiceRequest<ChangePasswordRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("CurrentPassword", Generator.EncryptPassword(request.Data.CurrentPassword.Trim()), DbType.String);
            parameters.Add("NewPassword", Generator.EncryptPassword(request.Data.NewPassword.Trim()), DbType.String);
            ////parameters.Add("StatusTypeActive", value: StatusType.Active, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);

            return await _baseRepository.QueryFirstOrDefaultAsync<int>(StoredProcedures.EMP_CHANGE_PASSWORD, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task UpdateProfileById(ServiceRequest<UpdateProfileByIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("FirstName", request.Data.FirstName, DbType.String);
            parameters.Add("LastName", request.Data.LastName, DbType.String);
            parameters.Add("MobileNumber", request.Data.MobileNumber, DbType.String);
            parameters.Add("OccupationId", request.Data.OccupationId, DbType.Int32);
            parameters.Add("OrganisationId", request.Data.OrganisationId, DbType.Int32);

            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);

            await _baseRepository.QueryFirstOrDefaultAsync<int>(StoredProcedures.MASTER_UPDATE_PROFILE_BY_ID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task UpdateProfileWithImageById(ServiceRequest<UpdateProfileWithImageByIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("FirstName", request.Data.FirstName, DbType.String);
            parameters.Add("LastName", request.Data.LastName, DbType.String);
            parameters.Add("MobileNumber", request.Data.MobileNumber, DbType.String);
            parameters.Add("OccupationId", request.Data.OccupationId, DbType.Int32);
            parameters.Add("OrganisationId", request.Data.OrganisationId, DbType.Int32);
            parameters.Add("ProfileImage", request.Data.ProfileImage, DbType.String);
            parameters.Add("IsImageUpdate", request.Data.IsImageUpdate, DbType.Boolean);

            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);

            await _baseRepository.QueryFirstOrDefaultAsync<int>(StoredProcedures.MASTER_UPDATE_PROFILE_WITH_IMAGE_BY_ID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<GetUserDetailsByUserIdResponse> GetUserDetailsByUserId(ServiceRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);

            return await _baseRepository.QueryFirstOrDefaultAsync<GetUserDetailsByUserIdResponse>(StoredProcedures.MASTER_GET_USER_DETAILS_BY_USER_ID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<GetProfileByIdResponse> GetProfileById(ServiceRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);

            var response = new GetProfileByIdResponse();
            var entities = new List<UserEntity>();
            var rights = new List<Rights>();

            try
            {
                using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.GET_PROFILE_BY_ID, parameters, commandType: CommandType.StoredProcedure))
                {
                    response = await multi.ReadFirstOrDefaultAsync<GetProfileByIdResponse>();
                    ////entities = (await multi.ReadAsync<UserEntity>()).ToList();
                    rights = (await multi.ReadAsync<Rights>()).ToList();
                }
            }
            catch (System.Exception ex)
            {
                throw;
            }

            if (response != null)
            {
                response.Entities = entities;
                response.Rights = rights;
            }

            return response;
        }
    }
}
