﻿namespace SSWhite.Data.Implementation.PickList
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Request;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.PickList;
    using SSWhite.Domain.Request.PickList;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Domain.Response.PickList;

    public class PickListRepository : IPickListRepository
    {
        private readonly IBaseRepository _baseRepository;

        public PickListRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task AddOrEditPickList(ServiceRequest<AddOrEditPickListRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("Description", request.Data.Description, DbType.String);
            parameters.Add("IsValueNameSame", request.Data.IsValueNameSame, DbType.Boolean);
            parameters.Add("IsValueAutoId", request.Data.IsValueAutoId, DbType.Boolean);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("DisplayOrder", request.Data.DisplayOrder, DbType.Int16);
            parameters.Add("OrderBy", request.Data.OrderBy, DbType.String);
            parameters.Add("DataType", request.Data.DataType, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_ADD_OR_EDIT_PICKLIST, parameters, commandType: CommandType.StoredProcedure);

        }

        public async Task DeletePickList(ServiceRequest<DeletePickListRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.String);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_DELETE_PICKLIST, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetAllPickListResponse>> GetAllPickList()
        {
            return (await _baseRepository.QueryAsync<GetAllPickListResponse>(StoredProcedures.DBO_GET_ALL_PICKLIST, null, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<GetPickListByIdResponse> GetPickListById(GetPickListByIdRequest request)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("Id", request.Id, DbType.String);

            return await _baseRepository.QueryFirstOrDefaultAsync<GetPickListByIdResponse>(StoredProcedures.DBO_GET_PICKLIST_BY_ID, parameteres, commandType: CommandType.StoredProcedure);

        }

        public async Task AddOrEditPickListValue(ServiceRequest<AddOrEditPickListValueRequest> request)
        {
            var pickListValueList = (from item in request.Data.PickListValue
                                     select new
                                     {
                                         item.Id,
                                         item.PickListId,
                                         item.IsDefault,
                                         item.DisplayOrder,
                                         item.Name,
                                         item.Value,
                                         item.Note,
                                         item.Description
                                     }).ToList();

            var parameters = new DynamicParameters();
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("IsEdit", request.Data.IsEdit, DbType.Boolean);
            parameters.Add("PickListValueType", pickListValueList.ToDataTable(), DbType.Object);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_ADD_OR_EDIT_PICKLISTVALUE, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeletePickListValue(ServiceRequest<DeletePickListValueRequest> request)
        {
            var idTypeList = (from item in request.Data.Id
                              select new
                              {
                                  item
                              }).ToList();

            var parameters = new DynamicParameters();
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("IdType", idTypeList.ToDataTable(), DbType.Object);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_DELETE_PICKLISTVALUE, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetPickListValueByIdResponse>> GetPickListValueById(GetPickListValueByIdRequest request)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("PickListId", request.PickListId, DbType.String);

            return (await _baseRepository.QueryAsync<GetPickListValueByIdResponse>(StoredProcedures.DBO_GET_PICKLISTVALUE_BY_ID, parameteres, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task AddPickListValueImage(ServiceRequest<AddPickListValueImageRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("PicklistValueId", request.Data.PicklistValueId, DbType.String);
            parameters.Add("ImagePath", request.Data.ImagePath, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_ADD_PICKLIST_VALUE_IMAGE, parameters, commandType: CommandType.StoredProcedure);
        }
    
        public async Task ChangePickListStatus(ServiceRequest<ChangePickListStatusRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("ModifiedOn", DateTime.Now, DbType.DateTime);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_CHANGE_PICKLIST_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }
    }

}
