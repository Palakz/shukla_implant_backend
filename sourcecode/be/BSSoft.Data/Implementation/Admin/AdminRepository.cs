﻿namespace SSWhite.Data.Implementation.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Admin;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Response.Admin;

    public class AdminRepository : IAdminRepository
    {
        private readonly IBaseRepository _baseRepository;

        public AdminRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task AddNewUser(ServiceRequest<AddUserRequest> request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("FirstName", request.Data.FirstName, DbType.String);
                parameters.Add("LastName", request.Data.LastName, DbType.String);
                parameters.Add("UserName", request.Data.UserName, DbType.String);
                parameters.Add("EmailAddress", request.Data.Email, DbType.String);
                parameters.Add("MobileNumber", request.Data.MobileNumber, DbType.Int64);
                parameters.Add("Password", Generator.GenerateRandomPassword(request.Data.Password), DbType.String);
                parameters.Add("OccupationId", request.Data.OccupationId, DbType.Int32);
                parameters.Add("OrganisationId", request.Data.OrganizationId, DbType.Int32);

                await _baseRepository.ExecuteAsync(StoredProcedures.DBO_ADD_NEW_USER, parameters, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task AddNewUserWithImage(ServiceRequest<AddNewUserWithImageRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("FirstName", request.Data.FirstName, DbType.String);
            parameters.Add("LastName", request.Data.LastName, DbType.String);
            parameters.Add("UserName", request.Data.UserName, DbType.String);
            parameters.Add("EmailAddress", request.Data.Email, DbType.String);
            parameters.Add("MobileNumber", request.Data.MobileNumber, DbType.Int64);
            parameters.Add("Password", Generator.GenerateRandomPassword(request.Data.Password), DbType.String);
            parameters.Add("OccupationId", request.Data.OccupationId, DbType.Int32);
            parameters.Add("OrganisationId", request.Data.OrganizationId, DbType.Int32);
            parameters.Add("ProfileImage", request.Data.ProfileImage, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_ADD_NEW_USER_WITH_IMAGE, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetAllUserResponse>> GetAllUser()
        {
            return (await _baseRepository.QueryAsync<GetAllUserResponse>(StoredProcedures.DBO_GET_ALL_USERS, null, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetAllUserDetailsResponse>> GetAllUserDetails()
        {
            return (await _baseRepository.QueryAsync<GetAllUserDetailsResponse>(StoredProcedures.DBO_GET_ALL_USERS_DETAILS, null, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task UpdateIsAdminApproveByUserId(ServiceRequest<UpdateIsAdminApproveByUserIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Data.UserId, DbType.Int32);
            parameters.Add("ApprovedStatus", request.Data.ApprovedStatus, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_UPDATE_ISADMIN_APPROVE_BY_USERID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllEmployeeLimitsResponse>> GetAllEmployeeLimits(GetAllEmployeeLimitsRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("PId", 1, DbType.Int32);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetAllEmployeeLimitsResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllEmployeeLimitsResponse>(StoredProcedures.EMPLOYEE_GET_ALL_EMPLOYEE_LIMIT, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task UpdateToken(UpdateTokenByIdRequest updateTokenByIdRequest)
        {
            try
            {
                var parameteres = new DynamicParameters();
                parameteres.Add("Id", updateTokenByIdRequest.Id, DbType.Guid);
                parameteres.Add("Token", updateTokenByIdRequest.Token, DbType.String);
                parameteres.Add("TokenGeneratedTime", updateTokenByIdRequest.TokenGeneratedTime, DbType.DateTime);
                await _baseRepository.ExecuteAsync(StoredProcedures.DBO_UPDATE_TOKEN_BY_ID, parameteres, commandType: CommandType.StoredProcedure);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ServiceSearchResponse<GetAllUserDetailsByEmailResponse>> GetUserDetailsByEmail(GetAllUserDetailsByEmailRequest request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("EmailAddress", request.Email, DbType.String);
                parameters.Add("UserName", request.UserName, DbType.String);

                var response = new ServiceSearchResponse<GetAllUserDetailsByEmailResponse>()
                {
                    Result = (await _baseRepository.QueryAsync<GetAllUserDetailsByEmailResponse>(StoredProcedures.DBO_GET_USER_DETAILS_BY_EMAIL, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                    TotalRecords = 1,
                };
                return response;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<CheckIfEmployeeIdExistsResponse> CheckIfEmployeeIdExists(CheckIfEmployeeIdExistsRequest checkIfEmployeeIdExistsRequest)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("EmployeeId", checkIfEmployeeIdExistsRequest.EmployeeId, DbType.String);
            return (await _baseRepository.QueryAsync<CheckIfEmployeeIdExistsResponse>(StoredProcedures.EMP_CHECK_IF_EMPLOYEEID_EXISTS, parameteres, commandType: CommandType.StoredProcedure)).FirstOrDefault();
        }

        public async Task UpdateDocumentApprovalList(List<UpdateDocumentApprovalListRequest> request)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("UpdateDocumentApprovalList", request.ToDataTable(), DbType.Object);

            await _baseRepository.ExecuteAsync(StoredProcedures.EMP_UPDATE_DOCUMENT_APPROVAL_LIST, parameteres, commandType: CommandType.StoredProcedure);
        }

        public async Task ConfirmEmail(ConfirmEmailRequest ConfirmEmailRequest)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("EmailAddress", ConfirmEmailRequest.Email, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.EMP_Confirm_Email, parameteres, commandType: CommandType.StoredProcedure);
        }

        public async Task UpdateIsHoldStatusByUserId(UpdateIsHoldStatusByUserIdRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.UserId, DbType.Int32);
            parameters.Add("HoldStatus", request.HoldStatus, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_UPDATE_ISHOLDSTATUSBYUSERID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task UpdateOccupationRightsByOccupationId(ServiceRequest<UpdateOccupationRightsByOccupationIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("RightIdList", request.Data.RightIdList.ToDataTable(), DbType.Object);
            parameters.Add("OccupationId", request.Data.OccupationId, DbType.Int32);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_UPDATE_OCCUPATIONRIGHTSBYOCCUPATIONID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task UpdateUserRightsByUserId(ServiceRequest<UpdateUserRightsByUserIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("RightIdList", request.Data.RightIdList.ToDataTable(), DbType.Object);
            parameters.Add("UserIdForRights", request.Data.UserIdForRights, DbType.Int32);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_UPDATE_USERRIGHTSBYUSERID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetUsersDetailsForDropDownResponse>> GetUsersDetailsForDropDown()
        {
            return (await _baseRepository.QueryAsync<GetUsersDetailsForDropDownResponse>(StoredProcedures.DBO_GET_USERSDETAILSFORDROPDOWN, null, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetModuleRightsByOccupationIdResponse>> GetModuleRightsByOccupationId(GetModuleRightsByOccupationIdRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("OccupationId", request.OccupationId, DbType.Int32);
            List<GetModuleRightsByOccupationIdResponse> response = new List<GetModuleRightsByOccupationIdResponse>();

            List<ModuleGroupRights> moduleGroupRights = new List<ModuleGroupRights>();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.DBO_GET_MODULERIGHTSBYOCCUPATIONID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = (await multi.ReadAsync<GetModuleRightsByOccupationIdResponse>()).ToList();
                moduleGroupRights = (await multi.ReadAsync<ModuleGroupRights>()).ToList();
            }

            foreach (var item in response)
            {
                foreach (var rights in moduleGroupRights)
                {
                    if (item.Id == rights.ModuleGroupId)
                    {
                        item.ModuleGroupRights.Add(rights);
                    }
                }
            }

            return response;
        }

        public async Task<List<GetModuleRightsByUserIdResponse>> GetModuleRightsByUserId(GetModuleRightsByUserIdRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.UserId, DbType.Int32);
            List<GetModuleRightsByUserIdResponse> response = new List<GetModuleRightsByUserIdResponse>();
            List<ModuleGroupRights> moduleGroupRights = new List<ModuleGroupRights>();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.DBO_GET_MODULERIGHTSBYUSERID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = (await multi.ReadAsync<GetModuleRightsByUserIdResponse>()).ToList();
                moduleGroupRights = (await multi.ReadAsync<ModuleGroupRights>()).ToList();
            }

            foreach (var item in response)
            {
                foreach (var rights in moduleGroupRights)
                {
                    if (item.Id == rights.ModuleGroupId)
                    {
                        item.ModuleGroupRights.Add(rights);
                    }
                }
            }

            return response;
        }
    }
}
